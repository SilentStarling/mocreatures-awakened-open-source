Hello! This is the Mocreatures Awakened branch from the original Mocreatures by DrZhark. The Awakened branch was
created by SilentStarling with the help of the Mocreatures community. In order to allow for more community involvement,
and to further progression of the branch, the source code is being made public. The source code for Mocreatures Awakened 
contains open source code from the original Mocreatures version, and the updates made to bring Awakened from 1.12.2 to 1.16.5.
This branch was made inside MCreator, and therefore would be easiest to be edited and worked on inside MCreator. Version
2022.2 is what was used. This branch is not abandoned, and may still be worked on in the future by SilentStarling and other
members of the community. If you want to take this source code and either add your own contributions to the project, or take
the code to work on your own version, that is fine! Please do not take credit for any work or new mobs/items/etc that are unique
to the Awakened branch. Reference this branch as I have with the original Mocreatures content. If you are taking this code as a baseline for your own work, please call your unique branch something else, as Mocreatures Awakened is reserved specifically for SilentStarling's branch. Thank you for the read, and I hope this material is helpful to you! Hope to see many updates and ideas in Mocreature's future.
-SilentStarling
