// Made with Blockbench 4.0.5
// Exported for Minecraft version 1.7 - 1.12
// Paste this class into your mod and generate all required imports
// Made by WerelWolf

public static class ModelBirdCockatoo extends ModelBase {
	private final ModelRenderer head;
	private final ModelRenderer body;
	private final ModelRenderer leftleg;
	private final ModelRenderer rightleg;
	private final ModelRenderer rwing;
	private final ModelRenderer lwing_r1;
	private final ModelRenderer lwing_r2;
	private final ModelRenderer lwing;
	private final ModelRenderer lwing_r3;
	private final ModelRenderer lwing_r4;
	private final ModelRenderer beak;
	private final ModelRenderer beak_r1;
	private final ModelRenderer tail;
	private final ModelRenderer tail_r1;
	private final ModelRenderer tail_r2;
	private final ModelRenderer tail_r3;
	private final ModelRenderer tail_r4;
	private final ModelRenderer head2;
	private final ModelRenderer head_r1;
	private final ModelRenderer head_r2;
	private final ModelRenderer head_r3;
	private final ModelRenderer head3;
	private final ModelRenderer head_r4;

	public ModelBirdCockatoo() {
		textureWidth = 64;
		textureHeight = 32;

		head = new ModelRenderer(this);
		head.setRotationPoint(0.0F, 15.0F, -3.0F);
		head.cubeList.add(new ModelBox(head, 0, 0, -1.5F, -2.0F, -2.0F, 3, 3, 3, 0.0F, false));

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 15.0F, 2.0F);
		setRotationAngle(body, 1.0472F, 0.0F, 0.0F);
		body.cubeList.add(new ModelBox(body, 0, 9, -2.0F, -3.732F, -4.1436F, 4, 8, 4, 0.0F, false));

		leftleg = new ModelRenderer(this);
		leftleg.setRotationPoint(0.0F, 24.0F, 0.0F);
		leftleg.cubeList.add(new ModelBox(leftleg, 26, 0, 0.0F, -4.0F, 0.0F, 3, 4, 3, 0.0F, false));

		rightleg = new ModelRenderer(this);
		rightleg.setRotationPoint(0.0F, 24.0F, 0.0F);
		rightleg.cubeList.add(new ModelBox(rightleg, 26, 0, -3.0F, -4.0F, 0.0F, 3, 4, 3, 0.0F, false));

		rwing = new ModelRenderer(this);
		rwing.setRotationPoint(-3.0F, 18.0F, 2.0F);
		rwing.cubeList.add(new ModelBox(rwing, 28, 18, 0.0F, -4.0F, -3.0F, 1, 2, 1, 0.0F, false));
		rwing.cubeList.add(new ModelBox(rwing, 52, 27, 0.5F, -4.0F, -2.0F, 0, 2, 3, 0.0F, false));

		lwing_r1 = new ModelRenderer(this);
		lwing_r1.setRotationPoint(1.0F, -2.1397F, 0.6575F);
		rwing.addChild(lwing_r1);
		setRotationAngle(lwing_r1, 0.48F, 0.0F, 0.0F);
		lwing_r1.cubeList.add(new ModelBox(lwing_r1, 58, 25, -0.5F, -1.5649F, -2.3087F, 0, 4, 3, 0.0F, false));
		lwing_r1.cubeList.add(new ModelBox(lwing_r1, 28, 18, -1.0F, -1.5649F, -3.3087F, 1, 4, 1, 0.0F, false));

		lwing_r2 = new ModelRenderer(this);
		lwing_r2.setRotationPoint(1.0F, -0.3313F, -0.469F);
		rwing.addChild(lwing_r2);
		setRotationAngle(lwing_r2, 1.2217F, 0.0F, 0.0F);
		lwing_r2.cubeList.add(new ModelBox(lwing_r2, 46, 23, -0.5F, 0.0F, -2.0F, 0, 6, 3, 0.0F, false));
		lwing_r2.cubeList.add(new ModelBox(lwing_r2, 28, 18, -1.0F, 0.0F, -2.0F, 1, 3, 1, 0.0F, false));

		lwing = new ModelRenderer(this);
		lwing.setRotationPoint(3.0F, 18.0F, 2.0F);
		lwing.cubeList.add(new ModelBox(lwing, 28, 18, -1.0F, -4.0F, -3.0F, 1, 2, 1, 0.0F, false));
		lwing.cubeList.add(new ModelBox(lwing, 52, 27, -0.5F, -4.0F, -2.0F, 0, 2, 3, 0.0F, false));

		lwing_r3 = new ModelRenderer(this);
		lwing_r3.setRotationPoint(0.0F, -2.1397F, 0.6575F);
		lwing.addChild(lwing_r3);
		setRotationAngle(lwing_r3, 0.48F, 0.0F, 0.0F);
		lwing_r3.cubeList.add(new ModelBox(lwing_r3, 58, 25, -0.5F, -1.5649F, -2.3087F, 0, 4, 3, 0.0F, false));
		lwing_r3.cubeList.add(new ModelBox(lwing_r3, 28, 18, -1.0F, -1.5649F, -3.3087F, 1, 4, 1, 0.0F, false));

		lwing_r4 = new ModelRenderer(this);
		lwing_r4.setRotationPoint(0.0F, -0.3313F, -0.469F);
		lwing.addChild(lwing_r4);
		setRotationAngle(lwing_r4, 1.2217F, 0.0F, 0.0F);
		lwing_r4.cubeList.add(new ModelBox(lwing_r4, 46, 23, -0.5F, 0.0F, -2.0F, 0, 6, 3, 0.0F, false));
		lwing_r4.cubeList.add(new ModelBox(lwing_r4, 28, 18, -1.0F, 0.0F, -2.0F, 1, 3, 1, 0.0F, false));

		beak = new ModelRenderer(this);
		beak.setRotationPoint(0.0F, 15.0F, -6.0F);
		beak.cubeList.add(new ModelBox(beak, 14, 0, -0.5F, -0.5F, 0.0F, 1, 1, 2, 0.0F, false));
		beak.cubeList.add(new ModelBox(beak, 41, 30, -0.5F, -0.5F, 0.0F, 1, 1, 1, 0.0F, false));

		beak_r1 = new ModelRenderer(this);
		beak_r1.setRotationPoint(0.0F, 5.5742F, 9.4527F);
		beak.addChild(beak_r1);
		setRotationAngle(beak_r1, 0.4363F, 0.0F, 0.0F);
		beak_r1.cubeList.add(new ModelBox(beak_r1, 40, 29, -0.5F, -9.5F, -6.0F, 1, 1, 2, 0.0F, false));

		tail = new ModelRenderer(this);
		tail.setRotationPoint(0.0F, 18.0F, 7.0F);
		setRotationAngle(tail, -0.4363F, 0.0F, 0.0F);
		tail.cubeList.add(new ModelBox(tail, 14, 25, -1.0F, 0.2675F, -3.1027F, 2, 0, 7, 0.0F, false));

		tail_r1 = new ModelRenderer(this);
		tail_r1.setRotationPoint(0.0F, 5.7675F, -2.3616F);
		tail.addChild(tail_r1);
		setRotationAngle(tail_r1, 0.0F, -0.4363F, 0.0F);
		tail_r1.cubeList.add(new ModelBox(tail_r1, 0, 23, -1.0F, -5.9659F, 0.2588F, 1, 1, 6, 0.0F, false));

		tail_r2 = new ModelRenderer(this);
		tail_r2.setRotationPoint(0.0F, 5.7675F, -2.3616F);
		tail.addChild(tail_r2);
		setRotationAngle(tail_r2, 0.0F, 0.3054F, 0.0F);
		tail_r2.cubeList.add(new ModelBox(tail_r2, 14, 25, -1.0F, -5.5F, -0.7412F, 2, 0, 7, 0.0F, false));

		tail_r3 = new ModelRenderer(this);
		tail_r3.setRotationPoint(0.0F, 5.7675F, -2.3616F);
		tail.addChild(tail_r3);
		setRotationAngle(tail_r3, 0.0F, -0.3054F, 0.0F);
		tail_r3.cubeList.add(new ModelBox(tail_r3, 14, 25, -1.0F, -5.5F, -0.7412F, 2, 0, 7, 0.0F, false));

		tail_r4 = new ModelRenderer(this);
		tail_r4.setRotationPoint(0.0F, 5.7675F, -2.3616F);
		tail.addChild(tail_r4);
		setRotationAngle(tail_r4, 0.0F, 0.4363F, 0.0F);
		tail_r4.cubeList.add(new ModelBox(tail_r4, 0, 23, 0.0F, -5.9659F, 0.2588F, 1, 1, 6, 0.0F, false));

		head2 = new ModelRenderer(this);
		head2.setRotationPoint(-1.0F, 12.0F, -3.0F);
		head2.cubeList.add(new ModelBox(head2, 21, 30, 0.5F, 0.0F, -2.0F, 1, 1, 1, 0.0F, false));

		head_r1 = new ModelRenderer(this);
		head_r1.setRotationPoint(1.5F, -0.9063F, -2.5774F);
		head2.addChild(head_r1);
		setRotationAngle(head_r1, -1.5708F, 0.0F, 0.0F);
		head_r1.cubeList.add(new ModelBox(head_r1, 21, 28, -1.0F, -4.0F, 0.0F, 1, 3, 1, 0.0F, false));

		head_r2 = new ModelRenderer(this);
		head_r2.setRotationPoint(1.5F, 0.5774F, 0.0937F);
		head2.addChild(head_r2);
		setRotationAngle(head_r2, -0.4363F, 0.0F, 0.0F);
		head_r2.cubeList.add(new ModelBox(head_r2, 21, 29, -1.0F, -3.0F, 0.0F, 1, 2, 1, 0.0F, false));

		head_r3 = new ModelRenderer(this);
		head_r3.setRotationPoint(1.5F, -0.9063F, -1.5774F);
		head2.addChild(head_r3);
		setRotationAngle(head_r3, -0.4363F, 0.0F, 0.0F);
		head_r3.cubeList.add(new ModelBox(head_r3, 21, 30, -1.0F, 0.0F, 0.0F, 1, 1, 1, 0.0F, false));

		head3 = new ModelRenderer(this);
		head3.setRotationPoint(-1.0F, 12.0F, -2.0F);
		head3.cubeList.add(new ModelBox(head3, 34, 28, 1.0F, 0.0F, -2.0F, 0, 1, 3, 0.0F, false));

		head_r4 = new ModelRenderer(this);
		head_r4.setRotationPoint(0.5F, -1.2189F, 2.1339F);
		head3.addChild(head_r4);
		setRotationAngle(head_r4, 1.0472F, 0.0F, 0.0F);
		head_r4.cubeList.add(new ModelBox(head_r4, 35, 24, 0.5F, -1.0F, -3.0F, 0, 1, 3, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		head.render(f5);
		body.render(f5);
		leftleg.render(f5);
		rightleg.render(f5);
		rwing.render(f5);
		lwing.render(f5);
		beak.render(f5);
		tail.render(f5);
		head2.render(f5);
		head3.render(f5);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
		this.leftleg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.head.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.head.rotateAngleX = f4 / (180F / (float) Math.PI);
		this.rightleg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
	}
}