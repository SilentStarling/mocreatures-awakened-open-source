// Made with Blockbench 3.8.4
// Exported for Minecraft version 1.15 - 1.16
// Paste this class into your mod and generate all required imports

public static class ModelCrab extends EntityModel<Entity> {
	private final ModelRenderer Shell;
	private final ModelRenderer ShellRight;
	private final ModelRenderer ShellLeft;
	private final ModelRenderer ShellBack;
	private final ModelRenderer LeftEye;
	private final ModelRenderer LeftEyeBase;
	private final ModelRenderer RightEyeBase;
	private final ModelRenderer RightEye;
	private final ModelRenderer RightArmA;
	private final ModelRenderer RightArmB;
	private final ModelRenderer RightArmC;
	private final ModelRenderer RightArmD;
	private final ModelRenderer LeftArmA;
	private final ModelRenderer LeftArmB;
	private final ModelRenderer LeftArmC;
	private final ModelRenderer LeftArmD;
	private final ModelRenderer LeftLeg1A;
	private final ModelRenderer LeftLeg1B;
	private final ModelRenderer LeftLeg2A;
	private final ModelRenderer LeftLeg2B;
	private final ModelRenderer LeftLeg3A;
	private final ModelRenderer LeftLeg3B;
	private final ModelRenderer LeftLeg4A;
	private final ModelRenderer LeftLeg4B;
	private final ModelRenderer LeftLeg4C;
	private final ModelRenderer RightLeg1A;
	private final ModelRenderer RightLeg1B;
	private final ModelRenderer RightLeg2A;
	private final ModelRenderer RightLeg2B;
	private final ModelRenderer RightLeg3A;
	private final ModelRenderer RightLeg3B;
	private final ModelRenderer RightLeg4A;
	private final ModelRenderer RightLeg4B;
	private final ModelRenderer RightLeg4C;

	public ModelCrab() {
		textureWidth = 64;
		textureHeight = 64;

		Shell = new ModelRenderer(this);
		Shell.setRotationPoint(0.0F, 16.0F, 0.0F);
		Shell.setTextureOffset(0, 0).addBox(-5.0F, 0.0F, -4.0F, 10.0F, 4.0F, 8.0F, 0.0F, false);

		ShellRight = new ModelRenderer(this);
		ShellRight.setRotationPoint(0.0F, 16.0F, 0.0F);
		setRotationAngle(ShellRight, 0.0F, 0.0F, 0.4189F);
		ShellRight.setTextureOffset(0, 23).addBox(4.6F, -2.0F, -4.0F, 3.0F, 3.0F, 8.0F, 0.0F, false);

		ShellLeft = new ModelRenderer(this);
		ShellLeft.setRotationPoint(0.0F, 16.0F, 0.0F);
		setRotationAngle(ShellLeft, 0.0F, 0.0F, -0.4189F);
		ShellLeft.setTextureOffset(0, 12).addBox(-7.6F, -2.0F, -4.0F, 3.0F, 3.0F, 8.0F, 0.0F, false);

		ShellBack = new ModelRenderer(this);
		ShellBack.setRotationPoint(0.0F, 16.0F, 0.0F);
		setRotationAngle(ShellBack, -0.4189F, 0.0F, 0.0F);
		ShellBack.setTextureOffset(10, 42).addBox(-5.0F, -1.6F, 3.6F, 10.0F, 3.0F, 3.0F, 0.0F, false);

		LeftEye = new ModelRenderer(this);
		LeftEye.setRotationPoint(0.0F, 16.0F, 0.0F);
		setRotationAngle(LeftEye, 0.0F, 0.0F, 0.1745F);
		LeftEye.setTextureOffset(0, 4).addBox(1.0F, -2.0F, -4.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		LeftEyeBase = new ModelRenderer(this);
		LeftEyeBase.setRotationPoint(0.0F, 16.0F, 0.0F);
		setRotationAngle(LeftEyeBase, 0.0F, 0.0F, 0.2094F);
		LeftEyeBase.setTextureOffset(0, 16).addBox(1.0F, 1.0F, -5.0F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		RightEyeBase = new ModelRenderer(this);
		RightEyeBase.setRotationPoint(0.0F, 16.0F, 0.0F);
		setRotationAngle(RightEyeBase, 0.0F, 0.0F, -0.2094F);
		RightEyeBase.setTextureOffset(0, 12).addBox(-3.0F, 1.0F, -5.0F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		RightEye = new ModelRenderer(this);
		RightEye.setRotationPoint(0.0F, 16.0F, 0.0F);
		setRotationAngle(RightEye, 0.0F, 0.0F, -0.1745F);
		RightEye.setTextureOffset(0, 0).addBox(-2.0F, -2.0F, -4.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		RightArmA = new ModelRenderer(this);
		RightArmA.setRotationPoint(-4.0F, 19.0F, -4.0F);
		setRotationAngle(RightArmA, 0.0F, -0.5236F, 0.0F);
		RightArmA.setTextureOffset(0, 34).addBox(-4.0F, -1.0F, -1.0F, 4.0F, 2.0F, 2.0F, 0.0F, false);

		RightArmB = new ModelRenderer(this);
		RightArmB.setRotationPoint(-4.0F, 0.0F, 0.0F);
		RightArmA.addChild(RightArmB);
		setRotationAngle(RightArmB, 0.0F, -2.0944F, 0.0F);
		RightArmB.setTextureOffset(0, 34).addBox(-4.0F, -1.5F, -1.0F, 4.0F, 3.0F, 2.0F, 0.0F, false);

		RightArmC = new ModelRenderer(this);
		RightArmC.setRotationPoint(-4.0F, 0.0F, 0.0F);
		RightArmB.addChild(RightArmC);
		RightArmC.setTextureOffset(0, 34).addBox(-3.0F, -1.5F, -1.0F, 3.0F, 1.0F, 2.0F, 0.0F, false);

		RightArmD = new ModelRenderer(this);
		RightArmD.setRotationPoint(-4.0F, 0.0F, 0.0F);
		RightArmB.addChild(RightArmD);
		RightArmD.setTextureOffset(16, 12).addBox(-2.0F, 0.5F, -0.5F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		LeftArmA = new ModelRenderer(this);
		LeftArmA.setRotationPoint(4.0F, 19.0F, -4.0F);
		setRotationAngle(LeftArmA, 0.0F, 0.5236F, 0.0F);
		LeftArmA.setTextureOffset(0, 38).addBox(0.0F, -1.0F, -1.0F, 4.0F, 2.0F, 2.0F, 0.0F, false);

		LeftArmB = new ModelRenderer(this);
		LeftArmB.setRotationPoint(4.0F, 0.0F, 0.0F);
		LeftArmA.addChild(LeftArmB);
		setRotationAngle(LeftArmB, 0.0F, 2.0944F, 0.0F);
		LeftArmB.setTextureOffset(22, 20).addBox(0.0F, -1.5F, -1.0F, 4.0F, 3.0F, 2.0F, 0.0F, false);

		LeftArmC = new ModelRenderer(this);
		LeftArmC.setRotationPoint(4.0F, 0.0F, 0.0F);
		LeftArmB.addChild(LeftArmC);
		LeftArmC.setTextureOffset(22, 25).addBox(0.0F, -1.5F, -1.0F, 3.0F, 1.0F, 2.0F, 0.0F, false);

		LeftArmD = new ModelRenderer(this);
		LeftArmD.setRotationPoint(4.0F, 0.0F, 0.0F);
		LeftArmB.addChild(LeftArmD);
		LeftArmD.setTextureOffset(16, 23).addBox(0.0F, 0.5F, -0.5F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		LeftLeg1A = new ModelRenderer(this);
		LeftLeg1A.setRotationPoint(5.0F, 19.5F, -2.5F);
		setRotationAngle(LeftLeg1A, 0.0F, 0.1745F, 0.4189F);
		LeftLeg1A.setTextureOffset(0, 54).addBox(0.0F, -0.5F, -0.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		LeftLeg1B = new ModelRenderer(this);
		LeftLeg1B.setRotationPoint(4.0F, 0.0F, 0.0F);
		LeftLeg1A.addChild(LeftLeg1B);
		setRotationAngle(LeftLeg1B, 0.0F, 0.0F, 0.5236F);
		LeftLeg1B.setTextureOffset(0, 56).addBox(0.0F, -0.5F, -0.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		LeftLeg2A = new ModelRenderer(this);
		LeftLeg2A.setRotationPoint(5.0F, 19.5F, 0.0F);
		setRotationAngle(LeftLeg2A, 0.0F, -0.0873F, 0.4189F);
		LeftLeg2A.setTextureOffset(0, 56).addBox(0.0F, -0.5F, -0.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		LeftLeg2B = new ModelRenderer(this);
		LeftLeg2B.setRotationPoint(4.0F, 0.0F, 0.0F);
		LeftLeg2A.addChild(LeftLeg2B);
		setRotationAngle(LeftLeg2B, 0.0F, 0.0F, 0.5236F);
		LeftLeg2B.setTextureOffset(0, 56).addBox(0.0F, -0.5F, -0.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		LeftLeg3A = new ModelRenderer(this);
		LeftLeg3A.setRotationPoint(5.0F, 19.5F, 2.5F);
		setRotationAngle(LeftLeg3A, 0.0F, -0.6981F, 0.4189F);
		LeftLeg3A.setTextureOffset(0, 56).addBox(0.0F, -0.5F, -0.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		LeftLeg3B = new ModelRenderer(this);
		LeftLeg3B.setRotationPoint(4.0F, 0.0F, 0.0F);
		LeftLeg3A.addChild(LeftLeg3B);
		setRotationAngle(LeftLeg3B, 0.0F, 0.0F, 0.5236F);
		LeftLeg3B.setTextureOffset(0, 56).addBox(0.0F, -0.5F, -0.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		LeftLeg4A = new ModelRenderer(this);
		LeftLeg4A.setRotationPoint(2.0F, 19.5F, 3.5F);
		setRotationAngle(LeftLeg4A, 0.0F, -0.6109F, 0.4189F);
		LeftLeg4A.setTextureOffset(0, 56).addBox(0.0F, -0.5F, -0.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		LeftLeg4B = new ModelRenderer(this);
		LeftLeg4B.setRotationPoint(4.0F, 0.0F, 0.0F);
		LeftLeg4A.addChild(LeftLeg4B);
		setRotationAngle(LeftLeg4B, 0.0F, -1.309F, 0.4189F);
		LeftLeg4B.setTextureOffset(0, 56).addBox(0.0F, -0.5F, -1.0F, 3.0F, 1.0F, 2.0F, 0.0F, false);

		LeftLeg4C = new ModelRenderer(this);
		LeftLeg4C.setRotationPoint(3.0F, 0.0F, 0.0F);
		LeftLeg4B.addChild(LeftLeg4C);
		setRotationAngle(LeftLeg4C, 0.0F, -0.8727F, 0.4189F);
		LeftLeg4C.setTextureOffset(0, 56).addBox(0.0F, -0.5F, -1.0F, 3.0F, 1.0F, 2.0F, 0.0F, false);

		RightLeg1A = new ModelRenderer(this);
		RightLeg1A.setRotationPoint(-5.0F, 19.5F, -2.5F);
		setRotationAngle(RightLeg1A, 0.0F, -0.1745F, -0.4189F);
		RightLeg1A.setTextureOffset(16, 23).addBox(-4.0F, -0.5F, -0.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		RightLeg1B = new ModelRenderer(this);
		RightLeg1B.setRotationPoint(-4.0F, 0.0F, 0.0F);
		RightLeg1A.addChild(RightLeg1B);
		setRotationAngle(RightLeg1B, 0.0F, 0.0F, -0.5236F);
		RightLeg1B.setTextureOffset(0, 48).addBox(-4.0F, -0.5F, -0.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		RightLeg2A = new ModelRenderer(this);
		RightLeg2A.setRotationPoint(-5.0F, 19.5F, 0.0F);
		setRotationAngle(RightLeg2A, 0.0F, 0.0873F, -0.4189F);
		RightLeg2A.setTextureOffset(0, 44).addBox(-4.0F, -0.5F, -0.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		RightLeg2B = new ModelRenderer(this);
		RightLeg2B.setRotationPoint(-4.0F, 0.0F, 0.0F);
		RightLeg2A.addChild(RightLeg2B);
		setRotationAngle(RightLeg2B, 0.0F, 0.0F, -0.5236F);
		RightLeg2B.setTextureOffset(0, 50).addBox(-4.0F, -0.5F, -0.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		RightLeg3A = new ModelRenderer(this);
		RightLeg3A.setRotationPoint(-5.0F, 19.5F, 2.5F);
		setRotationAngle(RightLeg3A, 0.0F, 0.6981F, -0.4189F);
		RightLeg3A.setTextureOffset(0, 46).addBox(-4.0F, -0.5F, -0.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		RightLeg3B = new ModelRenderer(this);
		RightLeg3B.setRotationPoint(-4.0F, 0.0F, 0.0F);
		RightLeg3A.addChild(RightLeg3B);
		setRotationAngle(RightLeg3B, 0.0F, 0.0F, -0.5236F);
		RightLeg3B.setTextureOffset(0, 52).addBox(-4.0F, -0.5F, -0.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		RightLeg4A = new ModelRenderer(this);
		RightLeg4A.setRotationPoint(-3.0F, 19.5F, 3.5F);
		setRotationAngle(RightLeg4A, 0.0F, 0.6109F, -0.4189F);
		RightLeg4A.setTextureOffset(12, 34).addBox(-4.0F, -0.5F, -0.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		RightLeg4B = new ModelRenderer(this);
		RightLeg4B.setRotationPoint(-4.0F, 0.0F, 0.0F);
		RightLeg4A.addChild(RightLeg4B);
		setRotationAngle(RightLeg4B, 0.0F, 1.309F, -0.4189F);
		RightLeg4B.setTextureOffset(12, 36).addBox(-3.0F, -0.5F, -1.0F, 3.0F, 1.0F, 2.0F, 0.0F, false);

		RightLeg4C = new ModelRenderer(this);
		RightLeg4C.setRotationPoint(-3.0F, 0.0F, 0.0F);
		RightLeg4B.addChild(RightLeg4C);
		setRotationAngle(RightLeg4C, 0.0F, 0.8727F, -0.4189F);
		RightLeg4C.setTextureOffset(12, 39).addBox(-3.0F, -0.5F, -1.0F, 3.0F, 1.0F, 2.0F, 0.0F, true);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks,
			float netHeadYaw, float headPitch) {
		// previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		Shell.render(matrixStack, buffer, packedLight, packedOverlay);
		ShellRight.render(matrixStack, buffer, packedLight, packedOverlay);
		ShellLeft.render(matrixStack, buffer, packedLight, packedOverlay);
		ShellBack.render(matrixStack, buffer, packedLight, packedOverlay);
		LeftEye.render(matrixStack, buffer, packedLight, packedOverlay);
		LeftEyeBase.render(matrixStack, buffer, packedLight, packedOverlay);
		RightEyeBase.render(matrixStack, buffer, packedLight, packedOverlay);
		RightEye.render(matrixStack, buffer, packedLight, packedOverlay);
		RightArmA.render(matrixStack, buffer, packedLight, packedOverlay);
		LeftArmA.render(matrixStack, buffer, packedLight, packedOverlay);
		LeftLeg1A.render(matrixStack, buffer, packedLight, packedOverlay);
		LeftLeg2A.render(matrixStack, buffer, packedLight, packedOverlay);
		LeftLeg3A.render(matrixStack, buffer, packedLight, packedOverlay);
		LeftLeg4A.render(matrixStack, buffer, packedLight, packedOverlay);
		RightLeg1A.render(matrixStack, buffer, packedLight, packedOverlay);
		RightLeg2A.render(matrixStack, buffer, packedLight, packedOverlay);
		RightLeg3A.render(matrixStack, buffer, packedLight, packedOverlay);
		RightLeg4A.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}