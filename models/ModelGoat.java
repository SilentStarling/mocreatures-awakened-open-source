// Made with Blockbench 4.5.2
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports

public static class ModelGoat extends EntityModel<Entity> {
	private final ModelRenderer head;
	private final ModelRenderer body;
	private final ModelRenderer BackL;
	private final ModelRenderer BackR;
	private final ModelRenderer FrontR;
	private final ModelRenderer FrontL;
	private final ModelRenderer Tail;

	public ModelGoat() {
		textureWidth = 64;
		textureHeight = 64;

		head = new ModelRenderer(this);
		head.setRotationPoint(0.0F, 9.8312F, -11.95F);
		head.setTextureOffset(12, 2).addBox(1.7F, -6.7312F, 1.95F, 1.0F, 1.0F, 1.0F, -0.15F, false);
		head.setTextureOffset(12, 6).addBox(1.4F, -7.1312F, 1.25F, 1.0F, 1.0F, 1.0F, -0.1F, false);
		head.setTextureOffset(4, 14).addBox(1.2F, -6.7312F, 0.45F, 1.0F, 1.0F, 1.0F, -0.05F, false);
		head.setTextureOffset(12, 14).addBox(0.9F, -5.8312F, -0.25F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		head.setTextureOffset(12, 24).addBox(0.5F, -4.8312F, -0.75F, 1.0F, 1.0F, 1.0F, 0.1F, false);
		head.setTextureOffset(0, 24).addBox(-1.5F, -4.8312F, -0.75F, 1.0F, 1.0F, 1.0F, 0.1F, false);
		head.setTextureOffset(8, 14).addBox(-1.9F, -5.8312F, -0.25F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		head.setTextureOffset(0, 14).addBox(-2.2F, -6.7312F, 0.45F, 1.0F, 1.0F, 1.0F, -0.05F, false);
		head.setTextureOffset(12, 4).addBox(-2.4F, -7.1312F, 1.25F, 1.0F, 1.0F, 1.0F, -0.1F, false);
		head.setTextureOffset(10, 8).addBox(-2.7F, -6.7312F, 1.95F, 1.0F, 1.0F, 1.0F, -0.15F, false);
		head.setTextureOffset(9, 0).addBox(1.5F, -3.8312F, -0.05F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		head.setTextureOffset(10, 10).addBox(-3.5F, -3.8312F, -0.05F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		head.setTextureOffset(27, 27).addBox(-1.5F, -2.8312F, -5.05F, 3.0F, 3.0F, 3.0F, 0.0F, false);
		head.setTextureOffset(0, 0).addBox(-1.5F, -3.8312F, -2.05F, 3.0F, 5.0F, 3.0F, 0.0F, false);
		head.setTextureOffset(28, 9).addBox(-1.0F, 0.1688F, -5.05F, 2.0F, 1.0F, 3.0F, 0.0F, false);
		head.setTextureOffset(0, 8).addBox(-0.5F, 1.1688F, -4.05F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 13.8333F, -2.3333F);
		body.setTextureOffset(0, 24).addBox(-1.5F, -4.8333F, -10.6667F, 3.0F, 4.0F, 6.0F, -0.2F, false);
		body.setTextureOffset(0, 0).addBox(-3.0F, -4.8333F, -5.6667F, 6.0F, 8.0F, 16.0F, 0.0F, false);
		body.setTextureOffset(12, 24).addBox(-2.5F, 3.1667F, 3.3333F, 5.0F, 1.0F, 4.0F, 0.0F, false);

		BackL = new ModelRenderer(this);
		BackL.setRotationPoint(2.0F, 16.5F, 6.0F);
		BackL.setTextureOffset(0, 34).addBox(-1.0F, 0.5F, -1.0F, 2.0F, 7.0F, 2.0F, 0.0F, false);

		BackR = new ModelRenderer(this);
		BackR.setRotationPoint(-2.0F, 16.5F, 6.0F);
		BackR.setTextureOffset(26, 33).addBox(-1.0F, 0.5F, -1.0F, 2.0F, 7.0F, 2.0F, 0.0F, false);

		FrontR = new ModelRenderer(this);
		FrontR.setRotationPoint(-2.0F, 16.5F, -6.0F);
		FrontR.setTextureOffset(28, 0).addBox(-1.0F, 0.5F, -1.0F, 2.0F, 7.0F, 2.0F, 0.0F, false);

		FrontL = new ModelRenderer(this);
		FrontL.setRotationPoint(2.0F, 16.5F, -6.0F);
		FrontL.setTextureOffset(18, 29).addBox(-1.0F, 0.5F, -1.0F, 2.0F, 7.0F, 2.0F, 0.0F, false);

		Tail = new ModelRenderer(this);
		Tail.setRotationPoint(0.0F, 10.0F, 8.0F);
		Tail.setTextureOffset(0, 8).addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 4.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		head.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		body.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		BackL.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		BackR.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		FrontR.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		FrontL.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		Tail.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		this.head.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.head.rotateAngleX = f4 / (180F / (float) Math.PI);
		this.BackR.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.FrontR.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.Tail.rotateAngleY = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
		this.BackL.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.FrontL.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
	}
}