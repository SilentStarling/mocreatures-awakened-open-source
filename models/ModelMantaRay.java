// Made with Blockbench 3.8.4
// Exported for Minecraft version 1.15 - 1.16
// Paste this class into your mod and generate all required imports

public static class ModelMantaRay extends EntityModel<Entity> {
	private final ModelRenderer Body;
	private final ModelRenderer BodyTail;
	private final ModelRenderer BodyU;
	private final ModelRenderer Right;
	private final ModelRenderer RWingA;
	private final ModelRenderer RWingB;
	private final ModelRenderer RWingC;
	private final ModelRenderer RWingD;
	private final ModelRenderer RWingE;
	private final ModelRenderer RWingF;
	private final ModelRenderer RWingG;
	private final ModelRenderer REye;
	private final ModelRenderer Left;
	private final ModelRenderer LWingA;
	private final ModelRenderer LWingB;
	private final ModelRenderer LWingC;
	private final ModelRenderer LWingD;
	private final ModelRenderer LWingE;
	private final ModelRenderer LWingF;
	private final ModelRenderer LWingG;
	private final ModelRenderer LEye;
	private final ModelRenderer Tail;

	public ModelMantaRay() {
		textureWidth = 64;
		textureHeight = 32;

		Body = new ModelRenderer(this);
		Body.setRotationPoint(0.0F, 22.0F, -5.0F);
		Body.setTextureOffset(26, 0).addBox(-4.0F, -1.0F, 0.0F, 8.0F, 2.0F, 11.0F, 0.0F, false);

		BodyTail = new ModelRenderer(this);
		BodyTail.setRotationPoint(0.0F, 0.0F, 12.0F);
		Body.addChild(BodyTail);
		setRotationAngle(BodyTail, 0.0F, 1.0F, 0.0F);
		BodyTail.setTextureOffset(0, 20).addBox(-1.8F, -0.5F, -3.2F, 5.0F, 1.0F, 5.0F, 0.0F, false);

		BodyU = new ModelRenderer(this);
		BodyU.setRotationPoint(0.0F, -1.0F, 1.0F);
		Body.addChild(BodyU);
		BodyU.setTextureOffset(0, 11).addBox(-3.0F, -1.0F, 0.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);

		Right = new ModelRenderer(this);
		Right.setRotationPoint(-3.0F, 22.0F, -4.8F);
		Right.setTextureOffset(10, 26).addBox(-0.5F, -1.0F, -4.0F, 1.0F, 2.0F, 4.0F, 0.0F, false);

		RWingA = new ModelRenderer(this);
		RWingA.setRotationPoint(-1.0F, 0.0F, 5.8F);
		Right.addChild(RWingA);
		RWingA.setTextureOffset(0, 0).addBox(-3.0F, -0.5F, -5.0F, 3.0F, 1.0F, 10.0F, 0.0F, false);

		RWingB = new ModelRenderer(this);
		RWingB.setRotationPoint(-1.0F, 0.0F, 5.8F);
		Right.addChild(RWingB);
		RWingB.setTextureOffset(2, 2).addBox(-6.0F, -0.5F, -4.0F, 3.0F, 1.0F, 8.0F, 0.0F, false);

		RWingC = new ModelRenderer(this);
		RWingC.setRotationPoint(-1.0F, 0.0F, 5.8F);
		Right.addChild(RWingC);
		RWingC.setTextureOffset(5, 4).addBox(-8.0F, -0.5F, -3.0F, 2.0F, 1.0F, 6.0F, 0.0F, false);

		RWingD = new ModelRenderer(this);
		RWingD.setRotationPoint(-1.0F, 0.0F, 5.8F);
		Right.addChild(RWingD);
		RWingD.setTextureOffset(6, 5).addBox(-10.0F, -0.5F, -2.5F, 2.0F, 1.0F, 5.0F, 0.0F, false);

		RWingE = new ModelRenderer(this);
		RWingE.setRotationPoint(-1.0F, 0.0F, 5.8F);
		Right.addChild(RWingE);
		RWingE.setTextureOffset(7, 6).addBox(-12.0F, -0.5F, -2.0F, 2.0F, 1.0F, 4.0F, 0.0F, false);

		RWingF = new ModelRenderer(this);
		RWingF.setRotationPoint(-1.0F, 0.0F, 5.8F);
		Right.addChild(RWingF);
		RWingF.setTextureOffset(8, 7).addBox(-14.0F, -0.5F, -1.5F, 2.0F, 1.0F, 3.0F, 0.0F, false);

		RWingG = new ModelRenderer(this);
		RWingG.setRotationPoint(-1.0F, 0.0F, 5.8F);
		Right.addChild(RWingG);
		RWingG.setTextureOffset(9, 8).addBox(-16.0F, -0.5F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		REye = new ModelRenderer(this);
		REye.setRotationPoint(3.0F, -1.0F, 0.8F);
		Right.addChild(REye);
		REye.setTextureOffset(0, 3).addBox(2.0F, -2.0F, 1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		Left = new ModelRenderer(this);
		Left.setRotationPoint(3.0F, 22.0F, -4.8F);
		Left.setTextureOffset(0, 26).addBox(-0.5F, -1.0F, -4.0F, 1.0F, 2.0F, 4.0F, 0.0F, false);

		LWingA = new ModelRenderer(this);
		LWingA.setRotationPoint(1.0F, 0.0F, 5.8F);
		Left.addChild(LWingA);
		LWingA.setTextureOffset(0, 0).addBox(0.0F, -0.5F, -5.0F, 3.0F, 1.0F, 10.0F, 0.0F, true);

		LWingB = new ModelRenderer(this);
		LWingB.setRotationPoint(1.0F, 0.0F, 5.8F);
		Left.addChild(LWingB);
		LWingB.setTextureOffset(2, 2).addBox(3.0F, -0.5F, -4.0F, 3.0F, 1.0F, 8.0F, 0.0F, true);

		LWingC = new ModelRenderer(this);
		LWingC.setRotationPoint(1.0F, 0.0F, 5.8F);
		Left.addChild(LWingC);
		LWingC.setTextureOffset(5, 4).addBox(6.0F, -0.5F, -3.0F, 2.0F, 1.0F, 6.0F, 0.0F, true);

		LWingD = new ModelRenderer(this);
		LWingD.setRotationPoint(1.0F, 0.0F, 5.8F);
		Left.addChild(LWingD);
		LWingD.setTextureOffset(6, 5).addBox(8.0F, -0.5F, -2.5F, 2.0F, 1.0F, 5.0F, 0.0F, true);

		LWingE = new ModelRenderer(this);
		LWingE.setRotationPoint(1.0F, 0.0F, 5.8F);
		Left.addChild(LWingE);
		LWingE.setTextureOffset(7, 6).addBox(10.0F, -0.5F, -2.0F, 2.0F, 1.0F, 4.0F, 0.0F, true);

		LWingF = new ModelRenderer(this);
		LWingF.setRotationPoint(1.0F, 0.0F, 5.8F);
		Left.addChild(LWingF);
		LWingF.setTextureOffset(8, 7).addBox(12.0F, -0.5F, -1.5F, 2.0F, 1.0F, 3.0F, 0.0F, true);

		LWingG = new ModelRenderer(this);
		LWingG.setRotationPoint(1.0F, 0.0F, 5.8F);
		Left.addChild(LWingG);
		LWingG.setTextureOffset(9, 8).addBox(14.0F, -0.5F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, true);

		LEye = new ModelRenderer(this);
		LEye.setRotationPoint(-3.0F, -1.0F, 0.8F);
		Left.addChild(LEye);
		LEye.setTextureOffset(0, 0).addBox(-3.0F, -2.0F, 1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		Tail = new ModelRenderer(this);
		Tail.setRotationPoint(0.0F, 22.0F, 8.0F);
		Tail.setTextureOffset(30, 15).addBox(-0.5F, -0.5F, 1.0F, 1.0F, 1.0F, 16.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks,
			float netHeadYaw, float headPitch) {
		// previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		Body.render(matrixStack, buffer, packedLight, packedOverlay);
		Right.render(matrixStack, buffer, packedLight, packedOverlay);
		Left.render(matrixStack, buffer, packedLight, packedOverlay);
		Tail.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}