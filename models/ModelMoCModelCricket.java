// Made with Blockbench 3.6.6
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

public static class ModelMoCModelCricket extends EntityModel<Entity> {
	private final ModelRenderer Thorax;
	private final ModelRenderer Abdomen;
	private final ModelRenderer TailA;
	private final ModelRenderer TailB;
	private final ModelRenderer FrontLegs;
	private final ModelRenderer MidLegs;
	private final ModelRenderer ThighLeft;
	private final ModelRenderer ThighLeftB;
	private final ModelRenderer ThighRight;
	private final ModelRenderer ThighRightB;
	private final ModelRenderer LegLeft;
	private final ModelRenderer LegLeftB;
	private final ModelRenderer LegRight;
	private final ModelRenderer LegRightB;
	private final ModelRenderer FoldedWings;
	private final ModelRenderer HeadMovement;
	private final ModelRenderer Head;
	private final ModelRenderer Antenna;
	private final ModelRenderer AntennaB;
	private final ModelRenderer FlyingWings;
	private final ModelRenderer LeftWing;
	private final ModelRenderer RightWing;

	public ModelMoCModelCricket() {
		textureWidth = 32;
		textureHeight = 32;

		Thorax = new ModelRenderer(this);
		Thorax.setRotationPoint(0.0F, 21.0F, -1.0F);
		Thorax.setTextureOffset(0, 0).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		Abdomen = new ModelRenderer(this);
		Abdomen.setRotationPoint(0.0F, 22.0F, 0.0F);
		setRotationAngle(Abdomen, 1.4277F, 0.0F, 0.0F);
		Abdomen.setTextureOffset(8, 0).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		TailA = new ModelRenderer(this);
		TailA.setRotationPoint(0.0F, 22.0F, 2.8F);
		setRotationAngle(TailA, 1.3087F, 0.0F, 0.0F);
		TailA.setTextureOffset(4, 9).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 3.0F, 0.0F, 0.0F, false);

		TailB = new ModelRenderer(this);
		TailB.setRotationPoint(0.0F, 23.0F, 2.8F);
		setRotationAngle(TailB, 1.6656F, 0.0F, 0.0F);
		TailB.setTextureOffset(4, 7).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);

		FrontLegs = new ModelRenderer(this);
		FrontLegs.setRotationPoint(0.0F, 23.0F, -1.8F);
		setRotationAngle(FrontLegs, -0.8328F, 0.0F, 0.0F);
		FrontLegs.setTextureOffset(0, 7).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);

		MidLegs = new ModelRenderer(this);
		MidLegs.setRotationPoint(0.0F, 23.0F, -1.2F);
		setRotationAngle(MidLegs, 1.0707F, 0.0F, 0.0F);
		MidLegs.setTextureOffset(0, 13).addBox(-2.0F, 0.0F, 0.0F, 4.0F, 2.0F, 0.0F, 0.0F, false);

		ThighLeft = new ModelRenderer(this);
		ThighLeft.setRotationPoint(0.5F, 23.0F, 0.0F);
		setRotationAngle(ThighLeft, -0.4887F, 0.2618F, 0.0F);
		ThighLeft.setTextureOffset(8, 5).addBox(0.0F, -3.0F, 0.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		ThighLeftB = new ModelRenderer(this);
		ThighLeftB.setRotationPoint(0.5F, 22.5F, 0.0F);
		setRotationAngle(ThighLeftB, -1.7628F, 0.0F, 0.0F);
		ThighLeftB.setTextureOffset(8, 5).addBox(0.0F, -3.0F, 0.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		ThighRight = new ModelRenderer(this);
		ThighRight.setRotationPoint(-0.5F, 23.0F, 0.0F);
		setRotationAngle(ThighRight, -0.4887F, -0.2618F, 0.0F);
		ThighRight.setTextureOffset(12, 5).addBox(-1.0F, -3.0F, 0.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		ThighRightB = new ModelRenderer(this);
		ThighRightB.setRotationPoint(-0.5F, 22.5F, 0.0F);
		setRotationAngle(ThighRightB, -1.7628F, 0.0F, 0.0F);
		ThighRightB.setTextureOffset(12, 5).addBox(-1.0F, -3.0F, 0.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		LegLeft = new ModelRenderer(this);
		LegLeft.setRotationPoint(2.0F, 21.0F, 2.5F);
		LegLeft.setTextureOffset(0, 15).addBox(0.0F, 0.0F, -1.0F, 0.0F, 3.0F, 2.0F, 0.0F, false);

		LegLeftB = new ModelRenderer(this);
		LegLeftB.setRotationPoint(1.5F, 23.0F, 2.9F);
		setRotationAngle(LegLeftB, 1.2492F, 0.0F, 0.0F);
		LegLeftB.setTextureOffset(4, 15).addBox(0.0F, 0.0F, -1.0F, 0.0F, 3.0F, 2.0F, 0.0F, false);

		LegRight = new ModelRenderer(this);
		LegRight.setRotationPoint(-2.0F, 21.0F, 2.5F);
		LegRight.setTextureOffset(4, 15).addBox(0.0F, 0.0F, -1.0F, 0.0F, 3.0F, 2.0F, 0.0F, false);

		LegRightB = new ModelRenderer(this);
		LegRightB.setRotationPoint(-1.5F, 23.0F, 2.9F);
		setRotationAngle(LegRightB, 1.2492F, 0.0F, 0.0F);
		LegRightB.setTextureOffset(4, 15).addBox(0.0F, 0.0F, -1.0F, 0.0F, 3.0F, 2.0F, 0.0F, false);

		FoldedWings = new ModelRenderer(this);
		FoldedWings.setRotationPoint(0.0F, 20.9F, -2.0F);
		setRotationAngle(FoldedWings, 0.0F, -1.5708F, 0.0F);
		FoldedWings.setTextureOffset(0, 26).addBox(0.0F, 0.0F, -1.0F, 6.0F, 0.0F, 2.0F, 0.0F, false);

		HeadMovement = new ModelRenderer(this);
		HeadMovement.setRotationPoint(0.0F, 23.0F, -2.0F);

		Head = new ModelRenderer(this);
		Head.setRotationPoint(0.0F, -0.5F, 0.0F);
		HeadMovement.addChild(Head);
		setRotationAngle(Head, -2.1712F, 0.0F, 0.0F);
		Head.setTextureOffset(0, 4).addBox(-0.5F, 0.0F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		Antenna = new ModelRenderer(this);
		Antenna.setRotationPoint(0.0F, -0.5F, -1.0F);
		HeadMovement.addChild(Antenna);
		setRotationAngle(Antenna, -2.7363F, 0.0F, 0.0F);
		Antenna.setTextureOffset(0, 11).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);

		AntennaB = new ModelRenderer(this);
		AntennaB.setRotationPoint(0.0F, -2.3F, -1.8F);
		HeadMovement.addChild(AntennaB);
		setRotationAngle(AntennaB, 2.8851F, 0.0F, 0.0F);
		AntennaB.setTextureOffset(0, 9).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);

		FlyingWings = new ModelRenderer(this);
		FlyingWings.setRotationPoint(0.0F, 24.0F, 0.0F);

		LeftWing = new ModelRenderer(this);
		LeftWing.setRotationPoint(0.0F, -3.1F, -1.0F);
		FlyingWings.addChild(LeftWing);
		setRotationAngle(LeftWing, 0.0F, -1.5446F, 0.0436F);
		LeftWing.setTextureOffset(0, 30).addBox(0.0F, 0.0F, -1.0F, 6.0F, 0.0F, 2.0F, 0.0F, false);

		RightWing = new ModelRenderer(this);
		RightWing.setRotationPoint(0.0F, -3.1F, -1.0F);
		FlyingWings.addChild(RightWing);
		setRotationAngle(RightWing, 0.0F, 1.5883F, -0.0436F);
		RightWing.setTextureOffset(0, 28).addBox(-6.0F, 0.0F, -1.0F, 6.0F, 0.0F, 2.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		Thorax.render(matrixStack, buffer, packedLight, packedOverlay);
		Abdomen.render(matrixStack, buffer, packedLight, packedOverlay);
		TailA.render(matrixStack, buffer, packedLight, packedOverlay);
		TailB.render(matrixStack, buffer, packedLight, packedOverlay);
		FrontLegs.render(matrixStack, buffer, packedLight, packedOverlay);
		MidLegs.render(matrixStack, buffer, packedLight, packedOverlay);
		ThighLeft.render(matrixStack, buffer, packedLight, packedOverlay);
		ThighLeftB.render(matrixStack, buffer, packedLight, packedOverlay);
		ThighRight.render(matrixStack, buffer, packedLight, packedOverlay);
		ThighRightB.render(matrixStack, buffer, packedLight, packedOverlay);
		LegLeft.render(matrixStack, buffer, packedLight, packedOverlay);
		LegLeftB.render(matrixStack, buffer, packedLight, packedOverlay);
		LegRight.render(matrixStack, buffer, packedLight, packedOverlay);
		LegRightB.render(matrixStack, buffer, packedLight, packedOverlay);
		FoldedWings.render(matrixStack, buffer, packedLight, packedOverlay);
		HeadMovement.render(matrixStack, buffer, packedLight, packedOverlay);
		FlyingWings.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
		this.HeadMovement.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.HeadMovement.rotateAngleX = f4 / (180F / (float) Math.PI);
	}
}