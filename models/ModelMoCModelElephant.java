// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

public static class ModelMoCModelElephant extends EntityModel<Entity> {
	private final ModelRenderer Back;
	private final ModelRenderer Hump;
	private final ModelRenderer Body;
	private final ModelRenderer Skirt;
	private final ModelRenderer StorageRightBedroll;
	private final ModelRenderer StorageLeftBedroll;
	private final ModelRenderer StorageFrontRightChest;
	private final ModelRenderer StorageBackRightChest;
	private final ModelRenderer StorageFrontLeftChest;
	private final ModelRenderer StorageBackLeftChest;
	private final ModelRenderer StorageRightBlankets;
	private final ModelRenderer StorageLeftBlankets;
	private final ModelRenderer HarnessBlanket;
	private final ModelRenderer HarnessUpperBelt;
	private final ModelRenderer HarnessLowerBelt;
	private final ModelRenderer CabinPillow;
	private final ModelRenderer CabinLeftRail;
	private final ModelRenderer Cabin;
	private final ModelRenderer CabinRightRail;
	private final ModelRenderer CabinBackRail;
	private final ModelRenderer CabinRoof;
	private final ModelRenderer FortNeckBeam;
	private final ModelRenderer FortBackBeam;
	private final ModelRenderer TuskLD1;
	private final ModelRenderer TuskLD2;
	private final ModelRenderer TuskLD3;
	private final ModelRenderer TuskLD4;
	private final ModelRenderer TuskLD5;
	private final ModelRenderer TuskRD1;
	private final ModelRenderer TuskRD2;
	private final ModelRenderer TuskRD3;
	private final ModelRenderer TuskRD4;
	private final ModelRenderer TuskRD5;
	private final ModelRenderer TuskLI1;
	private final ModelRenderer TuskLI2;
	private final ModelRenderer TuskLI3;
	private final ModelRenderer TuskLI4;
	private final ModelRenderer TuskLI5;
	private final ModelRenderer TuskRI1;
	private final ModelRenderer TuskRI2;
	private final ModelRenderer TuskRI3;
	private final ModelRenderer TuskRI4;
	private final ModelRenderer TuskRI5;
	private final ModelRenderer TuskLW1;
	private final ModelRenderer TuskLW2;
	private final ModelRenderer TuskLW3;
	private final ModelRenderer TuskLW4;
	private final ModelRenderer TuskLW5;
	private final ModelRenderer TuskRW1;
	private final ModelRenderer TuskRW2;
	private final ModelRenderer TuskRW3;
	private final ModelRenderer TuskRW4;
	private final ModelRenderer TuskRW5;
	private final ModelRenderer FortFloor1;
	private final ModelRenderer FortFloor2;
	private final ModelRenderer FortFloor3;
	private final ModelRenderer FortBackWall;
	private final ModelRenderer FortBackLeftWall;
	private final ModelRenderer FortBackRightWall;
	private final ModelRenderer StorageUpLeft;
	private final ModelRenderer StorageUpRight;
	private final ModelRenderer HeadMovement;
	private final ModelRenderer Head;
	private final ModelRenderer Neck;
	private final ModelRenderer HeadBump;
	private final ModelRenderer Chin;
	private final ModelRenderer LowerLip;
	private final ModelRenderer LeftSmallEar;
	private final ModelRenderer LeftBigEar;
	private final ModelRenderer RightSmallEar;
	private final ModelRenderer RightBigEar;
	private final ModelRenderer TrunkMovement;
	private final ModelRenderer TrunkA;
	private final ModelRenderer TrunkB;
	private final ModelRenderer TrunkC;
	private final ModelRenderer TrunkD;
	private final ModelRenderer TrunkE;
	private final ModelRenderer RightTuskA;
	private final ModelRenderer RightTuskB;
	private final ModelRenderer RightTuskC;
	private final ModelRenderer RightTuskD;
	private final ModelRenderer LeftTuskA;
	private final ModelRenderer LeftTuskB;
	private final ModelRenderer LeftTuskC;
	private final ModelRenderer LeftTuskD;
	private final ModelRenderer TailMovement;
	private final ModelRenderer TailRoot;
	private final ModelRenderer Tail;
	private final ModelRenderer TailPlush;
	private final ModelRenderer RFrontMovement;
	private final ModelRenderer FrontRightUpperLeg;
	private final ModelRenderer FrontRightLowerLeg;
	private final ModelRenderer RBackMovement;
	private final ModelRenderer BackRightUpperLeg;
	private final ModelRenderer BackRightLowerLeg;
	private final ModelRenderer LFrontMovement;
	private final ModelRenderer FrontLeftUpperLeg;
	private final ModelRenderer FrontLeftLowerLeg;
	private final ModelRenderer LBackMovement;
	private final ModelRenderer BackLeftUpperLeg;
	private final ModelRenderer BackLeftLowerLeg;

	public ModelMoCModelElephant() {
		textureWidth = 128;
		textureHeight = 256;

		Back = new ModelRenderer(this);
		Back.setRotationPoint(0.0F, -4.0F, -3.0F);
		Back.setTextureOffset(0, 48).addBox(-5.0F, -10.0F, -10.0F, 10.0F, 2.0F, 26.0F, 0.0F, false);

		Hump = new ModelRenderer(this);
		Hump.setRotationPoint(0.0F, -13.0F, -5.5F);
		Hump.setTextureOffset(88, 30).addBox(-6.0F, -2.0F, -3.0F, 12.0F, 3.0F, 8.0F, 0.0F, false);

		Body = new ModelRenderer(this);
		Body.setRotationPoint(0.0F, -2.0F, -3.0F);
		Body.setTextureOffset(0, 0).addBox(-8.0F, -10.0F, -10.0F, 16.0F, 20.0F, 28.0F, 0.0F, false);

		Skirt = new ModelRenderer(this);
		Skirt.setRotationPoint(0.0F, 8.0F, -3.0F);
		setRotationAngle(Skirt, 1.5708F, 0.0F, 0.0F);
		Skirt.setTextureOffset(28, 94).addBox(-8.0F, -10.0F, -6.0F, 16.0F, 28.0F, 6.0F, 0.0F, false);

		StorageRightBedroll = new ModelRenderer(this);
		StorageRightBedroll.setRotationPoint(-9.0F, -10.2F, 1.0F);
		setRotationAngle(StorageRightBedroll, 0.0F, 0.0F, 0.4189F);
		StorageRightBedroll.setTextureOffset(90, 231).addBox(-2.5F, 8.0F, -8.0F, 3.0F, 3.0F, 16.0F, 0.0F, false);

		StorageLeftBedroll = new ModelRenderer(this);
		StorageLeftBedroll.setRotationPoint(9.0F, -10.2F, 1.0F);
		setRotationAngle(StorageLeftBedroll, 0.0F, 0.0F, -0.4189F);
		StorageLeftBedroll.setTextureOffset(90, 231).addBox(-0.5F, 8.0F, -8.0F, 3.0F, 3.0F, 16.0F, 0.0F, false);

		StorageFrontRightChest = new ModelRenderer(this);
		StorageFrontRightChest.setRotationPoint(-11.0F, -1.2F, -4.5F);
		setRotationAngle(StorageFrontRightChest, 0.0F, 0.0F, -0.2618F);
		StorageFrontRightChest.setTextureOffset(76, 208).addBox(-3.5F, 0.0F, -5.0F, 5.0F, 8.0F, 10.0F, 0.0F, false);

		StorageBackRightChest = new ModelRenderer(this);
		StorageBackRightChest.setRotationPoint(-11.0F, -1.2F, 6.5F);
		setRotationAngle(StorageBackRightChest, 0.0F, 0.0F, -0.2618F);
		StorageBackRightChest.setTextureOffset(76, 208).addBox(-3.5F, 0.0F, -5.0F, 5.0F, 8.0F, 10.0F, 0.0F, false);

		StorageFrontLeftChest = new ModelRenderer(this);
		StorageFrontLeftChest.setRotationPoint(11.0F, -1.2F, -4.5F);
		setRotationAngle(StorageFrontLeftChest, 0.0F, 0.0F, 0.2618F);
		StorageFrontLeftChest.setTextureOffset(76, 226).addBox(-1.5F, 0.0F, -5.0F, 5.0F, 8.0F, 10.0F, 0.0F, false);

		StorageBackLeftChest = new ModelRenderer(this);
		StorageBackLeftChest.setRotationPoint(11.0F, -1.2F, 6.5F);
		setRotationAngle(StorageBackLeftChest, 0.0F, 0.0F, 0.2618F);
		StorageBackLeftChest.setTextureOffset(76, 226).addBox(-1.5F, 0.0F, -5.0F, 5.0F, 8.0F, 10.0F, 0.0F, false);

		StorageRightBlankets = new ModelRenderer(this);
		StorageRightBlankets.setRotationPoint(-9.0F, -10.2F, 1.0F);
		StorageRightBlankets.setTextureOffset(0, 228).addBox(-4.5F, -1.0F, -7.0F, 5.0F, 10.0F, 14.0F, 0.0F, false);

		StorageLeftBlankets = new ModelRenderer(this);
		StorageLeftBlankets.setRotationPoint(9.0F, -10.2F, 1.0F);
		StorageLeftBlankets.setTextureOffset(38, 228).addBox(-0.5F, -1.0F, -7.0F, 5.0F, 10.0F, 14.0F, 0.0F, false);

		HarnessBlanket = new ModelRenderer(this);
		HarnessBlanket.setRotationPoint(0.0F, -13.2F, -3.5F);
		HarnessBlanket.setTextureOffset(0, 196).addBox(-8.5F, -2.0F, -3.0F, 17.0F, 14.0F, 18.0F, 0.0F, false);

		HarnessUpperBelt = new ModelRenderer(this);
		HarnessUpperBelt.setRotationPoint(0.0F, -2.0F, -2.5F);
		HarnessUpperBelt.setTextureOffset(70, 196).addBox(-8.5F, 0.5F, -2.0F, 17.0F, 10.0F, 2.0F, 0.0F, false);

		HarnessLowerBelt = new ModelRenderer(this);
		HarnessLowerBelt.setRotationPoint(0.0F, -2.0F, 7.0F);
		HarnessLowerBelt.setTextureOffset(70, 196).addBox(-8.5F, 0.5F, -2.5F, 17.0F, 10.0F, 2.0F, 0.0F, false);

		CabinPillow = new ModelRenderer(this);
		CabinPillow.setRotationPoint(0.0F, -16.0F, 2.0F);
		CabinPillow.setTextureOffset(76, 146).addBox(-6.5F, 0.0F, -6.5F, 13.0F, 4.0F, 13.0F, 0.0F, false);

		CabinLeftRail = new ModelRenderer(this);
		CabinLeftRail.setRotationPoint(0.0F, -23.0F, 1.5F);
		setRotationAngle(CabinLeftRail, 0.0F, 1.5708F, 0.0F);
		CabinLeftRail.setTextureOffset(56, 147).addBox(-7.0F, 0.0F, 7.0F, 14.0F, 1.0F, 1.0F, 0.0F, false);

		Cabin = new ModelRenderer(this);
		Cabin.setRotationPoint(0.0F, -35.0F, 2.0F);
		Cabin.setTextureOffset(0, 128).addBox(-7.0F, 0.0F, -7.0F, 14.0F, 20.0F, 14.0F, 0.0F, false);

		CabinRightRail = new ModelRenderer(this);
		CabinRightRail.setRotationPoint(0.0F, -23.0F, 1.5F);
		setRotationAngle(CabinRightRail, 0.0F, -1.5708F, 0.0F);
		CabinRightRail.setTextureOffset(56, 147).addBox(-7.0F, 0.0F, 7.0F, 14.0F, 1.0F, 1.0F, 0.0F, false);

		CabinBackRail = new ModelRenderer(this);
		CabinBackRail.setRotationPoint(0.0F, -23.0F, 1.5F);
		CabinBackRail.setTextureOffset(56, 147).addBox(-7.0F, 0.0F, 7.0F, 14.0F, 1.0F, 1.0F, 0.0F, false);

		CabinRoof = new ModelRenderer(this);
		CabinRoof.setRotationPoint(0.0F, -34.0F, 2.0F);
		CabinRoof.setTextureOffset(56, 128).addBox(-7.5F, 0.0F, -7.5F, 15.0F, 4.0F, 15.0F, 0.0F, false);

		FortNeckBeam = new ModelRenderer(this);
		FortNeckBeam.setRotationPoint(0.0F, -16.0F, 10.0F);
		FortNeckBeam.setTextureOffset(26, 180).addBox(-12.0F, 0.0F, -20.5F, 24.0F, 4.0F, 4.0F, 0.0F, false);

		FortBackBeam = new ModelRenderer(this);
		FortBackBeam.setRotationPoint(0.0F, -16.0F, 10.0F);
		FortBackBeam.setTextureOffset(26, 180).addBox(-12.0F, 0.0F, 0.0F, 24.0F, 4.0F, 4.0F, 0.0F, false);

		TuskLD1 = new ModelRenderer(this);
		TuskLD1.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLD1, 0.6981F, 0.0F, -0.1745F);
		TuskLD1.setTextureOffset(108, 207).addBox(1.3F, 5.5F, -24.2F, 3.0F, 3.0F, 7.0F, 0.0F, false);

		TuskLD2 = new ModelRenderer(this);
		TuskLD2.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLD2, 0.1745F, 0.0F, -0.1745F);
		TuskLD2.setTextureOffset(112, 199).addBox(1.29F, 16.5F, -21.9F, 3.0F, 3.0F, 5.0F, 0.0F, false);

		TuskLD3 = new ModelRenderer(this);
		TuskLD3.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLD3, -0.3491F, 0.0F, -0.1745F);
		TuskLD3.setTextureOffset(110, 190).addBox(1.3F, 24.9F, -15.5F, 3.0F, 3.0F, 6.0F, 0.0F, false);

		TuskLD4 = new ModelRenderer(this);
		TuskLD4.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLD4, 0.1745F, 0.0F, -0.1745F);
		TuskLD4.setTextureOffset(86, 175).addBox(2.7F, 14.5F, -21.9F, 0.0F, 7.0F, 5.0F, 0.0F, false);

		TuskLD5 = new ModelRenderer(this);
		TuskLD5.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLD5, -0.3491F, 0.0F, -0.1745F);
		TuskLD5.setTextureOffset(112, 225).addBox(2.7F, 22.9F, -17.5F, 0.0F, 7.0F, 8.0F, 0.0F, false);

		TuskRD1 = new ModelRenderer(this);
		TuskRD1.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRD1, 0.6981F, 0.0F, 0.1745F);
		TuskRD1.setTextureOffset(108, 207).addBox(-4.3F, 5.5F, -24.2F, 3.0F, 3.0F, 7.0F, 0.0F, false);

		TuskRD2 = new ModelRenderer(this);
		TuskRD2.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRD2, 0.1745F, 0.0F, 0.1745F);
		TuskRD2.setTextureOffset(112, 199).addBox(-4.29F, 16.5F, -21.9F, 3.0F, 3.0F, 5.0F, 0.0F, false);

		TuskRD3 = new ModelRenderer(this);
		TuskRD3.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRD3, -0.3491F, 0.0F, 0.1745F);
		TuskRD3.setTextureOffset(110, 190).addBox(-4.3F, 24.9F, -15.5F, 3.0F, 3.0F, 6.0F, 0.0F, false);

		TuskRD4 = new ModelRenderer(this);
		TuskRD4.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRD4, 0.1745F, 0.0F, 0.1745F);
		TuskRD4.setTextureOffset(86, 163).addBox(-2.8F, 14.5F, -21.9F, 0.0F, 7.0F, 5.0F, 0.0F, false);

		TuskRD5 = new ModelRenderer(this);
		TuskRD5.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRD5, -0.3491F, 0.0F, 0.1745F);
		TuskRD5.setTextureOffset(112, 232).addBox(-2.8F, 22.9F, -17.5F, 0.0F, 7.0F, 8.0F, 0.0F, false);

		TuskLI1 = new ModelRenderer(this);
		TuskLI1.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLI1, 0.6981F, 0.0F, -0.1745F);
		TuskLI1.setTextureOffset(108, 180).addBox(1.3F, 5.5F, -24.2F, 3.0F, 3.0F, 7.0F, 0.0F, false);

		TuskLI2 = new ModelRenderer(this);
		TuskLI2.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLI2, 0.1745F, 0.0F, -0.1745F);
		TuskLI2.setTextureOffset(112, 172).addBox(1.29F, 16.5F, -21.9F, 3.0F, 3.0F, 5.0F, 0.0F, false);

		TuskLI3 = new ModelRenderer(this);
		TuskLI3.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLI3, -0.3491F, 0.0F, -0.1745F);
		TuskLI3.setTextureOffset(110, 163).addBox(1.3F, 24.9F, -15.5F, 3.0F, 3.0F, 6.0F, 0.0F, false);

		TuskLI4 = new ModelRenderer(this);
		TuskLI4.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLI4, 0.1745F, 0.0F, -0.1745F);
		TuskLI4.setTextureOffset(96, 175).addBox(2.7F, 14.5F, -21.9F, 0.0F, 7.0F, 5.0F, 0.0F, false);

		TuskLI5 = new ModelRenderer(this);
		TuskLI5.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLI5, -0.3491F, 0.0F, -0.1745F);
		TuskLI5.setTextureOffset(112, 209).addBox(2.7F, 22.9F, -17.5F, 0.0F, 7.0F, 8.0F, 0.0F, false);

		TuskRI1 = new ModelRenderer(this);
		TuskRI1.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRI1, 0.6981F, 0.0F, 0.1745F);
		TuskRI1.setTextureOffset(108, 180).addBox(-4.3F, 5.5F, -24.2F, 3.0F, 3.0F, 7.0F, 0.0F, false);

		TuskRI2 = new ModelRenderer(this);
		TuskRI2.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRI2, 0.1745F, 0.0F, 0.1745F);
		TuskRI2.setTextureOffset(112, 172).addBox(-4.29F, 16.5F, -21.9F, 3.0F, 3.0F, 5.0F, 0.0F, false);

		TuskRI3 = new ModelRenderer(this);
		TuskRI3.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRI3, -0.3491F, 0.0F, 0.1745F);
		TuskRI3.setTextureOffset(110, 163).addBox(-4.3F, 24.9F, -15.5F, 3.0F, 3.0F, 6.0F, 0.0F, false);

		TuskRI4 = new ModelRenderer(this);
		TuskRI4.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRI4, 0.1745F, 0.0F, 0.1745F);
		TuskRI4.setTextureOffset(96, 163).addBox(-2.8F, 14.5F, -21.9F, 0.0F, 7.0F, 5.0F, 0.0F, false);

		TuskRI5 = new ModelRenderer(this);
		TuskRI5.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRI5, -0.3491F, 0.0F, 0.1745F);
		TuskRI5.setTextureOffset(112, 216).addBox(-2.8F, 22.9F, -17.5F, 0.0F, 7.0F, 8.0F, 0.0F, false);

		TuskLW1 = new ModelRenderer(this);
		TuskLW1.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLW1, 0.6981F, 0.0F, -0.1745F);
		TuskLW1.setTextureOffset(56, 166).addBox(1.3F, 5.5F, -24.2F, 3.0F, 3.0F, 7.0F, 0.0F, false);

		TuskLW2 = new ModelRenderer(this);
		TuskLW2.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLW2, 0.1745F, 0.0F, -0.1745F);
		TuskLW2.setTextureOffset(60, 158).addBox(1.29F, 16.5F, -21.9F, 3.0F, 3.0F, 5.0F, 0.0F, false);

		TuskLW3 = new ModelRenderer(this);
		TuskLW3.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLW3, -0.3491F, 0.0F, -0.1745F);
		TuskLW3.setTextureOffset(58, 149).addBox(1.3F, 24.9F, -15.5F, 3.0F, 3.0F, 6.0F, 0.0F, false);

		TuskLW4 = new ModelRenderer(this);
		TuskLW4.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLW4, 0.1745F, 0.0F, -0.1745F);
		TuskLW4.setTextureOffset(46, 164).addBox(2.7F, 14.5F, -21.9F, 0.0F, 7.0F, 5.0F, 0.0F, false);

		TuskLW5 = new ModelRenderer(this);
		TuskLW5.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskLW5, -0.3491F, 0.0F, -0.1745F);
		TuskLW5.setTextureOffset(52, 192).addBox(2.7F, 22.9F, -17.5F, 0.0F, 7.0F, 8.0F, 0.0F, false);

		TuskRW1 = new ModelRenderer(this);
		TuskRW1.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRW1, 0.6981F, 0.0F, 0.1745F);
		TuskRW1.setTextureOffset(56, 166).addBox(-4.3F, 5.5F, -24.2F, 3.0F, 3.0F, 7.0F, 0.0F, false);

		TuskRW2 = new ModelRenderer(this);
		TuskRW2.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRW2, 0.1745F, 0.0F, 0.1745F);
		TuskRW2.setTextureOffset(60, 158).addBox(-4.29F, 16.5F, -21.9F, 3.0F, 3.0F, 5.0F, 0.0F, false);

		TuskRW3 = new ModelRenderer(this);
		TuskRW3.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRW3, -0.3491F, 0.0F, 0.1745F);
		TuskRW3.setTextureOffset(58, 149).addBox(-4.3F, 24.9F, -15.5F, 3.0F, 3.0F, 6.0F, 0.0F, false);

		TuskRW4 = new ModelRenderer(this);
		TuskRW4.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRW4, 0.1745F, 0.0F, 0.1745F);
		TuskRW4.setTextureOffset(46, 157).addBox(-2.8F, 14.5F, -21.9F, 0.0F, 7.0F, 5.0F, 0.0F, false);

		TuskRW5 = new ModelRenderer(this);
		TuskRW5.setRotationPoint(0.0F, -10.0F, -16.5F);
		setRotationAngle(TuskRW5, -0.3491F, 0.0F, 0.1745F);
		TuskRW5.setTextureOffset(52, 199).addBox(-2.8F, 22.9F, -17.5F, 0.0F, 7.0F, 8.0F, 0.0F, false);

		FortFloor1 = new ModelRenderer(this);
		FortFloor1.setRotationPoint(0.0F, -16.0F, 10.0F);
		setRotationAngle(FortFloor1, 1.5708F, 0.0F, 1.5708F);
		FortFloor1.setTextureOffset(0, 176).addBox(-0.5F, -20.0F, -6.0F, 1.0F, 8.0F, 12.0F, 0.0F, false);

		FortFloor2 = new ModelRenderer(this);
		FortFloor2.setRotationPoint(0.0F, -16.0F, 10.0F);
		setRotationAngle(FortFloor2, 1.5708F, 0.0F, 1.5708F);
		FortFloor2.setTextureOffset(0, 176).addBox(-0.5F, -12.0F, -6.0F, 1.0F, 8.0F, 12.0F, 0.0F, false);

		FortFloor3 = new ModelRenderer(this);
		FortFloor3.setRotationPoint(0.0F, -16.0F, 10.0F);
		setRotationAngle(FortFloor3, 1.5708F, 0.0F, 1.5708F);
		FortFloor3.setTextureOffset(0, 176).addBox(-0.5F, -4.0F, -6.0F, 1.0F, 8.0F, 12.0F, 0.0F, false);

		FortBackWall = new ModelRenderer(this);
		FortBackWall.setRotationPoint(0.0F, -16.0F, 10.0F);
		setRotationAngle(FortBackWall, 0.0F, 1.5708F, 0.0F);
		FortBackWall.setTextureOffset(0, 176).addBox(-5.0F, -6.2F, -6.0F, 1.0F, 8.0F, 12.0F, 0.0F, false);

		FortBackLeftWall = new ModelRenderer(this);
		FortBackLeftWall.setRotationPoint(0.0F, -16.0F, 10.0F);
		FortBackLeftWall.setTextureOffset(0, 176).addBox(6.0F, -6.0F, -7.0F, 1.0F, 8.0F, 12.0F, 0.0F, false);

		FortBackRightWall = new ModelRenderer(this);
		FortBackRightWall.setRotationPoint(0.0F, -16.0F, 10.0F);
		FortBackRightWall.setTextureOffset(0, 176).addBox(-7.0F, -6.0F, -7.0F, 1.0F, 8.0F, 12.0F, 0.0F, false);

		StorageUpLeft = new ModelRenderer(this);
		StorageUpLeft.setRotationPoint(0.0F, -16.0F, 10.0F);
		setRotationAngle(StorageUpLeft, 0.0F, 0.0F, -0.384F);
		StorageUpLeft.setTextureOffset(76, 226).addBox(6.5F, 1.0F, -14.0F, 5.0F, 8.0F, 10.0F, 0.0F, false);

		StorageUpRight = new ModelRenderer(this);
		StorageUpRight.setRotationPoint(0.0F, -16.0F, 10.0F);
		setRotationAngle(StorageUpRight, 0.0F, 0.0F, 0.384F);
		StorageUpRight.setTextureOffset(76, 208).addBox(-11.5F, 1.0F, -14.0F, 5.0F, 8.0F, 10.0F, 0.0F, false);

		HeadMovement = new ModelRenderer(this);
		HeadMovement.setRotationPoint(0.0F, -11.0F, -12.0F);

		Head = new ModelRenderer(this);
		Head.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(Head);
		setRotationAngle(Head, -0.1745F, 0.0F, 0.0F);
		Head.setTextureOffset(60, 0).addBox(-5.5F, -6.0F, -8.0F, 11.0F, 15.0F, 10.0F, 0.0F, false);

		Neck = new ModelRenderer(this);
		Neck.setRotationPoint(0.0F, 3.0F, 2.0F);
		HeadMovement.addChild(Neck);
		setRotationAngle(Neck, -0.2618F, 0.0F, 0.0F);
		Neck.setTextureOffset(46, 48).addBox(-4.95F, -6.0F, -8.0F, 10.0F, 14.0F, 8.0F, 0.0F, false);

		HeadBump = new ModelRenderer(this);
		HeadBump.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(HeadBump);
		setRotationAngle(HeadBump, -0.1745F, 0.0F, 0.0F);
		HeadBump.setTextureOffset(104, 41).addBox(-3.0F, -9.0F, -6.0F, 6.0F, 3.0F, 6.0F, 0.0F, false);

		Chin = new ModelRenderer(this);
		Chin.setRotationPoint(0.0F, -16.0F, -11.5F);
		HeadMovement.addChild(Chin);
		setRotationAngle(Chin, 2.0541F, 0.0F, 0.0F);
		Chin.setTextureOffset(104, 41).addBox(-1.5F, -3.0F, -21.4667F, 3.0F, 5.0F, 4.0F, 0.0F, false);
		Chin.setTextureOffset(86, 56).addBox(-1.5F, -8.0F, -28.1667F, 3.0F, 5.0F, 4.0F, 0.0F, false);

		LowerLip = new ModelRenderer(this);
		LowerLip.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(LowerLip);
		setRotationAngle(LowerLip, 1.5708F, 0.0F, 0.0F);
		LowerLip.setTextureOffset(80, 65).addBox(-2.0F, -3.0F, -14.4667F, 4.0F, 2.0F, 6.0F, 0.0F, false);
		LowerLip.setTextureOffset(80, 65).addBox(-2.0F, -4.0F, -14.4667F, 4.0F, 2.0F, 6.0F, 0.0F, false);

		LeftSmallEar = new ModelRenderer(this);
		LeftSmallEar.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(LeftSmallEar);
		setRotationAngle(LeftSmallEar, -0.1745F, -0.5236F, 0.5236F);
		LeftSmallEar.setTextureOffset(102, 0).addBox(2.0F, -8.0F, -5.0F, 8.0F, 10.0F, 1.0F, 0.0F, false);

		LeftBigEar = new ModelRenderer(this);
		LeftBigEar.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(LeftBigEar);
		setRotationAngle(LeftBigEar, -0.1745F, -0.5236F, 0.5236F);
		LeftBigEar.setTextureOffset(102, 0).addBox(2.0F, -8.0F, -5.0F, 12.0F, 14.0F, 1.0F, 0.0F, false);

		RightSmallEar = new ModelRenderer(this);
		RightSmallEar.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(RightSmallEar);
		setRotationAngle(RightSmallEar, -0.1745F, 0.5236F, -0.5236F);
		RightSmallEar.setTextureOffset(106, 15).addBox(-10.0F, -8.0F, -5.0F, 8.0F, 10.0F, 1.0F, 0.0F, false);

		RightBigEar = new ModelRenderer(this);
		RightBigEar.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(RightBigEar);
		setRotationAngle(RightBigEar, -0.1745F, 0.5236F, -0.5236F);
		RightBigEar.setTextureOffset(102, 15).addBox(-14.0F, -8.0F, -5.0F, 12.0F, 14.0F, 1.0F, 0.0F, false);

		TrunkMovement = new ModelRenderer(this);
		TrunkMovement.setRotationPoint(0.0F, 11.0F, -9.0F);
		HeadMovement.addChild(TrunkMovement);

		TrunkA = new ModelRenderer(this);
		TrunkA.setRotationPoint(0.0F, -3.0F, -1.4667F);
		TrunkMovement.addChild(TrunkA);
		setRotationAngle(TrunkA, 1.5708F, 0.0F, 0.0F);
		TrunkA.setTextureOffset(0, 76).addBox(-4.0F, -2.5F, -10.0F, 8.0F, 7.0F, 10.0F, 0.0F, false);

		TrunkB = new ModelRenderer(this);
		TrunkB.setRotationPoint(0.0F, 6.5F, -1.5F);
		TrunkMovement.addChild(TrunkB);
		setRotationAngle(TrunkB, 1.6581F, 0.0F, 0.0F);
		TrunkB.setTextureOffset(0, 93).addBox(-3.0F, -2.5F, -7.0F, 6.0F, 5.0F, 7.0F, 0.0F, false);

		TrunkC = new ModelRenderer(this);
		TrunkC.setRotationPoint(0.0F, 13.0F, -1.0F);
		TrunkMovement.addChild(TrunkC);
		setRotationAngle(TrunkC, 1.9199F, 0.0F, 0.0F);
		TrunkC.setTextureOffset(0, 105).addBox(-2.5F, -2.0F, -4.0F, 5.0F, 4.0F, 5.0F, 0.0F, false);

		TrunkD = new ModelRenderer(this);
		TrunkD.setRotationPoint(0.0F, 16.0F, -0.5F);
		TrunkMovement.addChild(TrunkD);
		setRotationAngle(TrunkD, 2.2166F, 0.0F, 0.0F);
		TrunkD.setTextureOffset(0, 114).addBox(-2.0F, -1.5F, -5.0F, 4.0F, 3.0F, 5.0F, 0.0F, false);

		TrunkE = new ModelRenderer(this);
		TrunkE.setRotationPoint(0.0F, 19.5F, 2.0F);
		TrunkMovement.addChild(TrunkE);
		setRotationAngle(TrunkE, 2.5307F, 0.0F, 0.0F);
		TrunkE.setTextureOffset(0, 122).addBox(-1.5F, -1.0F, -4.0F, 3.0F, 2.0F, 4.0F, 0.0F, false);

		RightTuskA = new ModelRenderer(this);
		RightTuskA.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(RightTuskA);
		setRotationAngle(RightTuskA, 1.2217F, 0.0F, 0.1745F);
		RightTuskA.setTextureOffset(2, 60).addBox(-3.8F, -3.5F, -19.0F, 2.0F, 2.0F, 10.0F, 0.0F, false);

		RightTuskB = new ModelRenderer(this);
		RightTuskB.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(RightTuskB);
		setRotationAngle(RightTuskB, 0.6981F, 0.0F, 0.1745F);
		RightTuskB.setTextureOffset(0, 0).addBox(-3.8F, 6.2F, -24.2F, 2.0F, 2.0F, 7.0F, 0.0F, false);

		RightTuskC = new ModelRenderer(this);
		RightTuskC.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(RightTuskC);
		setRotationAngle(RightTuskC, 0.1745F, 0.0F, 0.1745F);
		RightTuskC.setTextureOffset(0, 18).addBox(-3.8F, 17.1F, -21.9F, 2.0F, 2.0F, 5.0F, 0.0F, false);

		RightTuskD = new ModelRenderer(this);
		RightTuskD.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(RightTuskD);
		setRotationAngle(RightTuskD, -0.3491F, 0.0F, 0.1745F);
		RightTuskD.setTextureOffset(14, 18).addBox(-3.8F, 25.5F, -14.5F, 2.0F, 2.0F, 5.0F, 0.0F, false);

		LeftTuskA = new ModelRenderer(this);
		LeftTuskA.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(LeftTuskA);
		setRotationAngle(LeftTuskA, 1.2217F, 0.0F, -0.1745F);
		LeftTuskA.setTextureOffset(2, 48).addBox(1.8F, -3.5F, -19.0F, 2.0F, 2.0F, 10.0F, 0.0F, false);

		LeftTuskB = new ModelRenderer(this);
		LeftTuskB.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(LeftTuskB);
		setRotationAngle(LeftTuskB, 0.6981F, 0.0F, -0.1745F);
		LeftTuskB.setTextureOffset(0, 9).addBox(1.8F, 6.2F, -24.2F, 2.0F, 2.0F, 7.0F, 0.0F, false);

		LeftTuskC = new ModelRenderer(this);
		LeftTuskC.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(LeftTuskC);
		setRotationAngle(LeftTuskC, 0.1745F, 0.0F, -0.1745F);
		LeftTuskC.setTextureOffset(0, 18).addBox(1.8F, 17.1F, -21.9F, 2.0F, 2.0F, 5.0F, 0.0F, false);

		LeftTuskD = new ModelRenderer(this);
		LeftTuskD.setRotationPoint(0.0F, 1.0F, -4.5F);
		HeadMovement.addChild(LeftTuskD);
		setRotationAngle(LeftTuskD, -0.3491F, 0.0F, -0.1745F);
		LeftTuskD.setTextureOffset(14, 18).addBox(1.8F, 25.5F, -14.5F, 2.0F, 2.0F, 5.0F, 0.0F, false);

		TailMovement = new ModelRenderer(this);
		TailMovement.setRotationPoint(0.0F, -5.0F, 15.0F);

		TailRoot = new ModelRenderer(this);
		TailRoot.setRotationPoint(0.0F, -3.0F, 0.0F);
		TailMovement.addChild(TailRoot);
		setRotationAngle(TailRoot, 0.2967F, 0.0F, 0.0F);
		TailRoot.setTextureOffset(20, 105).addBox(-1.0F, 0.0F, -2.0F, 2.0F, 10.0F, 2.0F, 0.0F, false);

		Tail = new ModelRenderer(this);
		Tail.setRotationPoint(0.0F, -3.0F, 0.0F);
		TailMovement.addChild(Tail);
		setRotationAngle(Tail, 0.1134F, 0.0F, 0.0F);
		Tail.setTextureOffset(20, 117).addBox(-1.0F, 9.7F, -0.2F, 2.0F, 6.0F, 2.0F, 0.0F, false);

		TailPlush = new ModelRenderer(this);
		TailPlush.setRotationPoint(0.0F, -3.0F, 0.0F);
		TailMovement.addChild(TailPlush);
		setRotationAngle(TailPlush, 0.1134F, 0.0F, 0.0F);
		TailPlush.setTextureOffset(26, 76).addBox(-1.5F, 15.5F, -0.7F, 3.0F, 6.0F, 3.0F, 0.0F, false);

		RFrontMovement = new ModelRenderer(this);
		RFrontMovement.setRotationPoint(-5.0F, 6.0F, -10.0F);

		FrontRightUpperLeg = new ModelRenderer(this);
		FrontRightUpperLeg.setRotationPoint(0.4F, -2.0F, 0.4F);
		RFrontMovement.addChild(FrontRightUpperLeg);
		FrontRightUpperLeg.setTextureOffset(100, 109).addBox(-3.5F, 0.0F, -3.5F, 7.0F, 12.0F, 7.0F, 0.0F, false);

		FrontRightLowerLeg = new ModelRenderer(this);
		FrontRightLowerLeg.setRotationPoint(0.4F, 8.0F, 0.4F);
		RFrontMovement.addChild(FrontRightLowerLeg);
		FrontRightLowerLeg.setTextureOffset(100, 73).addBox(-3.5F, 0.0F, -3.5F, 7.0F, 10.0F, 7.0F, 0.0F, false);

		RBackMovement = new ModelRenderer(this);
		RBackMovement.setRotationPoint(-5.0F, 6.0F, 11.0F);

		BackRightUpperLeg = new ModelRenderer(this);
		BackRightUpperLeg.setRotationPoint(0.4F, -2.0F, 0.6F);
		RBackMovement.addChild(BackRightUpperLeg);
		BackRightUpperLeg.setTextureOffset(72, 109).addBox(-3.5F, 0.0F, -3.5F, 7.0F, 12.0F, 7.0F, 0.0F, false);

		BackRightLowerLeg = new ModelRenderer(this);
		BackRightLowerLeg.setRotationPoint(0.4F, 8.0F, 0.6F);
		RBackMovement.addChild(BackRightLowerLeg);
		BackRightLowerLeg.setTextureOffset(100, 56).addBox(-3.5F, 0.0F, -3.5F, 7.0F, 10.0F, 7.0F, 0.0F, false);

		LFrontMovement = new ModelRenderer(this);
		LFrontMovement.setRotationPoint(5.0F, 6.0F, -10.0F);

		FrontLeftUpperLeg = new ModelRenderer(this);
		FrontLeftUpperLeg.setRotationPoint(-0.4F, -2.0F, 0.4F);
		LFrontMovement.addChild(FrontLeftUpperLeg);
		FrontLeftUpperLeg.setTextureOffset(100, 90).addBox(-3.5F, 0.0F, -3.5F, 7.0F, 12.0F, 7.0F, 0.0F, false);

		FrontLeftLowerLeg = new ModelRenderer(this);
		FrontLeftLowerLeg.setRotationPoint(-0.4F, 8.0F, 0.4F);
		LFrontMovement.addChild(FrontLeftLowerLeg);
		FrontLeftLowerLeg.setTextureOffset(72, 73).addBox(-3.5F, 0.0F, -3.5F, 7.0F, 10.0F, 7.0F, 0.0F, false);

		LBackMovement = new ModelRenderer(this);
		LBackMovement.setRotationPoint(5.0F, 6.0F, 11.0F);

		BackLeftUpperLeg = new ModelRenderer(this);
		BackLeftUpperLeg.setRotationPoint(-0.4F, -2.0F, 0.6F);
		LBackMovement.addChild(BackLeftUpperLeg);
		BackLeftUpperLeg.setTextureOffset(72, 90).addBox(-3.5F, 0.0F, -3.5F, 7.0F, 12.0F, 7.0F, 0.0F, false);

		BackLeftLowerLeg = new ModelRenderer(this);
		BackLeftLowerLeg.setRotationPoint(-0.4F, 8.0F, 0.6F);
		LBackMovement.addChild(BackLeftLowerLeg);
		BackLeftLowerLeg.setTextureOffset(44, 77).addBox(-3.5F, 0.0F, -3.5F, 7.0F, 10.0F, 7.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		Back.render(matrixStack, buffer, packedLight, packedOverlay);
		Hump.render(matrixStack, buffer, packedLight, packedOverlay);
		Body.render(matrixStack, buffer, packedLight, packedOverlay);
		Skirt.render(matrixStack, buffer, packedLight, packedOverlay);
		StorageRightBedroll.render(matrixStack, buffer, packedLight, packedOverlay);
		StorageLeftBedroll.render(matrixStack, buffer, packedLight, packedOverlay);
		StorageFrontRightChest.render(matrixStack, buffer, packedLight, packedOverlay);
		StorageBackRightChest.render(matrixStack, buffer, packedLight, packedOverlay);
		StorageFrontLeftChest.render(matrixStack, buffer, packedLight, packedOverlay);
		StorageBackLeftChest.render(matrixStack, buffer, packedLight, packedOverlay);
		StorageRightBlankets.render(matrixStack, buffer, packedLight, packedOverlay);
		StorageLeftBlankets.render(matrixStack, buffer, packedLight, packedOverlay);
		HarnessBlanket.render(matrixStack, buffer, packedLight, packedOverlay);
		HarnessUpperBelt.render(matrixStack, buffer, packedLight, packedOverlay);
		HarnessLowerBelt.render(matrixStack, buffer, packedLight, packedOverlay);
		CabinPillow.render(matrixStack, buffer, packedLight, packedOverlay);
		CabinLeftRail.render(matrixStack, buffer, packedLight, packedOverlay);
		Cabin.render(matrixStack, buffer, packedLight, packedOverlay);
		CabinRightRail.render(matrixStack, buffer, packedLight, packedOverlay);
		CabinBackRail.render(matrixStack, buffer, packedLight, packedOverlay);
		CabinRoof.render(matrixStack, buffer, packedLight, packedOverlay);
		FortNeckBeam.render(matrixStack, buffer, packedLight, packedOverlay);
		FortBackBeam.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLD1.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLD2.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLD3.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLD4.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLD5.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRD1.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRD2.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRD3.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRD4.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRD5.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLI1.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLI2.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLI3.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLI4.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLI5.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRI1.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRI2.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRI3.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRI4.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRI5.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLW1.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLW2.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLW3.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLW4.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskLW5.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRW1.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRW2.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRW3.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRW4.render(matrixStack, buffer, packedLight, packedOverlay);
		TuskRW5.render(matrixStack, buffer, packedLight, packedOverlay);
		FortFloor1.render(matrixStack, buffer, packedLight, packedOverlay);
		FortFloor2.render(matrixStack, buffer, packedLight, packedOverlay);
		FortFloor3.render(matrixStack, buffer, packedLight, packedOverlay);
		FortBackWall.render(matrixStack, buffer, packedLight, packedOverlay);
		FortBackLeftWall.render(matrixStack, buffer, packedLight, packedOverlay);
		FortBackRightWall.render(matrixStack, buffer, packedLight, packedOverlay);
		StorageUpLeft.render(matrixStack, buffer, packedLight, packedOverlay);
		StorageUpRight.render(matrixStack, buffer, packedLight, packedOverlay);
		HeadMovement.render(matrixStack, buffer, packedLight, packedOverlay);
		TailMovement.render(matrixStack, buffer, packedLight, packedOverlay);
		RFrontMovement.render(matrixStack, buffer, packedLight, packedOverlay);
		RBackMovement.render(matrixStack, buffer, packedLight, packedOverlay);
		LFrontMovement.render(matrixStack, buffer, packedLight, packedOverlay);
		LBackMovement.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
		this.LFrontMovement.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.LBackMovement.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.RBackMovement.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.Tail.rotateAngleY = MathHelper.cos(f * 0.6662F) * f1;
		this.HeadMovement.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.HeadMovement.rotateAngleX = f4 / (180F / (float) Math.PI);
		this.RFrontMovement.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
	}
}