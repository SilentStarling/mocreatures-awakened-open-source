// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

public static class ModelMoCModelFirefly extends EntityModel<Entity> {
	private final ModelRenderer RearLegs;
	private final ModelRenderer MidLegs;
	private final ModelRenderer Tail;
	private final ModelRenderer Abdomen;
	private final ModelRenderer FrontLegs;
	private final ModelRenderer RightShellOpen;
	private final ModelRenderer LeftShellOpen;
	private final ModelRenderer Thorax;
	private final ModelRenderer RightShell;
	private final ModelRenderer LeftShell;
	private final ModelRenderer LeftWing;
	private final ModelRenderer RightWing;
	private final ModelRenderer MovementHead;
	private final ModelRenderer Head;
	private final ModelRenderer Antenna;

	public ModelMoCModelFirefly() {
		textureWidth = 32;
		textureHeight = 32;

		RearLegs = new ModelRenderer(this);
		RearLegs.setRotationPoint(0.0F, 23.0F, -0.4F);
		setRotationAngle(RearLegs, 1.2492F, 0.0F, 0.0F);
		RearLegs.setTextureOffset(0, 9).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 3.0F, 0.0F, 0.0F, false);

		MidLegs = new ModelRenderer(this);
		MidLegs.setRotationPoint(0.0F, 23.0F, -1.2F);
		setRotationAngle(MidLegs, 1.0707F, 0.0F, 0.0F);
		MidLegs.setTextureOffset(0, 9).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);

		Tail = new ModelRenderer(this);
		Tail.setRotationPoint(0.0F, 21.3F, 1.5F);
		setRotationAngle(Tail, 1.1302F, 0.0F, 0.0F);
		Tail.setTextureOffset(8, 17).addBox(-1.0F, 0.5F, -1.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		Abdomen = new ModelRenderer(this);
		Abdomen.setRotationPoint(0.0F, 22.0F, 0.0F);
		setRotationAngle(Abdomen, 1.4277F, 0.0F, 0.0F);
		Abdomen.setTextureOffset(8, 0).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		FrontLegs = new ModelRenderer(this);
		FrontLegs.setRotationPoint(0.0F, 23.0F, -1.8F);
		setRotationAngle(FrontLegs, -0.8328F, 0.0F, 0.0F);
		FrontLegs.setTextureOffset(0, 7).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);

		RightShellOpen = new ModelRenderer(this);
		RightShellOpen.setRotationPoint(-1.0F, 21.0F, -2.0F);
		setRotationAngle(RightShellOpen, 1.22F, 0.0F, -0.6458F);
		RightShellOpen.setTextureOffset(0, 12).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 0.0F, 5.0F, 0.0F, false);

		LeftShellOpen = new ModelRenderer(this);
		LeftShellOpen.setRotationPoint(1.0F, 21.0F, -2.0F);
		setRotationAngle(LeftShellOpen, 1.22F, 0.0F, 0.6458F);
		LeftShellOpen.setTextureOffset(0, 12).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 0.0F, 5.0F, 0.0F, false);

		Thorax = new ModelRenderer(this);
		Thorax.setRotationPoint(0.0F, 21.0F, -1.0F);
		Thorax.setTextureOffset(0, 0).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		RightShell = new ModelRenderer(this);
		RightShell.setRotationPoint(-1.0F, 21.0F, -2.0F);
		setRotationAngle(RightShell, 0.0175F, 0.0F, -0.6458F);
		RightShell.setTextureOffset(0, 12).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 0.0F, 5.0F, 0.0F, false);

		LeftShell = new ModelRenderer(this);
		LeftShell.setRotationPoint(1.0F, 21.0F, -2.0F);
		setRotationAngle(LeftShell, 0.0175F, 0.0F, 0.6458F);
		LeftShell.setTextureOffset(0, 12).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 0.0F, 5.0F, 0.0F, false);

		LeftWing = new ModelRenderer(this);
		LeftWing.setRotationPoint(1.0F, 21.0F, -1.0F);
		setRotationAngle(LeftWing, 0.0F, 1.0472F, 0.0F);
		LeftWing.setTextureOffset(15, 12).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 0.0F, 5.0F, 0.0F, false);

		RightWing = new ModelRenderer(this);
		RightWing.setRotationPoint(-1.0F, 21.0F, -1.0F);
		setRotationAngle(RightWing, 0.0F, -1.0472F, 0.0F);
		RightWing.setTextureOffset(15, 12).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 0.0F, 5.0F, 0.0F, false);

		MovementHead = new ModelRenderer(this);
		MovementHead.setRotationPoint(0.0F, 22.5F, -2.0F);

		Head = new ModelRenderer(this);
		Head.setRotationPoint(0.0F, 0.0F, 0.0F);
		MovementHead.addChild(Head);
		setRotationAngle(Head, -2.1712F, 0.0F, 0.0F);
		Head.setTextureOffset(0, 4).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		Antenna = new ModelRenderer(this);
		Antenna.setRotationPoint(0.0F, 0.0F, -1.0F);
		MovementHead.addChild(Antenna);
		setRotationAngle(Antenna, -1.6656F, 0.0F, 0.0F);
		Antenna.setTextureOffset(0, 7).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 1.0F, 0.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		RearLegs.render(matrixStack, buffer, packedLight, packedOverlay);
		MidLegs.render(matrixStack, buffer, packedLight, packedOverlay);
		Tail.render(matrixStack, buffer, packedLight, packedOverlay);
		Abdomen.render(matrixStack, buffer, packedLight, packedOverlay);
		FrontLegs.render(matrixStack, buffer, packedLight, packedOverlay);
		RightShellOpen.render(matrixStack, buffer, packedLight, packedOverlay);
		LeftShellOpen.render(matrixStack, buffer, packedLight, packedOverlay);
		Thorax.render(matrixStack, buffer, packedLight, packedOverlay);
		RightShell.render(matrixStack, buffer, packedLight, packedOverlay);
		LeftShell.render(matrixStack, buffer, packedLight, packedOverlay);
		LeftWing.render(matrixStack, buffer, packedLight, packedOverlay);
		RightWing.render(matrixStack, buffer, packedLight, packedOverlay);
		MovementHead.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
		this.MovementHead.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.MovementHead.rotateAngleX = f4 / (180F / (float) Math.PI);
	}
}