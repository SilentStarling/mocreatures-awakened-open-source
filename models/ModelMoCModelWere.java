// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

public static class ModelMoCModelWere extends EntityModel<Entity> {
	private final ModelRenderer Neck2;
	private final ModelRenderer SideburnL;
	private final ModelRenderer SideburnR;
	private final ModelRenderer Chest;
	private final ModelRenderer Abdomen;
	private final ModelRenderer HeadMovement;
	private final ModelRenderer Head;
	private final ModelRenderer Nose;
	private final ModelRenderer Snout;
	private final ModelRenderer TeethU;
	private final ModelRenderer TeethL;
	private final ModelRenderer Mouth;
	private final ModelRenderer LEar;
	private final ModelRenderer REar;
	private final ModelRenderer Neck;
	private final ModelRenderer LArmMovement;
	private final ModelRenderer LArmB;
	private final ModelRenderer LArmA;
	private final ModelRenderer LArmC;
	private final ModelRenderer LFinger1;
	private final ModelRenderer LHand;
	private final ModelRenderer LFinger2;
	private final ModelRenderer LFinger3;
	private final ModelRenderer LFinger4;
	private final ModelRenderer LFinger5;
	private final ModelRenderer RArmMovement;
	private final ModelRenderer RArmB;
	private final ModelRenderer RArmC;
	private final ModelRenderer RHand;
	private final ModelRenderer RArmA;
	private final ModelRenderer RFinger1;
	private final ModelRenderer RFinger2;
	private final ModelRenderer RFinger3;
	private final ModelRenderer RFinger4;
	private final ModelRenderer RFinger5;
	private final ModelRenderer TailMovement;
	private final ModelRenderer TailA;
	private final ModelRenderer TailB;
	private final ModelRenderer TailC;
	private final ModelRenderer TailD;
	private final ModelRenderer LLegMovement;
	private final ModelRenderer LLegB;
	private final ModelRenderer LFoot;
	private final ModelRenderer LLegC;
	private final ModelRenderer LLegA;
	private final ModelRenderer RLegMovement;
	private final ModelRenderer RLegA;
	private final ModelRenderer RFoot;
	private final ModelRenderer RLegC;
	private final ModelRenderer RLegB;

	public ModelMoCModelWere() {
		textureWidth = 64;
		textureHeight = 128;

		Neck2 = new ModelRenderer(this);
		Neck2.setRotationPoint(0.0F, -1.0F, -6.0F);
		setRotationAngle(Neck2, -0.4538F, 0.0F, 0.0F);
		Neck2.setTextureOffset(0, 14).addBox(-1.5F, -2.0F, -5.0F, 3.0F, 4.0F, 7.0F, 0.0F, false);

		SideburnL = new ModelRenderer(this);
		SideburnL.setRotationPoint(0.0F, -8.0F, -6.0F);
		setRotationAngle(SideburnL, -0.2094F, 0.4189F, -0.0873F);
		SideburnL.setTextureOffset(28, 33).addBox(3.0F, 0.0F, -2.0F, 2.0F, 6.0F, 6.0F, 0.0F, false);

		SideburnR = new ModelRenderer(this);
		SideburnR.setRotationPoint(0.0F, -8.0F, -6.0F);
		setRotationAngle(SideburnR, -0.2094F, -0.4189F, 0.0873F);
		SideburnR.setTextureOffset(28, 45).addBox(-5.0F, 0.0F, -2.0F, 2.0F, 6.0F, 6.0F, 0.0F, false);

		Chest = new ModelRenderer(this);
		Chest.setRotationPoint(0.0F, -6.0F, -2.5F);
		setRotationAngle(Chest, 0.6413F, 0.0F, 0.0F);
		Chest.setTextureOffset(20, 15).addBox(-4.0F, 0.0F, -7.0F, 8.0F, 8.0F, 10.0F, 0.0F, false);

		Abdomen = new ModelRenderer(this);
		Abdomen.setRotationPoint(0.0F, 4.5F, 5.0F);
		setRotationAngle(Abdomen, 0.2695F, 0.0F, 0.0F);
		Abdomen.setTextureOffset(0, 40).addBox(-3.0F, -8.0F, -8.0F, 6.0F, 14.0F, 8.0F, 0.0F, false);

		HeadMovement = new ModelRenderer(this);
		HeadMovement.setRotationPoint(0.0F, -3.0F, -7.0F);

		Head = new ModelRenderer(this);
		Head.setRotationPoint(0.0F, 0.0F, 0.0F);
		HeadMovement.addChild(Head);
		Head.setTextureOffset(0, 0).addBox(-4.0F, -8.0F, -5.0F, 8.0F, 8.0F, 6.0F, 0.0F, false);

		Nose = new ModelRenderer(this);
		Nose.setRotationPoint(0.0F, -5.0F, 1.0F);
		HeadMovement.addChild(Nose);
		setRotationAngle(Nose, 0.2793F, 0.0F, 0.0F);
		Nose.setTextureOffset(44, 33).addBox(-1.5F, -1.7F, -12.3F, 3.0F, 2.0F, 7.0F, 0.0F, false);

		Snout = new ModelRenderer(this);
		Snout.setRotationPoint(0.0F, -5.0F, 1.0F);
		HeadMovement.addChild(Snout);
		Snout.setTextureOffset(0, 25).addBox(-2.0F, 2.0F, -12.0F, 4.0F, 2.0F, 6.0F, 0.0F, false);

		TeethU = new ModelRenderer(this);
		TeethU.setRotationPoint(0.0F, -5.0F, 1.0F);
		HeadMovement.addChild(TeethU);
		TeethU.setTextureOffset(46, 18).addBox(-2.0F, 4.01F, -12.0F, 4.0F, 2.0F, 5.0F, 0.0F, false);

		TeethL = new ModelRenderer(this);
		TeethL.setRotationPoint(0.0F, -5.0F, 1.0F);
		HeadMovement.addChild(TeethL);
		setRotationAngle(TeethL, 2.5307F, 0.0F, 0.0F);
		TeethL.setTextureOffset(20, 109).addBox(-1.5F, -12.5F, 2.01F, 3.0F, 5.0F, 2.0F, 0.0F, false);

		Mouth = new ModelRenderer(this);
		Mouth.setRotationPoint(0.0F, -5.0F, 1.0F);
		HeadMovement.addChild(Mouth);
		setRotationAngle(Mouth, 2.5307F, 0.0F, 0.0F);
		Mouth.setTextureOffset(42, 69).addBox(-1.5F, -12.5F, 0.0F, 3.0F, 9.0F, 2.0F, 0.0F, false);

		LEar = new ModelRenderer(this);
		LEar.setRotationPoint(0.0F, -5.0F, 1.0F);
		HeadMovement.addChild(LEar);
		setRotationAngle(LEar, 0.0F, 0.0F, 0.1745F);
		LEar.setTextureOffset(13, 14).addBox(0.5F, -7.5F, -1.0F, 3.0F, 5.0F, 1.0F, 0.0F, false);

		REar = new ModelRenderer(this);
		REar.setRotationPoint(0.0F, -5.0F, 1.0F);
		HeadMovement.addChild(REar);
		setRotationAngle(REar, 0.0F, 0.0F, -0.1745F);
		REar.setTextureOffset(22, 0).addBox(-3.5F, -7.5F, -1.0F, 3.0F, 5.0F, 1.0F, 0.0F, false);

		Neck = new ModelRenderer(this);
		Neck.setRotationPoint(0.0F, -2.0F, 5.0F);
		HeadMovement.addChild(Neck);
		setRotationAngle(Neck, -0.6025F, 0.0F, 0.0F);
		Neck.setTextureOffset(28, 0).addBox(-3.5F, -3.0F, -7.0F, 7.0F, 8.0F, 7.0F, 0.0F, false);

		LArmMovement = new ModelRenderer(this);
		LArmMovement.setRotationPoint(6.0F, -3.0F, -1.0F);

		LArmB = new ModelRenderer(this);
		LArmB.setRotationPoint(-2.0F, -1.0F, -1.0F);
		LArmMovement.addChild(LArmB);
		setRotationAngle(LArmB, 0.2618F, 0.0F, -0.3491F);
		LArmB.setTextureOffset(48, 89).addBox(-0.5F, 1.0F, -1.5F, 4.0F, 8.0F, 4.0F, 0.0F, false);

		LArmA = new ModelRenderer(this);
		LArmA.setRotationPoint(-2.0F, -1.0F, -1.0F);
		LArmMovement.addChild(LArmA);
		setRotationAngle(LArmA, 0.632F, 0.0F, 0.0F);
		LArmA.setTextureOffset(0, 98).addBox(0.0F, -3.0F, -2.0F, 5.0F, 5.0F, 5.0F, 0.0F, false);

		LArmC = new ModelRenderer(this);
		LArmC.setRotationPoint(-2.0F, -1.0F, -1.0F);
		LArmMovement.addChild(LArmC);
		setRotationAngle(LArmC, -0.3491F, 0.0F, 0.0F);
		LArmC.setTextureOffset(48, 101).addBox(2.0F, 5.0F, 3.0F, 4.0F, 7.0F, 4.0F, 0.0F, false);

		LFinger1 = new ModelRenderer(this);
		LFinger1.setRotationPoint(-2.0F, -1.0F, -1.0F);
		LArmMovement.addChild(LFinger1);
		LFinger1.setTextureOffset(8, 124).addBox(2.0F, 15.5F, 1.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		LHand = new ModelRenderer(this);
		LHand.setRotationPoint(-2.0F, -1.0F, -1.0F);
		LArmMovement.addChild(LHand);
		LHand.setTextureOffset(32, 111).addBox(2.0F, 12.5F, -1.5F, 4.0F, 3.0F, 4.0F, 0.0F, false);

		LFinger2 = new ModelRenderer(this);
		LFinger2.setRotationPoint(-2.0F, -1.0F, -1.0F);
		LArmMovement.addChild(LFinger2);
		LFinger2.setTextureOffset(0, 124).addBox(2.5F, 15.5F, -1.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		LFinger3 = new ModelRenderer(this);
		LFinger3.setRotationPoint(-2.0F, -1.0F, -1.0F);
		LArmMovement.addChild(LFinger3);
		LFinger3.setTextureOffset(0, 119).addBox(3.8F, 15.5F, -1.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);

		LFinger4 = new ModelRenderer(this);
		LFinger4.setRotationPoint(-2.0F, -1.0F, -1.0F);
		LArmMovement.addChild(LFinger4);
		LFinger4.setTextureOffset(4, 119).addBox(5.0F, 15.5F, -0.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);

		LFinger5 = new ModelRenderer(this);
		LFinger5.setRotationPoint(-2.0F, -1.0F, -1.0F);
		LArmMovement.addChild(LFinger5);
		LFinger5.setTextureOffset(4, 124).addBox(5.0F, 15.5F, 1.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		RArmMovement = new ModelRenderer(this);
		RArmMovement.setRotationPoint(-7.0F, -3.0F, 0.0F);

		RArmB = new ModelRenderer(this);
		RArmB.setRotationPoint(3.0F, -1.0F, -2.0F);
		RArmMovement.addChild(RArmB);
		setRotationAngle(RArmB, 0.2618F, 0.0F, 0.3491F);
		RArmB.setTextureOffset(48, 77).addBox(-3.5F, 1.0F, -1.5F, 4.0F, 8.0F, 4.0F, 0.0F, false);

		RArmC = new ModelRenderer(this);
		RArmC.setRotationPoint(3.0F, -1.0F, -2.0F);
		RArmMovement.addChild(RArmC);
		setRotationAngle(RArmC, -0.3491F, 0.0F, 0.0F);
		RArmC.setTextureOffset(48, 112).addBox(-6.0F, 5.0F, 3.0F, 4.0F, 7.0F, 4.0F, 0.0F, false);

		RHand = new ModelRenderer(this);
		RHand.setRotationPoint(3.0F, -1.0F, -2.0F);
		RArmMovement.addChild(RHand);
		RHand.setTextureOffset(32, 118).addBox(-6.0F, 12.5F, -1.5F, 4.0F, 3.0F, 4.0F, 0.0F, false);

		RArmA = new ModelRenderer(this);
		RArmA.setRotationPoint(3.0F, -1.0F, -2.0F);
		RArmMovement.addChild(RArmA);
		setRotationAngle(RArmA, 0.632F, 0.0F, 0.0F);
		RArmA.setTextureOffset(0, 108).addBox(-5.0F, -3.0F, -2.0F, 5.0F, 5.0F, 5.0F, 0.0F, false);

		RFinger1 = new ModelRenderer(this);
		RFinger1.setRotationPoint(3.0F, -1.0F, -2.0F);
		RArmMovement.addChild(RFinger1);
		RFinger1.setTextureOffset(8, 120).addBox(-3.5F, 15.0F, 1.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		RFinger2 = new ModelRenderer(this);
		RFinger2.setRotationPoint(3.0F, -1.0F, -2.0F);
		RArmMovement.addChild(RFinger2);
		RFinger2.setTextureOffset(12, 124).addBox(-3.5F, 15.5F, -1.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		RFinger3 = new ModelRenderer(this);
		RFinger3.setRotationPoint(3.0F, -1.0F, -2.0F);
		RArmMovement.addChild(RFinger3);
		RFinger3.setTextureOffset(12, 119).addBox(-4.8F, 15.5F, -1.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);

		RFinger4 = new ModelRenderer(this);
		RFinger4.setRotationPoint(3.0F, -1.0F, -2.0F);
		RArmMovement.addChild(RFinger4);
		RFinger4.setTextureOffset(16, 119).addBox(-6.0F, 15.5F, -0.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);

		RFinger5 = new ModelRenderer(this);
		RFinger5.setRotationPoint(3.0F, -1.0F, -2.0F);
		RArmMovement.addChild(RFinger5);
		RFinger5.setTextureOffset(16, 124).addBox(-6.0F, 15.5F, 1.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		TailMovement = new ModelRenderer(this);
		TailMovement.setRotationPoint(0.0F, 10.0F, 5.0F);

		TailA = new ModelRenderer(this);
		TailA.setRotationPoint(0.0F, -0.5F, 1.0F);
		TailMovement.addChild(TailA);
		setRotationAngle(TailA, 1.0647F, 0.0F, 0.0F);
		TailA.setTextureOffset(52, 42).addBox(-1.5F, -1.0F, -2.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);

		TailB = new ModelRenderer(this);
		TailB.setRotationPoint(0.0F, -0.5F, 1.0F);
		TailMovement.addChild(TailB);
		setRotationAngle(TailB, 0.7505F, 0.0F, 0.0F);
		TailB.setTextureOffset(48, 49).addBox(-2.0F, 2.0F, -2.0F, 4.0F, 6.0F, 4.0F, 0.0F, false);

		TailC = new ModelRenderer(this);
		TailC.setRotationPoint(0.0F, -0.5F, 1.0F);
		TailMovement.addChild(TailC);
		setRotationAngle(TailC, 1.0996F, 0.0F, 0.0F);
		TailC.setTextureOffset(48, 59).addBox(-2.0F, 6.8F, -4.6F, 4.0F, 6.0F, 4.0F, 0.0F, false);

		TailD = new ModelRenderer(this);
		TailD.setRotationPoint(0.0F, -0.5F, 1.0F);
		TailMovement.addChild(TailD);
		setRotationAngle(TailD, 1.0996F, 0.0F, 0.0F);
		TailD.setTextureOffset(52, 69).addBox(-1.5F, 9.8F, -4.1F, 3.0F, 5.0F, 3.0F, 0.0F, false);

		LLegMovement = new ModelRenderer(this);
		LLegMovement.setRotationPoint(4.0F, 11.0F, 0.0F);

		LLegB = new ModelRenderer(this);
		LLegB.setRotationPoint(-1.0F, -1.5F, 3.0F);
		LLegMovement.addChild(LLegB);
		setRotationAngle(LLegB, -0.8446F, 0.0F, 0.0F);
		LLegB.setTextureOffset(0, 76).addBox(-0.1F, 4.2F, 0.5F, 2.0F, 2.0F, 5.0F, 0.0F, false);

		LFoot = new ModelRenderer(this);
		LFoot.setRotationPoint(-1.0F, -1.5F, 3.0F);
		LLegMovement.addChild(LFoot);
		LFoot.setTextureOffset(0, 93).addBox(-0.5067F, 12.5F, -5.0F, 3.0F, 2.0F, 3.0F, 0.0F, false);

		LLegC = new ModelRenderer(this);
		LLegC.setRotationPoint(-1.0F, -1.5F, 3.0F);
		LLegMovement.addChild(LLegC);
		setRotationAngle(LLegC, -0.2861F, 0.0F, 0.0F);
		LLegC.setTextureOffset(0, 83).addBox(0.0F, 6.2F, 0.5F, 2.0F, 8.0F, 2.0F, 0.0F, false);

		LLegA = new ModelRenderer(this);
		LLegA.setRotationPoint(-1.0F, -1.5F, 3.0F);
		LLegMovement.addChild(LLegA);
		setRotationAngle(LLegA, -0.8127F, 0.0F, 0.0F);
		LLegA.setTextureOffset(0, 64).addBox(-0.5F, -1.5F, -3.5F, 3.0F, 8.0F, 5.0F, 0.0F, false);

		RLegMovement = new ModelRenderer(this);
		RLegMovement.setRotationPoint(-4.0F, 11.0F, 0.0F);

		RLegA = new ModelRenderer(this);
		RLegA.setRotationPoint(1.0F, -1.5F, 3.0F);
		RLegMovement.addChild(RLegA);
		setRotationAngle(RLegA, -0.8127F, 0.0F, 0.0F);
		RLegA.setTextureOffset(12, 64).addBox(-2.5F, -1.5F, -3.5F, 3.0F, 8.0F, 5.0F, 0.0F, false);

		RFoot = new ModelRenderer(this);
		RFoot.setRotationPoint(1.0F, -1.5F, 3.0F);
		RLegMovement.addChild(RFoot);
		RFoot.setTextureOffset(14, 93).addBox(-2.5067F, 12.5F, -5.0F, 3.0F, 2.0F, 3.0F, 0.0F, false);

		RLegC = new ModelRenderer(this);
		RLegC.setRotationPoint(1.0F, -1.5F, 3.0F);
		RLegMovement.addChild(RLegC);
		setRotationAngle(RLegC, -0.2861F, 0.0F, 0.0F);
		RLegC.setTextureOffset(14, 83).addBox(-2.0F, 6.2F, 0.5F, 2.0F, 8.0F, 2.0F, 0.0F, false);

		RLegB = new ModelRenderer(this);
		RLegB.setRotationPoint(1.0F, -1.5F, 3.0F);
		RLegMovement.addChild(RLegB);
		setRotationAngle(RLegB, -0.8446F, 0.0F, 0.0F);
		RLegB.setTextureOffset(14, 76).addBox(-1.9F, 4.2F, 0.5F, 2.0F, 2.0F, 5.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		Neck2.render(matrixStack, buffer, packedLight, packedOverlay);
		SideburnL.render(matrixStack, buffer, packedLight, packedOverlay);
		SideburnR.render(matrixStack, buffer, packedLight, packedOverlay);
		Chest.render(matrixStack, buffer, packedLight, packedOverlay);
		Abdomen.render(matrixStack, buffer, packedLight, packedOverlay);
		HeadMovement.render(matrixStack, buffer, packedLight, packedOverlay);
		LArmMovement.render(matrixStack, buffer, packedLight, packedOverlay);
		RArmMovement.render(matrixStack, buffer, packedLight, packedOverlay);
		TailMovement.render(matrixStack, buffer, packedLight, packedOverlay);
		LLegMovement.render(matrixStack, buffer, packedLight, packedOverlay);
		RLegMovement.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
		this.RArmMovement.rotateAngleX = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
		this.LLegMovement.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.HeadMovement.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.HeadMovement.rotateAngleX = f4 / (180F / (float) Math.PI);
		this.TailMovement.rotateAngleZ = MathHelper.cos(f * 0.6662F) * f1;
		this.RLegMovement.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.LArmMovement.rotateAngleX = MathHelper.cos(f * 0.6662F) * f1;
	}
}