// Made with Blockbench 4.2.4
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports

public static class ModelScorpion extends EntityModel<Entity> {
	private final ModelRenderer Body;
	private final ModelRenderer cube_r1;
	private final ModelRenderer Metasoma;
	private final ModelRenderer Metasoma2;
	private final ModelRenderer Metasoma3;
	private final ModelRenderer Metasoma4;
	private final ModelRenderer Metasoma5;
	private final ModelRenderer Sting;
	private final ModelRenderer cube_r2;
	private final ModelRenderer RightChelicerae;
	private final ModelRenderer LeftChelicerae;
	private final ModelRenderer RightArm;
	private final ModelRenderer cube_r3;
	private final ModelRenderer RightPedipalp;
	private final ModelRenderer RightPedipalp2;
	private final ModelRenderer RightPedipalp3;
	private final ModelRenderer LeftArm;
	private final ModelRenderer cube_r4;
	private final ModelRenderer LeftPedipalp;
	private final ModelRenderer LeftPedipalp2;
	private final ModelRenderer LeftPedipalp3;
	private final ModelRenderer RightFLeg;
	private final ModelRenderer cube_r5;
	private final ModelRenderer cube_r6;
	private final ModelRenderer cube_r7;
	private final ModelRenderer RightFMLeg;
	private final ModelRenderer cube_r8;
	private final ModelRenderer cube_r9;
	private final ModelRenderer cube_r10;
	private final ModelRenderer RightBMLeg;
	private final ModelRenderer cube_r11;
	private final ModelRenderer cube_r12;
	private final ModelRenderer cube_r13;
	private final ModelRenderer RightBLeg;
	private final ModelRenderer cube_r14;
	private final ModelRenderer cube_r15;
	private final ModelRenderer cube_r16;
	private final ModelRenderer LeftFLeg;
	private final ModelRenderer cube_r17;
	private final ModelRenderer cube_r18;
	private final ModelRenderer cube_r19;
	private final ModelRenderer LeftFMLeg;
	private final ModelRenderer cube_r20;
	private final ModelRenderer cube_r21;
	private final ModelRenderer cube_r22;
	private final ModelRenderer LeftBMLeg;
	private final ModelRenderer cube_r23;
	private final ModelRenderer cube_r24;
	private final ModelRenderer cube_r25;
	private final ModelRenderer LeftBLeg;
	private final ModelRenderer cube_r26;
	private final ModelRenderer cube_r27;
	private final ModelRenderer cube_r28;
	private final ModelRenderer Babies;
	private final ModelRenderer Babie1;
	private final ModelRenderer Babie2;
	private final ModelRenderer Babie3;
	private final ModelRenderer Babie4;
	private final ModelRenderer Babie5;

	public ModelScorpion() {
		textureWidth = 64;
		textureHeight = 64;

		Body = new ModelRenderer(this);
		Body.setRotationPoint(0.0F, 18.0F, 0.0F);
		Body.setTextureOffset(0, 0).addBox(-5.0F, -5.0F, -6.5F, 10.0F, 5.0F, 13.0F, 0.0F, false);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(0.0F, -2.0F, 5.5F);
		Body.addChild(cube_r1);
		setRotationAngle(cube_r1, 0.0873F, 0.0F, 0.0F);
		cube_r1.setTextureOffset(0, 18).addBox(-4.0F, -2.0F, 0.0F, 8.0F, 4.0F, 10.0F, 0.0F, false);

		Metasoma = new ModelRenderer(this);
		Metasoma.setRotationPoint(0.0F, -3.0F, 14.0F);
		Body.addChild(Metasoma);
		setRotationAngle(Metasoma, 0.6109F, 0.0F, 0.0F);
		Metasoma.setTextureOffset(0, 32).addBox(-3.0F, -1.7132F, 0.4096F, 6.0F, 4.0F, 6.0F, 0.0F, false);

		Metasoma2 = new ModelRenderer(this);
		Metasoma2.setRotationPoint(0.0F, 0.0F, 5.75F);
		Metasoma.addChild(Metasoma2);
		setRotationAngle(Metasoma2, 0.5236F, 0.0F, 0.0F);
		Metasoma2.setTextureOffset(0, 42).addBox(-2.0F, -1.5468F, -0.2887F, 4.0F, 4.0F, 6.0F, 0.0F, false);

		Metasoma3 = new ModelRenderer(this);
		Metasoma3.setRotationPoint(0.0F, -2.0F, 5.5F);
		Metasoma2.addChild(Metasoma3);
		setRotationAngle(Metasoma3, 0.4363F, 0.0F, 0.0F);
		Metasoma3.setTextureOffset(0, 52).addBox(-1.5F, 0.275F, -1.25F, 3.0F, 3.0F, 6.0F, 0.0F, false);

		Metasoma4 = new ModelRenderer(this);
		Metasoma4.setRotationPoint(0.0F, 0.75F, 3.75F);
		Metasoma3.addChild(Metasoma4);
		setRotationAngle(Metasoma4, 0.9163F, 0.0F, 0.0F);
		Metasoma4.setTextureOffset(24, 32).addBox(-1.5F, -1.1956F, -0.3967F, 3.0F, 3.0F, 6.0F, -0.001F, false);

		Metasoma5 = new ModelRenderer(this);
		Metasoma5.setRotationPoint(0.0F, -1.0F, 5.0F);
		Metasoma4.addChild(Metasoma5);
		setRotationAngle(Metasoma5, 0.5672F, 0.0F, 0.0F);
		Metasoma5.setTextureOffset(24, 41).addBox(-1.5F, -0.4564F, -0.9981F, 3.0F, 3.0F, 6.0F, 0.0F, false);

		Sting = new ModelRenderer(this);
		Sting.setRotationPoint(0.0F, 1.0F, 7.0F);
		Metasoma5.addChild(Sting);
		setRotationAngle(Sting, 0.48F, 0.0F, 0.0F);
		Sting.setTextureOffset(30, 50).addBox(-1.5F, -5.1913F, -2.4619F, 3.0F, 5.0F, 3.0F, -0.001F, false);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(0.0F, -4.5F, -1.5F);
		Sting.addChild(cube_r2);
		setRotationAngle(cube_r2, -0.6109F, 0.0F, 0.0F);
		cube_r2.setTextureOffset(26, 50).addBox(-0.5F, -3.8918F, -0.9881F, 1.0F, 4.0F, 1.0F, 0.0F, false);

		RightChelicerae = new ModelRenderer(this);
		RightChelicerae.setRotationPoint(3.0F, -2.0F, -6.5F);
		Body.addChild(RightChelicerae);
		setRotationAngle(RightChelicerae, 0.0F, -0.3927F, 0.0F);
		RightChelicerae.setTextureOffset(18, 58).addBox(-3.0F, -2.0F, -1.0F, 4.0F, 4.0F, 2.0F, -0.001F, false);

		LeftChelicerae = new ModelRenderer(this);
		LeftChelicerae.setRotationPoint(-3.0F, -2.0F, -6.5F);
		Body.addChild(LeftChelicerae);
		setRotationAngle(LeftChelicerae, 0.0F, 0.3927F, 0.0F);
		LeftChelicerae.setTextureOffset(30, 58).addBox(-1.0F, -2.0F, -1.0F, 4.0F, 4.0F, 2.0F, -0.001F, false);

		RightArm = new ModelRenderer(this);
		RightArm.setRotationPoint(5.0F, -1.0F, -5.5F);
		Body.addChild(RightArm);
		setRotationAngle(RightArm, 0.0F, -0.3491F, -0.6981F);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(0.0F, 0.0F, 0.0F);
		RightArm.addChild(cube_r3);
		setRotationAngle(cube_r3, 0.0F, 0.0F, 1.5708F);
		cube_r3.setTextureOffset(26, 18).addBox(-1.0F, -7.0F, -1.0F, 2.0F, 7.0F, 2.0F, 0.0F, false);

		RightPedipalp = new ModelRenderer(this);
		RightPedipalp.setRotationPoint(7.0F, 0.0F, 0.0F);
		RightArm.addChild(RightPedipalp);
		setRotationAngle(RightPedipalp, 0.2618F, 0.0873F, 0.3491F);
		RightPedipalp.setTextureOffset(42, 55).addBox(-1.75F, -1.25F, -6.5F, 3.0F, 3.0F, 6.0F, 0.0F, false);

		RightPedipalp2 = new ModelRenderer(this);
		RightPedipalp2.setRotationPoint(-0.5F, 0.75F, -6.0F);
		RightPedipalp.addChild(RightPedipalp2);
		setRotationAngle(RightPedipalp2, 0.2618F, 0.4363F, 0.0873F);
		RightPedipalp2.setTextureOffset(42, 39).addBox(-0.5F, -0.5F, -7.0F, 2.0F, 1.0F, 7.0F, 0.0F, false);

		RightPedipalp3 = new ModelRenderer(this);
		RightPedipalp3.setRotationPoint(-1.5F, 0.5F, -5.25F);
		RightPedipalp.addChild(RightPedipalp3);
		setRotationAngle(RightPedipalp3, 0.2618F, 0.288F, 0.0873F);
		RightPedipalp3.setTextureOffset(42, 31).addBox(-1.25F, -0.5F, -6.25F, 1.0F, 1.0F, 7.0F, 0.0F, false);

		LeftArm = new ModelRenderer(this);
		LeftArm.setRotationPoint(-5.0F, -1.0F, -5.5F);
		Body.addChild(LeftArm);
		setRotationAngle(LeftArm, 0.0F, 0.3491F, 0.6981F);

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(0.0F, 0.0F, 0.0F);
		LeftArm.addChild(cube_r4);
		setRotationAngle(cube_r4, 0.0F, 0.0F, -1.5708F);
		cube_r4.setTextureOffset(26, 18).addBox(-1.0F, -7.0F, -1.0F, 2.0F, 7.0F, 2.0F, 0.0F, true);

		LeftPedipalp = new ModelRenderer(this);
		LeftPedipalp.setRotationPoint(-7.0F, 0.0F, 0.0F);
		LeftArm.addChild(LeftPedipalp);
		setRotationAngle(LeftPedipalp, 0.2618F, -0.0873F, -0.3491F);
		LeftPedipalp.setTextureOffset(42, 55).addBox(-1.25F, -1.25F, -6.5F, 3.0F, 3.0F, 6.0F, 0.0F, true);

		LeftPedipalp2 = new ModelRenderer(this);
		LeftPedipalp2.setRotationPoint(0.5F, 0.75F, -6.0F);
		LeftPedipalp.addChild(LeftPedipalp2);
		setRotationAngle(LeftPedipalp2, 0.2618F, -0.4363F, -0.0873F);
		LeftPedipalp2.setTextureOffset(42, 39).addBox(-1.5F, -0.5F, -7.0F, 2.0F, 1.0F, 7.0F, 0.0F, true);

		LeftPedipalp3 = new ModelRenderer(this);
		LeftPedipalp3.setRotationPoint(1.5F, 0.5F, -5.25F);
		LeftPedipalp.addChild(LeftPedipalp3);
		setRotationAngle(LeftPedipalp3, 0.2618F, -0.288F, -0.0873F);
		LeftPedipalp3.setTextureOffset(42, 31).addBox(0.25F, -0.5F, -6.25F, 1.0F, 1.0F, 7.0F, 0.0F, true);

		RightFLeg = new ModelRenderer(this);
		RightFLeg.setRotationPoint(5.0F, -1.0F, -2.5F);
		Body.addChild(RightFLeg);
		setRotationAngle(RightFLeg, 0.0F, -0.1745F, -0.2618F);

		cube_r5 = new ModelRenderer(this);
		cube_r5.setRotationPoint(7.75F, 5.0F, 0.0F);
		RightFLeg.addChild(cube_r5);
		setRotationAngle(cube_r5, 0.0F, -0.0436F, 1.5708F);
		cube_r5.setTextureOffset(52, 16).addBox(-0.25F, -0.8F, -0.925F, 5.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r6 = new ModelRenderer(this);
		cube_r6.setRotationPoint(7.0F, 0.25F, 0.0F);
		RightFLeg.addChild(cube_r6);
		setRotationAngle(cube_r6, 0.0F, 0.0873F, 1.3963F);
		cube_r6.setTextureOffset(50, 0).addBox(0.0F, -1.25F, -1.05F, 5.0F, 2.0F, 2.0F, 0.0F, false);

		cube_r7 = new ModelRenderer(this);
		cube_r7.setRotationPoint(3.5F, 0.0F, 0.0F);
		RightFLeg.addChild(cube_r7);
		setRotationAngle(cube_r7, 0.0F, 0.0F, 1.5708F);
		cube_r7.setTextureOffset(38, 0).addBox(-1.0F, -3.5F, -1.0F, 2.0F, 7.0F, 2.0F, 0.0F, false);

		RightFMLeg = new ModelRenderer(this);
		RightFMLeg.setRotationPoint(5.0F, -1.0F, 0.5F);
		Body.addChild(RightFMLeg);
		setRotationAngle(RightFMLeg, 0.0F, -0.5236F, -0.3491F);

		cube_r8 = new ModelRenderer(this);
		cube_r8.setRotationPoint(7.75F, 5.0F, 0.0F);
		RightFMLeg.addChild(cube_r8);
		setRotationAngle(cube_r8, 0.0F, -0.0436F, 1.5708F);
		cube_r8.setTextureOffset(50, 18).addBox(-1.05F, -0.825F, -0.95F, 6.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r9 = new ModelRenderer(this);
		cube_r9.setRotationPoint(7.0F, 0.25F, 0.0F);
		RightFMLeg.addChild(cube_r9);
		setRotationAngle(cube_r9, 0.0F, 0.0873F, 1.3963F);
		cube_r9.setTextureOffset(50, 4).addBox(-0.3F, -1.15F, -1.05F, 5.0F, 2.0F, 2.0F, 0.0F, false);

		cube_r10 = new ModelRenderer(this);
		cube_r10.setRotationPoint(3.5F, 0.0F, 0.0F);
		RightFMLeg.addChild(cube_r10);
		setRotationAngle(cube_r10, 0.0F, 0.0F, 1.5708F);
		cube_r10.setTextureOffset(38, 0).addBox(-1.0F, -3.5F, -1.0F, 2.0F, 7.0F, 2.0F, 0.0F, false);

		RightBMLeg = new ModelRenderer(this);
		RightBMLeg.setRotationPoint(5.0F, -1.5F, 3.5F);
		Body.addChild(RightBMLeg);
		setRotationAngle(RightBMLeg, 0.0F, -0.7854F, -0.3491F);

		cube_r11 = new ModelRenderer(this);
		cube_r11.setRotationPoint(7.75F, 5.0F, 0.0F);
		RightBMLeg.addChild(cube_r11);
		setRotationAngle(cube_r11, 0.0F, -0.0436F, 1.5708F);
		cube_r11.setTextureOffset(50, 20).addBox(-1.05F, -0.525F, -1.35F, 6.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r12 = new ModelRenderer(this);
		cube_r12.setRotationPoint(7.0F, 0.25F, 0.0F);
		RightBMLeg.addChild(cube_r12);
		setRotationAngle(cube_r12, 0.0F, 0.1309F, 1.4399F);
		cube_r12.setTextureOffset(48, 8).addBox(-0.1F, -1.1F, -1.05F, 6.0F, 2.0F, 2.0F, 0.0F, false);

		cube_r13 = new ModelRenderer(this);
		cube_r13.setRotationPoint(3.5F, 0.0F, 0.0F);
		RightBMLeg.addChild(cube_r13);
		setRotationAngle(cube_r13, 0.0F, 0.0F, 1.5708F);
		cube_r13.setTextureOffset(38, 0).addBox(-1.0F, -3.5F, -1.0F, 2.0F, 7.0F, 2.0F, 0.0F, false);

		RightBLeg = new ModelRenderer(this);
		RightBLeg.setRotationPoint(5.0F, -2.0F, 6.5F);
		Body.addChild(RightBLeg);
		setRotationAngle(RightBLeg, 0.0F, -1.1345F, -0.2618F);

		cube_r14 = new ModelRenderer(this);
		cube_r14.setRotationPoint(7.75F, 5.0F, 0.0F);
		RightBLeg.addChild(cube_r14);
		setRotationAngle(cube_r14, 0.0F, 0.0436F, 1.4835F);
		cube_r14.setTextureOffset(48, 22).addBox(-1.5F, -0.875F, -1.575F, 7.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r15 = new ModelRenderer(this);
		cube_r15.setRotationPoint(7.0F, 0.25F, 0.0F);
		RightBLeg.addChild(cube_r15);
		setRotationAngle(cube_r15, 0.0F, 0.1745F, 1.4399F);
		cube_r15.setTextureOffset(46, 12).addBox(-0.325F, -1.575F, -1.075F, 7.0F, 2.0F, 2.0F, 0.0F, false);

		cube_r16 = new ModelRenderer(this);
		cube_r16.setRotationPoint(3.5F, 0.0F, 0.0F);
		RightBLeg.addChild(cube_r16);
		setRotationAngle(cube_r16, 0.0F, 0.0F, 1.5708F);
		cube_r16.setTextureOffset(38, 0).addBox(-1.0F, -3.5F, -1.0F, 2.0F, 7.0F, 2.0F, 0.0F, false);

		LeftFLeg = new ModelRenderer(this);
		LeftFLeg.setRotationPoint(-5.0F, -1.0F, -2.5F);
		Body.addChild(LeftFLeg);
		setRotationAngle(LeftFLeg, 0.0F, 0.1745F, 0.2618F);

		cube_r17 = new ModelRenderer(this);
		cube_r17.setRotationPoint(-7.75F, 5.0F, 0.0F);
		LeftFLeg.addChild(cube_r17);
		setRotationAngle(cube_r17, 0.0F, 0.0436F, -1.5708F);
		cube_r17.setTextureOffset(52, 16).addBox(-4.75F, -0.8F, -0.925F, 5.0F, 1.0F, 1.0F, 0.0F, true);

		cube_r18 = new ModelRenderer(this);
		cube_r18.setRotationPoint(-7.0F, 0.25F, 0.0F);
		LeftFLeg.addChild(cube_r18);
		setRotationAngle(cube_r18, 0.0F, -0.0873F, -1.3963F);
		cube_r18.setTextureOffset(50, 0).addBox(-5.0F, -1.25F, -1.05F, 5.0F, 2.0F, 2.0F, 0.0F, true);

		cube_r19 = new ModelRenderer(this);
		cube_r19.setRotationPoint(-3.5F, 0.0F, 0.0F);
		LeftFLeg.addChild(cube_r19);
		setRotationAngle(cube_r19, 0.0F, 0.0F, -1.5708F);
		cube_r19.setTextureOffset(38, 0).addBox(-1.0F, -3.5F, -1.0F, 2.0F, 7.0F, 2.0F, 0.0F, true);

		LeftFMLeg = new ModelRenderer(this);
		LeftFMLeg.setRotationPoint(-5.0F, -1.0F, 0.5F);
		Body.addChild(LeftFMLeg);
		setRotationAngle(LeftFMLeg, 0.0F, 0.5236F, 0.3491F);

		cube_r20 = new ModelRenderer(this);
		cube_r20.setRotationPoint(-7.75F, 5.0F, 0.0F);
		LeftFMLeg.addChild(cube_r20);
		setRotationAngle(cube_r20, 0.0F, 0.0436F, -1.5708F);
		cube_r20.setTextureOffset(50, 18).addBox(-4.95F, -0.825F, -0.95F, 6.0F, 1.0F, 1.0F, 0.0F, true);

		cube_r21 = new ModelRenderer(this);
		cube_r21.setRotationPoint(-7.0F, 0.25F, 0.0F);
		LeftFMLeg.addChild(cube_r21);
		setRotationAngle(cube_r21, 0.0F, -0.0873F, -1.3963F);
		cube_r21.setTextureOffset(50, 4).addBox(-4.7F, -1.15F, -1.05F, 5.0F, 2.0F, 2.0F, 0.0F, true);

		cube_r22 = new ModelRenderer(this);
		cube_r22.setRotationPoint(-3.5F, 0.0F, 0.0F);
		LeftFMLeg.addChild(cube_r22);
		setRotationAngle(cube_r22, 0.0F, 0.0F, -1.5708F);
		cube_r22.setTextureOffset(38, 0).addBox(-1.0F, -3.5F, -1.0F, 2.0F, 7.0F, 2.0F, 0.0F, true);

		LeftBMLeg = new ModelRenderer(this);
		LeftBMLeg.setRotationPoint(-5.0F, -1.5F, 3.5F);
		Body.addChild(LeftBMLeg);
		setRotationAngle(LeftBMLeg, 0.0F, 0.7854F, 0.3491F);

		cube_r23 = new ModelRenderer(this);
		cube_r23.setRotationPoint(-7.75F, 5.0F, 0.0F);
		LeftBMLeg.addChild(cube_r23);
		setRotationAngle(cube_r23, 0.0F, 0.0436F, -1.5708F);
		cube_r23.setTextureOffset(50, 20).addBox(-4.95F, -0.525F, -1.35F, 6.0F, 1.0F, 1.0F, 0.0F, true);

		cube_r24 = new ModelRenderer(this);
		cube_r24.setRotationPoint(-7.0F, 0.25F, 0.0F);
		LeftBMLeg.addChild(cube_r24);
		setRotationAngle(cube_r24, 0.0F, -0.1309F, -1.4399F);
		cube_r24.setTextureOffset(48, 8).addBox(-5.9F, -1.1F, -1.05F, 6.0F, 2.0F, 2.0F, 0.0F, true);

		cube_r25 = new ModelRenderer(this);
		cube_r25.setRotationPoint(-3.5F, 0.0F, 0.0F);
		LeftBMLeg.addChild(cube_r25);
		setRotationAngle(cube_r25, 0.0F, 0.0F, -1.5708F);
		cube_r25.setTextureOffset(38, 0).addBox(-1.0F, -3.5F, -1.0F, 2.0F, 7.0F, 2.0F, 0.0F, true);

		LeftBLeg = new ModelRenderer(this);
		LeftBLeg.setRotationPoint(-5.0F, -2.0F, 6.5F);
		Body.addChild(LeftBLeg);
		setRotationAngle(LeftBLeg, 0.0F, 1.1345F, 0.2618F);

		cube_r26 = new ModelRenderer(this);
		cube_r26.setRotationPoint(-7.75F, 5.0F, 0.0F);
		LeftBLeg.addChild(cube_r26);
		setRotationAngle(cube_r26, 0.0F, -0.0436F, -1.4835F);
		cube_r26.setTextureOffset(48, 22).addBox(-5.5F, -0.875F, -1.575F, 7.0F, 1.0F, 1.0F, 0.0F, true);

		cube_r27 = new ModelRenderer(this);
		cube_r27.setRotationPoint(-7.0F, 0.25F, 0.0F);
		LeftBLeg.addChild(cube_r27);
		setRotationAngle(cube_r27, 0.0F, -0.1745F, -1.4399F);
		cube_r27.setTextureOffset(46, 12).addBox(-6.675F, -1.575F, -1.075F, 7.0F, 2.0F, 2.0F, 0.0F, true);

		cube_r28 = new ModelRenderer(this);
		cube_r28.setRotationPoint(-3.5F, 0.0F, 0.0F);
		LeftBLeg.addChild(cube_r28);
		setRotationAngle(cube_r28, 0.0F, 0.0F, -1.5708F);
		cube_r28.setTextureOffset(38, 0).addBox(-1.0F, -3.5F, -1.0F, 2.0F, 7.0F, 2.0F, 0.0F, true);

		Babies = new ModelRenderer(this);
		Babies.setRotationPoint(0.0F, -5.5F, 2.5F);
		Body.addChild(Babies);

		Babie1 = new ModelRenderer(this);
		Babie1.setRotationPoint(0.0F, -0.5F, 0.0F);
		Babies.addChild(Babie1);
		Babie1.setTextureOffset(48, 24).addBox(-1.5F, -1.0F, -2.5F, 3.0F, 2.0F, 5.0F, 0.0F, false);

		Babie2 = new ModelRenderer(this);
		Babie2.setRotationPoint(-2.0F, 0.475F, 4.0F);
		Babies.addChild(Babie2);
		setRotationAngle(Babie2, 0.0F, 0.8727F, 0.0F);
		Babie2.setTextureOffset(48, 24).addBox(-1.5F, -1.0F, -2.5F, 3.0F, 2.0F, 5.0F, 0.0F, false);

		Babie3 = new ModelRenderer(this);
		Babie3.setRotationPoint(1.0F, 0.5F, 7.5F);
		Babies.addChild(Babie3);
		setRotationAngle(Babie3, 0.0F, -1.1781F, 0.0F);
		Babie3.setTextureOffset(48, 24).addBox(-1.0381F, -1.0F, -2.3087F, 3.0F, 2.0F, 5.0F, 0.0F, false);

		Babie4 = new ModelRenderer(this);
		Babie4.setRotationPoint(5.0F, 0.75F, 2.0F);
		Babies.addChild(Babie4);
		setRotationAngle(Babie4, 0.0F, 2.7053F, -0.2182F);
		Babie4.setTextureOffset(48, 24).addBox(-1.5F, -1.0F, -2.5F, 3.0F, 2.0F, 5.0F, 0.0F, false);

		Babie5 = new ModelRenderer(this);
		Babie5.setRotationPoint(-5.25F, 1.0F, -1.25F);
		Babies.addChild(Babie5);
		setRotationAngle(Babie5, 0.2618F, 2.618F, 0.3054F);
		Babie5.setTextureOffset(48, 24).addBox(-1.5F, -1.0F, -2.5F, 3.0F, 2.0F, 5.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks,
			float netHeadYaw, float headPitch) {
		// previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		Body.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}