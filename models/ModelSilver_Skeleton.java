// Made with Blockbench 4.2.4
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports

public static class ModelSilver_Skeleton extends EntityModel<Entity> {
	private final ModelRenderer head;
	private final ModelRenderer body;
	private final ModelRenderer Shield_r1;
	private final ModelRenderer right_leg;
	private final ModelRenderer right_leg2;
	private final ModelRenderer left_leg;
	private final ModelRenderer left_leg2;
	private final ModelRenderer right_arm;
	private final ModelRenderer silver_sword;
	private final ModelRenderer left_arm;
	private final ModelRenderer silver_sword2;

	public ModelSilver_Skeleton() {
		textureWidth = 64;
		textureHeight = 64;

		head = new ModelRenderer(this);
		head.setRotationPoint(0.0F, -2.5F, 0.0F);
		head.setTextureOffset(0, 0).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 9.5F, 0.0F);
		body.setTextureOffset(32, 0).addBox(-4.0F, -12.0F, -2.0F, 8.0F, 12.0F, 4.0F, 0.0F, false);

		Shield_r1 = new ModelRenderer(this);
		Shield_r1.setRotationPoint(0.0F, -8.0F, 3.0F);
		body.addChild(Shield_r1);
		setRotationAngle(Shield_r1, -0.1745F, 0.0F, 0.0F);
		Shield_r1.setTextureOffset(44, 54).addBox(-4.0F, -3.75F, -0.5F, 8.0F, 8.0F, 2.0F, 0.0F, false);

		right_leg = new ModelRenderer(this);
		right_leg.setRotationPoint(-2.0F, 10.0F, 0.0F);
		right_leg.setTextureOffset(0, 16).addBox(-1.5F, 0.0F, -2.0F, 3.0F, 6.0F, 3.0F, 0.0F, false);
		right_leg.setTextureOffset(0, 46).addBox(-2.0F, 1.0F, -2.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);

		right_leg2 = new ModelRenderer(this);
		right_leg2.setRotationPoint(0.0F, 6.0F, -0.5F);
		right_leg.addChild(right_leg2);
		right_leg2.setTextureOffset(0, 25).addBox(-1.5F, 0.0F, -1.5F, 3.0F, 6.0F, 3.0F, 0.0F, false);
		right_leg2.setTextureOffset(0, 54).addBox(-2.0F, 2.0F, -2.0F, 4.0F, 6.0F, 4.0F, 0.0F, false);

		left_leg = new ModelRenderer(this);
		left_leg.setRotationPoint(2.0F, 10.0F, 0.0F);
		left_leg.setTextureOffset(12, 16).addBox(-1.5F, 0.0F, -2.0F, 3.0F, 6.0F, 3.0F, 0.0F, true);
		left_leg.setTextureOffset(16, 46).addBox(-2.0F, 1.0F, -2.5F, 4.0F, 4.0F, 4.0F, 0.0F, true);

		left_leg2 = new ModelRenderer(this);
		left_leg2.setRotationPoint(0.0F, 6.0F, -0.5F);
		left_leg.addChild(left_leg2);
		left_leg2.setTextureOffset(12, 25).addBox(-1.5F, 0.0F, -1.5F, 3.0F, 6.0F, 3.0F, 0.0F, true);
		left_leg2.setTextureOffset(16, 54).addBox(-2.0F, 2.0F, -2.0F, 4.0F, 6.0F, 4.0F, 0.0F, true);

		right_arm = new ModelRenderer(this);
		right_arm.setRotationPoint(-6.0F, 0.0F, -0.5F);
		right_arm.setTextureOffset(24, 16).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 12.0F, 3.0F, 0.0F, false);
		right_arm.setTextureOffset(48, 16).addBox(-2.0F, -2.0F, -2.0F, 4.0F, 11.0F, 4.0F, 0.0F, false);

		silver_sword = new ModelRenderer(this);
		silver_sword.setRotationPoint(0.0F, 9.5F, 0.0F);
		right_arm.addChild(silver_sword);
		silver_sword.setTextureOffset(52, 46).addBox(-0.5F, -0.5F, -2.5F, 1.0F, 1.0F, 5.0F, 0.0F, false);
		silver_sword.setTextureOffset(48, 46).addBox(-0.5F, -1.5F, -3.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		silver_sword.setTextureOffset(28, 28).addBox(0.0F, -1.5F, -13.5F, 0.0F, 3.0F, 10.0F, 0.0F, false);

		left_arm = new ModelRenderer(this);
		left_arm.setRotationPoint(6.0F, 0.0F, -0.5F);
		setRotationAngle(left_arm, -0.0436F, 0.0F, 0.0F);
		left_arm.setTextureOffset(36, 16).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 12.0F, 3.0F, 0.0F, true);
		left_arm.setTextureOffset(48, 31).addBox(-2.0F, -2.0F, -2.0F, 4.0F, 11.0F, 4.0F, 0.0F, true);

		silver_sword2 = new ModelRenderer(this);
		silver_sword2.setRotationPoint(0.0F, 9.5F, 0.0F);
		left_arm.addChild(silver_sword2);
		silver_sword2.setTextureOffset(52, 46).addBox(-0.5F, -0.5F, -2.5F, 1.0F, 1.0F, 5.0F, 0.0F, true);
		silver_sword2.setTextureOffset(48, 50).addBox(-0.5F, -1.5F, -3.5F, 1.0F, 3.0F, 1.0F, 0.0F, true);
		silver_sword2.setTextureOffset(28, 31).addBox(0.0F, -1.5F, -13.5F, 0.0F, 3.0F, 10.0F, 0.0F, true);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		head.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		body.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		right_leg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		left_leg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		right_arm.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		left_arm.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		this.head.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.head.rotateAngleX = f4 / (180F / (float) Math.PI);
		this.left_leg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.right_arm.rotateAngleX = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
		this.right_leg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.left_arm.rotateAngleX = MathHelper.cos(f * 0.6662F) * f1;
	}
}