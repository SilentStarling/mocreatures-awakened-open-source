// Made with Blockbench 3.8.4
// Exported for Minecraft version 1.15 - 1.16
// Paste this class into your mod and generate all required imports

public static class ModelTurtle extends EntityModel<Entity> {
	private final ModelRenderer FLleg;
	private final ModelRenderer FRleg;
	private final ModelRenderer BLleg;
	private final ModelRenderer BRleg;
	private final ModelRenderer Head;
	private final ModelRenderer Tail;
	private final ModelRenderer Body;

	public ModelTurtle() {
		textureWidth = 64;
		textureHeight = 32;

		FLleg = new ModelRenderer(this);
		FLleg.setRotationPoint(3.5F, 20.5F, -3.5F);
		FLleg.setTextureOffset(0, 9).addBox(-1.0F, -0.5F, -1.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		FRleg = new ModelRenderer(this);
		FRleg.setRotationPoint(-3.5F, 20.5F, -3.5F);
		FRleg.setTextureOffset(0, 0).addBox(-1.0F, -0.5F, -1.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		BLleg = new ModelRenderer(this);
		BLleg.setRotationPoint(3.5F, 20.5F, 3.5F);
		BLleg.setTextureOffset(0, 9).addBox(-1.0F, -0.5F, -1.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		BRleg = new ModelRenderer(this);
		BRleg.setRotationPoint(-3.5F, 20.5F, 3.5F);
		BRleg.setTextureOffset(0, 0).addBox(-1.0F, -0.5F, -1.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		Head = new ModelRenderer(this);
		Head.setRotationPoint(0.0F, 20.0F, -5.0F);
		Head.setTextureOffset(10, 0).addBox(-1.5F, -1.0F, -3.5F, 3.0F, 2.0F, 4.0F, 0.0F, false);

		Tail = new ModelRenderer(this);
		Tail.setRotationPoint(0.0F, 20.5F, 4.5F);
		Tail.setTextureOffset(0, 5).addBox(-1.0F, -0.5F, -0.5F, 2.0F, 1.0F, 3.0F, 0.0F, false);

		Body = new ModelRenderer(this);
		Body.setRotationPoint(0.0F, 24.0F, 0.0F);
		Body.setTextureOffset(28, 0).addBox(-4.5F, -5.0F, -4.5F, 9.0F, 1.0F, 9.0F, 0.0F, false);
		Body.setTextureOffset(0, 22).addBox(-4.0F, -7.0F, -4.0F, 8.0F, 2.0F, 8.0F, 0.0F, false);
		Body.setTextureOffset(40, 10).addBox(-3.0F, -8.0F, -3.0F, 6.0F, 1.0F, 6.0F, 0.0F, false);
		Body.setTextureOffset(0, 12).addBox(-4.0F, -4.0F, -4.0F, 8.0F, 1.0F, 8.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		FLleg.render(matrixStack, buffer, packedLight, packedOverlay);
		FRleg.render(matrixStack, buffer, packedLight, packedOverlay);
		BLleg.render(matrixStack, buffer, packedLight, packedOverlay);
		BRleg.render(matrixStack, buffer, packedLight, packedOverlay);
		Head.render(matrixStack, buffer, packedLight, packedOverlay);
		Tail.render(matrixStack, buffer, packedLight, packedOverlay);
		Body.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
		this.BLleg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
		this.FRleg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.FLleg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.BRleg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.Tail.rotateAngleZ = MathHelper.cos(f * 1.0F) * -1.0F * f1;
	}
}