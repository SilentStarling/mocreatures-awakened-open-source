// Made with Blockbench 4.2.4
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports

public static class ModelWorg extends EntityModel<Entity> {
	private final ModelRenderer Body;
	private final ModelRenderer cube_r1;
	private final ModelRenderer cube_r2;
	private final ModelRenderer cube_r3;
	private final ModelRenderer cube_r4;
	private final ModelRenderer cube_r5;
	private final ModelRenderer cube_r6;
	private final ModelRenderer cube_r7;
	private final ModelRenderer cube_r8;
	private final ModelRenderer Head;
	private final ModelRenderer cube_r9;
	private final ModelRenderer cube_r10;
	private final ModelRenderer H2;
	private final ModelRenderer cube_r11;
	private final ModelRenderer cube_r12;
	private final ModelRenderer Mouth;
	private final ModelRenderer cube_r13;
	private final ModelRenderer RightEar;
	private final ModelRenderer RightEar2;
	private final ModelRenderer LeftEar;
	private final ModelRenderer LeftEar2;
	private final ModelRenderer Saddle;
	private final ModelRenderer cube_r14;
	private final ModelRenderer cube_r15;
	private final ModelRenderer RightStirrup;
	private final ModelRenderer LeftStirrup;
	private final ModelRenderer Collar;
	private final ModelRenderer cube_r16;
	private final ModelRenderer RightRein;
	private final ModelRenderer cube_r17;
	private final ModelRenderer LeftRein;
	private final ModelRenderer cube_r18;
	private final ModelRenderer Chest;
	private final ModelRenderer cube_r19;
	private final ModelRenderer Tail;
	private final ModelRenderer Tail2;
	private final ModelRenderer Tail3;
	private final ModelRenderer RightFrontLeg;
	private final ModelRenderer RightFrontLeg2;
	private final ModelRenderer RightFrontLeg3;
	private final ModelRenderer RightFrontFoot;
	private final ModelRenderer RightFrontFoot2;
	private final ModelRenderer RFF;
	private final ModelRenderer cube_r20;
	private final ModelRenderer RFF2;
	private final ModelRenderer cube_r21;
	private final ModelRenderer RFF3;
	private final ModelRenderer cube_r22;
	private final ModelRenderer RFF4;
	private final ModelRenderer cube_r23;
	private final ModelRenderer RFF5;
	private final ModelRenderer cube_r24;
	private final ModelRenderer LeftFrontLeg;
	private final ModelRenderer LeftFrontLeg2;
	private final ModelRenderer LeftFrontLeg3;
	private final ModelRenderer LeftFrontFoot;
	private final ModelRenderer LeftFrontFoot2;
	private final ModelRenderer LFF;
	private final ModelRenderer cube_r25;
	private final ModelRenderer LFF2;
	private final ModelRenderer cube_r26;
	private final ModelRenderer LFF3;
	private final ModelRenderer cube_r27;
	private final ModelRenderer LFF4;
	private final ModelRenderer cube_r28;
	private final ModelRenderer LFF5;
	private final ModelRenderer cube_r29;
	private final ModelRenderer RightBackLeg;
	private final ModelRenderer RightBackLeg2;
	private final ModelRenderer RightBackLeg3;
	private final ModelRenderer cube_r30;
	private final ModelRenderer RightBackFoot;
	private final ModelRenderer RightBackFoot2;
	private final ModelRenderer RBF;
	private final ModelRenderer cube_r31;
	private final ModelRenderer RBF2;
	private final ModelRenderer cube_r32;
	private final ModelRenderer RBF3;
	private final ModelRenderer cube_r33;
	private final ModelRenderer RBF4;
	private final ModelRenderer cube_r34;
	private final ModelRenderer RBF5;
	private final ModelRenderer cube_r35;
	private final ModelRenderer LeftBackLeg;
	private final ModelRenderer LeftBackLeg2;
	private final ModelRenderer LeftBackLeg3;
	private final ModelRenderer cube_r36;
	private final ModelRenderer LeftBackFoot;
	private final ModelRenderer LeftBackFoot2;
	private final ModelRenderer LBF;
	private final ModelRenderer cube_r37;
	private final ModelRenderer LBF2;
	private final ModelRenderer cube_r38;
	private final ModelRenderer LBF3;
	private final ModelRenderer cube_r39;
	private final ModelRenderer LBF4;
	private final ModelRenderer cube_r40;
	private final ModelRenderer LBF5;
	private final ModelRenderer cube_r41;

	public ModelWorg() {
		textureWidth = 128;
		textureHeight = 128;

		Body = new ModelRenderer(this);
		Body.setRotationPoint(0.0F, 11.75F, -7.0F);
		Body.setTextureOffset(65, 62).addBox(-5.0F, -11.0F, -13.0F, 10.0F, 10.0F, 5.0F, 0.0F, false);
		Body.setTextureOffset(0, 0).addBox(-6.5F, -13.0F, -8.0F, 13.0F, 14.0F, 16.0F, 0.0F, false);
		Body.setTextureOffset(4, 4).addBox(-5.5F, -15.0F, -6.0F, 11.0F, 3.0F, 12.0F, 0.0F, false);
		Body.setTextureOffset(36, 40).addBox(-6.0F, 0.0F, -7.0F, 12.0F, 4.0F, 14.0F, -0.001F, false);
		Body.setTextureOffset(0, 30).addBox(-5.5F, -10.0F, 8.0F, 11.0F, 10.0F, 14.0F, 0.0F, false);
		Body.setTextureOffset(0, 66).addBox(-5.0F, -9.0F, 22.0F, 10.0F, 10.0F, 4.0F, 0.0F, false);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(0.0F, -10.0F, 22.0F);
		Body.addChild(cube_r1);
		setRotationAngle(cube_r1, -0.0873F, 0.0F, 0.0F);
		cube_r1.setTextureOffset(89, 27).addBox(-2.5F, 0.0F, 0.0F, 5.0F, 3.0F, 6.0F, 0.0F, false);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(0.0F, 1.0F, 22.0F);
		Body.addChild(cube_r2);
		setRotationAngle(cube_r2, -0.2618F, 0.0F, 0.0F);
		cube_r2.setTextureOffset(74, 49).addBox(-5.0F, -1.0F, -4.0F, 10.0F, 1.0F, 4.0F, 0.0F, false);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(-0.5F, -13.0F, 8.0F);
		Body.addChild(cube_r3);
		setRotationAngle(cube_r3, -0.3491F, 0.0F, 0.0F);
		cube_r3.setTextureOffset(0, 54).addBox(-5.0F, 0.0F, 0.0F, 11.0F, 3.0F, 9.0F, -0.001F, false);

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(0.0F, 1.0F, 8.0F);
		Body.addChild(cube_r4);
		setRotationAngle(cube_r4, 0.1309F, 0.0F, 0.0F);
		cube_r4.setTextureOffset(48, 20).addBox(-5.5F, -0.5F, -2.0F, 11.0F, 2.0F, 10.0F, -0.001F, false);
		cube_r4.setTextureOffset(32, 58).addBox(-5.5F, -1.0F, 0.0F, 11.0F, 1.0F, 8.0F, -0.001F, false);

		cube_r5 = new ModelRenderer(this);
		cube_r5.setRotationPoint(0.0F, -15.0F, 6.0F);
		Body.addChild(cube_r5);
		setRotationAngle(cube_r5, -0.8727F, 0.0F, 0.0F);
		cube_r5.setTextureOffset(6, 60).addBox(-5.5F, 0.0F, 0.0F, 11.0F, 3.0F, 3.0F, -0.001F, false);

		cube_r6 = new ModelRenderer(this);
		cube_r6.setRotationPoint(0.0F, -15.0F, -6.0F);
		Body.addChild(cube_r6);
		setRotationAngle(cube_r6, 0.8727F, 0.0F, 0.0F);
		cube_r6.setTextureOffset(13, 13).addBox(-5.5F, 0.0F, -3.0F, 11.0F, 3.0F, 3.0F, -0.001F, false);

		cube_r7 = new ModelRenderer(this);
		cube_r7.setRotationPoint(0.0F, 1.0F, -8.0F);
		Body.addChild(cube_r7);
		setRotationAngle(cube_r7, -0.4363F, 0.0F, 0.0F);
		cube_r7.setTextureOffset(80, 15).addBox(-5.0F, 0.0F, -5.0F, 10.0F, 1.0F, 5.0F, -0.001F, false);
		cube_r7.setTextureOffset(36, 32).addBox(-5.0F, -2.0F, -5.0F, 10.0F, 2.0F, 5.0F, -0.001F, false);

		cube_r8 = new ModelRenderer(this);
		cube_r8.setRotationPoint(0.0F, -13.0F, -8.0F);
		Body.addChild(cube_r8);
		setRotationAngle(cube_r8, 0.4363F, 0.0F, 0.0F);
		cube_r8.setTextureOffset(76, 0).addBox(-5.0F, 0.0F, -5.0F, 10.0F, 2.0F, 5.0F, -0.001F, false);

		Head = new ModelRenderer(this);
		Head.setRotationPoint(0.0F, 5.65F, -19.0F);
		setRotationAngle(Head, 0.2618F, 0.0F, 0.0F);
		Head.setTextureOffset(28, 67).addBox(-4.0F, -4.5F, -3.5F, 8.0F, 9.0F, 5.0F, 0.0F, false);

		cube_r9 = new ModelRenderer(this);
		cube_r9.setRotationPoint(0.0F, 4.5F, -3.5F);
		Head.addChild(cube_r9);
		setRotationAngle(cube_r9, -0.0873F, 0.0F, 0.0F);
		cube_r9.setTextureOffset(0, 95).addBox(-3.0F, -5.0F, -2.75F, 6.0F, 5.0F, 3.0F, -0.001F, false);

		cube_r10 = new ModelRenderer(this);
		cube_r10.setRotationPoint(0.0F, -4.5F, -3.5F);
		Head.addChild(cube_r10);
		setRotationAngle(cube_r10, 0.1745F, 0.0F, 0.0F);
		cube_r10.setTextureOffset(95, 62).addBox(-3.0F, 0.0F, -3.0F, 6.0F, 5.0F, 3.0F, 0.0F, false);

		H2 = new ModelRenderer(this);
		H2.setRotationPoint(-0.25F, -4.5F, -3.5F);
		Head.addChild(H2);
		setRotationAngle(H2, 0.2618F, 0.0F, 0.0F);
		H2.setTextureOffset(88, 7).addBox(-1.75F, 2.75F, -10.0F, 4.0F, 1.0F, 7.0F, -0.001F, false);
		H2.setTextureOffset(84, 85).addBox(-1.75F, -0.25F, -10.0F, 4.0F, 3.0F, 7.0F, 0.0F, false);

		cube_r11 = new ModelRenderer(this);
		cube_r11.setRotationPoint(-1.75F, -0.25F, -5.0F);
		H2.addChild(cube_r11);
		setRotationAngle(cube_r11, 0.0F, 0.0873F, -1.0472F);
		cube_r11.setTextureOffset(42, 8).addBox(-0.75F, -0.25F, -1.75F, 1.0F, 1.0F, 3.0F, -0.25F, true);

		cube_r12 = new ModelRenderer(this);
		cube_r12.setRotationPoint(2.25F, -0.25F, -5.0F);
		H2.addChild(cube_r12);
		setRotationAngle(cube_r12, 0.0F, -0.0873F, 1.0472F);
		cube_r12.setTextureOffset(42, 8).addBox(-0.25F, -0.25F, -1.75F, 1.0F, 1.0F, 3.0F, -0.25F, false);

		Mouth = new ModelRenderer(this);
		Mouth.setRotationPoint(0.0F, 1.25F, -6.25F);
		Head.addChild(Mouth);
		Mouth.setTextureOffset(88, 70).addBox(-2.0F, 0.0F, -6.25F, 4.0F, 2.0F, 7.0F, 0.0F, false);
		Mouth.setTextureOffset(81, 54).addBox(-2.0F, -1.0F, -6.25F, 4.0F, 1.0F, 7.0F, -0.001F, false);

		cube_r13 = new ModelRenderer(this);
		cube_r13.setRotationPoint(0.0F, -0.5F, 0.75F);
		Mouth.addChild(cube_r13);
		setRotationAngle(cube_r13, 0.0873F, 0.0F, 0.0F);
		cube_r13.setTextureOffset(100, 90).addBox(-1.5F, -0.25F, -6.0F, 3.0F, 1.0F, 6.0F, 0.0F, false);

		RightEar = new ModelRenderer(this);
		RightEar.setRotationPoint(3.0F, -3.5F, -4.5F);
		Head.addChild(RightEar);
		setRotationAngle(RightEar, -0.7854F, 0.5236F, -0.1745F);
		RightEar.setTextureOffset(31, 56).addBox(-0.5F, -2.5F, -1.0F, 1.0F, 3.0F, 2.0F, 0.0F, false);

		RightEar2 = new ModelRenderer(this);
		RightEar2.setRotationPoint(0.0F, -2.5F, 0.25F);
		RightEar.addChild(RightEar2);
		setRotationAngle(RightEar2, 0.0F, 0.0F, 0.4363F);
		RightEar2.setTextureOffset(11, 0).addBox(-0.5F, -1.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		LeftEar = new ModelRenderer(this);
		LeftEar.setRotationPoint(-3.0F, -3.5F, -4.5F);
		Head.addChild(LeftEar);
		setRotationAngle(LeftEar, -0.7854F, -0.5236F, 0.1745F);
		LeftEar.setTextureOffset(0, 54).addBox(-0.5F, -2.5F, -1.0F, 1.0F, 3.0F, 2.0F, 0.0F, false);

		LeftEar2 = new ModelRenderer(this);
		LeftEar2.setRotationPoint(0.0F, -2.5F, 0.25F);
		LeftEar.addChild(LeftEar2);
		setRotationAngle(LeftEar2, 0.0F, 0.0F, -0.4363F);
		LeftEar2.setTextureOffset(0, 0).addBox(-0.5F, -1.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		Saddle = new ModelRenderer(this);
		Saddle.setRotationPoint(0.0F, 1.0F, 0.0F);
		setRotationAngle(Saddle, -0.3491F, 0.0F, 0.0F);
		Saddle.setTextureOffset(50, 6).addBox(-4.0F, -4.5F, -0.5F, 8.0F, 2.0F, 6.0F, 0.0F, false);

		cube_r14 = new ModelRenderer(this);
		cube_r14.setRotationPoint(0.0F, -4.5F, 1.0F);
		Saddle.addChild(cube_r14);
		setRotationAngle(cube_r14, -0.1745F, 0.0F, 0.0F);
		cube_r14.setTextureOffset(65, 0).addBox(-2.5F, -1.0F, -1.5F, 5.0F, 2.0F, 3.0F, 0.0F, false);

		cube_r15 = new ModelRenderer(this);
		cube_r15.setRotationPoint(0.0F, -4.5F, 9.75F);
		Saddle.addChild(cube_r15);
		setRotationAngle(cube_r15, 0.2182F, 0.0F, 0.0F);
		cube_r15.setTextureOffset(15, 81).addBox(-4.0F, -1.0F, -5.5F, 8.0F, 2.0F, 4.0F, -0.001F, false);

		RightStirrup = new ModelRenderer(this);
		RightStirrup.setRotationPoint(-4.5F, -3.0F, 3.0F);
		Saddle.addChild(RightStirrup);
		setRotationAngle(RightStirrup, 0.0F, 0.0F, 0.6109F);
		RightStirrup.setTextureOffset(57, 0).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 5.0F, 1.0F, 0.0F, false);
		RightStirrup.setTextureOffset(72, 5).addBox(-0.5F, 4.5F, -1.0F, 1.0F, 2.0F, 2.0F, 0.0F, false);

		LeftStirrup = new ModelRenderer(this);
		LeftStirrup.setRotationPoint(4.5F, -3.0F, 3.0F);
		Saddle.addChild(LeftStirrup);
		setRotationAngle(LeftStirrup, 0.0F, 0.0F, -0.6109F);
		LeftStirrup.setTextureOffset(61, 0).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 5.0F, 1.0F, 0.0F, false);
		LeftStirrup.setTextureOffset(101, 0).addBox(-0.5F, 4.5F, -1.0F, 1.0F, 2.0F, 2.0F, 0.0F, false);

		Collar = new ModelRenderer(this);
		Collar.setRotationPoint(0.0F, 12.65F, -22.4F);
		Saddle.addChild(Collar);
		setRotationAngle(Collar, 0.6109F, 0.0F, 0.0F);
		Collar.setTextureOffset(111, 15).addBox(-3.0F, -2.4F, -0.5F, 6.0F, 4.0F, 1.0F, 0.0F, false);

		cube_r16 = new ModelRenderer(this);
		cube_r16.setRotationPoint(-0.5F, -1.5F, 0.5F);
		Collar.addChild(cube_r16);
		setRotationAngle(cube_r16, 0.7854F, 0.0F, 0.0F);
		cube_r16.setTextureOffset(107, 2).addBox(-3.0F, 1.45F, -3.25F, 7.0F, 1.0F, 1.0F, 0.0F, false);

		RightRein = new ModelRenderer(this);
		RightRein.setRotationPoint(-3.0F, 1.6F, 0.5F);
		Collar.addChild(RightRein);
		setRotationAngle(RightRein, 0.3927F, -0.4363F, 0.0F);
		RightRein.setTextureOffset(105, -9).addBox(0.0F, -0.5F, -0.5F, 0.0F, 1.0F, 9.0F, 0.0F, false);

		cube_r17 = new ModelRenderer(this);
		cube_r17.setRotationPoint(0.0F, 0.0F, 8.5F);
		RightRein.addChild(cube_r17);
		setRotationAngle(cube_r17, -0.3491F, 0.4363F, 0.0873F);
		cube_r17.setTextureOffset(105, -9).addBox(0.0F, -0.5F, 9.0F, 0.0F, 1.0F, 9.0F, 0.0F, false);
		cube_r17.setTextureOffset(105, -9).addBox(0.0F, -0.5F, 0.0F, 0.0F, 1.0F, 9.0F, 0.0F, false);

		LeftRein = new ModelRenderer(this);
		LeftRein.setRotationPoint(3.0F, 1.6F, 0.5F);
		Collar.addChild(LeftRein);
		setRotationAngle(LeftRein, 0.3927F, 0.4363F, 0.0F);
		LeftRein.setTextureOffset(105, -8).addBox(0.0F, -0.5F, -0.5F, 0.0F, 1.0F, 9.0F, 0.0F, true);

		cube_r18 = new ModelRenderer(this);
		cube_r18.setRotationPoint(0.0F, 0.0F, 8.5F);
		LeftRein.addChild(cube_r18);
		setRotationAngle(cube_r18, -0.3491F, -0.4363F, -0.0873F);
		cube_r18.setTextureOffset(105, -8).addBox(0.0F, -0.5F, 9.0F, 0.0F, 1.0F, 9.0F, 0.0F, true);
		cube_r18.setTextureOffset(105, -8).addBox(0.0F, -0.5F, 0.0F, 0.0F, 1.0F, 9.0F, 0.0F, true);

		Chest = new ModelRenderer(this);
		Chest.setRotationPoint(0.0F, 1.0F, 0.0F);
		setRotationAngle(Chest, -0.3491F, 0.0F, 0.0F);

		cube_r19 = new ModelRenderer(this);
		cube_r19.setRotationPoint(0.0F, -5.0F, 2.5F);
		Chest.addChild(cube_r19);
		setRotationAngle(cube_r19, -1.5708F, 0.0F, 0.0F);
		cube_r19.setTextureOffset(45, 106).addBox(-5.0F, -8.5F, -2.5F, 10.0F, 4.0F, 5.0F, 0.0F, false);

		Tail = new ModelRenderer(this);
		Tail.setRotationPoint(0.0F, 3.75F, 21.0F);
		setRotationAngle(Tail, -0.1745F, 0.0F, 0.0F);
		Tail.setTextureOffset(102, 47).addBox(-2.0F, -1.5F, -0.5F, 4.0F, 3.0F, 4.0F, 0.0F, false);
		Tail.setTextureOffset(58, 15).addBox(-2.0F, 1.5F, -0.5F, 4.0F, 1.0F, 4.0F, -0.001F, false);

		Tail2 = new ModelRenderer(this);
		Tail2.setRotationPoint(0.0F, 0.0F, 3.5F);
		Tail.addChild(Tail2);
		setRotationAngle(Tail2, 0.2618F, 0.0F, 0.0F);
		Tail2.setTextureOffset(66, 32).addBox(-1.5F, -1.5F, -0.5F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		Tail2.setTextureOffset(36, 39).addBox(-1.5F, 1.5F, -0.5F, 3.0F, 1.0F, 4.0F, -0.001F, false);

		Tail3 = new ModelRenderer(this);
		Tail3.setRotationPoint(0.0F, 0.0F, 3.5F);
		Tail2.addChild(Tail3);
		setRotationAngle(Tail3, 0.3491F, 0.0F, 0.0F);
		Tail3.setTextureOffset(13, 98).addBox(-1.0F, -1.5F, -0.5F, 2.0F, 3.0F, 5.0F, 0.0F, false);
		Tail3.setTextureOffset(83, 102).addBox(-1.0F, 1.5F, -0.5F, 2.0F, 1.0F, 5.0F, -0.001F, false);

		RightFrontLeg = new ModelRenderer(this);
		RightFrontLeg.setRotationPoint(8.0F, 8.0F, -10.0F);
		setRotationAngle(RightFrontLeg, 0.1745F, 0.0F, 0.0F);
		RightFrontLeg.setTextureOffset(47, 74).addBox(-2.5F, -6.25F, -3.0F, 4.0F, 10.0F, 7.0F, 0.0F, false);

		RightFrontLeg2 = new ModelRenderer(this);
		RightFrontLeg2.setRotationPoint(0.0F, 3.75F, 3.5F);
		RightFrontLeg.addChild(RightFrontLeg2);
		setRotationAngle(RightFrontLeg2, -0.4363F, 0.0F, 0.0F);
		RightFrontLeg2.setTextureOffset(54, 91).addBox(-2.0F, 0.0F, -5.0F, 3.0F, 9.0F, 5.0F, 0.0F, false);
		RightFrontLeg2.setTextureOffset(104, 5).addBox(-2.0F, 2.0F, -0.5F, 3.0F, 7.0F, 2.0F, -0.001F, false);

		RightFrontLeg3 = new ModelRenderer(this);
		RightFrontLeg3.setRotationPoint(-0.5F, 9.0F, -1.0F);
		RightFrontLeg2.addChild(RightFrontLeg3);
		setRotationAngle(RightFrontLeg3, -0.1745F, 0.0F, 0.0F);
		RightFrontLeg3.setTextureOffset(99, 79).addBox(-1.75F, -2.0F, -3.5F, 3.0F, 7.0F, 4.0F, 0.0F, false);

		RightFrontFoot = new ModelRenderer(this);
		RightFrontFoot.setRotationPoint(-0.25F, 4.0F, 0.0F);
		RightFrontLeg3.addChild(RightFrontFoot);
		setRotationAngle(RightFrontFoot, 0.4363F, 0.0F, 0.0F);
		RightFrontFoot.setTextureOffset(70, 99).addBox(-2.0F, -1.0F, -4.5F, 4.0F, 2.0F, 5.0F, 0.0F, false);

		RightFrontFoot2 = new ModelRenderer(this);
		RightFrontFoot2.setRotationPoint(0.0F, 0.0F, -3.5F);
		RightFrontFoot.addChild(RightFrontFoot2);

		RFF = new ModelRenderer(this);
		RFF.setRotationPoint(1.5F, 0.0F, 0.0F);
		RightFrontFoot2.addChild(RFF);
		setRotationAngle(RFF, 0.0F, -0.2618F, 0.0F);
		RFF.setTextureOffset(91, 82).addBox(-0.5F, -0.5F, -2.0F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r20 = new ModelRenderer(this);
		cube_r20.setRotationPoint(0.0F, -0.5F, -2.25F);
		RFF.addChild(cube_r20);
		setRotationAngle(cube_r20, 0.4363F, 0.0F, 0.0F);
		cube_r20.setTextureOffset(65, 91).addBox(-0.5F, 0.0F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		RFF2 = new ModelRenderer(this);
		RFF2.setRotationPoint(0.5F, 0.0F, 0.0F);
		RightFrontFoot2.addChild(RFF2);
		setRotationAngle(RFF2, 0.0F, -0.0873F, 0.0F);
		RFF2.setTextureOffset(49, 91).addBox(-0.5326F, -0.5F, -2.2479F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r21 = new ModelRenderer(this);
		cube_r21.setRotationPoint(0.0F, -0.5F, -2.25F);
		RFF2.addChild(cube_r21);
		setRotationAngle(cube_r21, 0.4363F, 0.0F, 0.0F);
		cube_r21.setTextureOffset(90, 62).addBox(-0.5326F, -0.1048F, -1.7246F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		RFF3 = new ModelRenderer(this);
		RFF3.setRotationPoint(-0.5F, 0.0F, 0.0F);
		RightFrontFoot2.addChild(RFF3);
		setRotationAngle(RFF3, 0.0F, 0.0873F, 0.0F);
		RFF3.setTextureOffset(89, 36).addBox(-0.4674F, -0.5F, -2.2479F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r22 = new ModelRenderer(this);
		cube_r22.setRotationPoint(0.0F, -0.5F, -2.25F);
		RFF3.addChild(cube_r22);
		setRotationAngle(cube_r22, 0.4363F, 0.0F, 0.0F);
		cube_r22.setTextureOffset(89, 30).addBox(-0.4674F, -0.1048F, -1.7246F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		RFF4 = new ModelRenderer(this);
		RFF4.setRotationPoint(-1.5F, 0.0F, 0.0F);
		RightFrontFoot2.addChild(RFF4);
		setRotationAngle(RFF4, 0.0F, 0.2618F, 0.0F);
		RFF4.setTextureOffset(88, 79).addBox(-0.5F, -0.5F, -2.0F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r23 = new ModelRenderer(this);
		cube_r23.setRotationPoint(0.0F, -0.5F, -2.25F);
		RFF4.addChild(cube_r23);
		setRotationAngle(cube_r23, 0.4363F, 0.0F, 0.0F);
		cube_r23.setTextureOffset(37, 88).addBox(-0.5F, 0.0F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		RFF5 = new ModelRenderer(this);
		RFF5.setRotationPoint(-1.5F, -0.25F, 0.5F);
		RightFrontFoot.addChild(RFF5);
		setRotationAngle(RFF5, -0.0873F, 0.5236F, -0.2618F);
		RFF5.setTextureOffset(88, 11).addBox(-0.5F, -0.5F, -2.0F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r24 = new ModelRenderer(this);
		cube_r24.setRotationPoint(0.0F, -0.5F, -2.25F);
		RFF5.addChild(cube_r24);
		setRotationAngle(cube_r24, 0.4363F, 0.0F, 0.0F);
		cube_r24.setTextureOffset(88, 8).addBox(-0.5F, 0.0F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		LeftFrontLeg = new ModelRenderer(this);
		LeftFrontLeg.setRotationPoint(-8.0F, 8.0F, -10.0F);
		setRotationAngle(LeftFrontLeg, 0.1745F, 0.0F, 0.0F);
		LeftFrontLeg.setTextureOffset(74, 32).addBox(-1.5F, -6.25F, -3.0F, 4.0F, 10.0F, 7.0F, 0.0F, false);

		LeftFrontLeg2 = new ModelRenderer(this);
		LeftFrontLeg2.setRotationPoint(0.0F, 3.75F, 3.5F);
		LeftFrontLeg.addChild(LeftFrontLeg2);
		setRotationAngle(LeftFrontLeg2, -0.4363F, 0.0F, 0.0F);
		LeftFrontLeg2.setTextureOffset(38, 91).addBox(-1.0F, 0.0F, -5.0F, 3.0F, 9.0F, 5.0F, 0.0F, false);
		LeftFrontLeg2.setTextureOffset(27, 103).addBox(-1.0F, 2.0F, -0.5F, 3.0F, 7.0F, 2.0F, -0.001F, false);

		LeftFrontLeg3 = new ModelRenderer(this);
		LeftFrontLeg3.setRotationPoint(0.5F, 9.0F, -1.0F);
		LeftFrontLeg2.addChild(LeftFrontLeg3);
		setRotationAngle(LeftFrontLeg3, -0.1745F, 0.0F, 0.0F);
		LeftFrontLeg3.setTextureOffset(96, 36).addBox(-1.25F, -2.0F, -3.5F, 3.0F, 7.0F, 4.0F, 0.0F, false);

		LeftFrontFoot = new ModelRenderer(this);
		LeftFrontFoot.setRotationPoint(0.25F, 4.0F, 0.0F);
		LeftFrontLeg3.addChild(LeftFrontFoot);
		setRotationAngle(LeftFrontFoot, 0.4363F, 0.0F, 0.0F);
		LeftFrontFoot.setTextureOffset(83, 95).addBox(-2.0F, -1.0F, -4.5F, 4.0F, 2.0F, 5.0F, 0.0F, false);

		LeftFrontFoot2 = new ModelRenderer(this);
		LeftFrontFoot2.setRotationPoint(0.0F, 0.0F, -3.5F);
		LeftFrontFoot.addChild(LeftFrontFoot2);

		LFF = new ModelRenderer(this);
		LFF.setRotationPoint(-1.5F, 0.0F, 0.0F);
		LeftFrontFoot2.addChild(LFF);
		setRotationAngle(LFF, 0.0F, 0.2618F, 0.0F);
		LFF.setTextureOffset(80, 8).addBox(-0.5F, -0.5F, -2.0F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r25 = new ModelRenderer(this);
		cube_r25.setRotationPoint(0.0F, -0.5F, -2.25F);
		LFF.addChild(cube_r25);
		setRotationAngle(cube_r25, 0.4363F, 0.0F, 0.0F);
		cube_r25.setTextureOffset(0, 80).addBox(-0.5F, 0.0F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		LFF2 = new ModelRenderer(this);
		LFF2.setRotationPoint(-0.5F, 0.0F, 0.0F);
		LeftFrontFoot2.addChild(LFF2);
		setRotationAngle(LFF2, 0.0F, 0.0873F, 0.0F);
		LFF2.setTextureOffset(78, 58).addBox(-0.4674F, -0.5F, -2.2479F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r26 = new ModelRenderer(this);
		cube_r26.setRotationPoint(0.0F, -0.5F, -2.25F);
		LFF2.addChild(cube_r26);
		setRotationAngle(cube_r26, 0.4363F, 0.0F, 0.0F);
		cube_r26.setTextureOffset(78, 15).addBox(-0.4674F, -0.1048F, -1.7246F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		LFF3 = new ModelRenderer(this);
		LFF3.setRotationPoint(0.5F, 0.0F, 0.0F);
		LeftFrontFoot2.addChild(LFF3);
		setRotationAngle(LFF3, 0.0F, -0.0873F, 0.0F);
		LFF3.setTextureOffset(68, 77).addBox(-0.5326F, -0.5F, -2.2479F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r27 = new ModelRenderer(this);
		cube_r27.setRotationPoint(0.0F, -0.5F, -2.25F);
		LFF3.addChild(cube_r27);
		setRotationAngle(cube_r27, 0.4363F, 0.0F, 0.0F);
		cube_r27.setTextureOffset(62, 77).addBox(-0.5326F, -0.1048F, -1.7246F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		LFF4 = new ModelRenderer(this);
		LFF4.setRotationPoint(1.5F, 0.0F, 0.0F);
		LeftFrontFoot2.addChild(LFF4);
		setRotationAngle(LFF4, 0.0F, -0.2618F, 0.0F);
		LFF4.setTextureOffset(76, 7).addBox(-0.5F, -0.5F, -2.0F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r28 = new ModelRenderer(this);
		cube_r28.setRotationPoint(0.0F, -0.5F, -2.25F);
		LFF4.addChild(cube_r28);
		setRotationAngle(cube_r28, 0.4363F, 0.0F, 0.0F);
		cube_r28.setTextureOffset(74, 59).addBox(-0.5F, 0.0F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		LFF5 = new ModelRenderer(this);
		LFF5.setRotationPoint(1.5F, -0.25F, 0.5F);
		LeftFrontFoot.addChild(LFF5);
		setRotationAngle(LFF5, -0.0873F, -0.5236F, 0.2618F);
		LFF5.setTextureOffset(74, 16).addBox(-0.5F, -0.5F, -2.0F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r29 = new ModelRenderer(this);
		cube_r29.setRotationPoint(0.0F, -0.5F, -2.25F);
		LFF5.addChild(cube_r29);
		setRotationAngle(cube_r29, 0.4363F, 0.0F, 0.0F);
		cube_r29.setTextureOffset(70, 58).addBox(-0.5F, 0.0F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		RightBackLeg = new ModelRenderer(this);
		RightBackLeg.setRotationPoint(7.0F, 9.0F, 13.0F);
		setRotationAngle(RightBackLeg, -0.3491F, 0.0F, 0.0F);
		RightBackLeg.setTextureOffset(0, 80).addBox(-2.5F, -5.25F, -3.5F, 4.0F, 8.0F, 7.0F, 0.0F, false);
		RightBackLeg.setTextureOffset(0, 103).addBox(-2.5F, -3.25F, 3.0F, 4.0F, 6.0F, 2.0F, -0.001F, false);

		RightBackLeg2 = new ModelRenderer(this);
		RightBackLeg2.setRotationPoint(-0.5F, 2.75F, 0.0F);
		RightBackLeg.addChild(RightBackLeg2);
		setRotationAngle(RightBackLeg2, 1.0472F, 0.0F, 0.0F);
		RightBackLeg2.setTextureOffset(22, 87).addBox(-1.5F, -2.0F, -2.5F, 3.0F, 11.0F, 5.0F, 0.0F, false);
		RightBackLeg2.setTextureOffset(54, 67).addBox(-1.5F, 5.0F, 1.75F, 3.0F, 4.0F, 2.0F, -0.001F, false);

		RightBackLeg3 = new ModelRenderer(this);
		RightBackLeg3.setRotationPoint(0.0F, 9.0F, 0.0F);
		RightBackLeg2.addChild(RightBackLeg3);
		setRotationAngle(RightBackLeg3, -0.9599F, 0.0F, 0.0F);
		RightBackLeg3.setTextureOffset(97, 98).addBox(-2.0F, -2.0F, -2.0F, 3.0F, 7.0F, 4.0F, 0.0F, false);

		cube_r30 = new ModelRenderer(this);
		cube_r30.setRotationPoint(-15.0F, 0.0F, 2.0F);
		RightBackLeg3.addChild(cube_r30);
		setRotationAngle(cube_r30, 0.1745F, 0.0F, 0.0F);
		cube_r30.setTextureOffset(103, 70).addBox(13.0F, -2.75F, -0.5F, 3.0F, 4.0F, 2.0F, -0.001F, false);

		RightBackFoot = new ModelRenderer(this);
		RightBackFoot.setRotationPoint(-0.5F, 4.0F, 1.5F);
		RightBackLeg3.addChild(RightBackFoot);
		setRotationAngle(RightBackFoot, 0.4363F, 0.0F, 0.0F);
		RightBackFoot.setTextureOffset(96, 54).addBox(-2.0F, -1.0F, -4.5F, 4.0F, 2.0F, 5.0F, 0.0F, false);

		RightBackFoot2 = new ModelRenderer(this);
		RightBackFoot2.setRotationPoint(0.25F, 0.0F, -3.5F);
		RightBackFoot.addChild(RightBackFoot2);
		setRotationAngle(RightBackFoot2, -0.1745F, 0.0F, 0.0F);

		RBF = new ModelRenderer(this);
		RBF.setRotationPoint(1.5F, 0.0F, 0.0F);
		RightBackFoot2.addChild(RBF);
		setRotationAngle(RBF, 0.0F, -0.2618F, 0.0F);
		RBF.setTextureOffset(33, 87).addBox(-0.5F, -0.5F, -2.0F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r31 = new ModelRenderer(this);
		cube_r31.setRotationPoint(0.0F, -0.5F, -2.25F);
		RBF.addChild(cube_r31);
		setRotationAngle(cube_r31, 0.4363F, 0.0F, 0.0F);
		cube_r31.setTextureOffset(86, 27).addBox(-0.5F, 0.0F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		RBF2 = new ModelRenderer(this);
		RBF2.setRotationPoint(0.5F, 0.0F, 0.0F);
		RightBackFoot2.addChild(RBF2);
		setRotationAngle(RBF2, 0.0F, -0.0873F, 0.0F);
		RBF2.setTextureOffset(41, 85).addBox(-0.5326F, -0.5F, -2.2479F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r32 = new ModelRenderer(this);
		cube_r32.setRotationPoint(0.0F, -0.5F, -2.25F);
		RBF2.addChild(cube_r32);
		setRotationAngle(cube_r32, 0.4363F, 0.0F, 0.0F);
		cube_r32.setTextureOffset(84, 80).addBox(-0.5326F, -0.1048F, -1.7246F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		RBF3 = new ModelRenderer(this);
		RBF3.setRotationPoint(-0.5F, 0.0F, 0.0F);
		RightBackFoot2.addChild(RBF3);
		setRotationAngle(RBF3, 0.0F, 0.0873F, 0.0F);
		RBF3.setTextureOffset(84, 77).addBox(-0.4674F, -0.5F, -2.2479F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r33 = new ModelRenderer(this);
		cube_r33.setRotationPoint(0.0F, -0.5F, -2.25F);
		RBF3.addChild(cube_r33);
		setRotationAngle(cube_r33, 0.4363F, 0.0F, 0.0F);
		cube_r33.setTextureOffset(84, 7).addBox(-0.4674F, -0.1048F, -1.7246F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		RBF4 = new ModelRenderer(this);
		RBF4.setRotationPoint(-1.5F, 0.0F, 0.0F);
		RightBackFoot2.addChild(RBF4);
		setRotationAngle(RBF4, 0.0F, 0.2618F, 0.0F);
		RBF4.setTextureOffset(0, 83).addBox(-0.5F, -0.5F, -2.0F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r34 = new ModelRenderer(this);
		cube_r34.setRotationPoint(0.0F, -0.5F, -2.25F);
		RBF4.addChild(cube_r34);
		setRotationAngle(cube_r34, 0.4363F, 0.0F, 0.0F);
		cube_r34.setTextureOffset(40, 81).addBox(-0.5F, 0.0F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		RBF5 = new ModelRenderer(this);
		RBF5.setRotationPoint(-1.25F, -0.25F, 0.5F);
		RightBackFoot.addChild(RBF5);
		setRotationAngle(RBF5, -0.0873F, 0.5236F, -0.2618F);
		RBF5.setTextureOffset(69, 80).addBox(-0.5F, -0.5F, -2.0F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r35 = new ModelRenderer(this);
		cube_r35.setRotationPoint(0.0F, -0.5F, -2.25F);
		RBF5.addChild(cube_r35);
		setRotationAngle(cube_r35, 0.4363F, 0.0F, 0.0F);
		cube_r35.setTextureOffset(80, 27).addBox(-0.5F, 0.0F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		LeftBackLeg = new ModelRenderer(this);
		LeftBackLeg.setRotationPoint(-7.0F, 9.0F, 12.5F);
		setRotationAngle(LeftBackLeg, -0.3491F, 0.0F, 0.0F);
		LeftBackLeg.setTextureOffset(69, 77).addBox(-1.5F, -5.25F, -3.5F, 4.0F, 8.0F, 7.0F, 0.0F, false);
		LeftBackLeg.setTextureOffset(42, 0).addBox(-1.5F, -3.25F, 3.0F, 4.0F, 6.0F, 2.0F, -0.001F, false);

		LeftBackLeg2 = new ModelRenderer(this);
		LeftBackLeg2.setRotationPoint(0.5F, 2.75F, 0.0F);
		LeftBackLeg.addChild(LeftBackLeg2);
		setRotationAngle(LeftBackLeg2, 1.0472F, 0.0F, 0.0F);
		LeftBackLeg2.setTextureOffset(0, 0).addBox(-1.5F, -2.0F, -2.5F, 3.0F, 11.0F, 5.0F, 0.0F, false);
		LeftBackLeg2.setTextureOffset(37, 105).addBox(-1.5F, 5.0F, 2.0F, 3.0F, 4.0F, 2.0F, -0.001F, false);

		LeftBackLeg3 = new ModelRenderer(this);
		LeftBackLeg3.setRotationPoint(0.0F, 9.0F, 0.0F);
		LeftBackLeg2.addChild(LeftBackLeg3);
		setRotationAngle(LeftBackLeg3, -0.9599F, 0.0F, 0.0F);
		LeftBackLeg3.setTextureOffset(0, 30).addBox(-1.0F, -2.0F, -2.0F, 3.0F, 7.0F, 4.0F, 0.0F, false);

		cube_r36 = new ModelRenderer(this);
		cube_r36.setRotationPoint(0.0F, 0.0F, 2.0F);
		LeftBackLeg3.addChild(cube_r36);
		setRotationAngle(cube_r36, 0.1745F, 0.0F, 0.0F);
		cube_r36.setTextureOffset(105, 27).addBox(-1.0F, -2.75F, -0.5F, 3.0F, 4.0F, 2.0F, -0.001F, false);

		LeftBackFoot = new ModelRenderer(this);
		LeftBackFoot.setRotationPoint(0.5F, 4.0F, 1.5F);
		LeftBackLeg3.addChild(LeftBackFoot);
		setRotationAngle(LeftBackFoot, 0.4363F, 0.0F, 0.0F);
		LeftBackFoot.setTextureOffset(70, 92).addBox(-2.0F, -1.0F, -4.5F, 4.0F, 2.0F, 5.0F, 0.0F, false);

		LeftBackFoot2 = new ModelRenderer(this);
		LeftBackFoot2.setRotationPoint(-0.25F, 0.0F, -3.5F);
		LeftBackFoot.addChild(LeftBackFoot2);
		setRotationAngle(LeftBackFoot2, -0.1745F, 0.0F, 0.0F);

		LBF = new ModelRenderer(this);
		LBF.setRotationPoint(-1.5F, 0.0F, 0.0F);
		LeftBackFoot2.addChild(LBF);
		setRotationAngle(LBF, 0.0F, 0.2618F, 0.0F);
		LBF.setTextureOffset(70, 15).addBox(-0.5F, -0.5F, -2.0F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r37 = new ModelRenderer(this);
		cube_r37.setRotationPoint(0.0F, -0.5F, -2.25F);
		LBF.addChild(cube_r37);
		setRotationAngle(cube_r37, 0.4363F, 0.0F, 0.0F);
		cube_r37.setTextureOffset(66, 59).addBox(-0.5F, 0.0F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		LBF2 = new ModelRenderer(this);
		LBF2.setRotationPoint(-0.5F, 0.0F, 0.0F);
		LeftBackFoot2.addChild(LBF2);
		setRotationAngle(LBF2, 0.0F, 0.0873F, 0.0F);
		LBF2.setTextureOffset(24, 66).addBox(-0.4674F, -0.5F, -2.2479F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r38 = new ModelRenderer(this);
		cube_r38.setRotationPoint(0.0F, -0.5F, -2.25F);
		LBF2.addChild(cube_r38);
		setRotationAngle(cube_r38, 0.4363F, 0.0F, 0.0F);
		cube_r38.setTextureOffset(62, 61).addBox(-0.4674F, -0.1048F, -1.7246F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		LBF3 = new ModelRenderer(this);
		LBF3.setRotationPoint(0.5F, 0.0F, 0.0F);
		LeftBackFoot2.addChild(LBF3);
		setRotationAngle(LBF3, 0.0F, -0.0873F, 0.0F);
		LBF3.setTextureOffset(62, 58).addBox(-0.5326F, -0.5F, -2.2479F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r39 = new ModelRenderer(this);
		cube_r39.setRotationPoint(0.0F, -0.5F, -2.25F);
		LBF3.addChild(cube_r39);
		setRotationAngle(cube_r39, 0.4363F, 0.0F, 0.0F);
		cube_r39.setTextureOffset(61, 32).addBox(-0.5326F, -0.1048F, -1.7246F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		LBF4 = new ModelRenderer(this);
		LBF4.setRotationPoint(1.5F, 0.0F, 0.0F);
		LeftBackFoot2.addChild(LBF4);
		setRotationAngle(LBF4, 0.0F, -0.2618F, 0.0F);
		LBF4.setTextureOffset(0, 59).addBox(-0.5F, -0.5F, -2.0F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r40 = new ModelRenderer(this);
		cube_r40.setRotationPoint(0.0F, -0.5F, -2.25F);
		LBF4.addChild(cube_r40);
		setRotationAngle(cube_r40, 0.4363F, 0.0F, 0.0F);
		cube_r40.setTextureOffset(47, 8).addBox(-0.5F, 0.0F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		LBF5 = new ModelRenderer(this);
		LBF5.setRotationPoint(1.25F, -0.25F, 0.5F);
		LeftBackFoot.addChild(LBF5);
		setRotationAngle(LBF5, -0.0873F, -0.5236F, 0.2618F);
		LBF5.setTextureOffset(6, 41).addBox(-0.5F, -0.5F, -2.0F, 1.0F, 1.0F, 2.0F, 0.25F, false);

		cube_r41 = new ModelRenderer(this);
		cube_r41.setRotationPoint(0.0F, -0.5F, -2.25F);
		LBF5.addChild(cube_r41);
		setRotationAngle(cube_r41, 0.4363F, 0.0F, 0.0F);
		cube_r41.setTextureOffset(0, 41).addBox(-0.5F, 0.0F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		Body.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		Head.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		Saddle.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		Chest.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		Tail.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		RightFrontLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		LeftFrontLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		RightBackLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		LeftBackLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
		this.LeftBackLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.RightFrontLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.Tail.rotateAngleZ = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.LeftFrontLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.RightBackLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
	}
}