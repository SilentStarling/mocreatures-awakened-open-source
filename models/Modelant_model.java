// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

public static class Modelant_model extends EntityModel<Entity> {
	private final ModelRenderer legs;
	private final ModelRenderer cube_r1;
	private final ModelRenderer cube_r2;
	private final ModelRenderer cube_r3;
	private final ModelRenderer head;
	private final ModelRenderer cube_r4;
	private final ModelRenderer cube_r5;
	private final ModelRenderer cube_r6;
	private final ModelRenderer body;

	public Modelant_model() {
		textureWidth = 32;
		textureHeight = 32;

		legs = new ModelRenderer(this);
		legs.setRotationPoint(0.0F, 24.0F, 0.0F);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(-1.0F, -2.0F, 3.0F);
		legs.addChild(cube_r1);
		setRotationAngle(cube_r1, -0.6981F, 0.0F, 0.0F);
		cube_r1.setTextureOffset(0, 8).addBox(-1.0F, -1.0F, -1.0F, 1.0F, 0.0F, 5.0F, 0.0F, false);
		cube_r1.setTextureOffset(7, 0).addBox(2.0F, -1.0F, -1.0F, 1.0F, 0.0F, 5.0F, 0.0F, false);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(-1.0F, -2.0F, 2.0F);
		legs.addChild(cube_r2);
		setRotationAngle(cube_r2, -0.6981F, 0.0F, 0.0F);
		cube_r2.setTextureOffset(2, 8).addBox(-1.0F, -1.0F, -1.0F, 1.0F, 0.0F, 5.0F, 0.0F, false);
		cube_r2.setTextureOffset(5, 0).addBox(2.0F, -1.0F, -1.0F, 1.0F, 0.0F, 5.0F, 0.0F, false);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(-1.0F, 0.0F, -1.0F);
		legs.addChild(cube_r3);
		setRotationAngle(cube_r3, 0.6109F, 0.0F, 0.0F);
		cube_r3.setTextureOffset(4, 8).addBox(-1.0F, -1.0F, -1.0F, 1.0F, 0.0F, 5.0F, 0.0F, false);
		cube_r3.setTextureOffset(6, 8).addBox(2.0F, -1.0F, -1.0F, 1.0F, 0.0F, 5.0F, 0.0F, false);

		head = new ModelRenderer(this);
		head.setRotationPoint(0.0F, 24.0F, 0.0F);

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(1.7F, -4.4F, 0.0F);
		head.addChild(cube_r4);
		setRotationAngle(cube_r4, 0.0F, 0.0F, 0.48F);
		cube_r4.setTextureOffset(0, 0).addBox(-1.0F, -2.0F, 0.0F, 1.0F, 2.0F, 0.0F, 0.0F, false);

		cube_r5 = new ModelRenderer(this);
		cube_r5.setRotationPoint(-1.0F, -5.0F, 0.0F);
		head.addChild(cube_r5);
		setRotationAngle(cube_r5, 0.0F, 0.0F, -0.48F);
		cube_r5.setTextureOffset(0, 2).addBox(-1.0F, -2.0F, 0.0F, 1.0F, 2.0F, 0.0F, 0.0F, false);

		cube_r6 = new ModelRenderer(this);
		cube_r6.setRotationPoint(0.0F, -3.0F, -1.1F);
		head.addChild(cube_r6);
		setRotationAngle(cube_r6, -0.5236F, 0.0F, 0.0F);
		cube_r6.setTextureOffset(11, 11).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 24.0F, 0.0F);
		body.setTextureOffset(0, 0).addBox(-1.0F, -5.0F, 0.0F, 2.0F, 2.0F, 6.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		legs.render(matrixStack, buffer, packedLight, packedOverlay);
		head.render(matrixStack, buffer, packedLight, packedOverlay);
		body.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
	}
}