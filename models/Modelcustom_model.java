// Made with Blockbench 3.7.5
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

public static class Modelcustom_model extends EntityModel<Entity> {
	private final ModelRenderer bb_main;
	private final ModelRenderer cube_r1;
	private final ModelRenderer cube_r2;
	private final ModelRenderer cube_r3;
	private final ModelRenderer cube_r4;
	private final ModelRenderer cube_r5;
	private final ModelRenderer cube_r6;
	private final ModelRenderer cube_r7;
	private final ModelRenderer cube_r8;
	private final ModelRenderer cube_r9;
	private final ModelRenderer cube_r10;
	private final ModelRenderer cube_r11;
	private final ModelRenderer cube_r12;

	public Modelcustom_model() {
		textureWidth = 64;
		textureHeight = 64;

		bb_main = new ModelRenderer(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.setTextureOffset(4, 13).addBox(-2.1F, -3.996F, -4.6084F, 2.0F, 4.0F, 2.0F, 0.0F, false);
		bb_main.setTextureOffset(0, 12).addBox(-2.0F, -14.0F, -6.0F, 6.0F, 6.0F, 14.0F, 0.0F, false);
		bb_main.setTextureOffset(24, 2).addBox(-2.1F, -4.874F, 5.7301F, 2.0F, 4.0F, 2.0F, 0.0F, false);
		bb_main.setTextureOffset(18, 10).addBox(2.0F, -3.996F, -4.6084F, 2.0F, 4.0F, 2.0F, 0.0F, true);
		bb_main.setTextureOffset(2, 22).addBox(2.0F, -4.874F, 5.7301F, 2.0F, 4.0F, 2.0F, 0.0F, true);
		bb_main.setTextureOffset(12, 23).addBox(-0.5F, -17.4F, -11.1F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		bb_main.setTextureOffset(24, 33).addBox(0.0F, -16.3F, -13.1F, 2.0F, 0.0F, 2.0F, 0.0F, false);
		bb_main.setTextureOffset(34, 33).addBox(0.0F, -15.4F, -13.1F, 2.0F, 0.0F, 2.0F, 0.0F, false);
		bb_main.setTextureOffset(18, 32).addBox(1.2F, -18.0F, -9.6F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(22, 35).addBox(-0.2F, -18.0F, -9.6F, 1.0F, 1.0F, 1.0F, 0.0F, true);
		bb_main.setTextureOffset(30, 25).addBox(1.6F, -19.0F, -9.2F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(0, 36).addBox(-0.6F, -19.0F, -9.2F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(18, 36).addBox(-1.4F, -21.0F, -8.1F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(24, 36).addBox(2.1F, -20.0F, -8.7F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(31, 36).addBox(-1.1F, -20.0F, -8.7F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(36, 3).addBox(2.4F, -21.0F, -8.1F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(36, 9).addBox(3.1F, -22.0F, -8.1F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(36, 15).addBox(3.5F, -23.0F, -8.1F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(36, 21).addBox(2.4F, -21.0F, -9.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(36, 27).addBox(2.9F, -22.0F, -9.9F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(36, 33).addBox(-1.4F, -21.0F, -9.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(0, 37).addBox(-1.9F, -22.0F, -9.9F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(6, 37).addBox(-2.1F, -22.0F, -8.1F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(12, 37).addBox(-2.5F, -23.0F, -8.1F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(18, 37).addBox(1.6F, -19.0F, -10.4F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(24, 37).addBox(-0.6F, -19.0F, -10.4F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(30, 37).addBox(2.1F, -20.0F, -10.8F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(36, 1).addBox(-1.1F, -20.0F, -10.8F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(14, 33).addBox(-2.2F, -1.5F, -4.65F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bb_main.setTextureOffset(18, 34).addBox(1.9F, -1.5F, -4.65F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bb_main.setTextureOffset(34, 8).addBox(-2.2F, -1.5F, 5.65F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bb_main.setTextureOffset(34, 20).addBox(1.9F, -1.5F, 5.65F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(0.45F, -19.65F, -9.4F);
		bb_main.addChild(cube_r1);
		setRotationAngle(cube_r1, 0.0F, 0.0F, 0.0873F);
		cube_r1.setTextureOffset(15, 9).addBox(-0.35F, -0.75F, -0.5F, 0.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(1.55F, -19.65F, -9.4F);
		bb_main.addChild(cube_r2);
		setRotationAngle(cube_r2, 0.0F, 0.0F, -0.0873F);
		cube_r2.setTextureOffset(11, 7).addBox(-0.35F, -0.75F, -0.5F, 0.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(0.15F, -20.65F, -8.2F);
		bb_main.addChild(cube_r3);
		setRotationAngle(cube_r3, 0.0F, 0.0F, 0.0873F);
		cube_r3.setTextureOffset(7, 5).addBox(-0.35F, -0.75F, -0.5F, 0.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(1.85F, -20.65F, -8.2F);
		bb_main.addChild(cube_r4);
		setRotationAngle(cube_r4, 0.0F, 0.0F, -0.0873F);
		cube_r4.setTextureOffset(20, 3).addBox(-0.35F, -0.75F, -0.5F, 0.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r5 = new ModelRenderer(this);
		cube_r5.setRotationPoint(2.8F, -17.0F, -9.1F);
		bb_main.addChild(cube_r5);
		setRotationAngle(cube_r5, 0.0F, 0.0F, 0.9163F);
		cube_r5.setTextureOffset(28, 35).addBox(-1.0F, -1.5F, -0.5F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		cube_r6 = new ModelRenderer(this);
		cube_r6.setRotationPoint(-0.8F, -17.0F, -9.1F);
		bb_main.addChild(cube_r6);
		setRotationAngle(cube_r6, 0.0F, 0.0F, -0.9163F);
		cube_r6.setTextureOffset(23, 32).addBox(-1.0F, -1.5F, -0.5F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		cube_r7 = new ModelRenderer(this);
		cube_r7.setRotationPoint(1.0F, -15.0F, -3.9F);
		bb_main.addChild(cube_r7);
		setRotationAngle(cube_r7, -0.7854F, 0.0F, 0.0F);
		cube_r7.setTextureOffset(14, 18).addBox(-2.0F, 0.5F, -3.5F, 4.0F, 4.0F, 5.0F, 0.0F, false);

		cube_r8 = new ModelRenderer(this);
		cube_r8.setRotationPoint(1.0F, -14.0F, 8.7F);
		bb_main.addChild(cube_r8);
		setRotationAngle(cube_r8, 0.7854F, 0.0F, 0.0F);
		cube_r8.setTextureOffset(14, 21).addBox(-1.5F, -1.0F, -2.0F, 3.0F, 2.0F, 4.0F, 0.0F, false);

		cube_r9 = new ModelRenderer(this);
		cube_r9.setRotationPoint(3.05F, -8.0F, 5.0F);
		bb_main.addChild(cube_r9);
		setRotationAngle(cube_r9, 0.1309F, 0.0F, 0.0F);
		cube_r9.setTextureOffset(28, 30).addBox(-1.05F, -2.0F, -1.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
		cube_r9.setTextureOffset(26, 32).addBox(-5.15F, -2.0F, -1.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		cube_r10 = new ModelRenderer(this);
		cube_r10.setRotationPoint(3.05F, -5.7633F, 5.6784F);
		bb_main.addChild(cube_r10);
		setRotationAngle(cube_r10, 0.6981F, 0.0F, 0.0F);
		cube_r10.setTextureOffset(30, 26).addBox(-1.05F, -1.0F, -1.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		cube_r10.setTextureOffset(4, 32).addBox(-5.15F, -1.0F, -1.0F, 2.0F, 3.0F, 2.0F, 0.0F, true);

		cube_r11 = new ModelRenderer(this);
		cube_r11.setRotationPoint(3.05F, -8.1F, -3.7F);
		bb_main.addChild(cube_r11);
		setRotationAngle(cube_r11, 0.2618F, 0.0F, 0.0F);
		cube_r11.setTextureOffset(30, 18).addBox(-1.05F, -1.5F, -1.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		cube_r11.setTextureOffset(6, 6).addBox(-5.15F, -1.5F, -1.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		cube_r12 = new ModelRenderer(this);
		cube_r12.setRotationPoint(3.1F, -4.9051F, -3.5251F);
		bb_main.addChild(cube_r12);
		setRotationAngle(cube_r12, -0.0873F, 0.0F, 0.0F);
		cube_r12.setTextureOffset(14, 14).addBox(-1.1F, -2.1F, -1.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		cube_r12.setTextureOffset(28, 31).addBox(-5.2F, -2.1F, -1.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks,
			float netHeadYaw, float headPitch) {
		// previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		bb_main.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}