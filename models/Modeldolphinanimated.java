// Made with Blockbench 3.6.6

public static class Modeldolphinanimated extends AnimatedEntityModel {

	private final AnimatedModelRenderer body;
	private final AnimatedModelRenderer head;
	private final AnimatedModelRenderer fins;
	private final AnimatedModelRenderer tailfin;
	private final AnimatedModelRenderer tailbody;

	public Modeldolphinanimated() {
		textureWidth = 128;
		textureHeight = 128;
		body = new AnimatedModelRenderer(this);
		body.setRotationPoint(0.0F, 24.0F, 0.0F);
		body.setTextureOffset(24, 60).addBox(-3.0625F, -7.0F, -8.0F, 6.0F, 8.0F, 18.0F, 0.0F, false);
		body.setModelRendererName("body");
		this.registerModelRenderer(body);

		head = new AnimatedModelRenderer(this);
		head.setRotationPoint(0.0F, 24.0F, 0.0F);
		head.setTextureOffset(0, 0).addBox(-1.5625F, -2.875F, -19.25F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		head.setTextureOffset(31, 31).addBox(-2.5625F, -6.375F, -15.25F, 5.0F, 7.0F, 8.0F, 0.0F, false);
		head.setModelRendererName("head");
		this.registerModelRenderer(head);

		fins = new AnimatedModelRenderer(this);
		fins.setRotationPoint(0.0F, 24.0F, 0.0F);
		fins.setTextureOffset(31, 0).addBox(-0.5625F, -9.5077F, -4.8155F, 1.0F, 4.0F, 8.0F, 0.0F, true);
		fins.setTextureOffset(0, 43).addBox(0.9F, 0.075F, -4.425F, 8.0F, 1.0F, 4.0F, 0.0F, false);
		fins.setTextureOffset(42, 0).addBox(-8.9F, 0.075F, -4.425F, 8.0F, 1.0F, 4.0F, 0.0F, false);
		fins.setModelRendererName("fins");
		this.registerModelRenderer(fins);

		tailfin = new AnimatedModelRenderer(this);
		tailfin.setRotationPoint(-0.0208F, 21.5667F, 19.1333F);
		tailfin.setTextureOffset(46, 9).addBox(-6.4292F, -0.4667F, -0.8833F, 8.0F, 1.0F, 4.0F, 0.0F, false);
		tailfin.setTextureOffset(45, 23).addBox(-1.5292F, -0.4667F, -0.8833F, 8.0F, 1.0F, 4.0F, 0.0F, false);
		tailfin.setModelRendererName("tailfin");
		this.registerModelRenderer(tailfin);

		tailbody = new AnimatedModelRenderer(this);
		tailbody.setRotationPoint(-0.0625F, 21.5F, 9.9F);
		tailbody.setTextureOffset(0, 27).addBox(-2.5F, -2.5F, -1.0F, 5.0F, 5.0F, 10.0F, 0.1F, false);
		tailbody.setModelRendererName("tailbody");
		this.registerModelRenderer(tailbody);

		this.rootBones.add(body);
		this.rootBones.add(head);
		this.rootBones.add(fins);
		this.rootBones.add(tailfin);
		this.rootBones.add(tailbody);
	}

	@Override
	public ResourceLocation getAnimationFileLocation() {
		return new ResourceLocation("MODID", "animations/ANIMATIONFILE.json");
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
	}
}