// Made with Blockbench 3.8.4
// Exported for Minecraft version 1.15 - 1.16
// Paste this class into your mod and generate all required imports

public static class Modelfox extends EntityModel<Entity> {
	private final ModelRenderer chest;
	private final ModelRenderer cube_r1;
	private final ModelRenderer belly;
	private final ModelRenderer cube_r2;
	private final ModelRenderer cube_r3;
	private final ModelRenderer head;
	private final ModelRenderer cube_r4;
	private final ModelRenderer cube_r5;
	private final ModelRenderer cube_r6;
	private final ModelRenderer cube_r7;
	private final ModelRenderer cube_r8;
	private final ModelRenderer cube_r9;
	private final ModelRenderer tail;
	private final ModelRenderer cube_r10;
	private final ModelRenderer cube_r11;
	private final ModelRenderer cube_r12;
	private final ModelRenderer cube_r13;
	private final ModelRenderer neck;
	private final ModelRenderer cube_r14;
	private final ModelRenderer cube_r15;
	private final ModelRenderer cube_r16;
	private final ModelRenderer FrontRLeg;
	private final ModelRenderer cube_r17;
	private final ModelRenderer cube_r18;
	private final ModelRenderer FrontLLeg;
	private final ModelRenderer cube_r19;
	private final ModelRenderer cube_r20;
	private final ModelRenderer BackRLeg;
	private final ModelRenderer cube_r21;
	private final ModelRenderer cube_r22;
	private final ModelRenderer cube_r23;
	private final ModelRenderer BackLLeg;
	private final ModelRenderer cube_r24;
	private final ModelRenderer cube_r25;
	private final ModelRenderer cube_r26;

	public Modelfox() {
		textureWidth = 128;
		textureHeight = 128;

		chest = new ModelRenderer(this);
		chest.setRotationPoint(0.0F, 24.0F, 0.0F);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(-2.0F, -11.7622F, -0.3176F);
		chest.addChild(cube_r1);
		setRotationAngle(cube_r1, 0.0524F, 0.0F, 0.0F);
		cube_r1.setTextureOffset(24, 8).addBox(-0.9F, -1.9F, -5.1F, 6.0F, 6.0F, 6.0F, 0.1F, false);

		belly = new ModelRenderer(this);
		belly.setRotationPoint(0.0F, 24.0F, 0.0F);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(-1.0F, -10.9F, 8.6F);
		belly.addChild(cube_r2);
		setRotationAngle(cube_r2, 0.0349F, 0.0F, 0.0F);
		cube_r2.setTextureOffset(0, 32).addBox(-2.0F, -3.0F, -3.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(-0.75F, -10.9F, 1.4F);
		belly.addChild(cube_r3);
		setRotationAngle(cube_r3, -0.0349F, 0.0F, 0.0F);
		cube_r3.setTextureOffset(24, 24).addBox(-2.1F, -3.1F, -0.9F, 6.0F, 6.0F, 6.0F, -0.1F, false);

		head = new ModelRenderer(this);
		head.setRotationPoint(0.2243F, 7.8093F, -7.6877F);

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(0.3257F, 2.1871F, -3.3301F);
		head.addChild(cube_r4);
		setRotationAngle(cube_r4, 0.0524F, 0.0F, 0.0F);
		cube_r4.setTextureOffset(9, 21).addBox(-0.8F, -0.8F, -1.2F, 1.0F, 1.0F, 4.0F, -0.8F, false);

		cube_r5 = new ModelRenderer(this);
		cube_r5.setRotationPoint(-1.5743F, -1.9884F, -0.3115F);
		head.addChild(cube_r5);
		setRotationAngle(cube_r5, 0.6589F, 0.0F, 0.0F);
		cube_r5.setTextureOffset(0, 4).addBox(-1.15F, -1.15F, 0.15F, 2.0F, 2.0F, 0.0F, -0.15F, false);
		cube_r5.setTextureOffset(0, 4).addBox(2.25F, -1.15F, 0.15F, 2.0F, 2.0F, 0.0F, -0.15F, false);

		cube_r6 = new ModelRenderer(this);
		cube_r6.setRotationPoint(1.9257F, -2.8129F, 1.1699F);
		head.addChild(cube_r6);
		setRotationAngle(cube_r6, 0.3229F, 0.0F, 0.0F);
		cube_r6.setTextureOffset(1, 18).addBox(-1.1F, -2.1F, -1.9F, 2.0F, 3.0F, 0.0F, -0.1F, false);

		cube_r7 = new ModelRenderer(this);
		cube_r7.setRotationPoint(-1.5743F, -2.1783F, -0.7267F);
		head.addChild(cube_r7);
		setRotationAngle(cube_r7, 0.3229F, 0.0F, 0.0F);
		cube_r7.setTextureOffset(0, 0).addBox(-1.1F, -2.1F, 0.1F, 2.0F, 3.0F, 0.0F, -0.1F, false);

		cube_r8 = new ModelRenderer(this);
		cube_r8.setRotationPoint(0.2257F, 0.8871F, -2.5301F);
		head.addChild(cube_r8);
		setRotationAngle(cube_r8, 0.3229F, 0.0F, 0.0F);
		cube_r8.setTextureOffset(46, 34).addBox(-1.15F, -1.15F, -1.85F, 2.0F, 2.0F, 4.0F, -0.15F, false);

		cube_r9 = new ModelRenderer(this);
		cube_r9.setRotationPoint(0.2257F, 0.0737F, -0.64F);
		head.addChild(cube_r9);
		setRotationAngle(cube_r9, 0.1745F, 0.0F, 0.0F);
		cube_r9.setTextureOffset(44, 0).addBox(-3.23F, -2.23F, -1.77F, 6.0F, 4.0F, 4.0F, -0.23F, false);

		tail = new ModelRenderer(this);
		tail.setRotationPoint(0.05F, 11.375F, 11.625F);

		cube_r10 = new ModelRenderer(this);
		cube_r10.setRotationPoint(1.2F, 11.675F, 2.575F);
		tail.addChild(cube_r10);
		setRotationAngle(cube_r10, 0.1004F, 0.0F, 0.0F);
		cube_r10.setTextureOffset(40, 62).addBox(-2.1F, -2.1F, 0.1F, 2.0F, 2.0F, 2.0F, -0.1F, false);

		cube_r11 = new ModelRenderer(this);
		cube_r11.setRotationPoint(0.95F, 10.375F, 2.625F);
		tail.addChild(cube_r11);
		setRotationAngle(cube_r11, 0.1876F, 0.0F, 0.0F);
		cube_r11.setTextureOffset(56, 10).addBox(-1.9F, -5.9F, -0.1F, 2.0F, 6.0F, 2.0F, 0.1F, false);

		cube_r12 = new ModelRenderer(this);
		cube_r12.setRotationPoint(0.95F, 2.925F, 0.575F);
		tail.addChild(cube_r12);
		setRotationAngle(cube_r12, 0.432F, 0.0F, 0.0F);
		cube_r12.setTextureOffset(56, 10).addBox(-1.8F, -3.8F, -0.2F, 2.0F, 6.0F, 2.0F, 0.2F, false);

		cube_r13 = new ModelRenderer(this);
		cube_r13.setRotationPoint(0.95F, 0.825F, -2.825F);
		tail.addChild(cube_r13);
		setRotationAngle(cube_r13, 0.2356F, 0.0F, 0.0F);
		cube_r13.setTextureOffset(14, 47).addBox(-2.0F, -2.0F, 0.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);

		neck = new ModelRenderer(this);
		neck.setRotationPoint(0.25F, 24.0F, 0.0F);

		cube_r14 = new ModelRenderer(this);
		cube_r14.setRotationPoint(-0.25F, -10.9716F, -5.3361F);
		neck.addChild(cube_r14);
		setRotationAngle(cube_r14, -1.1606F, 0.0F, 0.0F);
		cube_r14.setTextureOffset(2, 2).addBox(-2.3F, -1.3F, -3.7F, 5.0F, 3.0F, 7.0F, -0.3F, false);

		cube_r15 = new ModelRenderer(this);
		cube_r15.setRotationPoint(0.7F, -12.4216F, -5.6861F);
		neck.addChild(cube_r15);
		setRotationAngle(cube_r15, -1.0428F, 0.0F, 0.0F);
		cube_r15.setTextureOffset(0, 14).addBox(-3.25F, -0.25F, -3.75F, 5.0F, 2.0F, 8.0F, -0.25F, false);

		cube_r16 = new ModelRenderer(this);
		cube_r16.setRotationPoint(3.1F, -13.5F, -8.05F);
		neck.addChild(cube_r16);
		setRotationAngle(cube_r16, -0.7374F, 0.0F, 0.0F);
		cube_r16.setTextureOffset(26, 38).addBox(-6.2F, -4.2F, -1.8F, 6.0F, 4.0F, 6.0F, -0.2F, false);

		FrontRLeg = new ModelRenderer(this);
		FrontRLeg.setRotationPoint(-2.2833F, 16.5167F, -3.7333F);
		FrontRLeg.setTextureOffset(20, 59).addBox(-0.9667F, 5.4833F, -1.2667F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		cube_r17 = new ModelRenderer(this);
		cube_r17.setRotationPoint(-1.0167F, 1.2833F, -0.4667F);
		FrontRLeg.addChild(cube_r17);
		setRotationAngle(cube_r17, 0.192F, 0.0F, 0.0F);
		cube_r17.setTextureOffset(25, 13).addBox(0.1F, -3.9F, -0.1F, 2.0F, 4.0F, 2.0F, 0.1F, false);

		cube_r18 = new ModelRenderer(this);
		cube_r18.setRotationPoint(-0.7667F, 7.2333F, -1.2667F);
		FrontRLeg.addChild(cube_r18);
		setRotationAngle(cube_r18, -0.1222F, 0.0F, 0.0F);
		cube_r18.setTextureOffset(28, 50).addBox(-0.1F, -8.1F, 0.1F, 2.0F, 8.0F, 2.0F, -0.1F, false);

		FrontLLeg = new ModelRenderer(this);
		FrontLLeg.setRotationPoint(2.2833F, 16.5167F, -3.7333F);
		FrontLLeg.setTextureOffset(20, 60).addBox(-1.0333F, 5.4833F, -1.2667F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		cube_r19 = new ModelRenderer(this);
		cube_r19.setRotationPoint(1.0167F, 1.2833F, -0.4667F);
		FrontLLeg.addChild(cube_r19);
		setRotationAngle(cube_r19, 0.192F, 0.0F, 0.0F);
		cube_r19.setTextureOffset(52, 42).addBox(-1.9F, -3.9F, -0.1F, 2.0F, 4.0F, 2.0F, 0.1F, false);

		cube_r20 = new ModelRenderer(this);
		cube_r20.setRotationPoint(1.0167F, 7.2333F, -1.2667F);
		FrontLLeg.addChild(cube_r20);
		setRotationAngle(cube_r20, -0.1222F, 0.0F, 0.0F);
		cube_r20.setTextureOffset(28, 50).addBox(-2.1F, -8.1F, 0.1F, 2.0F, 8.0F, 2.0F, -0.1F, false);

		BackRLeg = new ModelRenderer(this);
		BackRLeg.setRotationPoint(-2.475F, 15.3625F, 9.05F);
		BackRLeg.setTextureOffset(20, 60).addBox(-0.975F, 6.4375F, -1.05F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		cube_r21 = new ModelRenderer(this);
		cube_r21.setRotationPoint(-1.025F, 2.2375F, -1.0F);
		BackRLeg.addChild(cube_r21);
		setRotationAngle(cube_r21, -0.6545F, 0.0F, 0.0F);
		cube_r21.setTextureOffset(29, 16).addBox(-0.05F, -2.05F, 0.05F, 2.0F, 2.0F, 2.0F, -0.05F, false);

		cube_r22 = new ModelRenderer(this);
		cube_r22.setRotationPoint(-0.725F, 7.6375F, -1.05F);
		BackRLeg.addChild(cube_r22);
		setRotationAngle(cube_r22, -0.2443F, 0.0F, 0.0F);
		cube_r22.setTextureOffset(28, 50).addBox(-0.1F, -8.1F, 0.1F, 2.0F, 8.0F, 2.0F, -0.1F, false);

		cube_r23 = new ModelRenderer(this);
		cube_r23.setRotationPoint(0.075F, -0.2125F, 0.0F);
		BackRLeg.addChild(cube_r23);
		setRotationAngle(cube_r23, 0.4014F, 0.0F, 0.0F);
		cube_r23.setTextureOffset(0, 46).addBox(-0.95F, -1.95F, -2.05F, 2.0F, 4.0F, 4.0F, 0.05F, false);

		BackLLeg = new ModelRenderer(this);
		BackLLeg.setRotationPoint(2.425F, 15.3625F, 9.05F);
		BackLLeg.setTextureOffset(20, 60).addBox(-0.975F, 6.4375F, -1.05F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		cube_r24 = new ModelRenderer(this);
		cube_r24.setRotationPoint(1.075F, 2.2375F, -1.0F);
		BackLLeg.addChild(cube_r24);
		setRotationAngle(cube_r24, -0.6545F, 0.0F, 0.0F);
		cube_r24.setTextureOffset(58, 32).addBox(-2.05F, -2.05F, 0.05F, 2.0F, 2.0F, 2.0F, -0.05F, false);

		cube_r25 = new ModelRenderer(this);
		cube_r25.setRotationPoint(1.025F, 7.6375F, -1.05F);
		BackLLeg.addChild(cube_r25);
		setRotationAngle(cube_r25, -0.2443F, 0.0F, 0.0F);
		cube_r25.setTextureOffset(0, 56).addBox(-2.1F, -8.1F, 0.1F, 2.0F, 8.0F, 2.0F, -0.1F, false);

		cube_r26 = new ModelRenderer(this);
		cube_r26.setRotationPoint(-0.025F, -0.2125F, 0.0F);
		BackLLeg.addChild(cube_r26);
		setRotationAngle(cube_r26, 0.4014F, 0.0F, 0.0F);
		cube_r26.setTextureOffset(14, 46).addBox(-0.95F, -1.95F, -2.05F, 2.0F, 4.0F, 4.0F, 0.05F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		chest.render(matrixStack, buffer, packedLight, packedOverlay);
		belly.render(matrixStack, buffer, packedLight, packedOverlay);
		head.render(matrixStack, buffer, packedLight, packedOverlay);
		tail.render(matrixStack, buffer, packedLight, packedOverlay);
		neck.render(matrixStack, buffer, packedLight, packedOverlay);
		FrontRLeg.render(matrixStack, buffer, packedLight, packedOverlay);
		FrontLLeg.render(matrixStack, buffer, packedLight, packedOverlay);
		BackRLeg.render(matrixStack, buffer, packedLight, packedOverlay);
		BackLLeg.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
		this.head.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.head.rotateAngleX = f4 / (180F / (float) Math.PI);
		this.FrontLLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.tail.rotateAngleZ = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.BackRLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.BackLLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.FrontRLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
	}
}