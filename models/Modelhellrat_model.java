// Made with Blockbench 3.7.5
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

public static class Modelhellrat_model extends EntityModel<Entity> {
	private final ModelRenderer Butt;
	private final ModelRenderer Body;
	private final ModelRenderer tail;
	private final ModelRenderer RFront;
	private final ModelRenderer LFront;
	private final ModelRenderer RBack;
	private final ModelRenderer LBack;
	private final ModelRenderer MovementHead;
	private final ModelRenderer head;
	private final ModelRenderer ears;
	private final ModelRenderer whiskers;

	public Modelhellrat_model() {
		textureWidth = 64;
		textureHeight = 64;

		Butt = new ModelRenderer(this);
		Butt.setRotationPoint(0.0F, 24.0F, 0.0F);
		Butt.setTextureOffset(25, 1).addBox(-4.0F, -8.0F, -1.0F, 7.0F, 6.0F, 8.0F, 0.0F, false);

		Body = new ModelRenderer(this);
		Body.setRotationPoint(0.0F, 24.0F, 0.0F);
		Body.setTextureOffset(32, 15).addBox(-3.0F, -7.1F, -7.0F, 5.0F, 5.0F, 7.0F, 0.0F, false);

		tail = new ModelRenderer(this);
		tail.setRotationPoint(-1.0F, 19.0F, 7.0F);
		tail.setTextureOffset(0, 37).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 12.0F, 0.0F, false);

		RFront = new ModelRenderer(this);
		RFront.setRotationPoint(-1.5F, 22.0F, -3.25F);
		RFront.setTextureOffset(5, 23).addBox(-0.5F, -1.0F, -0.75F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		RFront.setTextureOffset(4, 27).addBox(-0.5F, 1.0F, -2.75F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		LFront = new ModelRenderer(this);
		LFront.setRotationPoint(0.5F, 22.0F, -3.25F);
		LFront.setTextureOffset(5, 25).addBox(-0.5F, -1.0F, -0.75F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		LFront.setTextureOffset(4, 28).addBox(-0.5F, 1.0F, -2.75F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		RBack = new ModelRenderer(this);
		RBack.setRotationPoint(-2.5F, 22.0F, 3.75F);
		RBack.setTextureOffset(4, 19).addBox(-0.5F, -1.0F, -0.75F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		RBack.setTextureOffset(3, 21).addBox(-0.5F, 1.0F, -2.75F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		LBack = new ModelRenderer(this);
		LBack.setRotationPoint(1.5F, 22.0F, 3.75F);
		LBack.setTextureOffset(4, 20).addBox(-0.5F, -1.0F, -0.75F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		LBack.setTextureOffset(3, 21).addBox(-0.5F, 1.0F, -2.75F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		MovementHead = new ModelRenderer(this);
		MovementHead.setRotationPoint(0.0F, 20.0F, -7.0F);

		head = new ModelRenderer(this);
		head.setRotationPoint(-0.5F, 0.0F, 0.0F);
		MovementHead.addChild(head);
		head.setTextureOffset(0, 0).addBox(-1.5F, -2.0F, -4.0F, 3.0F, 4.0F, 6.0F, 0.0F, false);

		ears = new ModelRenderer(this);
		ears.setRotationPoint(0.0F, 4.0F, 7.0F);
		MovementHead.addChild(ears);
		ears.setTextureOffset(24, 26).addBox(0.0F, -8.0F, -8.0F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		ears.setTextureOffset(16, 26).addBox(-4.0F, -8.0F, -8.0F, 3.0F, 3.0F, 1.0F, 0.0F, false);

		whiskers = new ModelRenderer(this);
		whiskers.setRotationPoint(0.0F, 4.0F, 7.0F);
		MovementHead.addChild(whiskers);
		whiskers.setTextureOffset(25, 21).addBox(1.0F, -6.0F, -11.0F, 3.0F, 2.0F, 0.0F, 0.0F, false);
		whiskers.setTextureOffset(25, 17).addBox(-5.0F, -6.0F, -11.0F, 3.0F, 2.0F, 0.0F, 0.0F, false);
		whiskers.setTextureOffset(26, 23).addBox(1.0F, -3.0F, -10.0F, 2.0F, 1.0F, 0.0F, 0.0F, false);
		whiskers.setTextureOffset(25, 19).addBox(-4.0F, -3.0F, -10.0F, 2.0F, 1.0F, 0.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		Butt.render(matrixStack, buffer, packedLight, packedOverlay);
		Body.render(matrixStack, buffer, packedLight, packedOverlay);
		tail.render(matrixStack, buffer, packedLight, packedOverlay);
		RFront.render(matrixStack, buffer, packedLight, packedOverlay);
		LFront.render(matrixStack, buffer, packedLight, packedOverlay);
		RBack.render(matrixStack, buffer, packedLight, packedOverlay);
		LBack.render(matrixStack, buffer, packedLight, packedOverlay);
		MovementHead.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		this.head.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.head.rotateAngleX = f4 / (180F / (float) Math.PI);
		this.tail.rotateAngleZ = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
		this.RFront.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.RBack.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.LFront.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.LBack.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
	}
}