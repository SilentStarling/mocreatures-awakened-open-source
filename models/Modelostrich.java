// Made with Blockbench 3.6.6
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

public static class Modelostrich extends EntityModel<Entity> {
	private final ModelRenderer Body;
	private final ModelRenderer Neck;
	private final ModelRenderer Wings;
	private final ModelRenderer Tail;
	private final ModelRenderer NeckFeather;
	private final ModelRenderer FlappyWings;
	private final ModelRenderer Saddle;
	private final ModelRenderer Flags;
	private final ModelRenderer MovementHead;
	private final ModelRenderer Head;
	private final ModelRenderer Horn;
	private final ModelRenderer Beak;
	private final ModelRenderer MovementRLeg;
	private final ModelRenderer MovementLLeg;

	public Modelostrich() {
		textureWidth = 128;
		textureHeight = 128;

		Body = new ModelRenderer(this);
		Body.setRotationPoint(0.0F, 24.0F, 0.0F);
		Body.setTextureOffset(0, 0).addBox(-4.0F, -22.0F, -6.0F, 8.0F, 10.0F, 16.0F, 0.0F, false);

		Neck = new ModelRenderer(this);
		Neck.setRotationPoint(4.7F, 2.7F, 4.0F);
		Neck.setTextureOffset(0, 44).addBox(-6.2F, -2.7F, -12.0F, 3.0F, 8.0F, 3.0F, 0.0F, false);

		Wings = new ModelRenderer(this);
		Wings.setRotationPoint(0.0F, 24.0F, 0.0F);
		Wings.setTextureOffset(0, 26).addBox(4.2F, -22.3F, -3.0F, 1.0F, 4.0F, 14.0F, 0.0F, false);
		Wings.setTextureOffset(32, 26).addBox(-5.2F, -22.3F, -3.0F, 1.0F, 4.0F, 14.0F, 0.0F, false);
		Wings.setTextureOffset(16, 30).addBox(3.28F, -18.3F, -2.5F, 1.0F, 4.0F, 14.0F, 0.0F, false);
		Wings.setTextureOffset(0, 44).addBox(-4.28F, -18.3F, -2.5F, 1.0F, 4.0F, 14.0F, 0.0F, false);

		Tail = new ModelRenderer(this);
		Tail.setRotationPoint(0.0F, 24.0F, 0.0F);
		Tail.setTextureOffset(32, 26).addBox(-0.5F, -21.0F, 13.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);
		Tail.setTextureOffset(16, 26).addBox(-2.6F, -21.0F, 12.5F, 1.0F, 4.0F, 6.0F, 0.0F, false);
		Tail.setTextureOffset(0, 26).addBox(1.6F, -21.0F, 12.5F, 1.0F, 4.0F, 6.0F, 0.0F, false);
		Tail.setTextureOffset(32, 0).addBox(-2.4F, -20.0F, 10.0F, 5.0F, 5.0F, 5.0F, 0.0F, false);

		NeckFeather = new ModelRenderer(this);
		NeckFeather.setRotationPoint(0.0F, 24.0F, 0.0F);
		NeckFeather.setTextureOffset(10, 60).addBox(0.0F, -28.4F, -7.5F, 0.0F, 7.0F, 4.0F, 0.0F, false);
		NeckFeather.setTextureOffset(2, 60).addBox(0.0F, -36.0F, -8.0F, 0.0F, 9.0F, 4.0F, 0.0F, false);

		FlappyWings = new ModelRenderer(this);
		FlappyWings.setRotationPoint(0.0F, 24.0F, 0.0F);
		FlappyWings.setTextureOffset(28, 68).addBox(-18.0F, -24.0F, -4.0F, 15.0F, 2.0F, 2.0F, 0.0F, false);
		FlappyWings.setTextureOffset(28, 72).addBox(3.0F, -24.0F, -4.0F, 15.0F, 2.0F, 2.0F, 0.0F, true);
		FlappyWings.setTextureOffset(2, 76).addBox(3.0F, -23.0F, -2.0F, 15.0F, 0.0F, 15.0F, 0.0F, true);
		FlappyWings.setTextureOffset(2, 76).addBox(-18.0F, -23.0F, -2.0F, 15.0F, 0.0F, 15.0F, 0.0F, false);

		Saddle = new ModelRenderer(this);
		Saddle.setRotationPoint(0.0F, 24.0F, 0.0F);
		Saddle.setTextureOffset(66, 23).addBox(-4.0F, -22.5F, -3.0F, 8.0F, 1.0F, 8.0F, 0.0F, false);
		Saddle.setTextureOffset(66, 32).addBox(-1.5F, -23.0F, -3.0F, 3.0F, 1.0F, 2.0F, 0.0F, false);
		Saddle.setTextureOffset(78, 32).addBox(-4.0F, -23.0F, 3.0F, 8.0F, 1.0F, 2.0F, 0.0F, false);
		Saddle.setTextureOffset(71, 40).addBox(-4.5F, -22.0F, -0.5F, 1.0F, 6.0F, 1.0F, 0.0F, false);
		Saddle.setTextureOffset(82, 40).addBox(3.5F, -22.0F, -0.5F, 1.0F, 6.0F, 1.0F, 0.0F, true);
		Saddle.setTextureOffset(86, 40).addBox(-4.5F, -16.0F, -1.0F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		Saddle.setTextureOffset(86, 40).addBox(3.5F, -16.0F, -1.0F, 1.0F, 2.0F, 2.0F, 0.0F, true);
		Saddle.setTextureOffset(72, 8).addBox(-4.5F, -24.0F, 5.0F, 9.0F, 4.0F, 7.0F, 0.0F, false);

		Flags = new ModelRenderer(this);
		Flags.setRotationPoint(0.0F, -18.5F, 10.5F);
		Flags.setTextureOffset(106, 1).addBox(-0.5F, 4.5F, -4.5F, 1.0F, 17.0F, 1.0F, 0.0F, false);
		Flags.setTextureOffset(101, 14).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 18).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 22).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 26).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 30).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 34).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 38).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 42).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 46).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 50).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 54).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 58).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 62).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 66).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 70).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);
		Flags.setTextureOffset(101, 74).addBox(0.5F, 5.5F, -0.5F, 0.0F, 4.0F, 10.0F, 0.0F, false);

		MovementHead = new ModelRenderer(this);
		MovementHead.setRotationPoint(0.0F, 0.7408F, -6.6864F);
		MovementHead.setTextureOffset(46, 55).addBox(-1.0F, -5.0408F, -0.8136F, 2.0F, 5.0F, 2.0F, 0.0F, false);
		MovementHead.setTextureOffset(38, 55).addBox(-1.0F, -9.7368F, -1.2457F, 2.0F, 5.0F, 2.0F, 0.0F, false);

		Head = new ModelRenderer(this);
		Head.setRotationPoint(0.0F, -10.7408F, -2.8136F);
		MovementHead.addChild(Head);
		Head.setTextureOffset(1, 0).addBox(-1.5F, -0.9F, 0.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);

		Horn = new ModelRenderer(this);
		Horn.setRotationPoint(0.5F, -13.7408F, -3.8136F);
		MovementHead.addChild(Horn);
		Horn.setTextureOffset(70, 68).addBox(-1.0F, -2.2F, 1.1F, 1.0F, 7.0F, 1.0F, 0.0F, false);

		Beak = new ModelRenderer(this);
		Beak.setRotationPoint(0.0F, 23.2592F, 6.6864F);
		MovementHead.addChild(Beak);
		Beak.setTextureOffset(32, 36).addBox(-1.5F, -32.9F, -10.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);
		Beak.setTextureOffset(16, 36).addBox(-1.5F, -33.9F, -10.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);
		Beak.setTextureOffset(0, 36).addBox(-1.0F, -33.9F, -12.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		Beak.setTextureOffset(32, 11).addBox(-1.0F, -32.9F, -12.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		Beak.setTextureOffset(6, 36).addBox(-1.5F, -34.3F, -11.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);
		Beak.setTextureOffset(32, 14).addBox(-1.5F, -32.5F, -11.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);
		Beak.setTextureOffset(30, 27).addBox(-1.0F, -34.3837F, -13.4952F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		Beak.setTextureOffset(24, 26).addBox(-1.0F, -32.4163F, -13.4952F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		MovementRLeg = new ModelRenderer(this);
		MovementRLeg.setRotationPoint(-4.0F, 10.05F, 3.85F);
		MovementRLeg.setTextureOffset(48, 11).addBox(-2.0F, -4.05F, -2.35F, 4.0F, 6.0F, 5.0F, 0.0F, false);
		MovementRLeg.setTextureOffset(54, 0).addBox(-1.5F, 0.95F, -0.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);
		MovementRLeg.setTextureOffset(48, 22).addBox(-1.0F, 3.65F, -0.55F, 2.0F, 10.0F, 2.0F, 0.0F, false);
		MovementRLeg.setTextureOffset(16, 48).addBox(-1.0F, 12.95F, -3.85F, 2.0F, 1.0F, 5.0F, 0.0F, false);

		MovementLLeg = new ModelRenderer(this);
		MovementLLeg.setRotationPoint(4.0F, 10.05F, 3.85F);
		MovementLLeg.setTextureOffset(41, 44).addBox(-2.0F, -4.05F, -2.35F, 4.0F, 6.0F, 5.0F, 0.0F, false);
		MovementLLeg.setTextureOffset(53, 31).addBox(-1.5F, 0.95F, -0.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);
		MovementLLeg.setTextureOffset(30, 48).addBox(-1.0F, 3.65F, -0.55F, 2.0F, 10.0F, 2.0F, 0.0F, false);
		MovementLLeg.setTextureOffset(0, 10).addBox(-1.0F, 12.95F, -3.85F, 2.0F, 1.0F, 5.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		Body.render(matrixStack, buffer, packedLight, packedOverlay);
		Neck.render(matrixStack, buffer, packedLight, packedOverlay);
		Wings.render(matrixStack, buffer, packedLight, packedOverlay);
		Tail.render(matrixStack, buffer, packedLight, packedOverlay);
		NeckFeather.render(matrixStack, buffer, packedLight, packedOverlay);
		FlappyWings.render(matrixStack, buffer, packedLight, packedOverlay);
		Saddle.render(matrixStack, buffer, packedLight, packedOverlay);
		Flags.render(matrixStack, buffer, packedLight, packedOverlay);
		MovementHead.render(matrixStack, buffer, packedLight, packedOverlay);
		MovementRLeg.render(matrixStack, buffer, packedLight, packedOverlay);
		MovementLLeg.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
		this.MovementHead.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.MovementHead.rotateAngleX = f4 / (180F / (float) Math.PI);
		this.MovementRLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.MovementLLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
	}
}