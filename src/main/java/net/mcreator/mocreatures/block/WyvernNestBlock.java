
package net.mcreator.mocreatures.block;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.world.IBlockReader;
import net.minecraft.util.math.BlockPos;
import net.minecraft.loot.LootContext;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.item.BlockItem;
import net.minecraft.block.material.Material;
import net.minecraft.block.SoundType;
import net.minecraft.block.BlockState;
import net.minecraft.block.Block;

import net.mcreator.mocreatures.itemgroup.MocItemGroup;
import net.mcreator.mocreatures.MocreaturesModElements;

import java.util.List;
import java.util.Collections;

@MocreaturesModElements.ModElement.Tag
public class WyvernNestBlock extends MocreaturesModElements.ModElement {
	@ObjectHolder("mocreatures:wyvern_nest")
	public static final Block block = null;

	public WyvernNestBlock(MocreaturesModElements instance) {
		super(instance, 492);
	}

	@Override
	public void initElements() {
		elements.blocks.add(() -> new CustomBlock());
		elements.items.add(() -> new BlockItem(block, new Item.Properties().group(MocItemGroup.tab)).setRegistryName(block.getRegistryName()));
	}

	public static class CustomBlock extends Block {
		public CustomBlock() {
			super(Block.Properties.create(Material.WOOD).sound(SoundType.VINE).hardnessAndResistance(1f, 10f).setLightLevel(s -> 0));
			setRegistryName("wyvern_nest");
		}

		@Override
		public int getOpacity(BlockState state, IBlockReader worldIn, BlockPos pos) {
			return 15;
		}

		@Override
		public List<ItemStack> getDrops(BlockState state, LootContext.Builder builder) {
			List<ItemStack> dropsOriginal = super.getDrops(state, builder);
			if (!dropsOriginal.isEmpty())
				return dropsOriginal;
			return Collections.singletonList(new ItemStack(this, 1));
		}
	}
}
