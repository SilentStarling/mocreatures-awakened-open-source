
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.AntEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class AntRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(AntEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new Modelant_model(), 0.3f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/ant.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.7.4
	// Exported for Minecraft version 1.15
	// Paste this class into your mod and generate all required imports
	public static class Modelant_model extends EntityModel<Entity> {
		private final ModelRenderer legs;
		private final ModelRenderer cube_r1;
		private final ModelRenderer cube_r2;
		private final ModelRenderer cube_r3;
		private final ModelRenderer head;
		private final ModelRenderer cube_r4;
		private final ModelRenderer cube_r5;
		private final ModelRenderer cube_r6;
		private final ModelRenderer body;

		public Modelant_model() {
			textureWidth = 32;
			textureHeight = 32;
			legs = new ModelRenderer(this);
			legs.setRotationPoint(0.0F, 24.0F, 0.0F);
			cube_r1 = new ModelRenderer(this);
			cube_r1.setRotationPoint(-1.0F, -2.0F, 3.0F);
			legs.addChild(cube_r1);
			setRotationAngle(cube_r1, -0.6981F, 0.0F, 0.0F);
			cube_r1.setTextureOffset(0, 8).addBox(-1.0F, -1.0F, -1.0F, 1.0F, 0.0F, 5.0F, 0.0F, false);
			cube_r1.setTextureOffset(7, 0).addBox(2.0F, -1.0F, -1.0F, 1.0F, 0.0F, 5.0F, 0.0F, false);
			cube_r2 = new ModelRenderer(this);
			cube_r2.setRotationPoint(-1.0F, -2.0F, 2.0F);
			legs.addChild(cube_r2);
			setRotationAngle(cube_r2, -0.6981F, 0.0F, 0.0F);
			cube_r2.setTextureOffset(2, 8).addBox(-1.0F, -1.0F, -1.0F, 1.0F, 0.0F, 5.0F, 0.0F, false);
			cube_r2.setTextureOffset(5, 0).addBox(2.0F, -1.0F, -1.0F, 1.0F, 0.0F, 5.0F, 0.0F, false);
			cube_r3 = new ModelRenderer(this);
			cube_r3.setRotationPoint(-1.0F, 0.0F, -1.0F);
			legs.addChild(cube_r3);
			setRotationAngle(cube_r3, 0.6109F, 0.0F, 0.0F);
			cube_r3.setTextureOffset(4, 8).addBox(-1.0F, -1.0F, -1.0F, 1.0F, 0.0F, 5.0F, 0.0F, false);
			cube_r3.setTextureOffset(6, 8).addBox(2.0F, -1.0F, -1.0F, 1.0F, 0.0F, 5.0F, 0.0F, false);
			head = new ModelRenderer(this);
			head.setRotationPoint(0.0F, 24.0F, 0.0F);
			cube_r4 = new ModelRenderer(this);
			cube_r4.setRotationPoint(1.7F, -4.4F, 0.0F);
			head.addChild(cube_r4);
			setRotationAngle(cube_r4, 0.0F, 0.0F, 0.48F);
			cube_r4.setTextureOffset(0, 0).addBox(-1.0F, -2.0F, 0.0F, 1.0F, 2.0F, 0.0F, 0.0F, false);
			cube_r5 = new ModelRenderer(this);
			cube_r5.setRotationPoint(-1.0F, -5.0F, 0.0F);
			head.addChild(cube_r5);
			setRotationAngle(cube_r5, 0.0F, 0.0F, -0.48F);
			cube_r5.setTextureOffset(0, 2).addBox(-1.0F, -2.0F, 0.0F, 1.0F, 2.0F, 0.0F, 0.0F, false);
			cube_r6 = new ModelRenderer(this);
			cube_r6.setRotationPoint(0.0F, -3.0F, -1.1F);
			head.addChild(cube_r6);
			setRotationAngle(cube_r6, -0.5236F, 0.0F, 0.0F);
			cube_r6.setTextureOffset(11, 11).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			body = new ModelRenderer(this);
			body.setRotationPoint(0.0F, 24.0F, 0.0F);
			body.setTextureOffset(0, 0).addBox(-1.0F, -5.0F, 0.0F, 2.0F, 2.0F, 6.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			legs.render(matrixStack, buffer, packedLight, packedOverlay);
			head.render(matrixStack, buffer, packedLight, packedOverlay);
			body.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {

		}
	}

}
