
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.BearPolarEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class BearPolarRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(BearPolarEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelBear(), 0.7f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/bearpolar.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 4.2.2
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelBear extends EntityModel<Entity> {
		private final ModelRenderer Head;
		private final ModelRenderer cube_r2_r1;
		private final ModelRenderer cube_r1_r1;
		private final ModelRenderer cube_r3_r1;
		private final ModelRenderer Body;
		private final ModelRenderer cube_r5_r1;
		private final ModelRenderer cube_r7_r1;
		private final ModelRenderer cube_r6_r1;
		private final ModelRenderer RFrontLeg;
		private final ModelRenderer cube_r9_r1;
		private final ModelRenderer LFrontLeg;
		private final ModelRenderer cube_r9_r2;
		private final ModelRenderer RBackLeg;
		private final ModelRenderer cube_r8_r1;
		private final ModelRenderer LBackLeg;
		private final ModelRenderer cube_r8_r2;

		public ModelBear() {
			textureWidth = 128;
			textureHeight = 128;
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, 9.2475F, -8.5644F);
			Head.setTextureOffset(11, 64).addBox(-1.5F, 2.7525F, -9.0356F, 3.0F, 2.0F, 5.0F, 0.0F, false);
			cube_r2_r1 = new ModelRenderer(this);
			cube_r2_r1.setRotationPoint(0.0F, 14.7525F, 8.5644F);
			Head.addChild(cube_r2_r1);
			setRotationAngle(cube_r2_r1, 0.0F, 0.3054F, -0.1309F);
			cube_r2_r1.setTextureOffset(0, 4).addBox(0.0F, -20.0F, -11.0F, 3.0F, 3.0F, 1.0F, 0.0F, false);
			cube_r1_r1 = new ModelRenderer(this);
			cube_r1_r1.setRotationPoint(0.0F, 14.7525F, 8.5644F);
			Head.addChild(cube_r1_r1);
			setRotationAngle(cube_r1_r1, 0.0F, -0.3054F, 0.1309F);
			cube_r1_r1.setTextureOffset(0, 0).addBox(-3.0F, -20.0F, -11.0F, 3.0F, 3.0F, 1.0F, 0.0F, false);
			cube_r3_r1 = new ModelRenderer(this);
			cube_r3_r1.setRotationPoint(0.0F, 14.7525F, 8.5644F);
			Head.addChild(cube_r3_r1);
			setRotationAngle(cube_r3_r1, 0.1309F, 0.0F, 0.0F);
			cube_r3_r1.setTextureOffset(38, 27).addBox(-2.0F, -16.3F, -16.1F, 4.0F, 3.0F, 5.0F, 0.0F, false);
			cube_r3_r1.setTextureOffset(33, 36).addBox(-4.0F, -19.0F, -12.0F, 8.0F, 8.0F, 5.0F, 0.0F, false);
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, 24.0F, 0.0F);
			Body.setTextureOffset(0, 0).addBox(-5.0F, -19.0F, -5.0F, 10.0F, 10.0F, 10.0F, 0.0F, false);
			cube_r5_r1 = new ModelRenderer(this);
			cube_r5_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
			Body.addChild(cube_r5_r1);
			setRotationAngle(cube_r5_r1, 0.4363F, 0.0F, 0.0F);
			cube_r5_r1.setTextureOffset(14, 41).addBox(-1.5F, -9.3F, 17.3F, 3.0F, 3.0F, 3.0F, 0.0F, false);
			cube_r7_r1 = new ModelRenderer(this);
			cube_r7_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
			Body.addChild(cube_r7_r1);
			setRotationAngle(cube_r7_r1, -0.4363F, 0.0F, 0.0F);
			cube_r7_r1.setTextureOffset(0, 20).addBox(-4.5F, -19.4022F, -3.7929F, 9.0F, 11.0F, 10.0F, 0.0F, false);
			cube_r6_r1 = new ModelRenderer(this);
			cube_r6_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
			Body.addChild(cube_r6_r1);
			setRotationAngle(cube_r6_r1, 0.2618F, 0.0F, 0.0F);
			cube_r6_r1.setTextureOffset(33, 13).addBox(-3.5F, -19.2134F, -6.9749F, 7.0F, 7.0F, 7.0F, 0.0F, false);
			RFrontLeg = new ModelRenderer(this);
			RFrontLeg.setRotationPoint(-4.0667F, 14.1136F, -2.3961F);
			RFrontLeg.setTextureOffset(0, 59).addBox(-1.9333F, 2.8864F, -1.6039F, 4.0F, 6.0F, 4.0F, 0.0F, false);
			RFrontLeg.setTextureOffset(60, 0).addBox(-2.1333F, 7.8864F, -2.7039F, 4.0F, 2.0F, 5.0F, 0.0F, false);
			cube_r9_r1 = new ModelRenderer(this);
			cube_r9_r1.setRotationPoint(4.0667F, 9.8864F, 2.3961F);
			RFrontLeg.addChild(cube_r9_r1);
			setRotationAngle(cube_r9_r1, 0.2618F, 0.0F, 0.0F);
			cube_r9_r1.setTextureOffset(40, 0).addBox(-6.5F, -14.0F, -2.5F, 5.0F, 8.0F, 5.0F, 0.0F, false);
			LFrontLeg = new ModelRenderer(this);
			LFrontLeg.setRotationPoint(3.9667F, 14.0492F, -2.4133F);
			LFrontLeg.setTextureOffset(56, 9).addBox(-1.9667F, 2.9508F, -1.5867F, 4.0F, 6.0F, 4.0F, 0.0F, false);
			LFrontLeg.setTextureOffset(29, 62).addBox(-2.0667F, 7.9508F, -2.6867F, 4.0F, 2.0F, 5.0F, 0.0F, false);
			cube_r9_r2 = new ModelRenderer(this);
			cube_r9_r2.setRotationPoint(-3.9667F, 9.9508F, 2.4133F);
			LFrontLeg.addChild(cube_r9_r2);
			setRotationAngle(cube_r9_r2, 0.2618F, 0.0F, 0.0F);
			cube_r9_r2.setTextureOffset(34, 49).addBox(1.5F, -14.2F, -2.5F, 5.0F, 8.0F, 5.0F, 0.0F, false);
			RBackLeg = new ModelRenderer(this);
			RBackLeg.setRotationPoint(-3.6333F, 14.1565F, 9.5654F);
			RBackLeg.setTextureOffset(54, 31).addBox(-1.8667F, 2.8435F, -1.5654F, 4.0F, 6.0F, 4.0F, 0.0F, false);
			RBackLeg.setTextureOffset(54, 44).addBox(-1.7667F, 7.8435F, -2.6654F, 4.0F, 2.0F, 5.0F, 0.0F, false);
			cube_r8_r1 = new ModelRenderer(this);
			cube_r8_r1.setRotationPoint(3.6333F, 9.8435F, -9.5654F);
			RBackLeg.addChild(cube_r8_r1);
			setRotationAngle(cube_r8_r1, -0.1745F, 0.0F, 0.0F);
			cube_r8_r1.setTextureOffset(0, 41).addBox(-6.0F, -15.0F, 4.5F, 4.0F, 8.0F, 6.0F, 0.0F, false);
			LBackLeg = new ModelRenderer(this);
			LBackLeg.setRotationPoint(3.6333F, 14.1391F, 9.4669F);
			LBackLeg.setTextureOffset(56, 22).addBox(-2.2333F, 7.8609F, -2.5669F, 4.0F, 2.0F, 5.0F, 0.0F, false);
			LBackLeg.setTextureOffset(54, 54).addBox(-2.1333F, 2.8609F, -1.4669F, 4.0F, 6.0F, 4.0F, 0.0F, false);
			cube_r8_r2 = new ModelRenderer(this);
			cube_r8_r2.setRotationPoint(-3.6333F, 9.8609F, -9.4669F);
			LBackLeg.addChild(cube_r8_r2);
			setRotationAngle(cube_r8_r2, -0.1745F, 0.0F, 0.0F);
			cube_r8_r2.setTextureOffset(14, 49).addBox(2.0F, -15.0F, 4.2F, 4.0F, 8.0F, 6.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Head.render(matrixStack, buffer, packedLight, packedOverlay);
			Body.render(matrixStack, buffer, packedLight, packedOverlay);
			RFrontLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			LFrontLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			RBackLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			LBackLeg.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
			this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.LBackLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.RFrontLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.LFrontLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.RBackLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		}
	}

}
