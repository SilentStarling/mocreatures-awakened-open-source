
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.BirdPigeonEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class BirdPigeonRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(BirdPigeonEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelBirdPidgeon(), 0.2f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/birdpidgeon.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 4.0.5
	// Exported for Minecraft version 1.7 - 1.12
	// Paste this class into your mod and generate all required imports
	// Made by WerelWolf
	public static class ModelBirdPidgeon extends EntityModel<Entity> {
		private final ModelRenderer head;
		private final ModelRenderer body;
		private final ModelRenderer leftleg;
		private final ModelRenderer rightleg;
		private final ModelRenderer rwing;
		private final ModelRenderer lwing_r1;
		private final ModelRenderer lwing_r2;
		private final ModelRenderer lwing;
		private final ModelRenderer lwing_r3;
		private final ModelRenderer lwing_r4;
		private final ModelRenderer beak;
		private final ModelRenderer beak_r1;
		private final ModelRenderer tail;
		private final ModelRenderer tail_r1;
		private final ModelRenderer tail_r2;

		public ModelBirdPidgeon() {
			textureWidth = 64;
			textureHeight = 32;
			head = new ModelRenderer(this);
			head.setRotationPoint(0.0F, 15.0F, -3.0F);
			addBoxHelper(head, 0, 0, -1.5F, -2.0F, -2.0F, 3, 3, 3, 0.0F, false);
			body = new ModelRenderer(this);
			body.setRotationPoint(0.0F, 15.0F, 2.0F);
			setRotationAngle(body, 1.0472F, 0.0F, 0.0F);
			addBoxHelper(body, 0, 9, -2.0F, -3.732F, -4.1436F, 4, 8, 4, 0.0F, false);
			leftleg = new ModelRenderer(this);
			leftleg.setRotationPoint(0.0F, 24.0F, 0.0F);
			addBoxHelper(leftleg, 26, 0, 0.0F, -4.0F, 0.0F, 3, 4, 3, 0.0F, false);
			rightleg = new ModelRenderer(this);
			rightleg.setRotationPoint(0.0F, 24.0F, 0.0F);
			addBoxHelper(rightleg, 26, 0, -3.0F, -4.0F, 0.0F, 3, 4, 3, 0.0F, false);
			rwing = new ModelRenderer(this);
			rwing.setRotationPoint(-3.0F, 18.0F, 2.0F);
			addBoxHelper(rwing, 28, 18, 0.0F, -4.0F, -3.0F, 1, 2, 1, 0.0F, false);
			addBoxHelper(rwing, 52, 27, 0.5F, -4.0F, -2.0F, 0, 2, 3, 0.0F, false);
			lwing_r1 = new ModelRenderer(this);
			lwing_r1.setRotationPoint(1.0F, -2.1397F, 0.6575F);
			rwing.addChild(lwing_r1);
			setRotationAngle(lwing_r1, 0.48F, 0.0F, 0.0F);
			addBoxHelper(lwing_r1, 58, 25, -0.5F, -1.5649F, -2.3087F, 0, 4, 3, 0.0F, false);
			addBoxHelper(lwing_r1, 28, 18, -1.0F, -1.5649F, -3.3087F, 1, 4, 1, 0.0F, false);
			lwing_r2 = new ModelRenderer(this);
			lwing_r2.setRotationPoint(1.0F, -0.3313F, -0.469F);
			rwing.addChild(lwing_r2);
			setRotationAngle(lwing_r2, 1.2217F, 0.0F, 0.0F);
			addBoxHelper(lwing_r2, 46, 23, -0.5F, 0.0F, -2.0F, 0, 6, 3, 0.0F, false);
			addBoxHelper(lwing_r2, 28, 18, -1.0F, 0.0F, -2.0F, 1, 3, 1, 0.0F, false);
			lwing = new ModelRenderer(this);
			lwing.setRotationPoint(3.0F, 18.0F, 2.0F);
			addBoxHelper(lwing, 28, 18, -1.0F, -4.0F, -3.0F, 1, 2, 1, 0.0F, false);
			addBoxHelper(lwing, 52, 27, -0.5F, -4.0F, -2.0F, 0, 2, 3, 0.0F, false);
			lwing_r3 = new ModelRenderer(this);
			lwing_r3.setRotationPoint(0.0F, -2.1397F, 0.6575F);
			lwing.addChild(lwing_r3);
			setRotationAngle(lwing_r3, 0.48F, 0.0F, 0.0F);
			addBoxHelper(lwing_r3, 58, 25, -0.5F, -1.5649F, -2.3087F, 0, 4, 3, 0.0F, false);
			addBoxHelper(lwing_r3, 28, 18, -1.0F, -1.5649F, -3.3087F, 1, 4, 1, 0.0F, false);
			lwing_r4 = new ModelRenderer(this);
			lwing_r4.setRotationPoint(0.0F, -0.3313F, -0.469F);
			lwing.addChild(lwing_r4);
			setRotationAngle(lwing_r4, 1.2217F, 0.0F, 0.0F);
			addBoxHelper(lwing_r4, 46, 23, -0.5F, 0.0F, -2.0F, 0, 6, 3, 0.0F, false);
			addBoxHelper(lwing_r4, 28, 18, -1.0F, 0.0F, -2.0F, 1, 3, 1, 0.0F, false);
			beak = new ModelRenderer(this);
			beak.setRotationPoint(0.0F, 15.0F, -6.0F);
			addBoxHelper(beak, 14, 0, -0.5F, -0.5F, 0.0F, 1, 1, 2, 0.0F, false);
			beak_r1 = new ModelRenderer(this);
			beak_r1.setRotationPoint(0.0F, 5.5742F, 9.4527F);
			beak.addChild(beak_r1);
			setRotationAngle(beak_r1, 0.4363F, 0.0F, 0.0F);
			addBoxHelper(beak_r1, 40, 29, -0.5F, -9.5F, -6.0F, 1, 1, 2, 0.0F, false);
			tail = new ModelRenderer(this);
			tail.setRotationPoint(0.0F, 17.0F, 7.0F);
			setRotationAngle(tail, 0.2618F, 0.0F, 0.0F);
			addBoxHelper(tail, 16, 27, -1.0F, 0.2675F, -3.1027F, 2, 0, 5, 0.0F, false);
			tail_r1 = new ModelRenderer(this);
			tail_r1.setRotationPoint(0.0F, 5.7675F, -2.3616F);
			tail.addChild(tail_r1);
			setRotationAngle(tail_r1, 0.0F, -0.4363F, 0.0F);
			addBoxHelper(tail_r1, 0, 23, -1.0F, -5.9659F, 0.2588F, 1, 1, 4, 0.0F, false);
			addBoxHelper(tail_r1, 16, 27, -1.0F, -5.5F, -0.7412F, 2, 0, 5, 0.0F, false);
			tail_r2 = new ModelRenderer(this);
			tail_r2.setRotationPoint(0.0F, 5.7675F, -2.3616F);
			tail.addChild(tail_r2);
			setRotationAngle(tail_r2, 0.0F, 0.4363F, 0.0F);
			addBoxHelper(tail_r2, 16, 27, -1.0F, -5.5F, -0.7412F, 2, 0, 5, 0.0F, false);
			addBoxHelper(tail_r2, 0, 23, 0.0F, -5.9659F, 0.2588F, 1, 1, 4, 0.0F, false);
		}

		@Override
		public void render(MatrixStack ms, IVertexBuilder vb, int i1, int i2, float f1, float f2, float f3, float f4) {
			head.render(ms, vb, i1, i2, f1, f2, f3, f4);
			body.render(ms, vb, i1, i2, f1, f2, f3, f4);
			leftleg.render(ms, vb, i1, i2, f1, f2, f3, f4);
			rightleg.render(ms, vb, i1, i2, f1, f2, f3, f4);
			rwing.render(ms, vb, i1, i2, f1, f2, f3, f4);
			lwing.render(ms, vb, i1, i2, f1, f2, f3, f4);
			beak.render(ms, vb, i1, i2, f1, f2, f3, f4);
			tail.render(ms, vb, i1, i2, f1, f2, f3, f4);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {

			this.leftleg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.head.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.rightleg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		}
	}

	@OnlyIn(Dist.CLIENT)
	public static void addBoxHelper(ModelRenderer renderer, int texU, int texV, float x, float y, float z, int dx, int dy, int dz, float delta) {
		addBoxHelper(renderer, texU, texV, x, y, z, dx, dy, dz, delta, renderer.mirror);
	}

	@OnlyIn(Dist.CLIENT)
	public static void addBoxHelper(ModelRenderer renderer, int texU, int texV, float x, float y, float z, int dx, int dy, int dz, float delta,
			boolean mirror) {
		renderer.mirror = mirror;
		renderer.addBox("", x, y, z, dx, dy, dz, delta, texU, texV);
	}
}
