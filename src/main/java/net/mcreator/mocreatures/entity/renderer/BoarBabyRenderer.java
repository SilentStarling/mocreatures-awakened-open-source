package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.BoarBabyEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class BoarBabyRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(BoarBabyEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelMoCModelBoar(), 0.3f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						BoarBabyEntity.CustomEntity _boar = (BoarBabyEntity.CustomEntity) entity;
						return new ResourceLocation("mocreatures:textures/moc_boar_" + String.valueOf(_boar.getVariant()) + ".png");
					}
				};
			});
		}
	}

	// Made with Blockbench 4.2.2
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelMoCModelBoar extends EntityModel<Entity> {
		private final ModelRenderer Body;
		private final ModelRenderer BodyMane;
		private final ModelRenderer Tail;
		private final ModelRenderer HeadMovement;
		private final ModelRenderer Head;
		private final ModelRenderer Trout;
		private final ModelRenderer Tusks;
		private final ModelRenderer Jaw;
		private final ModelRenderer LeftEar;
		private final ModelRenderer RightEar;
		private final ModelRenderer HeadMane;
		private final ModelRenderer LFrontMovement;
		private final ModelRenderer UpperLegLeft;
		private final ModelRenderer LowerLegLeft;
		private final ModelRenderer RFrontMovement;
		private final ModelRenderer UpperLegRight;
		private final ModelRenderer LowerLegRight;
		private final ModelRenderer LHindMovement;
		private final ModelRenderer UpperHindLegLeft;
		private final ModelRenderer LowerHindLegLeft;
		private final ModelRenderer RHindMovement;
		private final ModelRenderer UpperHindLegRight;
		private final ModelRenderer LowerHindLegRight;

		public ModelMoCModelBoar() {
			textureWidth = 64;
			textureHeight = 64;
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, 11.0F, -5.0F);
			setRotationAngle(Body, -0.0873F, 0.0F, 0.0F);
			Body.setTextureOffset(24, 0).addBox(-3.5F, 0.0F, 0.0F, 7.0F, 8.0F, 13.0F, 0.0F, false);
			BodyMane = new ModelRenderer(this);
			BodyMane.setRotationPoint(0.0F, 11.3F, -4.0F);
			setRotationAngle(BodyMane, -0.2618F, 0.0F, 0.0F);
			BodyMane.setTextureOffset(0, 27).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 2.0F, 9.0F, 0.0F, false);
			Tail = new ModelRenderer(this);
			Tail.setRotationPoint(0.0F, 13.4924F, 8.1743F);
			setRotationAngle(Tail, 0.0873F, 0.0F, 0.0F);
			Tail.setTextureOffset(60, 38).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 5.0F, 1.0F, 0.0F, false);
			HeadMovement = new ModelRenderer(this);
			HeadMovement.setRotationPoint(0.0F, 15.0F, -7.0F);
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, -4.0F, 2.0F);
			HeadMovement.addChild(Head);
			setRotationAngle(Head, 0.2618F, 0.0F, 0.0F);
			Head.setTextureOffset(0, 0).addBox(-3.0F, 0.0F, -5.0F, 6.0F, 6.0F, 5.0F, 0.0F, false);
			Trout = new ModelRenderer(this);
			Trout.setRotationPoint(0.0F, -4.0F, 2.0F);
			HeadMovement.addChild(Trout);
			setRotationAngle(Trout, 0.3491F, 0.0F, 0.0F);
			Trout.setTextureOffset(0, 11).addBox(-1.5F, 1.5F, -9.5F, 3.0F, 3.0F, 5.0F, 0.0F, false);
			Tusks = new ModelRenderer(this);
			Tusks.setRotationPoint(0.0F, -4.0F, 2.0F);
			HeadMovement.addChild(Tusks);
			setRotationAngle(Tusks, 0.3491F, 0.0F, 0.0F);
			Tusks.setTextureOffset(0, 24).addBox(-2.0F, 3.0F, -8.0F, 4.0F, 2.0F, 1.0F, 0.0F, false);
			Jaw = new ModelRenderer(this);
			Jaw.setRotationPoint(0.0F, -4.0F, 2.0F);
			HeadMovement.addChild(Jaw);
			setRotationAngle(Jaw, 0.2618F, 0.0F, 0.0F);
			Jaw.setTextureOffset(0, 19).addBox(-1.0F, 4.9F, -8.5F, 2.0F, 1.0F, 4.0F, 0.0F, false);
			LeftEar = new ModelRenderer(this);
			LeftEar.setRotationPoint(0.0F, -4.0F, 2.0F);
			HeadMovement.addChild(LeftEar);
			setRotationAngle(LeftEar, 0.6981F, 0.0F, 0.3491F);
			LeftEar.setTextureOffset(16, 11).addBox(1.0F, -4.0F, -2.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			RightEar = new ModelRenderer(this);
			RightEar.setRotationPoint(0.0F, -4.0F, 2.0F);
			HeadMovement.addChild(RightEar);
			setRotationAngle(RightEar, 0.6981F, 0.0F, -0.3491F);
			RightEar.setTextureOffset(16, 17).addBox(-3.0F, -4.0F, -2.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			HeadMane = new ModelRenderer(this);
			HeadMane.setRotationPoint(0.0F, -4.0F, 2.0F);
			HeadMovement.addChild(HeadMane);
			setRotationAngle(HeadMane, 0.4363F, 0.0F, 0.0F);
			HeadMane.setTextureOffset(23, 0).addBox(-1.0F, -2.0F, -5.0F, 2.0F, 2.0F, 5.0F, 0.0F, false);
			LFrontMovement = new ModelRenderer(this);
			LFrontMovement.setRotationPoint(3.0F, 17.0F, -3.0F);
			UpperLegLeft = new ModelRenderer(this);
			UpperLegLeft.setRotationPoint(0.5F, -1.0F, 0.5F);
			LFrontMovement.addChild(UpperLegLeft);
			setRotationAngle(UpperLegLeft, 0.1745F, 0.0F, 0.0F);
			UpperLegLeft.setTextureOffset(24, 21).addBox(0.0F, -2.0F, -2.0F, 1.0F, 5.0F, 3.0F, 0.0F, false);
			LowerLegLeft = new ModelRenderer(this);
			LowerLegLeft.setRotationPoint(0.5F, -1.0F, 0.5F);
			LFrontMovement.addChild(LowerLegLeft);
			LowerLegLeft.setTextureOffset(24, 29).addBox(-1.5F, 2.0F, -1.0F, 2.0F, 6.0F, 2.0F, 0.0F, false);
			RFrontMovement = new ModelRenderer(this);
			RFrontMovement.setRotationPoint(-4.0F, 17.0F, -3.0F);
			UpperLegRight = new ModelRenderer(this);
			UpperLegRight.setRotationPoint(0.5F, -1.0F, 0.5F);
			RFrontMovement.addChild(UpperLegRight);
			setRotationAngle(UpperLegRight, 0.1745F, 0.0F, 0.0F);
			UpperLegRight.setTextureOffset(32, 21).addBox(-1.0F, -2.0F, -2.0F, 1.0F, 5.0F, 3.0F, 0.0F, false);
			LowerLegRight = new ModelRenderer(this);
			LowerLegRight.setRotationPoint(0.5F, -1.0F, 0.5F);
			RFrontMovement.addChild(LowerLegRight);
			LowerLegRight.setTextureOffset(32, 29).addBox(-0.5F, 2.0F, -1.0F, 2.0F, 6.0F, 2.0F, 0.0F, false);
			LHindMovement = new ModelRenderer(this);
			LHindMovement.setRotationPoint(4.0F, 17.0F, 5.5F);
			UpperHindLegLeft = new ModelRenderer(this);
			UpperHindLegLeft.setRotationPoint(-1.0F, -1.0F, 0.0F);
			LHindMovement.addChild(UpperHindLegLeft);
			setRotationAngle(UpperHindLegLeft, -0.2618F, 0.0F, 0.0F);
			UpperHindLegLeft.setTextureOffset(54, 21).addBox(0.5F, -2.0F, -2.0F, 1.0F, 5.0F, 4.0F, 0.0F, false);
			LowerHindLegLeft = new ModelRenderer(this);
			LowerHindLegLeft.setRotationPoint(-1.0F, -1.0F, 0.0F);
			LHindMovement.addChild(LowerHindLegLeft);
			LowerHindLegLeft.setTextureOffset(56, 30).addBox(-1.0F, 2.0F, 0.0F, 2.0F, 6.0F, 2.0F, 0.0F, false);
			RHindMovement = new ModelRenderer(this);
			RHindMovement.setRotationPoint(-4.0F, 17.0F, 5.5F);
			UpperHindLegRight = new ModelRenderer(this);
			UpperHindLegRight.setRotationPoint(1.0F, -1.0F, 0.0F);
			RHindMovement.addChild(UpperHindLegRight);
			setRotationAngle(UpperHindLegRight, -0.2618F, 0.0F, 0.0F);
			UpperHindLegRight.setTextureOffset(44, 21).addBox(-1.5F, -2.0F, -2.0F, 1.0F, 5.0F, 4.0F, 0.0F, false);
			LowerHindLegRight = new ModelRenderer(this);
			LowerHindLegRight.setRotationPoint(1.0F, -1.0F, 0.0F);
			RHindMovement.addChild(LowerHindLegRight);
			LowerHindLegRight.setTextureOffset(46, 30).addBox(-1.0F, 2.0F, 0.0F, 2.0F, 6.0F, 2.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			matrixStack.push();
            matrixStack.translate(0f, 1.5f - 0.6 * 1.5f, 0f);
            matrixStack.scale(0.6f, 0.6f, 0.6f);
			Body.render(matrixStack, buffer, packedLight, packedOverlay);
			BodyMane.render(matrixStack, buffer, packedLight, packedOverlay);
			Tail.render(matrixStack, buffer, packedLight, packedOverlay);
			HeadMovement.render(matrixStack, buffer, packedLight, packedOverlay);
			LFrontMovement.render(matrixStack, buffer, packedLight, packedOverlay);
			RFrontMovement.render(matrixStack, buffer, packedLight, packedOverlay);
			LHindMovement.render(matrixStack, buffer, packedLight, packedOverlay);
			RHindMovement.render(matrixStack, buffer, packedLight, packedOverlay);
			matrixStack.pop();
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
			this.LFrontMovement.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.HeadMovement.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.HeadMovement.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.RFrontMovement.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.RHindMovement.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.LHindMovement.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.Tail.rotateAngleZ = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
		}
	}

}
