
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.BunnyBabyEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;
import org.apache.logging.log4j.core.osgi.BundleContextSelector;

@OnlyIn(Dist.CLIENT)
public class BunnyBabyRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(BunnyBabyEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new Modelbunny(), 0.3f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						BunnyBabyEntity.CustomEntity _bunny = (BunnyBabyEntity.CustomEntity) entity;
						return new ResourceLocation("mocreatures:textures/moc_bunny_" + String.valueOf(_bunny.getVariant()) + ".png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.8.4
	// Exported for Minecraft version 1.15 - 1.16
	// Paste this class into your mod and generate all required imports
	public static class Modelbunny extends EntityModel<Entity> {
		private final ModelRenderer head;
		private final ModelRenderer body;
		private final ModelRenderer rfront;
		private final ModelRenderer lfront;
		private final ModelRenderer rback;
		private final ModelRenderer lback;

		public Modelbunny() {
			textureWidth = 64;
			textureHeight = 64;
			head = new ModelRenderer(this);
			head.setRotationPoint(2.0F, 22.0F, -3.0F);
			head.setTextureOffset(13, 22).addBox(-1.2F, -8.0F, -1.0F, 1.0F, 5.0F, 2.0F, 0.0F, false);
			head.setTextureOffset(0, 0).addBox(-3.7F, -8.0F, -1.0F, 1.0F, 5.0F, 2.0F, 0.0F, false);
			head.setTextureOffset(0, 14).addBox(-4.0F, -4.0F, -2.0F, 4.0F, 4.0F, 3.0F, 0.0F, false);
			head.setTextureOffset(25, 14).addBox(-3.0F, -2.0F, -2.25F, 2.0F, 1.0F, 1.0F, 0.0F, false);
			body = new ModelRenderer(this);
			body.setRotationPoint(2.0F, 24.0F, -3.0F);
			body.setTextureOffset(0, 0).addBox(-5.0F, -7.0F, 1.0F, 6.0F, 5.0F, 8.0F, 0.0F, false);
			body.setTextureOffset(15, 15).addBox(-3.5F, -6.0F, 9.0F, 3.0F, 3.0F, 3.0F, 0.0F, false);
			rfront = new ModelRenderer(this);
			rfront.setRotationPoint(0.0F, 24.0F, 0.0F);
			rfront.setTextureOffset(20, 27).addBox(-3.0F, -2.0F, -2.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			lfront = new ModelRenderer(this);
			lfront.setRotationPoint(0.0F, 24.0F, 0.0F);
			lfront.setTextureOffset(20, 22).addBox(1.0F, -2.0F, -2.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			rback = new ModelRenderer(this);
			rback.setRotationPoint(0.0F, 24.0F, 0.0F);
			rback.setTextureOffset(21, 0).addBox(-3.0F, -2.0F, 2.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
			lback = new ModelRenderer(this);
			lback.setRotationPoint(0.0F, 24.0F, 0.0F);
			lback.setTextureOffset(0, 22).addBox(1.0F, -2.0F, 2.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		}

		@Override
		public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
			// previously the render function, render code was moved to a method below
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			matrixStack.push();
            matrixStack.translate(0f, 1.5f - 0.6 * 1.5f, 0f);
            matrixStack.scale(0.6f, 0.6f, 0.6f);
			head.render(matrixStack, buffer, packedLight, packedOverlay);
			body.render(matrixStack, buffer, packedLight, packedOverlay);
			rfront.render(matrixStack, buffer, packedLight, packedOverlay);
			lfront.render(matrixStack, buffer, packedLight, packedOverlay);
			rback.render(matrixStack, buffer, packedLight, packedOverlay);
			lback.render(matrixStack, buffer, packedLight, packedOverlay);
			matrixStack.pop();
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}
	}

}
