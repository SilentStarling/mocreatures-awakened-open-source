package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.ButterflyEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class ButterflyRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(ButterflyEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelMoCModelButterfly(), 0.3f) {
					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						ButterflyEntity.CustomEntity _butterfly = (ButterflyEntity.CustomEntity) entity;
						return new ResourceLocation("mocreatures:textures/moc_butterfly_" + String.valueOf(_butterfly.getVariant()) + ".png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.7.4
	// Exported for Minecraft version 1.15
	// Paste this class into your mod and generate all required imports
	public static class ModelMoCModelButterfly extends EntityModel<Entity> {
		private final ModelRenderer Abdomen;
		private final ModelRenderer FrontLegs;
		private final ModelRenderer RightAntenna;
		private final ModelRenderer LeftAntenna;
		private final ModelRenderer RearLegs;
		private final ModelRenderer MidLegs;
		private final ModelRenderer Head;
		private final ModelRenderer Thorax;
		private final ModelRenderer WingRight;
		private final ModelRenderer WingLeft;
		private final ModelRenderer Mouth;
		private final ModelRenderer WingLeftFront;
		private final ModelRenderer WingRightFront;
		private final ModelRenderer WingRightBack;
		private final ModelRenderer WingLeftBack;
		public ModelMoCModelButterfly() {
			textureWidth = 32;
			textureHeight = 32;
			Abdomen = new ModelRenderer(this);
			Abdomen.setRotationPoint(0.0F, 21.5F, 0.0F);
			setRotationAngle(Abdomen, 1.4277F, 0.0F, 0.0F);
			Abdomen.setTextureOffset(8, 1).addBox(-0.5F, 0.0F, -1.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);
			FrontLegs = new ModelRenderer(this);
			FrontLegs.setRotationPoint(0.0F, 21.5F, -1.8F);
			setRotationAngle(FrontLegs, 0.1487F, 0.0F, 0.0F);
			FrontLegs.setTextureOffset(0, 8).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 3.0F, 0.0F, 0.0F, false);
			RightAntenna = new ModelRenderer(this);
			RightAntenna.setRotationPoint(-0.5F, 21.7F, -2.3F);
			setRotationAngle(RightAntenna, -1.041F, 0.7854F, 0.0F);
			RightAntenna.setTextureOffset(0, 7).addBox(-0.5F, 0.0F, -1.0F, 1.0F, 0.0F, 1.0F, 0.0F, false);
			LeftAntenna = new ModelRenderer(this);
			LeftAntenna.setRotationPoint(0.5F, 21.7F, -2.3F);
			setRotationAngle(LeftAntenna, -1.041F, -0.7854F, 0.0F);
			LeftAntenna.setTextureOffset(4, 7).addBox(-0.5F, 0.0F, -1.0F, 1.0F, 0.0F, 1.0F, 0.0F, false);
			RearLegs = new ModelRenderer(this);
			RearLegs.setRotationPoint(0.0F, 22.5F, -0.4F);
			setRotationAngle(RearLegs, 1.0707F, 0.0F, 0.0F);
			RearLegs.setTextureOffset(0, 8).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 3.0F, 0.0F, 0.0F, false);
			MidLegs = new ModelRenderer(this);
			MidLegs.setRotationPoint(0.0F, 22.0F, -1.2F);
			setRotationAngle(MidLegs, 0.5949F, 0.0F, 0.0F);
			MidLegs.setTextureOffset(4, 8).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 3.0F, 0.0F, 0.0F, false);
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, 21.9F, -1.3F);
			setRotationAngle(Head, -2.1712F, 0.0F, 0.0F);
			Head.setTextureOffset(0, 11).addBox(-0.5F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
			Thorax = new ModelRenderer(this);
			Thorax.setRotationPoint(0.0F, 20.0F, -1.0F);
			Thorax.setTextureOffset(0, 0).addBox(-0.5F, 1.5F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			WingRight = new ModelRenderer(this);
			WingRight.setRotationPoint(-0.3F, 21.5F, -0.5F);
			WingRight.setTextureOffset(4, 14).addBox(-8.3F, 0.5F, 0.0F, 8.0F, 0.0F, 6.0F, 0.0F, false);
			WingLeft = new ModelRenderer(this);
			WingLeft.setRotationPoint(0.3F, 21.5F, 0.0F);
			WingLeft.setTextureOffset(4, 26).addBox(0.3F, 0.5F, -0.5F, 8.0F, 0.0F, 6.0F, 0.0F, false);
			Mouth = new ModelRenderer(this);
			Mouth.setRotationPoint(-0.2F, 22.0F, -2.5F);
			setRotationAngle(Mouth, 0.6549F, 0.0F, 0.0F);
			Mouth.setTextureOffset(0, 8).addBox(0.0F, 0.0F, 0.0F, 1.0F, 2.0F, 0.0F, 0.0F, false);
			WingLeftFront = new ModelRenderer(this);
			WingLeftFront.setRotationPoint(0.3F, 21.4F, -1.0F);
			WingLeftFront.setTextureOffset(4, 20).addBox(0.0F, 0.0F, -4.0F, 8.0F, 0.0F, 6.0F, 0.0F, false);
			WingRightFront = new ModelRenderer(this);
			WingRightFront.setRotationPoint(-0.3F, 21.4F, -1.0F);
			WingRightFront.setTextureOffset(4, 8).addBox(-8.0F, 0.0F, -4.0F, 8.0F, 0.0F, 6.0F, 0.0F, false);
			WingRightBack = new ModelRenderer(this);
			WingRightBack.setRotationPoint(0.3F, 21.2F, -1.0F);
			setRotationAngle(WingRightBack, 0.0F, 0.0F, -0.5934F);
			WingRightBack.setTextureOffset(14, 0).addBox(-5.0F, 0.0F, -1.0F, 5.0F, 0.0F, 8.0F, 0.0F, false);
			WingLeftBack = new ModelRenderer(this);
			WingLeftBack.setRotationPoint(0.3F, 21.2F, -1.0F);
			setRotationAngle(WingLeftBack, 0.0F, 0.0F, 0.5934F);
			WingLeftBack.setTextureOffset(4, 0).addBox(0.0F, 0.0F, -1.0F, 5.0F, 0.0F, 8.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Abdomen.render(matrixStack, buffer, packedLight, packedOverlay);
			FrontLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			RightAntenna.render(matrixStack, buffer, packedLight, packedOverlay);
			LeftAntenna.render(matrixStack, buffer, packedLight, packedOverlay);
			RearLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			MidLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			Head.render(matrixStack, buffer, packedLight, packedOverlay);
			Thorax.render(matrixStack, buffer, packedLight, packedOverlay);
			WingRight.render(matrixStack, buffer, packedLight, packedOverlay);
			WingLeft.render(matrixStack, buffer, packedLight, packedOverlay);
			Mouth.render(matrixStack, buffer, packedLight, packedOverlay);
			WingLeftFront.render(matrixStack, buffer, packedLight, packedOverlay);
			WingRightFront.render(matrixStack, buffer, packedLight, packedOverlay);
			WingRightBack.render(matrixStack, buffer, packedLight, packedOverlay);
			WingLeftBack.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
			this.WingRightBack.rotateAngleZ = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
			this.WingRightFront.rotateAngleZ = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
			this.WingLeft.rotateAngleZ = MathHelper.cos(f * 0.6662F) * f1;
			this.WingLeftFront.rotateAngleZ = MathHelper.cos(f * 0.6662F) * f1;
			this.WingRight.rotateAngleZ = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
			this.WingLeftBack.rotateAngleZ = MathHelper.cos(f * 0.6662F) * f1;
		}
	}
}
