
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.CrocodileEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class CrocodileRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(CrocodileEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelCrocodile(), 0.5f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/crocodile.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.9.3
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelCrocodile extends EntityModel<Entity> {
		private final ModelRenderer head;
		private final ModelRenderer FrontLLeg;
		private final ModelRenderer FrontRLeg;
		private final ModelRenderer BackLLeg;
		private final ModelRenderer BackRLeg;
		private final ModelRenderer body;
		private final ModelRenderer tail;

		public ModelCrocodile() {
			textureWidth = 64;
			textureHeight = 64;
			head = new ModelRenderer(this);
			head.setRotationPoint(0.0F, 24.0F, 0.0F);
			head.setTextureOffset(48, 36).addBox(-1.5F, -7.0F, -24.0F, 3.0F, 2.0F, 4.0F, 0.0F, false);
			head.setTextureOffset(24, 40).addBox(-2.5F, -5.0F, -20.0F, 5.0F, 2.0F, 6.0F, 0.0F, false);
			head.setTextureOffset(40, 14).addBox(-2.0F, -5.0F, -24.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
			head.setTextureOffset(0, 8).addBox(-2.0F, -7.0F, -20.0F, 4.0F, 2.0F, 6.0F, 0.0F, false);
			head.setTextureOffset(0, 37).addBox(-3.0F, -8.0F, -14.0F, 6.0F, 5.0F, 6.0F, 0.0F, false);
			head.setTextureOffset(0, 0).addBox(3.0F, -9.0F, -14.0F, 0.0F, 1.0F, 2.0F, 0.0F, false);
			head.setTextureOffset(0, 1).addBox(-3.0F, -9.0F, -14.0F, 0.0F, 1.0F, 2.0F, 0.0F, false);
			head.setTextureOffset(18, 35).addBox(-2.1F, -6.0F, -20.0F, 0.0F, 1.0F, 6.0F, 0.0F, false);
			head.setTextureOffset(44, 32).addBox(-1.6F, -6.0F, -24.0F, 0.0F, 1.0F, 4.0F, 0.0F, false);
			head.setTextureOffset(24, 46).addBox(1.4F, -5.0F, -24.4F, 0.0F, 1.0F, 4.0F, 0.0F, false);
			head.setTextureOffset(24, 46).addBox(-1.4F, -5.0F, -24.4F, 0.0F, 1.0F, 4.0F, 0.0F, false);
			head.setTextureOffset(0, 0).addBox(-1.0F, -6.0F, -24.1F, 2.0F, 1.0F, 0.0F, 0.0F, false);
			head.setTextureOffset(44, 32).addBox(1.6F, -6.0F, -24.0F, 0.0F, 1.0F, 4.0F, 0.0F, false);
			head.setTextureOffset(18, 34).addBox(2.1F, -6.0F, -20.0F, 0.0F, 1.0F, 6.0F, 0.0F, false);
			head.setTextureOffset(28, 49).addBox(1.9F, -5.0F, -20.5F, 0.0F, 1.0F, 6.0F, 0.0F, true);
			head.setTextureOffset(28, 50).addBox(-1.9F, -5.0F, -20.5F, 0.0F, 1.0F, 6.0F, 0.0F, false);
			FrontLLeg = new ModelRenderer(this);
			FrontLLeg.setRotationPoint(0.0F, 24.0F, 0.0F);
			FrontLLeg.setTextureOffset(52, 0).addBox(5.0F, -6.0F, -5.0F, 3.0F, 3.0F, 3.0F, 0.0F, false);
			FrontLLeg.setTextureOffset(0, 50).addBox(6.0F, -3.0F, -6.0F, 3.0F, 2.0F, 4.0F, 0.0F, false);
			FrontRLeg = new ModelRenderer(this);
			FrontRLeg.setRotationPoint(0.0F, 24.0F, 0.0F);
			FrontRLeg.setTextureOffset(14, 50).addBox(-9.0F, -3.0F, -6.0F, 3.0F, 2.0F, 4.0F, 0.0F, false);
			FrontRLeg.setTextureOffset(42, 50).addBox(-8.0F, -6.0F, -5.0F, 3.0F, 3.0F, 3.0F, 0.0F, false);
			BackLLeg = new ModelRenderer(this);
			BackLLeg.setRotationPoint(0.0F, 24.0F, 0.0F);
			BackLLeg.setTextureOffset(41, 43).addBox(6.0F, -3.0F, 6.0F, 3.0F, 2.0F, 5.0F, 0.0F, false);
			BackLLeg.setTextureOffset(28, 48).addBox(5.0F, -6.0F, 7.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
			BackRLeg = new ModelRenderer(this);
			BackRLeg.setRotationPoint(0.0F, 24.0F, 0.0F);
			BackRLeg.setTextureOffset(40, 7).addBox(-9.0F, -3.0F, 6.0F, 3.0F, 2.0F, 5.0F, 0.0F, false);
			BackRLeg.setTextureOffset(44, 25).addBox(-8.0F, -6.0F, 7.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
			body = new ModelRenderer(this);
			body.setRotationPoint(0.0F, 24.0F, 0.0F);
			body.setTextureOffset(24, 17).addBox(4.0F, -10.0F, -8.0F, 0.0F, 2.0F, 8.0F, 0.0F, false);
			body.setTextureOffset(16, 40).addBox(0.0F, -10.0F, -6.0F, 0.0F, 2.0F, 8.0F, 0.0F, false);
			body.setTextureOffset(24, 19).addBox(-4.0F, -10.0F, -8.0F, 0.0F, 2.0F, 8.0F, 0.0F, false);
			body.setTextureOffset(0, 10).addBox(-4.0F, -10.0F, 1.0F, 0.0F, 2.0F, 8.0F, 0.0F, false);
			body.setTextureOffset(0, 40).addBox(0.0F, -10.0F, 3.0F, 0.0F, 2.0F, 8.0F, 0.0F, false);
			body.setTextureOffset(0, 8).addBox(4.0F, -10.0F, 1.0F, 0.0F, 2.0F, 8.0F, 0.0F, false);
			body.setTextureOffset(0, 0).addBox(-5.0F, -8.0F, -8.0F, 10.0F, 5.0F, 20.0F, 0.0F, false);
			tail = new ModelRenderer(this);
			tail.setRotationPoint(0.0F, 24.0F, 0.0F);
			tail.setTextureOffset(0, 25).addBox(-4.0F, -7.5F, 12.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
			tail.setTextureOffset(24, 29).addBox(-3.0F, -7.0F, 20.0F, 6.0F, 3.0F, 8.0F, 0.0F, false);
			tail.setTextureOffset(0, 0).addBox(-2.0F, -6.5F, 28.0F, 4.0F, 2.0F, 6.0F, 0.0F, false);
			tail.setTextureOffset(40, 0).addBox(-1.5F, -6.0F, 34.0F, 3.0F, 1.0F, 6.0F, 0.0F, false);
			tail.setTextureOffset(0, 21).addBox(1.0F, -8.0F, 35.0F, 0.0F, 2.0F, 4.0F, 0.0F, false);
			tail.setTextureOffset(0, 25).addBox(1.5F, -8.5F, 29.0F, 0.0F, 2.0F, 4.0F, 0.0F, false);
			tail.setTextureOffset(24, 27).addBox(2.0F, -9.0F, 24.0F, 0.0F, 2.0F, 4.0F, 0.0F, false);
			tail.setTextureOffset(40, 21).addBox(2.5F, -9.0F, 20.0F, 0.0F, 2.0F, 4.0F, 0.0F, false);
			tail.setTextureOffset(40, 36).addBox(3.0F, -9.5F, 16.0F, 0.0F, 2.0F, 4.0F, 0.0F, false);
			tail.setTextureOffset(10, 46).addBox(3.5F, -9.5F, 12.0F, 0.0F, 2.0F, 4.0F, 0.0F, false);
			tail.setTextureOffset(44, 30).addBox(-3.5F, -9.5F, 12.0F, 0.0F, 2.0F, 4.0F, 0.0F, false);
			tail.setTextureOffset(44, 28).addBox(-3.0F, -9.5F, 16.0F, 0.0F, 2.0F, 4.0F, 0.0F, false);
			tail.setTextureOffset(40, 23).addBox(-2.5F, -9.0F, 20.0F, 0.0F, 2.0F, 4.0F, 0.0F, false);
			tail.setTextureOffset(0, 27).addBox(-2.0F, -9.0F, 24.0F, 0.0F, 2.0F, 4.0F, 0.0F, false);
			tail.setTextureOffset(0, 23).addBox(-1.0F, -8.0F, 35.0F, 0.0F, 2.0F, 4.0F, 0.0F, false);
			tail.setTextureOffset(24, 25).addBox(-1.5F, -8.5F, 29.0F, 0.0F, 2.0F, 4.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			head.render(matrixStack, buffer, packedLight, packedOverlay);
			FrontLLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			FrontRLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			BackLLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			BackRLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			body.render(matrixStack, buffer, packedLight, packedOverlay);
			tail.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {

			this.head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.head.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.FrontLLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.BackRLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.BackLLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.FrontRLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		}
	}

}
