
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.DarkPegasusEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class DarkPegasusRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(DarkPegasusEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new Modelpegasus(), 0.5f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/crabbrown.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.8.4
	// Exported for Minecraft version 1.15 - 1.16
	// Paste this class into your mod and generate all required imports
	public static class Modelpegasus extends EntityModel<Entity> {
		private final ModelRenderer bodynormalhorsezebra;
		private final ModelRenderer body;
		private final ModelRenderer TailA;
		private final ModelRenderer TailA_r1;
		private final ModelRenderer TailA_r2;
		private final ModelRenderer TailA_r3;
		private final ModelRenderer Head;
		private final ModelRenderer ear2_r1;
		private final ModelRenderer ear_r1;
		private final ModelRenderer UMouth_r1;
		private final ModelRenderer Neck;
		private final ModelRenderer Mane_r1;
		private final ModelRenderer Neck_r1;
		private final ModelRenderer wings;
		private final ModelRenderer angelwing;
		private final ModelRenderer cube_r1;
		private final ModelRenderer cube_r2;
		private final ModelRenderer cube_r3;
		private final ModelRenderer cube_r4;
		private final ModelRenderer cube_r5;
		private final ModelRenderer cube_r6;
		private final ModelRenderer cube_r7;
		private final ModelRenderer cube_r8;
		private final ModelRenderer cube_r9;
		private final ModelRenderer cube_r10;
		private final ModelRenderer legs;
		private final ModelRenderer leg1;
		private final ModelRenderer leg2;
		private final ModelRenderer leg3;
		private final ModelRenderer accessories;
		private final ModelRenderer chests;
		private final ModelRenderer saddle;
		private final ModelRenderer saddlemouth;
		private final ModelRenderer idkR_r1;
		private final ModelRenderer idkL_r1;
		private final ModelRenderer SaddleMouthLineR_r1;
		private final ModelRenderer headthing_r1;
		private final ModelRenderer Mouththing_r1;
		private final ModelRenderer SaddleHeadR_r1;
		private final ModelRenderer SaddleHeadL_r1;
		private final ModelRenderer SaddleMouthR_r1;
		private final ModelRenderer leg4;

		public Modelpegasus() {
			textureWidth = 128;
			textureHeight = 128;
			bodynormalhorsezebra = new ModelRenderer(this);
			bodynormalhorsezebra.setRotationPoint(0.0F, 11.0F, 9.0F);
			body = new ModelRenderer(this);
			body.setRotationPoint(17.0F, -8.5F, -3.0F);
			bodynormalhorsezebra.addChild(body);
			body.setTextureOffset(0, 0).addBox(-22.0F, 0.5F, -18.8F, 10.0F, 10.0F, 24.0F, 0.0F, false);
			TailA = new ModelRenderer(this);
			TailA.setRotationPoint(-17.0F, 1.5F, 5.0F);
			body.addChild(TailA);
			setRotationAngle(TailA, 0.5236F, 0.0F, 0.0F);
			TailA_r1 = new ModelRenderer(this);
			TailA_r1.setRotationPoint(0.0F, 1.3711F, -0.4344F);
			TailA.addChild(TailA_r1);
			setRotationAngle(TailA_r1, -0.0873F, 0.0F, 0.0F);
			TailA_r1.setTextureOffset(36, 82).addBox(-1.5F, -2.5F, -1.5F, 3.0F, 5.0F, 3.0F, 0.0F, false);
			TailA_r2 = new ModelRenderer(this);
			TailA_r2.setRotationPoint(0.0F, 12.4267F, -2.1923F);
			TailA.addChild(TailA_r2);
			setRotationAngle(TailA_r2, -0.2705F, 0.0F, 0.0F);
			TailA_r2.setTextureOffset(0, 73).addBox(-2.0F, -2.5F, -2.0F, 4.0F, 5.0F, 4.0F, 0.0F, false);
			TailA_r3 = new ModelRenderer(this);
			TailA_r3.setRotationPoint(0.0F, 6.0707F, -1.006F);
			TailA.addChild(TailA_r3);
			setRotationAngle(TailA_r3, -0.1309F, 0.0F, 0.0F);
			TailA_r3.setTextureOffset(68, 37).addBox(-2.0F, -4.5F, -2.0F, 4.0F, 9.0F, 4.0F, 0.0F, false);
			Head = new ModelRenderer(this);
			Head.setRotationPoint(-17.0F, -6.5F, -17.0F);
			body.addChild(Head);
			setRotationAngle(Head, 0.5236F, 0.0F, 0.0F);
			ear2_r1 = new ModelRenderer(this);
			ear2_r1.setRotationPoint(-1.85F, -5.8206F, -2.8656F);
			Head.addChild(ear2_r1);
			setRotationAngle(ear2_r1, 0.1309F, -0.1265F, -0.1047F);
			ear2_r1.setTextureOffset(15, 0).addBox(-1.0805F, -1.5575F, -0.3872F, 2.0F, 3.0F, 1.0F, 0.0F, false);
			ear_r1 = new ModelRenderer(this);
			ear_r1.setRotationPoint(1.85F, -5.8206F, -2.8656F);
			Head.addChild(ear_r1);
			setRotationAngle(ear_r1, 0.1309F, 0.1265F, 0.1047F);
			ear_r1.setTextureOffset(0, 61).addBox(-0.9195F, -1.5575F, -0.3872F, 2.0F, 3.0F, 1.0F, 0.0F, false);
			UMouth_r1 = new ModelRenderer(this);
			UMouth_r1.setRotationPoint(0.0F, -1.241F, -1.7664F);
			Head.addChild(UMouth_r1);
			setRotationAngle(UMouth_r1, 0.0873F, 0.0F, 0.0F);
			UMouth_r1.setTextureOffset(70, 58).addBox(-2.0F, -1.2002F, -10.7066F, 4.0F, 2.0F, 5.0F, 0.0F, false);
			UMouth_r1.setTextureOffset(0, 16).addBox(-2.5F, -3.6174F, -10.8129F, 5.0F, 3.0F, 5.0F, 0.0F, false);
			UMouth_r1.setTextureOffset(0, 61).addBox(-3.0F, -4.0051F, -6.5029F, 6.0F, 5.0F, 7.0F, 0.0F, false);
			Neck = new ModelRenderer(this);
			Neck.setRotationPoint(-17.0F, 4.5F, -14.0F);
			body.addChild(Neck);
			setRotationAngle(Neck, 0.5236F, 0.0F, 0.0F);
			Mane_r1 = new ModelRenderer(this);
			Mane_r1.setRotationPoint(0.0F, -12.2672F, 1.1355F);
			Neck.addChild(Mane_r1);
			setRotationAngle(Mane_r1, 0.0873F, 0.0F, 0.0F);
			Mane_r1.setTextureOffset(0, 34).addBox(-1.0F, -5.8034F, -0.8174F, 2.0F, 17.0F, 3.0F, 0.0F, false);
			Neck_r1 = new ModelRenderer(this);
			Neck_r1.setRotationPoint(0.0F, -6.5639F, -1.3636F);
			Neck.addChild(Neck_r1);
			setRotationAngle(Neck_r1, 0.0873F, 0.0F, 0.0F);
			Neck_r1.setTextureOffset(0, 0).addBox(-2.0F, -4.5F, -3.5F, 4.0F, 9.0F, 7.0F, 0.0F, false);
			wings = new ModelRenderer(this);
			wings.setRotationPoint(0.0F, 13.0F, -9.0F);
			bodynormalhorsezebra.addChild(wings);
			angelwing = new ModelRenderer(this);
			angelwing.setRotationPoint(-3.2F, -1.5F, -11.1F);
			wings.addChild(angelwing);
			cube_r1 = new ModelRenderer(this);
			cube_r1.setRotationPoint(29.4F, -18.125F, 4.325F);
			angelwing.addChild(cube_r1);
			setRotationAngle(cube_r1, 0.0F, -0.1309F, 0.0F);
			cube_r1.setTextureOffset(32, 0).addBox(-10.0F, 0.0F, -5.0F, 20.0F, 0.0F, 14.0F, 0.0F, false);
			cube_r2 = new ModelRenderer(this);
			cube_r2.setRotationPoint(-23.0F, -18.125F, 4.325F);
			angelwing.addChild(cube_r2);
			setRotationAngle(cube_r2, 0.0F, 0.1309F, 0.0F);
			cube_r2.setTextureOffset(32, 12).addBox(-10.0F, 0.0F, -5.0F, 20.0F, 0.0F, 14.0F, 0.0F, false);
			cube_r3 = new ModelRenderer(this);
			cube_r3.setRotationPoint(14.425F, -17.6F, 2.475F);
			angelwing.addChild(cube_r3);
			setRotationAngle(cube_r3, 0.0F, 0.1178F, -0.096F);
			cube_r3.setTextureOffset(0, 34).addBox(-7.0F, 0.0F, -3.0F, 14.0F, 0.0F, 13.0F, 0.0F, false);
			cube_r4 = new ModelRenderer(this);
			cube_r4.setRotationPoint(-8.025F, -17.6F, 2.475F);
			angelwing.addChild(cube_r4);
			setRotationAngle(cube_r4, 0.0F, -0.1178F, 0.096F);
			cube_r4.setTextureOffset(28, 34).addBox(-7.0F, 0.0F, -3.0F, 14.0F, 0.0F, 13.0F, 0.0F, false);
			cube_r5 = new ModelRenderer(this);
			cube_r5.setRotationPoint(35.4204F, -17.975F, 1.4067F);
			angelwing.addChild(cube_r5);
			setRotationAngle(cube_r5, 0.0393F, 1.7148F, 0.0F);
			cube_r5.setTextureOffset(76, 50).addBox(-0.5F, -0.5F, -2.5F, 1.0F, 1.0F, 6.0F, 0.0F, false);
			cube_r6 = new ModelRenderer(this);
			cube_r6.setRotationPoint(-29.0204F, -17.975F, 1.4067F);
			angelwing.addChild(cube_r6);
			setRotationAngle(cube_r6, 0.0393F, -1.7148F, 0.0F);
			cube_r6.setTextureOffset(74, 77).addBox(-0.5F, -0.5F, -2.5F, 1.0F, 1.0F, 6.0F, 0.0F, false);
			cube_r7 = new ModelRenderer(this);
			cube_r7.setRotationPoint(-20.4095F, -18.0172F, -0.0115F);
			angelwing.addChild(cube_r7);
			setRotationAngle(cube_r7, 0.0F, 0.2749F, -0.0175F);
			cube_r7.setTextureOffset(0, 57).addBox(-7.5F, -1.0F, -1.0F, 15.0F, 2.0F, 2.0F, 0.0F, false);
			cube_r8 = new ModelRenderer(this);
			cube_r8.setRotationPoint(26.8095F, -18.0172F, -0.0115F);
			angelwing.addChild(cube_r8);
			setRotationAngle(cube_r8, 0.0F, -0.2749F, 0.0175F);
			cube_r8.setTextureOffset(34, 58).addBox(-7.5F, -1.0F, -1.0F, 15.0F, 2.0F, 2.0F, 0.0F, false);
			cube_r9 = new ModelRenderer(this);
			cube_r9.setRotationPoint(-7.6F, -17.5F, -0.35F);
			angelwing.addChild(cube_r9);
			setRotationAngle(cube_r9, 0.0F, -0.2443F, 0.0873F);
			cube_r9.setTextureOffset(32, 46).addBox(-6.5F, -1.5F, -1.5F, 14.0F, 3.0F, 3.0F, 0.0F, false);
			cube_r10 = new ModelRenderer(this);
			cube_r10.setRotationPoint(14.0F, -17.5F, -0.35F);
			angelwing.addChild(cube_r10);
			setRotationAngle(cube_r10, 0.0F, 0.2443F, -0.0873F);
			cube_r10.setTextureOffset(42, 52).addBox(-7.5F, -1.5F, -1.5F, 14.0F, 3.0F, 3.0F, 0.0F, false);
			legs = new ModelRenderer(this);
			legs.setRotationPoint(17.0F, -8.5F, -3.0F);
			bodynormalhorsezebra.addChild(legs);
			leg1 = new ModelRenderer(this);
			leg1.setRotationPoint(-14.0F, 10.5F, -15.0F);
			legs.addChild(leg1);
			leg1.setTextureOffset(26, 62).addBox(-1.8F, -5.2F, -4.1F, 4.0F, 9.0F, 4.0F, 0.0F, false);
			leg1.setTextureOffset(0, 82).addBox(-1.3F, 3.6F, -3.6F, 3.0F, 5.0F, 3.0F, 0.0F, false);
			leg1.setTextureOffset(32, 75).addBox(-1.8F, 8.0F, -4.1F, 4.0F, 3.0F, 4.0F, 0.0F, false);
			leg2 = new ModelRenderer(this);
			leg2.setRotationPoint(-20.2F, 10.8F, -15.3F);
			legs.addChild(leg2);
			leg2.setTextureOffset(58, 62).addBox(-2.0F, -5.5F, -3.8F, 4.0F, 9.0F, 4.0F, 0.0F, false);
			leg2.setTextureOffset(64, 76).addBox(-2.0F, 7.7F, -3.8F, 4.0F, 3.0F, 4.0F, 0.0F, false);
			leg2.setTextureOffset(24, 82).addBox(-1.5F, 3.3F, -3.3F, 3.0F, 5.0F, 3.0F, 0.0F, false);
			leg3 = new ModelRenderer(this);
			leg3.setRotationPoint(-20.0F, 10.5F, -15.0F);
			legs.addChild(leg3);
			leg3.setTextureOffset(68, 24).addBox(4.2F, -4.9F, 15.7F, 4.0F, 9.0F, 4.0F, 0.0F, false);
			leg3.setTextureOffset(12, 82).addBox(4.7F, 3.6F, 16.2F, 3.0F, 5.0F, 3.0F, 0.0F, false);
			leg3.setTextureOffset(48, 75).addBox(4.2F, 8.0F, 15.7F, 4.0F, 3.0F, 4.0F, 0.0F, false);
			accessories = new ModelRenderer(this);
			accessories.setRotationPoint(3.0F, 11.0F, 9.0F);
			leg3.addChild(accessories);
			chests = new ModelRenderer(this);
			chests.setRotationPoint(-5.0F, -21.0F, 11.0F);
			accessories.addChild(chests);
			setRotationAngle(chests, 0.0F, -1.5708F, 0.0F);
			chests.setTextureOffset(26, 21).addBox(-8.3F, 0.0F, 0.0F, 8.0F, 8.0F, 3.0F, 0.0F, false);
			chests.setTextureOffset(26, 21).addBox(-8.3F, 0.0F, -13.0F, 8.0F, 8.0F, 3.0F, 0.0F, false);
			chests.setTextureOffset(0, 0).addBox(-2.2F, -0.5F, -12.0F, 1.0F, 1.0F, 14.0F, 0.0F, false);
			chests.setTextureOffset(0, 0).addBox(-7.2F, -0.5F, -12.0F, 1.0F, 1.0F, 14.0F, 0.0F, false);
			chests.setTextureOffset(0, 0).addBox(-5.3F, 2.35F, 2.7F, 2.0F, 2.0F, 1.0F, 0.0F, false);
			chests.setTextureOffset(0, 0).addBox(-5.3F, 2.35F, -13.6F, 2.0F, 2.0F, 1.0F, 0.0F, false);
			saddle = new ModelRenderer(this);
			saddle.setRotationPoint(5.0F, -21.0F, 11.0F);
			accessories.addChild(saddle);
			setRotationAngle(saddle, 0.0F, 1.5708F, 0.0F);
			saddle.setTextureOffset(0, 46).addBox(6.5F, -1.0F, -10.0F, 11.0F, 1.0F, 10.0F, 0.0F, false);
			saddle.setTextureOffset(67, 68).addBox(7.0F, -1.6F, -8.5F, 2.0F, 1.0F, 7.0F, 0.0F, false);
			saddle.setTextureOffset(32, 52).addBox(14.9F, -1.7F, -6.5F, 2.0F, 1.0F, 3.0F, 0.0F, false);
			saddle.setTextureOffset(48, 82).addBox(11.475F, -0.9F, -0.4F, 1.0F, 7.0F, 1.0F, 0.0F, false);
			saddle.setTextureOffset(20, 16).addBox(11.475F, -0.9F, -10.5F, 1.0F, 7.0F, 1.0F, 0.0F, false);
			saddle.setTextureOffset(19, 61).addBox(11.575F, 5.6F, -0.6F, 1.0F, 2.0F, 2.0F, 0.0F, false);
			saddle.setTextureOffset(0, 0).addBox(11.575F, 5.6F, -11.6F, 1.0F, 2.0F, 2.0F, 0.0F, false);
			saddlemouth = new ModelRenderer(this);
			saddlemouth.setRotationPoint(0.0F, -17.0F, -8.0F);
			accessories.addChild(saddlemouth);
			setRotationAngle(saddlemouth, 0.5236F, 0.0F, 0.0F);
			idkR_r1 = new ModelRenderer(this);
			idkR_r1.setRotationPoint(-3.5F, -12.7634F, -3.4101F);
			saddlemouth.addChild(idkR_r1);
			setRotationAngle(idkR_r1, 0.0873F, -0.1178F, 0.0F);
			idkR_r1.setTextureOffset(74, 72).addBox(-0.0624F, -0.273F, -2.4381F, 1.0F, 1.0F, 5.0F, 0.0F, false);
			idkL_r1 = new ModelRenderer(this);
			idkL_r1.setRotationPoint(3.5F, -12.7634F, -3.4101F);
			saddlemouth.addChild(idkL_r1);
			setRotationAngle(idkL_r1, 0.0873F, 0.1178F, 0.0F);
			idkL_r1.setTextureOffset(80, 16).addBox(-0.9376F, -0.273F, -2.4381F, 1.0F, 1.0F, 5.0F, 0.0F, false);
			SaddleMouthLineR_r1 = new ModelRenderer(this);
			SaddleMouthLineR_r1.setRotationPoint(3.3F, -7.5F, -4.0F);
			saddlemouth.addChild(SaddleMouthLineR_r1);
			setRotationAngle(SaddleMouthLineR_r1, -0.5018F, 0.0F, 0.0F);
			SaddleMouthLineR_r1.setTextureOffset(0, 43).addBox(0.0F, -4.5626F, -5.1075F, 0.0F, 3.0F, 16.0F, 0.0F, false);
			SaddleMouthLineR_r1.setTextureOffset(32, 44).addBox(-6.6F, -4.5626F, -5.1075F, 0.0F, 3.0F, 16.0F, 0.0F, false);
			headthing_r1 = new ModelRenderer(this);
			headthing_r1.setRotationPoint(-0.5F, -0.5F, 0.5F);
			saddlemouth.addChild(headthing_r1);
			setRotationAngle(headthing_r1, 0.0873F, 0.0F, 0.0F);
			headthing_r1.setTextureOffset(80, 9).addBox(-3.0F, -15.9592F, -0.5887F, 7.0F, 6.0F, 1.0F, 0.0F, false);
			Mouththing_r1 = new ModelRenderer(this);
			Mouththing_r1.setRotationPoint(0.1F, -12.9135F, -5.9008F);
			saddlemouth.addChild(Mouththing_r1);
			setRotationAngle(Mouththing_r1, 0.0873F, 0.0F, 0.0F);
			Mouththing_r1.setTextureOffset(49, 63).addBox(-3.1F, -2.6393F, -0.8297F, 6.0F, 5.0F, 1.0F, 0.0F, false);
			SaddleHeadR_r1 = new ModelRenderer(this);
			SaddleHeadR_r1.setRotationPoint(-2.5F, -12.3242F, -6.4294F);
			saddlemouth.addChild(SaddleHeadR_r1);
			setRotationAngle(SaddleHeadR_r1, 0.0873F, 0.0F, 0.0F);
			SaddleHeadR_r1.setTextureOffset(68, 27).addBox(-1.25F, -1.0321F, 4.6614F, 1.0F, 2.0F, 2.0F, 0.0F, false);
			SaddleHeadR_r1.setTextureOffset(38, 71).addBox(-0.7F, -1.0368F, -0.7911F, 1.0F, 2.0F, 2.0F, 0.0F, false);
			SaddleHeadL_r1 = new ModelRenderer(this);
			SaddleHeadL_r1.setRotationPoint(2.7F, -12.8703F, -0.5783F);
			saddlemouth.addChild(SaddleHeadL_r1);
			setRotationAngle(SaddleHeadL_r1, 0.0873F, 0.0F, 0.0F);
			SaddleHeadL_r1.setTextureOffset(0, 69).addBox(0.025F, -0.998F, -1.215F, 1.0F, 2.0F, 2.0F, 0.0F, false);
			SaddleMouthR_r1 = new ModelRenderer(this);
			SaddleMouthR_r1.setRotationPoint(2.7F, -12.3791F, -6.2245F);
			saddlemouth.addChild(SaddleMouthR_r1);
			setRotationAngle(SaddleMouthR_r1, 0.0873F, 0.0F, 0.0F);
			SaddleMouthR_r1.setTextureOffset(26, 69).addBox(-0.5F, -1.0F, -1.0F, 1.0F, 2.0F, 2.0F, 0.0F, false);
			leg4 = new ModelRenderer(this);
			leg4.setRotationPoint(-14.0F, 10.5F, 3.0F);
			legs.addChild(leg4);
			leg4.setTextureOffset(42, 62).addBox(-8.2F, -4.9F, -2.3F, 4.0F, 9.0F, 4.0F, 0.0F, false);
			leg4.setTextureOffset(16, 75).addBox(-7.8F, 8.0F, -2.3F, 4.0F, 3.0F, 4.0F, 0.0F, false);
			leg4.setTextureOffset(78, 65).addBox(-7.3F, 3.6F, -1.8F, 3.0F, 5.0F, 3.0F, 0.0F, false);
		}

		@Override
		public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
			// previously the render function, render code was moved to a method below
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			bodynormalhorsezebra.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}
	}

}
