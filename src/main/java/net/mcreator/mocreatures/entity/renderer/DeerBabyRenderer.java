
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.DeerBabyEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class DeerBabyRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(DeerBabyEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelDeer(), 0.3f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/babydeer.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 4.2.4
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelDeer extends EntityModel<Entity> {
		private final ModelRenderer Neck;
		private final ModelRenderer cube_r1;
		private final ModelRenderer Head;
		private final ModelRenderer cube_r2;
		private final ModelRenderer cube_r3;
		private final ModelRenderer Left_Ear;
		private final ModelRenderer cube_r4;
		private final ModelRenderer Right_Ear;
		private final ModelRenderer cube_r5;
		private final ModelRenderer Left_Antler;
		private final ModelRenderer cube_r6;
		private final ModelRenderer L_A_2;
		private final ModelRenderer L_A_3;
		private final ModelRenderer L_A_4;
		private final ModelRenderer L_A_5;
		private final ModelRenderer L_A_6;
		private final ModelRenderer L_A_7;
		private final ModelRenderer L_A_8;
		private final ModelRenderer L_A_9;
		private final ModelRenderer Right_Antler;
		private final ModelRenderer cube_r7;
		private final ModelRenderer R_A_2;
		private final ModelRenderer R_A_3;
		private final ModelRenderer R_A_4;
		private final ModelRenderer R_A_5;
		private final ModelRenderer R_A_6;
		private final ModelRenderer R_A_7;
		private final ModelRenderer R_A_8;
		private final ModelRenderer R_A_9;
		private final ModelRenderer Left_Back_Leg;
		private final ModelRenderer cube_r8;
		private final ModelRenderer cube_r9;
		private final ModelRenderer cube_r10;
		private final ModelRenderer Right_Back_Leg;
		private final ModelRenderer cube_r11;
		private final ModelRenderer cube_r12;
		private final ModelRenderer cube_r13;
		private final ModelRenderer Left_Front_Leg;
		private final ModelRenderer Right_Front_Leg;
		private final ModelRenderer bb_main;
		private final ModelRenderer cube_r14;
		private final ModelRenderer cube_r15;
		private final ModelRenderer cube_r16;

		public ModelDeer() {
			textureWidth = 64;
			textureHeight = 64;
			Neck = new ModelRenderer(this);
			Neck.setRotationPoint(0.0F, 5.5F, -13.0F);
			cube_r1 = new ModelRenderer(this);
			cube_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
			Neck.addChild(cube_r1);
			setRotationAngle(cube_r1, -1.3963F, 0.0F, 0.0F);
			cube_r1.setTextureOffset(0, 31).addBox(-2.0F, -2.65F, -5.25F, 4.0F, 5.0F, 7.0F, 0.0F, false);
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, -5.85F, -1.0F);
			Neck.addChild(Head);
			setRotationAngle(Head, -1.3963F, 0.0F, 0.0F);
			Head.setTextureOffset(32, 0).addBox(-2.5F, -3.0F, -3.75F, 5.0F, 6.0F, 5.0F, 0.0F, false);
			cube_r2 = new ModelRenderer(this);
			cube_r2.setRotationPoint(0.0F, 2.1F, -2.5F);
			Head.addChild(cube_r2);
			setRotationAngle(cube_r2, 0.4363F, 0.0F, 0.0F);
			cube_r2.setTextureOffset(0, 16).addBox(-1.0F, 0.0F, -0.5F, 2.0F, 4.0F, 1.0F, 0.0F, false);
			cube_r3 = new ModelRenderer(this);
			cube_r3.setRotationPoint(0.0F, 3.1F, -0.25F);
			Head.addChild(cube_r3);
			setRotationAngle(cube_r3, 0.1745F, 0.0F, 0.0F);
			cube_r3.setTextureOffset(52, 10).addBox(-1.5F, -1.0F, -1.5F, 3.0F, 4.0F, 3.0F, 0.0F, false);
			Left_Ear = new ModelRenderer(this);
			Left_Ear.setRotationPoint(1.5F, -1.75F, -2.75F);
			Head.addChild(Left_Ear);
			setRotationAngle(Left_Ear, 0.0F, 0.6109F, 0.0F);
			Left_Ear.setTextureOffset(46, 55).addBox(-0.0249F, -0.7462F, -0.9644F, 3.0F, 1.0F, 2.0F, 0.0F, false);
			cube_r4 = new ModelRenderer(this);
			cube_r4.setRotationPoint(2.75F, 0.5F, 0.0F);
			Left_Ear.addChild(cube_r4);
			setRotationAngle(cube_r4, 0.0F, 0.0F, 0.2618F);
			cube_r4.setTextureOffset(32, 2).addBox(-0.0878F, -1.2314F, -0.4644F, 1.0F, 1.0F, 1.0F, 0.0F, false);
			Right_Ear = new ModelRenderer(this);
			Right_Ear.setRotationPoint(-1.5F, -1.75F, -2.75F);
			Head.addChild(Right_Ear);
			setRotationAngle(Right_Ear, 0.0F, -0.6109F, 0.0F);
			Right_Ear.setTextureOffset(51, 58).addBox(-2.9751F, -0.7462F, -0.9644F, 3.0F, 1.0F, 2.0F, 0.0F, false);
			cube_r5 = new ModelRenderer(this);
			cube_r5.setRotationPoint(-2.75F, 0.5F, 0.0F);
			Right_Ear.addChild(cube_r5);
			setRotationAngle(cube_r5, 0.0F, 0.0F, -0.2618F);
			cube_r5.setTextureOffset(30, 0).addBox(-0.9122F, -1.2314F, -0.4644F, 1.0F, 1.0F, 1.0F, 0.0F, false);
			Left_Antler = new ModelRenderer(this);
			Left_Antler.setRotationPoint(2.25F, -0.25F, -3.75F);
			Head.addChild(Left_Antler);
			setRotationAngle(Left_Antler, -0.3491F, -0.5236F, 0.0F);
			Left_Antler.setTextureOffset(7, 55).addBox(-0.5217F, -0.2558F, -3.4511F, 1.0F, 1.0F, 5.0F, 0.0F, false);
			cube_r6 = new ModelRenderer(this);
			cube_r6.setRotationPoint(0.0F, 0.0F, 1.0F);
			Left_Antler.addChild(cube_r6);
			setRotationAngle(cube_r6, 0.2856F, 0.5973F, 0.4812F);
			cube_r6.setTextureOffset(28, 38).addBox(-0.5217F, -0.2558F, -2.4511F, 1.0F, 1.0F, 3.0F, 0.0F, false);
			L_A_2 = new ModelRenderer(this);
			L_A_2.setRotationPoint(0.5F, 0.0F, -2.5F);
			Left_Antler.addChild(L_A_2);
			setRotationAngle(L_A_2, -0.3054F, 0.6981F, 0.0F);
			L_A_2.setTextureOffset(55, 23).addBox(-1.048F, -0.2742F, -2.9042F, 1.0F, 1.0F, 3.0F, 0.0F, false);
			L_A_3 = new ModelRenderer(this);
			L_A_3.setRotationPoint(0.0F, 0.0F, -3.0F);
			L_A_2.addChild(L_A_3);
			setRotationAngle(L_A_3, 0.0F, 0.4363F, 0.0F);
			L_A_3.setTextureOffset(41, 49).addBox(-1.084F, -0.2742F, -1.9334F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			L_A_4 = new ModelRenderer(this);
			L_A_4.setRotationPoint(0.2F, 0.0F, -1.5F);
			Left_Antler.addChild(L_A_4);
			setRotationAngle(L_A_4, 0.5236F, -0.5236F, 0.3491F);
			L_A_4.setTextureOffset(54, 43).addBox(-0.4209F, -0.2894F, -3.1091F, 1.0F, 1.0F, 3.0F, 0.0F, false);
			L_A_5 = new ModelRenderer(this);
			L_A_5.setRotationPoint(0.5F, -0.5F, -3.0F);
			L_A_4.addChild(L_A_5);
			setRotationAngle(L_A_5, 0.5236F, 0.0F, 0.0F);
			L_A_5.setTextureOffset(31, 49).addBox(-0.9209F, 0.1278F, -2.1998F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			L_A_6 = new ModelRenderer(this);
			L_A_6.setRotationPoint(0.0F, 0.0F, -2.0F);
			L_A_5.addChild(L_A_6);
			setRotationAngle(L_A_6, 0.5236F, 0.0F, 0.0F);
			L_A_6.setTextureOffset(24, 16).addBox(-0.9209F, 0.0108F, -1.2369F, 1.0F, 1.0F, 1.0F, 0.0F, false);
			L_A_7 = new ModelRenderer(this);
			L_A_7.setRotationPoint(0.0F, 0.0F, -2.0F);
			Left_Antler.addChild(L_A_7);
			setRotationAngle(L_A_7, -0.829F, -0.0873F, 0.7854F);
			L_A_7.setTextureOffset(21, 49).addBox(-0.339F, -0.3988F, -1.8377F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			L_A_8 = new ModelRenderer(this);
			L_A_8.setRotationPoint(0.5F, -0.5F, -2.0F);
			L_A_7.addChild(L_A_8);
			setRotationAngle(L_A_8, 1.2217F, 0.0F, 0.0F);
			L_A_8.setTextureOffset(39, 54).addBox(-0.839F, 0.1871F, -5.0396F, 1.0F, 1.0F, 5.0F, 0.0F, false);
			L_A_9 = new ModelRenderer(this);
			L_A_9.setRotationPoint(0.0F, 0.0F, -5.0F);
			L_A_8.addChild(L_A_9);
			setRotationAngle(L_A_9, 0.8727F, 0.0F, 0.0F);
			L_A_9.setTextureOffset(10, 43).addBox(-0.839F, 0.0899F, -2.1688F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			Right_Antler = new ModelRenderer(this);
			Right_Antler.setRotationPoint(-2.25F, -0.25F, -3.75F);
			Head.addChild(Right_Antler);
			setRotationAngle(Right_Antler, -0.3491F, 0.5236F, 0.0F);
			Right_Antler.setTextureOffset(0, 54).addBox(-0.4783F, -0.2558F, -3.4511F, 1.0F, 1.0F, 5.0F, 0.0F, false);
			cube_r7 = new ModelRenderer(this);
			cube_r7.setRotationPoint(0.0F, 0.0F, 1.0F);
			Right_Antler.addChild(cube_r7);
			setRotationAngle(cube_r7, 0.2856F, -0.5973F, -0.4812F);
			cube_r7.setTextureOffset(19, 16).addBox(-0.4783F, -0.2558F, -2.4511F, 1.0F, 1.0F, 3.0F, 0.0F, false);
			R_A_2 = new ModelRenderer(this);
			R_A_2.setRotationPoint(-0.5F, 0.0F, -2.5F);
			Right_Antler.addChild(R_A_2);
			setRotationAngle(R_A_2, -0.3054F, -0.6981F, 0.0F);
			R_A_2.setTextureOffset(49, 42).addBox(0.048F, -0.2742F, -2.9042F, 1.0F, 1.0F, 3.0F, 0.0F, false);
			R_A_3 = new ModelRenderer(this);
			R_A_3.setRotationPoint(0.0F, 0.0F, -3.0F);
			R_A_2.addChild(R_A_3);
			setRotationAngle(R_A_3, 0.0F, -0.4363F, 0.0F);
			R_A_3.setTextureOffset(15, 33).addBox(0.084F, -0.2742F, -1.9334F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			R_A_4 = new ModelRenderer(this);
			R_A_4.setRotationPoint(-0.2F, 0.0F, -1.5F);
			Right_Antler.addChild(R_A_4);
			setRotationAngle(R_A_4, 0.5236F, 0.5236F, -0.3491F);
			R_A_4.setTextureOffset(40, 24).addBox(-0.5791F, -0.2894F, -3.1091F, 1.0F, 1.0F, 3.0F, 0.0F, false);
			R_A_5 = new ModelRenderer(this);
			R_A_5.setRotationPoint(-0.5F, -0.5F, -3.0F);
			R_A_4.addChild(R_A_5);
			setRotationAngle(R_A_5, 0.5236F, 0.0F, 0.0F);
			R_A_5.setTextureOffset(0, 33).addBox(-0.0791F, 0.1278F, -2.1998F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			R_A_6 = new ModelRenderer(this);
			R_A_6.setRotationPoint(0.0F, 0.0F, -2.0F);
			R_A_5.addChild(R_A_6);
			setRotationAngle(R_A_6, 0.5236F, 0.0F, 0.0F);
			R_A_6.setTextureOffset(0, 21).addBox(-0.0791F, 0.0108F, -1.2369F, 1.0F, 1.0F, 1.0F, 0.0F, false);
			R_A_7 = new ModelRenderer(this);
			R_A_7.setRotationPoint(0.0F, 0.0F, -2.0F);
			Right_Antler.addChild(R_A_7);
			setRotationAngle(R_A_7, -0.829F, 0.0873F, -0.7854F);
			R_A_7.setTextureOffset(15, 30).addBox(-0.661F, -0.3988F, -1.8377F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			R_A_8 = new ModelRenderer(this);
			R_A_8.setRotationPoint(-0.5F, -0.5F, -2.0F);
			R_A_7.addChild(R_A_8);
			setRotationAngle(R_A_8, 1.2217F, 0.0F, 0.0F);
			R_A_8.setTextureOffset(52, 17).addBox(-0.161F, 0.1871F, -5.0396F, 1.0F, 1.0F, 5.0F, 0.0F, false);
			R_A_9 = new ModelRenderer(this);
			R_A_9.setRotationPoint(0.0F, 0.0F, -5.0F);
			R_A_8.addChild(R_A_9);
			setRotationAngle(R_A_9, 0.8727F, 0.0F, 0.0F);
			R_A_9.setTextureOffset(0, 30).addBox(-0.161F, 0.0899F, -2.1688F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			Left_Back_Leg = new ModelRenderer(this);
			Left_Back_Leg.setRotationPoint(4.0F, 9.5F, 4.0F);
			setRotationAngle(Left_Back_Leg, -0.2618F, 0.0F, 0.0F);
			Left_Back_Leg.setTextureOffset(33, 38).addBox(-1.5F, -1.5F, -2.5F, 3.0F, 6.0F, 5.0F, 0.0F, false);
			cube_r8 = new ModelRenderer(this);
			cube_r8.setRotationPoint(0.25F, 13.0F, 2.25F);
			Left_Back_Leg.addChild(cube_r8);
			setRotationAngle(cube_r8, 0.2618F, 0.0F, 0.0F);
			cube_r8.setTextureOffset(47, 0).addBox(-1.0F, 0.5F, 0.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
			cube_r9 = new ModelRenderer(this);
			cube_r9.setRotationPoint(0.25F, 8.5F, 2.25F);
			Left_Back_Leg.addChild(cube_r9);
			setRotationAngle(cube_r9, 0.1745F, 0.0F, 0.0F);
			cube_r9.setTextureOffset(24, 0).addBox(-1.0F, -0.5F, -0.5F, 2.0F, 6.0F, 2.0F, -0.001F, false);
			cube_r10 = new ModelRenderer(this);
			cube_r10.setRotationPoint(0.0F, 4.0F, -0.5F);
			Left_Back_Leg.addChild(cube_r10);
			setRotationAngle(cube_r10, 0.6109F, 0.0F, 0.0F);
			cube_r10.setTextureOffset(52, 0).addBox(-0.75F, -1.0F, -1.75F, 2.0F, 7.0F, 3.0F, 0.0F, false);
			Right_Back_Leg = new ModelRenderer(this);
			Right_Back_Leg.setRotationPoint(-4.0F, 9.5F, 4.0F);
			setRotationAngle(Right_Back_Leg, -0.2618F, 0.0F, 0.0F);
			Right_Back_Leg.setTextureOffset(17, 38).addBox(-1.5F, -1.5F, -2.5F, 3.0F, 6.0F, 5.0F, 0.0F, false);
			cube_r11 = new ModelRenderer(this);
			cube_r11.setRotationPoint(-0.25F, 13.0F, 2.25F);
			Right_Back_Leg.addChild(cube_r11);
			setRotationAngle(cube_r11, 0.2618F, 0.0F, 0.0F);
			cube_r11.setTextureOffset(19, 20).addBox(-1.0F, 0.5F, 0.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
			cube_r12 = new ModelRenderer(this);
			cube_r12.setRotationPoint(-0.25F, 8.5F, 2.25F);
			Right_Back_Leg.addChild(cube_r12);
			setRotationAngle(cube_r12, 0.1745F, 0.0F, 0.0F);
			cube_r12.setTextureOffset(0, 0).addBox(-1.0F, -0.5F, -0.5F, 2.0F, 6.0F, 2.0F, -0.001F, false);
			cube_r13 = new ModelRenderer(this);
			cube_r13.setRotationPoint(0.0F, 4.0F, -0.5F);
			Right_Back_Leg.addChild(cube_r13);
			setRotationAngle(cube_r13, 0.6109F, 0.0F, 0.0F);
			cube_r13.setTextureOffset(34, 49).addBox(-1.25F, -1.0F, -1.75F, 2.0F, 7.0F, 3.0F, 0.0F, false);
			Left_Front_Leg = new ModelRenderer(this);
			Left_Front_Leg.setRotationPoint(4.0F, 11.0F, -7.5F);
			Left_Front_Leg.setTextureOffset(45, 24).addBox(-2.0F, -2.0F, -2.0F, 3.0F, 7.0F, 4.0F, 0.0F, false);
			Left_Front_Leg.setTextureOffset(24, 49).addBox(-1.5F, 5.0F, -1.5F, 2.0F, 8.0F, 3.0F, 0.0F, false);
			Right_Front_Leg = new ModelRenderer(this);
			Right_Front_Leg.setRotationPoint(-4.0F, 11.0F, -7.5F);
			Right_Front_Leg.setTextureOffset(0, 43).addBox(-1.0F, -2.0F, -2.0F, 3.0F, 7.0F, 4.0F, 0.0F, false);
			Right_Front_Leg.setTextureOffset(14, 49).addBox(-0.5F, 5.0F, -1.5F, 2.0F, 8.0F, 3.0F, 0.0F, false);
			bb_main = new ModelRenderer(this);
			bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
			bb_main.setTextureOffset(0, 0).addBox(-4.0F, -19.0F, -6.5F, 8.0F, 8.0F, 8.0F, 0.0F, false);
			bb_main.setTextureOffset(21, 25).addBox(-3.5F, -19.0F, 1.5F, 7.0F, 8.0F, 5.0F, 0.0F, false);
			bb_main.setTextureOffset(47, 47).addBox(-3.0F, -18.0F, 6.0F, 6.0F, 6.0F, 2.0F, 0.0F, false);
			cube_r14 = new ModelRenderer(this);
			cube_r14.setRotationPoint(0.0F, -10.0F, -6.5F);
			bb_main.addChild(cube_r14);
			setRotationAngle(cube_r14, -0.1745F, 0.0F, 0.0F);
			cube_r14.setTextureOffset(28, 12).addBox(-4.0F, -9.0F, -4.0F, 8.0F, 8.0F, 4.0F, -0.001F, false);
			cube_r15 = new ModelRenderer(this);
			cube_r15.setRotationPoint(0.0F, -10.0F, -6.5F);
			bb_main.addChild(cube_r15);
			setRotationAngle(cube_r15, -0.6981F, 0.0F, 0.0F);
			cube_r15.setTextureOffset(0, 16).addBox(-3.0F, -6.0F, -11.0F, 6.0F, 7.0F, 7.0F, 0.0F, false);
			cube_r16 = new ModelRenderer(this);
			cube_r16.setRotationPoint(0.0F, -17.5F, 8.0F);
			bb_main.addChild(cube_r16);
			setRotationAngle(cube_r16, -0.9599F, 0.0F, 0.0F);
			cube_r16.setTextureOffset(44, 35).addBox(-2.0F, 0.0F, 0.0F, 4.0F, 2.0F, 5.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			matrixStack.push();
            matrixStack.translate(0f, 1.5f - 0.6 * 1.5f, 0f);
            matrixStack.scale(0.6f, 0.6f, 0.6f);
			Neck.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Left_Back_Leg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Right_Back_Leg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Left_Front_Leg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Right_Front_Leg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			bb_main.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			matrixStack.pop();
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
			this.Neck.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Neck.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.Left_Back_Leg.rotateAngleX = MathHelper.cos(f * 0.7F) * 1.0F * f1;
			this.Left_Front_Leg.rotateAngleX = MathHelper.cos(f * 0.7F) * -1.0F * f1;
			this.Right_Front_Leg.rotateAngleX = MathHelper.cos(f * 0.7F) * 1.0F * f1;
			this.Right_Back_Leg.rotateAngleX = MathHelper.cos(f * 0.7F) * -1.0F * f1;
		}
	}

}
