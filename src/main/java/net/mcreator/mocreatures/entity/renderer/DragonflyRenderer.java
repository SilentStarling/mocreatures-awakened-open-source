package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.DragonflyEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class DragonflyRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(DragonflyEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelMoCModelDragonfly(), 0.3f) {
					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						DragonflyEntity.CustomEntity _dragonfly = (DragonflyEntity.CustomEntity) entity;
						return new ResourceLocation("mocreatures:textures/moc_dragonfly_" + String.valueOf(_dragonfly.getVariant()) + ".png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.7.4
	// Exported for Minecraft version 1.15
	// Paste this class into your mod and generate all required imports
	public static class ModelMoCModelDragonfly extends EntityModel<Entity> {
		private final ModelRenderer Abdomen;
		private final ModelRenderer FrontLegs;
		private final ModelRenderer RearLegs;
		private final ModelRenderer MidLegs;
		private final ModelRenderer Thorax;
		private final ModelRenderer MovementHead;
		private final ModelRenderer Head;
		private final ModelRenderer RAntenna;
		private final ModelRenderer LAntenna;
		private final ModelRenderer Mouth;
		private final ModelRenderer MovementLWing;
		private final ModelRenderer WingFrontLeft;
		private final ModelRenderer WingRearLeft;
		private final ModelRenderer MovementRWing;
		private final ModelRenderer WingRearRight;
		private final ModelRenderer WingFrontRight;
		public ModelMoCModelDragonfly() {
			textureWidth = 32;
			textureHeight = 32;
			Abdomen = new ModelRenderer(this);
			Abdomen.setRotationPoint(0.0F, 20.5F, 0.0F);
			setRotationAngle(Abdomen, 1.4277F, 0.0F, 0.0F);
			Abdomen.setTextureOffset(8, 0).addBox(-0.5F, 0.0F, -1.0F, 1.0F, 7.0F, 1.0F, 0.0F, false);
			FrontLegs = new ModelRenderer(this);
			FrontLegs.setRotationPoint(0.0F, 21.5F, -1.8F);
			setRotationAngle(FrontLegs, 0.1487F, 0.0F, 0.0F);
			FrontLegs.setTextureOffset(0, 8).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 3.0F, 0.0F, 0.0F, false);
			RearLegs = new ModelRenderer(this);
			RearLegs.setRotationPoint(0.0F, 22.0F, -0.4F);
			setRotationAngle(RearLegs, 1.0707F, 0.0F, 0.0F);
			RearLegs.setTextureOffset(8, 8).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 3.0F, 0.0F, 0.0F, false);
			MidLegs = new ModelRenderer(this);
			MidLegs.setRotationPoint(0.0F, 22.0F, -1.2F);
			setRotationAngle(MidLegs, 0.5949F, 0.0F, 0.0F);
			MidLegs.setTextureOffset(4, 8).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 3.0F, 0.0F, 0.0F, false);
			Thorax = new ModelRenderer(this);
			Thorax.setRotationPoint(0.0F, 20.0F, -1.0F);
			Thorax.setTextureOffset(0, 0).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			MovementHead = new ModelRenderer(this);
			MovementHead.setRotationPoint(0.0F, 21.0F, -2.0F);
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, 0.0F, 0.0F);
			MovementHead.addChild(Head);
			setRotationAngle(Head, -2.1712F, 0.0F, 0.0F);
			Head.setTextureOffset(0, 4).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
			RAntenna = new ModelRenderer(this);
			RAntenna.setRotationPoint(-0.5F, -1.3F, -0.3F);
			MovementHead.addChild(RAntenna);
			setRotationAngle(RAntenna, -1.041F, 0.7854F, 0.0F);
			RAntenna.setTextureOffset(0, 7).addBox(-0.5F, 0.0F, -1.0F, 1.0F, 0.0F, 1.0F, 0.0F, false);
			LAntenna = new ModelRenderer(this);
			LAntenna.setRotationPoint(0.5F, -1.3F, -0.3F);
			MovementHead.addChild(LAntenna);
			setRotationAngle(LAntenna, -1.041F, -0.7854F, 0.0F);
			LAntenna.setTextureOffset(4, 7).addBox(-0.5F, 0.0F, -1.0F, 1.0F, 0.0F, 1.0F, 0.0F, false);
			Mouth = new ModelRenderer(this);
			Mouth.setRotationPoint(0.0F, 0.1F, -0.3F);
			MovementHead.addChild(Mouth);
			setRotationAngle(Mouth, -2.1712F, 0.0F, 0.0F);
			Mouth.setTextureOffset(0, 11).addBox(-0.5F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
			MovementLWing = new ModelRenderer(this);
			MovementLWing.setRotationPoint(0.0F, 24.0F, 0.0F);
			WingFrontLeft = new ModelRenderer(this);
			WingFrontLeft.setRotationPoint(1.0F, -4.0F, -1.0F);
			MovementLWing.addChild(WingFrontLeft);
			setRotationAngle(WingFrontLeft, 0.0F, 0.1396F, -0.0873F);
			WingFrontLeft.setTextureOffset(0, 30).addBox(0.0F, 0.0F, -1.0F, 7.0F, 0.0F, 2.0F, 0.0F, false);
			WingRearLeft = new ModelRenderer(this);
			WingRearLeft.setRotationPoint(1.0F, -4.0F, -1.0F);
			MovementLWing.addChild(WingRearLeft);
			setRotationAngle(WingRearLeft, 0.0F, -0.3491F, 0.0873F);
			WingRearLeft.setTextureOffset(0, 26).addBox(0.0F, 0.0F, -1.0F, 7.0F, 0.0F, 2.0F, 0.0F, false);
			MovementRWing = new ModelRenderer(this);
			MovementRWing.setRotationPoint(0.0F, 24.0F, 0.0F);
			WingRearRight = new ModelRenderer(this);
			WingRearRight.setRotationPoint(-1.0F, -4.0F, -1.0F);
			MovementRWing.addChild(WingRearRight);
			setRotationAngle(WingRearRight, 0.0F, 0.3491F, -0.0873F);
			WingRearRight.setTextureOffset(0, 24).addBox(-7.0F, 0.0F, -1.0F, 7.0F, 0.0F, 2.0F, 0.0F, false);
			WingFrontRight = new ModelRenderer(this);
			WingFrontRight.setRotationPoint(-1.0F, -4.0F, -1.0F);
			MovementRWing.addChild(WingFrontRight);
			setRotationAngle(WingFrontRight, 0.0F, -0.1396F, 0.0873F);
			WingFrontRight.setTextureOffset(0, 28).addBox(-7.0F, 0.0F, -1.0F, 7.0F, 0.0F, 2.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Abdomen.render(matrixStack, buffer, packedLight, packedOverlay);
			FrontLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			RearLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			MidLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			Thorax.render(matrixStack, buffer, packedLight, packedOverlay);
			MovementHead.render(matrixStack, buffer, packedLight, packedOverlay);
			MovementLWing.render(matrixStack, buffer, packedLight, packedOverlay);
			MovementRWing.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
			this.MovementLWing.rotateAngleZ = MathHelper.cos(f * 0.6662F) * f1;
			this.MovementHead.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.MovementHead.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.MovementRWing.rotateAngleZ = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
		}
	}
}
