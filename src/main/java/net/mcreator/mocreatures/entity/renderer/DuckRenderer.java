
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.DuckEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class DuckRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(DuckEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelDuck(), 0.2f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						DuckEntity.CustomEntity _duck = (DuckEntity.CustomEntity) entity;
						return new ResourceLocation("mocreatures:textures/moc_duck_" + String.valueOf(_duck.getVariant()) + ".png");
					}
				};
			});
		}
	}

	// Made with Blockbench 4.2.4
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelDuck extends EntityModel<Entity> {
		private final ModelRenderer Neck;
		private final ModelRenderer Head;
		private final ModelRenderer RightWing;
		private final ModelRenderer LeftWing;
		private final ModelRenderer Tail;
		private final ModelRenderer RightLeg;
		private final ModelRenderer RightFoot;
		private final ModelRenderer cube_r1;
		private final ModelRenderer cube_r2;
		private final ModelRenderer cube_r3;
		private final ModelRenderer LeftLeg;
		private final ModelRenderer LeftFoot;
		private final ModelRenderer cube_r4;
		private final ModelRenderer cube_r5;
		private final ModelRenderer cube_r6;
		private final ModelRenderer bb_main;

		public ModelDuck() {
			textureWidth = 64;
			textureHeight = 64;
			Neck = new ModelRenderer(this);
			Neck.setRotationPoint(0.0F, 14.25F, -3.0F);
			setRotationAngle(Neck, -0.3491F, 0.0F, 0.0F);
			Neck.setTextureOffset(28, 16).addBox(-1.5F, -4.0F, -1.5F, 3.0F, 5.0F, 3.0F, -0.001F, false);
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, -3.4F, -0.5F);
			Neck.addChild(Head);
			setRotationAngle(Head, 0.3491F, 0.0F, 0.0F);
			Head.setTextureOffset(10, 16).addBox(-1.5F, -3.0F, -3.0F, 3.0F, 3.0F, 5.0F, 0.0F, false);
			Head.setTextureOffset(20, 29).addBox(-1.0F, -2.0F, -5.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			RightWing = new ModelRenderer(this);
			RightWing.setRotationPoint(-3.5F, 14.25F, -3.5F);
			RightWing.setTextureOffset(18, 16).addBox(-0.5F, 0.0F, -1.0F, 1.0F, 5.0F, 8.0F, 0.0F, true);
			LeftWing = new ModelRenderer(this);
			LeftWing.setRotationPoint(3.5F, 14.25F, -3.5F);
			LeftWing.setTextureOffset(0, 16).addBox(-0.5F, 0.0F, -1.0F, 1.0F, 5.0F, 8.0F, 0.0F, false);
			Tail = new ModelRenderer(this);
			Tail.setRotationPoint(0.0F, 15.75F, 4.0F);
			setRotationAngle(Tail, -0.5236F, 0.0F, 0.0F);
			Tail.setTextureOffset(22, 0).addBox(-2.5F, -1.5F, 0.0F, 5.0F, 3.0F, 3.0F, 0.0F, false);
			RightLeg = new ModelRenderer(this);
			RightLeg.setRotationPoint(2.0F, 20.25F, 1.0F);
			setRotationAngle(RightLeg, -0.0873F, 0.0F, 0.0F);
			RightLeg.setTextureOffset(21, 16).addBox(-0.5F, -0.75F, -0.25F, 1.0F, 4.0F, 1.0F, 0.25F, false);
			RightFoot = new ModelRenderer(this);
			RightFoot.setRotationPoint(0.0F, 3.25F, 0.75F);
			RightLeg.addChild(RightFoot);
			setRotationAngle(RightFoot, 0.0873F, 0.0F, 0.0F);
			RightFoot.setTextureOffset(0, 20).addBox(-0.5F, -0.5F, -3.0F, 1.0F, 1.0F, 3.0F, -0.1F, false);
			cube_r1 = new ModelRenderer(this);
			cube_r1.setRotationPoint(0.0F, 0.0F, -1.5F);
			RightFoot.addChild(cube_r1);
			setRotationAngle(cube_r1, 0.0F, 1.5708F, 0.0F);
			cube_r1.setTextureOffset(32, 9).addBox(-0.75F, -0.5F, -0.75F, 2.0F, 1.0F, 2.0F, -0.15F, false);
			cube_r1.setTextureOffset(32, 12).addBox(-0.75F, -0.5F, -1.25F, 2.0F, 1.0F, 2.0F, -0.15F, false);
			cube_r2 = new ModelRenderer(this);
			cube_r2.setRotationPoint(-0.75F, 0.5F, 0.25F);
			RightFoot.addChild(cube_r2);
			setRotationAngle(cube_r2, 0.0F, 0.4363F, 0.0F);
			cube_r2.setTextureOffset(22, 6).addBox(0.0F, -1.0F, -3.0F, 1.0F, 1.0F, 3.0F, -0.1F, false);
			cube_r3 = new ModelRenderer(this);
			cube_r3.setRotationPoint(0.75F, 0.5F, 0.25F);
			RightFoot.addChild(cube_r3);
			setRotationAngle(cube_r3, 0.0F, -0.4363F, 0.0F);
			cube_r3.setTextureOffset(12, 29).addBox(-1.0F, -1.0F, -3.0F, 1.0F, 1.0F, 3.0F, -0.1F, false);
			LeftLeg = new ModelRenderer(this);
			LeftLeg.setRotationPoint(-2.0F, 20.25F, 1.0F);
			setRotationAngle(LeftLeg, -0.0873F, 0.0F, 0.0F);
			LeftLeg.setTextureOffset(10, 16).addBox(-0.5F, -0.75F, -0.25F, 1.0F, 4.0F, 1.0F, 0.25F, false);
			LeftFoot = new ModelRenderer(this);
			LeftFoot.setRotationPoint(0.0F, 3.25F, 0.75F);
			LeftLeg.addChild(LeftFoot);
			setRotationAngle(LeftFoot, 0.0873F, 0.0F, 0.0F);
			LeftFoot.setTextureOffset(0, 0).addBox(-0.5F, -0.5F, -3.0F, 1.0F, 1.0F, 3.0F, -0.1F, false);
			cube_r4 = new ModelRenderer(this);
			cube_r4.setRotationPoint(0.0F, 0.0F, -1.5F);
			LeftFoot.addChild(cube_r4);
			setRotationAngle(cube_r4, 0.0F, -1.5708F, 0.0F);
			cube_r4.setTextureOffset(27, 6).addBox(-1.25F, -0.5F, -0.75F, 2.0F, 1.0F, 2.0F, -0.15F, false);
			cube_r4.setTextureOffset(28, 29).addBox(-1.25F, -0.5F, -1.25F, 2.0F, 1.0F, 2.0F, -0.15F, false);
			cube_r5 = new ModelRenderer(this);
			cube_r5.setRotationPoint(0.75F, 0.5F, 0.25F);
			LeftFoot.addChild(cube_r5);
			setRotationAngle(cube_r5, 0.0F, -0.4363F, 0.0F);
			cube_r5.setTextureOffset(0, 4).addBox(-1.0F, -1.0F, -3.0F, 1.0F, 1.0F, 3.0F, -0.1F, false);
			cube_r6 = new ModelRenderer(this);
			cube_r6.setRotationPoint(-0.75F, 0.5F, 0.25F);
			LeftFoot.addChild(cube_r6);
			setRotationAngle(cube_r6, 0.0F, 0.4363F, 0.0F);
			cube_r6.setTextureOffset(0, 16).addBox(0.0F, -1.0F, -3.0F, 1.0F, 1.0F, 3.0F, -0.1F, false);
			bb_main = new ModelRenderer(this);
			bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
			bb_main.setTextureOffset(0, 0).addBox(-3.0F, -9.75F, -5.0F, 6.0F, 6.0F, 10.0F, 0.0F, false);
			bb_main.setTextureOffset(0, 29).addBox(-2.5F, -9.25F, -5.5F, 5.0F, 5.0F, 1.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Neck.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			RightWing.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			LeftWing.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Tail.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			RightLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			LeftLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			bb_main.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
			this.LeftLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.RightLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		}
	}

}
