
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.server.Main;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.EntOakEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class EntOakRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(EntOakEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelEnt(), 1f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/ent_oak.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 4.2.4
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelEnt extends EntityModel<Entity> {
		private final ModelRenderer Main;
		private final ModelRenderer cube_r1;
		private final ModelRenderer cube_r2;
		private final ModelRenderer RightLeg;
		private final ModelRenderer cube_r3;
		private final ModelRenderer LeftLeg;
		private final ModelRenderer cube_r4;
		private final ModelRenderer RightArm;
		private final ModelRenderer RightForeArm;
		private final ModelRenderer LeftArm;
		private final ModelRenderer LeftForeArm;
		private final ModelRenderer Neck;
		private final ModelRenderer Head;
		private final ModelRenderer cube_r5;

		public ModelEnt() {
			textureWidth = 128;
			textureHeight = 256;
			Main = new ModelRenderer(this);
			Main.setRotationPoint(0.0F, -19.5F, 0.0F);
			Main.setTextureOffset(68, 36).addBox(-7.5F, -24.0F, -4.5F, 15.0F, 25.0F, 9.0F, 0.0F, false);
			cube_r1 = new ModelRenderer(this);
			cube_r1.setRotationPoint(4.5F, -22.0F, -1.25F);
			Main.addChild(cube_r1);
			setRotationAngle(cube_r1, 0.0F, 0.0F, -0.1745F);
			cube_r1.setTextureOffset(48, 122).addBox(-0.5F, -3.5F, -3.5F, 9.0F, 7.0F, 7.0F, 0.0F, false);
			cube_r2 = new ModelRenderer(this);
			cube_r2.setRotationPoint(-4.5F, -22.0F, -1.25F);
			Main.addChild(cube_r2);
			setRotationAngle(cube_r2, 0.0F, 0.0F, 0.1745F);
			cube_r2.setTextureOffset(48, 108).addBox(-8.5F, -3.5F, -3.5F, 9.0F, 7.0F, 7.0F, 0.0F, false);
			RightLeg = new ModelRenderer(this);
			RightLeg.setRotationPoint(-6.0F, -19.5F, 0.0F);
			RightLeg.setTextureOffset(0, 64).addBox(-3.0F, -1.5F, -3.0F, 6.0F, 20.0F, 6.0F, 0.0F, false);
			RightLeg.setTextureOffset(0, 0).addBox(-4.0F, 18.5F, -4.0F, 8.0F, 24.0F, 8.0F, 0.0F, false);
			RightLeg.setTextureOffset(32, 0).addBox(-4.5F, 23.5F, -4.5F, 9.0F, 20.0F, 9.0F, 0.0F, false);
			RightLeg.setTextureOffset(24, 83).addBox(-3.5F, 2.5F, -3.5F, 7.0F, 12.0F, 7.0F, 0.0F, false);
			cube_r3 = new ModelRenderer(this);
			cube_r3.setRotationPoint(-0.5F, 40.5F, -8.5F);
			RightLeg.addChild(cube_r3);
			setRotationAngle(cube_r3, 0.2618F, 0.0F, 0.0F);
			cube_r3.setTextureOffset(0, 192).addBox(-4.0F, -0.5F, -4.5F, 9.0F, 5.0F, 9.0F, 0.0F, false);
			LeftLeg = new ModelRenderer(this);
			LeftLeg.setRotationPoint(6.0F, -19.5F, 0.0F);
			LeftLeg.setTextureOffset(0, 90).addBox(-3.0F, -1.5F, -3.0F, 6.0F, 20.0F, 6.0F, 0.0F, false);
			LeftLeg.setTextureOffset(0, 32).addBox(-4.0F, 18.5F, -4.0F, 8.0F, 24.0F, 8.0F, 0.0F, false);
			LeftLeg.setTextureOffset(32, 29).addBox(-4.5F, 23.5F, -4.5F, 9.0F, 20.0F, 9.0F, 0.0F, false);
			LeftLeg.setTextureOffset(24, 64).addBox(-3.5F, 2.5F, -3.5F, 7.0F, 12.0F, 7.0F, 0.0F, false);
			cube_r4 = new ModelRenderer(this);
			cube_r4.setRotationPoint(-0.5F, 40.5F, -8.5F);
			LeftLeg.addChild(cube_r4);
			setRotationAngle(cube_r4, 0.2618F, 0.0F, 0.0F);
			cube_r4.setTextureOffset(0, 206).addBox(-4.0F, -0.5F, -4.5F, 9.0F, 5.0F, 9.0F, 0.0F, false);
			RightArm = new ModelRenderer(this);
			RightArm.setRotationPoint(-13.0F, -42.0F, -1.25F);
			setRotationAngle(RightArm, 0.0F, 0.0F, 0.1745F);
			RightArm.setTextureOffset(104, 108).addBox(-2.5F, -3.0F, -3.0F, 6.0F, 24.0F, 6.0F, 0.0F, false);
			RightForeArm = new ModelRenderer(this);
			RightForeArm.setRotationPoint(0.25F, 21.0F, 0.0F);
			RightArm.addChild(RightForeArm);
			setRotationAngle(RightForeArm, 0.0F, 0.0F, -0.1745F);
			RightForeArm.setTextureOffset(32, 169).addBox(-3.25F, -2.25F, -4.0F, 8.0F, 15.0F, 8.0F, 0.0F, false);
			RightForeArm.setTextureOffset(88, 226).addBox(-4.25F, 12.75F, -5.0F, 10.0F, 5.0F, 10.0F, 0.0F, false);
			RightForeArm.setTextureOffset(88, 201).addBox(-4.25F, 17.75F, -5.0F, 10.0F, 15.0F, 10.0F, 0.0F, false);
			LeftArm = new ModelRenderer(this);
			LeftArm.setRotationPoint(12.0F, -42.0F, -1.25F);
			setRotationAngle(LeftArm, 0.0F, 0.0F, -0.1745F);
			LeftArm.setTextureOffset(80, 108).addBox(-2.5F, -3.0F, -3.0F, 6.0F, 24.0F, 6.0F, 0.0F, false);
			LeftForeArm = new ModelRenderer(this);
			LeftForeArm.setRotationPoint(-0.75F, 21.0F, 0.0F);
			LeftArm.addChild(LeftForeArm);
			setRotationAngle(LeftForeArm, 0.0F, 0.0F, 0.1745F);
			LeftForeArm.setTextureOffset(0, 169).addBox(-3.25F, -2.25F, -4.0F, 8.0F, 15.0F, 8.0F, 0.0F, false);
			LeftForeArm.setTextureOffset(88, 241).addBox(-4.25F, 12.75F, -5.0F, 10.0F, 5.0F, 10.0F, 0.0F, false);
			LeftForeArm.setTextureOffset(88, 176).addBox(-4.25F, 17.75F, -5.0F, 10.0F, 15.0F, 10.0F, 0.0F, false);
			Neck = new ModelRenderer(this);
			Neck.setRotationPoint(0.0F, -40.75F, -0.5F);
			setRotationAngle(Neck, 0.0436F, 0.0F, 0.0F);
			Neck.setTextureOffset(52, 90).addBox(-4.0F, -11.0937F, -3.9226F, 8.0F, 10.0F, 8.0F, 0.0F, false);
			Neck.setTextureOffset(77, 36).addBox(-3.0F, -11.0937F, -4.9226F, 6.0F, 2.0F, 1.0F, 0.0F, false);
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, -51.5F, -0.5F);
			Head.setTextureOffset(52, 70).addBox(-4.5F, -5.6F, -3.675F, 9.0F, 7.0F, 8.0F, 0.0F, false);
			Head.setTextureOffset(83, 88).addBox(-6.0F, -15.1F, -4.75F, 12.0F, 10.0F, 10.0F, 0.0F, false);
			Head.setTextureOffset(0, 136).addBox(-10.0F, -26.1F, -6.75F, 20.0F, 13.0F, 20.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(0.0F, -39.6F, 3.75F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(-16.0F, -39.6F, 3.75F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(-16.0F, -39.6F, 3.75F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(-16.0F, -39.6F, -12.25F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(0.0F, -39.6F, -12.25F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(16.0F, -39.6F, -12.25F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(16.0F, -39.6F, 3.75F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(0.0F, -39.6F, -28.25F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(-16.0F, -39.6F, -28.25F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(-16.0F, -55.6F, -12.25F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(0.0F, -55.6F, -12.25F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(0.0F, -55.6F, 3.75F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(-16.0F, -55.6F, 3.75F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(0.0F, -39.6F, 19.75F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(-16.0F, -39.6F, 19.75F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(-32.0F, -39.6F, 3.75F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			Head.setTextureOffset(0, 224).addBox(-32.0F, -39.6F, -12.25F, 16.0F, 16.0F, 16.0F, 0.0F, false);
			cube_r5 = new ModelRenderer(this);
			cube_r5.setRotationPoint(0.0F, -7.6F, -4.0F);
			Head.addChild(cube_r5);
			setRotationAngle(cube_r5, -0.0873F, 0.0F, 0.0F);
			cube_r5.setTextureOffset(82, 88).addBox(-1.5F, 0.0F, -1.5F, 3.0F, 7.0F, 3.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Main.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			RightLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			LeftLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			RightArm.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			LeftArm.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Neck.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Head.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
			this.LeftLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.RightArm.rotateAngleX = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
			this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.RightLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.LeftArm.rotateAngleX = MathHelper.cos(f * 0.6662F) * f1;
		}
	}

}
