
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.IRenderTypeBuffer;

import net.mcreator.mocreatures.entity.FireflyEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class FireflyRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(FireflyEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelMoCModelFirefly(), 0.3f) {
					{
						this.addLayer(new GlowingLayer<>(this));
					}

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/firefly.png");
					}
				};
			});
		}
	}

	@OnlyIn(Dist.CLIENT)
	private static class GlowingLayer<T extends Entity, M extends EntityModel<T>> extends LayerRenderer<T, M> {
		public GlowingLayer(IEntityRenderer<T, M> er) {
			super(er);
		}

		public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, T entitylivingbaseIn, float limbSwing,
				float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch) {
			IVertexBuilder ivertexbuilder = bufferIn
					.getBuffer(RenderType.getEyes(new ResourceLocation("mocreatures:textures/entities/newglowfirefly.png")));
			this.getEntityModel().render(matrixStackIn, ivertexbuilder, 15728640, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1);
		}
	}

	// Made with Blockbench 3.7.4
	// Exported for Minecraft version 1.15
	// Paste this class into your mod and generate all required imports
	public static class ModelMoCModelFirefly extends EntityModel<Entity> {
		private final ModelRenderer RearLegs;
		private final ModelRenderer MidLegs;
		private final ModelRenderer Tail;
		private final ModelRenderer Abdomen;
		private final ModelRenderer FrontLegs;
		private final ModelRenderer RightShellOpen;
		private final ModelRenderer LeftShellOpen;
		private final ModelRenderer Thorax;
		private final ModelRenderer RightShell;
		private final ModelRenderer LeftShell;
		private final ModelRenderer LeftWing;
		private final ModelRenderer RightWing;
		private final ModelRenderer MovementHead;
		private final ModelRenderer Head;
		private final ModelRenderer Antenna;

		public ModelMoCModelFirefly() {
			textureWidth = 32;
			textureHeight = 32;
			RearLegs = new ModelRenderer(this);
			RearLegs.setRotationPoint(0.0F, 23.0F, -0.4F);
			setRotationAngle(RearLegs, 1.2492F, 0.0F, 0.0F);
			RearLegs.setTextureOffset(0, 9).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 3.0F, 0.0F, 0.0F, false);
			MidLegs = new ModelRenderer(this);
			MidLegs.setRotationPoint(0.0F, 23.0F, -1.2F);
			setRotationAngle(MidLegs, 1.0707F, 0.0F, 0.0F);
			MidLegs.setTextureOffset(0, 9).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
			Tail = new ModelRenderer(this);
			Tail.setRotationPoint(0.0F, 21.3F, 1.5F);
			setRotationAngle(Tail, 1.1302F, 0.0F, 0.0F);
			Tail.setTextureOffset(8, 17).addBox(-1.0F, 0.5F, -1.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
			Abdomen = new ModelRenderer(this);
			Abdomen.setRotationPoint(0.0F, 22.0F, 0.0F);
			setRotationAngle(Abdomen, 1.4277F, 0.0F, 0.0F);
			Abdomen.setTextureOffset(8, 0).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			FrontLegs = new ModelRenderer(this);
			FrontLegs.setRotationPoint(0.0F, 23.0F, -1.8F);
			setRotationAngle(FrontLegs, -0.8328F, 0.0F, 0.0F);
			FrontLegs.setTextureOffset(0, 7).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
			RightShellOpen = new ModelRenderer(this);
			RightShellOpen.setRotationPoint(-1.0F, 21.0F, -2.0F);
			setRotationAngle(RightShellOpen, 1.22F, 0.0F, -0.6458F);
			RightShellOpen.setTextureOffset(0, 12).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 0.0F, 5.0F, 0.0F, false);
			LeftShellOpen = new ModelRenderer(this);
			LeftShellOpen.setRotationPoint(1.0F, 21.0F, -2.0F);
			setRotationAngle(LeftShellOpen, 1.22F, 0.0F, 0.6458F);
			LeftShellOpen.setTextureOffset(0, 12).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 0.0F, 5.0F, 0.0F, false);
			Thorax = new ModelRenderer(this);
			Thorax.setRotationPoint(0.0F, 21.0F, -1.0F);
			Thorax.setTextureOffset(0, 0).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			RightShell = new ModelRenderer(this);
			RightShell.setRotationPoint(-1.0F, 21.0F, -2.0F);
			setRotationAngle(RightShell, 0.0175F, 0.0F, -0.6458F);
			RightShell.setTextureOffset(0, 12).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 0.0F, 5.0F, 0.0F, false);
			LeftShell = new ModelRenderer(this);
			LeftShell.setRotationPoint(1.0F, 21.0F, -2.0F);
			setRotationAngle(LeftShell, 0.0175F, 0.0F, 0.6458F);
			LeftShell.setTextureOffset(0, 12).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 0.0F, 5.0F, 0.0F, false);
			LeftWing = new ModelRenderer(this);
			LeftWing.setRotationPoint(1.0F, 21.0F, -1.0F);
			setRotationAngle(LeftWing, 0.0F, 1.0472F, 0.0F);
			LeftWing.setTextureOffset(15, 12).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 0.0F, 5.0F, 0.0F, false);
			RightWing = new ModelRenderer(this);
			RightWing.setRotationPoint(-1.0F, 21.0F, -1.0F);
			setRotationAngle(RightWing, 0.0F, -1.0472F, 0.0F);
			RightWing.setTextureOffset(15, 12).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 0.0F, 5.0F, 0.0F, false);
			MovementHead = new ModelRenderer(this);
			MovementHead.setRotationPoint(0.0F, 22.5F, -2.0F);
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, 0.0F, 0.0F);
			MovementHead.addChild(Head);
			setRotationAngle(Head, -2.1712F, 0.0F, 0.0F);
			Head.setTextureOffset(0, 4).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
			Antenna = new ModelRenderer(this);
			Antenna.setRotationPoint(0.0F, 0.0F, -1.0F);
			MovementHead.addChild(Antenna);
			setRotationAngle(Antenna, -1.6656F, 0.0F, 0.0F);
			Antenna.setTextureOffset(0, 7).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 1.0F, 0.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			RearLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			MidLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			Tail.render(matrixStack, buffer, packedLight, packedOverlay);
			Abdomen.render(matrixStack, buffer, packedLight, packedOverlay);
			FrontLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			RightShellOpen.render(matrixStack, buffer, packedLight, packedOverlay);
			LeftShellOpen.render(matrixStack, buffer, packedLight, packedOverlay);
			Thorax.render(matrixStack, buffer, packedLight, packedOverlay);
			RightShell.render(matrixStack, buffer, packedLight, packedOverlay);
			LeftShell.render(matrixStack, buffer, packedLight, packedOverlay);
			LeftWing.render(matrixStack, buffer, packedLight, packedOverlay);
			RightWing.render(matrixStack, buffer, packedLight, packedOverlay);
			MovementHead.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {

			this.MovementHead.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.MovementHead.rotateAngleX = f4 / (180F / (float) Math.PI);
		}
	}

}
