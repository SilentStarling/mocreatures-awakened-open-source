
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.FlyEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class FlyRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(FlyEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelMoCModelFly(), 0.3f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/fly.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.7.4
	// Exported for Minecraft version 1.15
	// Paste this class into your mod and generate all required imports
	public static class ModelMoCModelFly extends EntityModel<Entity> {
		private final ModelRenderer FrontLegs;
		private final ModelRenderer RearLegs;
		private final ModelRenderer MidLegs;
		private final ModelRenderer FoldedWings;
		private final ModelRenderer Head;
		private final ModelRenderer Tail;
		private final ModelRenderer Abdomen;
		private final ModelRenderer RightWing;
		private final ModelRenderer Thorax;
		private final ModelRenderer LeftWing;

		public ModelMoCModelFly() {
			textureWidth = 32;
			textureHeight = 32;
			FrontLegs = new ModelRenderer(this);
			FrontLegs.setRotationPoint(0.0F, 22.5F, -1.8F);
			setRotationAngle(FrontLegs, 0.1487F, 0.0F, 0.0F);
			FrontLegs.setTextureOffset(0, 7).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
			RearLegs = new ModelRenderer(this);
			RearLegs.setRotationPoint(0.0F, 22.5F, -0.4F);
			setRotationAngle(RearLegs, 1.0707F, 0.0F, 0.0F);
			RearLegs.setTextureOffset(0, 11).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
			MidLegs = new ModelRenderer(this);
			MidLegs.setRotationPoint(0.0F, 22.5F, -1.2F);
			setRotationAngle(MidLegs, 0.5949F, 0.0F, 0.0F);
			MidLegs.setTextureOffset(0, 9).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
			FoldedWings = new ModelRenderer(this);
			FoldedWings.setRotationPoint(0.0F, 20.5F, -2.0F);
			setRotationAngle(FoldedWings, 0.0873F, 0.0F, 0.0F);
			FoldedWings.setTextureOffset(4, 4).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 0.0F, 4.0F, 0.0F, false);
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, 21.5F, -2.0F);
			setRotationAngle(Head, -2.1712F, 0.0F, 0.0F);
			Head.setTextureOffset(0, 4).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
			Tail = new ModelRenderer(this);
			Tail.setRotationPoint(0.5F, 21.2F, 1.5F);
			setRotationAngle(Tail, 1.4277F, 0.0F, 0.0F);
			Tail.setTextureOffset(10, 2).addBox(-1.0F, 0.0F, -1.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
			Abdomen = new ModelRenderer(this);
			Abdomen.setRotationPoint(0.0F, 21.5F, 0.0F);
			setRotationAngle(Abdomen, 1.4277F, 0.0F, 0.0F);
			Abdomen.setTextureOffset(8, 0).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			RightWing = new ModelRenderer(this);
			RightWing.setRotationPoint(0.0F, 20.4F, -1.0F);
			setRotationAngle(RightWing, 0.0F, -1.0472F, 0.0F);
			RightWing.setTextureOffset(4, 4).addBox(-1.0F, 0.5F, -0.5F, 2.0F, 0.0F, 4.0F, 0.0F, false);
			Thorax = new ModelRenderer(this);
			Thorax.setRotationPoint(0.0F, 20.5F, -1.0F);
			Thorax.setTextureOffset(0, 0).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			LeftWing = new ModelRenderer(this);
			LeftWing.setRotationPoint(0.0F, 20.4F, -1.0F);
			setRotationAngle(LeftWing, 0.0F, 1.0472F, 0.0F);
			LeftWing.setTextureOffset(4, 4).addBox(-1.0F, 0.5F, -0.5F, 2.0F, 0.0F, 4.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			FrontLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			RearLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			MidLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			FoldedWings.render(matrixStack, buffer, packedLight, packedOverlay);
			Head.render(matrixStack, buffer, packedLight, packedOverlay);
			Tail.render(matrixStack, buffer, packedLight, packedOverlay);
			Abdomen.render(matrixStack, buffer, packedLight, packedOverlay);
			RightWing.render(matrixStack, buffer, packedLight, packedOverlay);
			Thorax.render(matrixStack, buffer, packedLight, packedOverlay);
			LeftWing.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {

			this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
		}
	}

}
