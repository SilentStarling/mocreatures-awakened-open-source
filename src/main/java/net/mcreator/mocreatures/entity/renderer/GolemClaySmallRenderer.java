
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.GolemClaySmallEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class GolemClaySmallRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(GolemClaySmallEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelMiniGolemTest(), 0.3f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/golemclay.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.8.4
	// Exported for Minecraft version 1.15 - 1.16
	// Paste this class into your mod and generate all required imports
	public static class ModelMiniGolemTest extends EntityModel<Entity> {
		private final ModelRenderer LArm;
		private final ModelRenderer RArm;
		private final ModelRenderer LLeg;
		private final ModelRenderer RLeg;
		private final ModelRenderer Head;
		private final ModelRenderer Body;

		public ModelMiniGolemTest() {
			textureWidth = 128;
			textureHeight = 128;
			LArm = new ModelRenderer(this);
			LArm.setRotationPoint(-5.0F, 11.5F, 0.0F);
			LArm.setTextureOffset(38, 37).addBox(-4.0F, -0.5F, -2.0F, 4.0F, 11.0F, 4.0F, 0.0F, false);
			LArm.setTextureOffset(30, 0).addBox(-5.0F, 1.5F, -3.0F, 6.0F, 3.0F, 6.0F, 0.0F, false);
			LArm.setTextureOffset(0, 20).addBox(-5.0F, 5.5F, -3.0F, 6.0F, 3.0F, 6.0F, 0.0F, false);
			RArm = new ModelRenderer(this);
			RArm.setRotationPoint(5.0F, 11.5F, 0.0F);
			RArm.setTextureOffset(0, 38).addBox(0.0F, -0.5F, -2.0F, 4.0F, 11.0F, 4.0F, 0.0F, false);
			RArm.setTextureOffset(0, 29).addBox(-1.0F, 1.5F, -3.0F, 6.0F, 3.0F, 6.0F, 0.0F, false);
			RArm.setTextureOffset(18, 23).addBox(-1.0F, 5.5F, -3.0F, 6.0F, 3.0F, 6.0F, 0.0F, false);
			LLeg = new ModelRenderer(this);
			LLeg.setRotationPoint(-2.5F, 20.0F, -0.25F);
			LLeg.setTextureOffset(16, 41).addBox(-1.5F, 0.0F, -1.75F, 3.0F, 3.0F, 4.0F, 0.0F, false);
			LLeg.setTextureOffset(36, 21).addBox(-1.5F, 3.0F, -2.75F, 3.0F, 1.0F, 5.0F, 0.0F, false);
			RLeg = new ModelRenderer(this);
			RLeg.setRotationPoint(2.5F, 20.0F, -0.25F);
			RLeg.setTextureOffset(38, 28).addBox(-1.5F, 0.0F, -1.75F, 3.0F, 3.0F, 4.0F, 0.0F, false);
			RLeg.setTextureOffset(35, 15).addBox(-1.5F, 3.0F, -2.75F, 3.0F, 1.0F, 5.0F, 0.0F, false);
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, 8.5F, 0.0F);
			Head.setTextureOffset(18, 32).addBox(-3.0F, -1.5F, -3.0F, 6.0F, 3.0F, 6.0F, 0.0F, false);
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, 15.0F, 0.0F);
			Body.setTextureOffset(0, 0).addBox(-5.0F, -5.0F, -5.0F, 10.0F, 10.0F, 10.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			LArm.render(matrixStack, buffer, packedLight, packedOverlay);
			RArm.render(matrixStack, buffer, packedLight, packedOverlay);
			LLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			RLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			Head.render(matrixStack, buffer, packedLight, packedOverlay);
			Body.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {

			this.LLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.LArm.rotateAngleX = MathHelper.cos(f * 0.6662F) * f1;
			this.RLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.RArm.rotateAngleX = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
		}
	}

}
