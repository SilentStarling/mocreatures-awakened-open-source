
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.GolemStoneEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class GolemStoneRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(GolemStoneEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelLargeGolem(), 0.7f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/stonegolem.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.8.4
	// Exported for Minecraft version 1.15 - 1.16
	// Paste this class into your mod and generate all required imports
	public static class ModelLargeGolem extends EntityModel<Entity> {
		private final ModelRenderer LArm;
		private final ModelRenderer RArm;
		private final ModelRenderer LLeg;
		private final ModelRenderer RLeg;
		private final ModelRenderer Head;
		private final ModelRenderer Body;

		public ModelLargeGolem() {
			textureWidth = 256;
			textureHeight = 256;
			LArm = new ModelRenderer(this);
			LArm.setRotationPoint(17.125F, -16.5F, 0.375F);
			LArm.setTextureOffset(0, 67).addBox(-5.125F, -4.5F, -6.375F, 11.0F, 11.0F, 12.0F, 0.0F, false);
			LArm.setTextureOffset(28, 95).addBox(-4.125F, 6.5F, -3.375F, 8.0F, 11.0F, 7.0F, 0.0F, false);
			LArm.setTextureOffset(93, 101).addBox(-3.125F, 17.5F, -3.375F, 6.0F, 12.0F, 7.0F, 0.0F, false);
			LArm.setTextureOffset(88, 26).addBox(-4.125F, 28.5F, -4.375F, 8.0F, 6.0F, 9.0F, 0.0F, false);
			RArm = new ModelRenderer(this);
			RArm.setRotationPoint(-17.125F, -16.5F, 0.375F);
			RArm.setTextureOffset(68, 5).addBox(-5.875F, -4.5F, -6.375F, 11.0F, 11.0F, 12.0F, 0.0F, false);
			RArm.setTextureOffset(100, 83).addBox(-3.875F, 6.5F, -3.375F, 8.0F, 11.0F, 7.0F, 0.0F, false);
			RArm.setTextureOffset(51, 106).addBox(-2.875F, 17.5F, -3.375F, 6.0F, 12.0F, 7.0F, 0.0F, false);
			RArm.setTextureOffset(93, 49).addBox(-3.875F, 28.5F, -4.375F, 8.0F, 6.0F, 9.0F, 0.0F, false);
			LLeg = new ModelRenderer(this);
			LLeg.setRotationPoint(7.0F, 10.75F, -0.25F);
			LLeg.setTextureOffset(34, 79).addBox(-4.0F, 9.25F, -6.75F, 8.0F, 4.0F, 12.0F, 0.0F, false);
			LLeg.setTextureOffset(72, 83).addBox(-3.0F, -4.75F, -3.75F, 6.0F, 15.0F, 9.0F, 0.0F, false);
			RLeg = new ModelRenderer(this);
			RLeg.setRotationPoint(-7.0F, 10.75F, -0.25F);
			RLeg.setTextureOffset(79, 67).addBox(-4.0F, 9.25F, -6.75F, 8.0F, 4.0F, 12.0F, 0.0F, false);
			RLeg.setTextureOffset(0, 88).addBox(-3.0F, -4.75F, -3.75F, 6.0F, 15.0F, 9.0F, 0.0F, false);
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, -24.0F, 0.5F);
			Head.setTextureOffset(45, 58).addBox(-6.0F, -3.0F, -5.5F, 12.0F, 10.0F, 11.0F, 0.0F, false);
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, -6.0F, 0.0F);
			Body.setTextureOffset(0, 47).addBox(-7.0F, 12.0F, -7.0F, 14.0F, 5.0F, 14.0F, 0.0F, false);
			Body.setTextureOffset(0, 17).addBox(-8.0F, -4.0F, -7.0F, 16.0F, 16.0F, 14.0F, 0.0F, false);
			Body.setTextureOffset(0, 0).addBox(-10.0F, -5.0F, -8.0F, 20.0F, 9.0F, 16.0F, 0.0F, false);
			Body.setTextureOffset(0, 0).addBox(-15.0F, -13.0F, -5.0F, 30.0F, 9.0F, 10.0F, 0.0F, false);
			Body.setTextureOffset(46, 33).addBox(-7.0F, -15.0F, -7.0F, 14.0F, 11.0F, 14.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			LArm.render(matrixStack, buffer, packedLight, packedOverlay);
			RArm.render(matrixStack, buffer, packedLight, packedOverlay);
			LLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			RLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			Head.render(matrixStack, buffer, packedLight, packedOverlay);
			Body.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {

			this.LLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.LArm.rotateAngleX = MathHelper.cos(f * 0.6662F) * f1;
			this.RLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.RArm.rotateAngleX = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
		}
	}

}
