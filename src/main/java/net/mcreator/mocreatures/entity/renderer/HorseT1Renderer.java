
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.HorseT1Entity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class HorseT1Renderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(HorseT1Entity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelHorse(), 0.5f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/horset1black.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 4.6.4
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelHorse extends EntityModel<Entity> {
		private final ModelRenderer Head;
		private final ModelRenderer cube_r1;
		private final ModelRenderer cube_r2;
		private final ModelRenderer cube_r3;
		private final ModelRenderer cube_r4;
		private final ModelRenderer Reigns;
		private final ModelRenderer cube_r5;
		private final ModelRenderer Body;
		private final ModelRenderer Tail;
		private final ModelRenderer cube_r6;
		private final ModelRenderer cube_r7;
		private final ModelRenderer cube_r8;
		private final ModelRenderer BackRLeg;
		private final ModelRenderer BackLLeg;
		private final ModelRenderer FrontLLeg;
		private final ModelRenderer FrontRLeg;
		private final ModelRenderer Saddle;
		private final ModelRenderer Chest;
		private final ModelRenderer cube_r9;

		public ModelHorse() {
			textureWidth = 128;
			textureHeight = 128;
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0286F, 1.9585F, -9.8679F);
			cube_r1 = new ModelRenderer(this);
			cube_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
			Head.addChild(cube_r1);
			setRotationAngle(cube_r1, 0.5236F, -0.0857F, 0.1501F);
			cube_r1.setTextureOffset(0, 12).addBox(-1.0F, -14.0F, 3.0F, 2.0F, 7.0F, 1.0F, 0.0F, false);
			cube_r2 = new ModelRenderer(this);
			cube_r2.setRotationPoint(0.0F, 0.0F, 0.0F);
			Head.addChild(cube_r2);
			setRotationAngle(cube_r2, 0.517F, 0.0869F, -0.1515F);
			cube_r2.setTextureOffset(0, 12).addBox(-1.2F, -14.0F, 3.0F, 2.0F, 7.0F, 1.0F, 0.0F, false);
			cube_r3 = new ModelRenderer(this);
			cube_r3.setRotationPoint(0.0F, 0.0F, 0.0F);
			Head.addChild(cube_r3);
			setRotationAngle(cube_r3, 0.5236F, 0.0F, 0.0F);
			cube_r3.setTextureOffset(24, 0).addBox(-0.4F, -15.0F, 1.0F, 1.0F, 8.0F, 1.0F, 0.0F, false);
			cube_r4 = new ModelRenderer(this);
			cube_r4.setRotationPoint(-0.0286F, -0.9585F, 9.8679F);
			Head.addChild(cube_r4);
			setRotationAngle(cube_r4, 0.5236F, 0.0F, 0.0F);
			cube_r4.setTextureOffset(24, 27).addBox(-1.8F, -10.0F, -16.3F, 4.0F, 2.0F, 5.0F, 0.0F, false);
			cube_r4.setTextureOffset(24, 18).addBox(-1.8F, -13.0F, -17.0F, 4.0F, 3.0F, 6.0F, 0.0F, false);
			cube_r4.setTextureOffset(0, 0).addBox(0.5F, -15.7F, -5.9F, 2.0F, 3.0F, 1.0F, 0.0F, false);
			cube_r4.setTextureOffset(0, 0).addBox(-2.4F, -15.7F, -5.9F, 2.0F, 3.0F, 1.0F, 0.0F, false);
			cube_r4.setTextureOffset(58, 1).addBox(-1.0F, -14.0F, -5.0F, 2.0F, 16.0F, 4.0F, 0.0F, false);
			cube_r4.setTextureOffset(0, 12).addBox(-2.0F, -13.0F, -12.0F, 4.0F, 14.0F, 8.0F, 0.0F, false);
			cube_r4.setTextureOffset(0, 0).addBox(-2.4F, -13.2F, -12.8F, 5.0F, 5.0F, 7.0F, 0.0F, false);
			Reigns = new ModelRenderer(this);
			Reigns.setRotationPoint(0.0F, 0.0F, 0.0F);
			Head.addChild(Reigns);
			cube_r5 = new ModelRenderer(this);
			cube_r5.setRotationPoint(-0.0286F, -1.9585F, 9.8679F);
			Reigns.addChild(cube_r5);
			setRotationAngle(cube_r5, 0.5236F, 0.0F, 0.0F);
			cube_r5.setTextureOffset(80, 12).addBox(-2.9F, -12.5F, -17.6F, 6.0F, 5.0F, 12.0F, 0.0F, false);
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, 11.0F, 9.0F);
			Body.setTextureOffset(0, 34).addBox(-5.0F, -9.0F, -19.0F, 10.0F, 10.0F, 24.0F, 0.0F, false);
			Tail = new ModelRenderer(this);
			Tail.setRotationPoint(0.1F, 6.1094F, 14.469F);
			cube_r6 = new ModelRenderer(this);
			cube_r6.setRotationPoint(-0.1F, 3.8906F, -5.469F);
			Tail.addChild(cube_r6);
			setRotationAngle(cube_r6, -1.2654F, 0.0F, 0.0F);
			cube_r6.setTextureOffset(24, 3).addBox(-1.3F, -12.0F, 2.0F, 3.0F, 4.0F, 7.0F, 0.0F, false);
			cube_r7 = new ModelRenderer(this);
			cube_r7.setRotationPoint(-0.1F, 3.8906F, -5.469F);
			Tail.addChild(cube_r7);
			setRotationAngle(cube_r7, -0.829F, 0.0F, 0.0F);
			cube_r7.setTextureOffset(38, 7).addBox(-1.4F, -10.0F, 0.4F, 3.0F, 4.0F, 7.0F, 0.0F, false);
			cube_r8 = new ModelRenderer(this);
			cube_r8.setRotationPoint(-0.1F, 3.8906F, -5.469F);
			Tail.addChild(cube_r8);
			setRotationAngle(cube_r8, -0.3491F, 0.0F, 0.0F);
			cube_r8.setTextureOffset(44, 0).addBox(-1.0F, -8.0F, 2.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
			BackRLeg = new ModelRenderer(this);
			BackRLeg.setRotationPoint(-3.4667F, 11.8333F, 11.3F);
			BackRLeg.setTextureOffset(96, 29).addBox(-2.0333F, -3.8333F, -2.5F, 4.0F, 9.0F, 5.0F, 0.0F, false);
			BackRLeg.setTextureOffset(44, 41).addBox(-1.5333F, 4.1667F, -1.5F, 3.0F, 5.0F, 3.0F, 0.0F, false);
			BackRLeg.setTextureOffset(44, 51).addBox(-1.9333F, 9.1667F, -2.0F, 4.0F, 3.0F, 4.0F, 0.0F, false);
			BackLLeg = new ModelRenderer(this);
			BackLLeg.setRotationPoint(3.5333F, 11.8333F, 11.3F);
			BackLLeg.setTextureOffset(78, 29).addBox(-2.0333F, -3.8333F, -2.5F, 4.0F, 9.0F, 5.0F, 0.0F, false);
			BackLLeg.setTextureOffset(96, 43).addBox(-1.5333F, 4.1667F, -1.5F, 3.0F, 5.0F, 3.0F, 0.0F, false);
			BackLLeg.setTextureOffset(78, 51).addBox(-1.9333F, 9.1667F, -2.0F, 4.0F, 3.0F, 4.0F, 0.0F, false);
			FrontLLeg = new ModelRenderer(this);
			FrontLLeg.setRotationPoint(3.75F, 11.6667F, -8.25F);
			FrontLLeg.setTextureOffset(44, 29).addBox(-1.45F, -3.6667F, -2.05F, 3.0F, 8.0F, 4.0F, 0.0F, false);
			FrontLLeg.setTextureOffset(44, 41).addBox(-1.5F, 4.3333F, -1.5F, 3.0F, 5.0F, 3.0F, 0.0F, false);
			FrontLLeg.setTextureOffset(44, 51).addBox(-2.05F, 9.3333F, -1.95F, 4.0F, 3.0F, 4.0F, 0.0F, false);
			FrontRLeg = new ModelRenderer(this);
			FrontRLeg.setRotationPoint(-3.8333F, 11.6667F, -8.25F);
			FrontRLeg.setTextureOffset(60, 29).addBox(-1.4667F, -3.6667F, -2.05F, 3.0F, 8.0F, 4.0F, 0.0F, false);
			FrontRLeg.setTextureOffset(60, 41).addBox(-1.4667F, 4.3333F, -1.5F, 3.0F, 5.0F, 3.0F, 0.0F, false);
			FrontRLeg.setTextureOffset(60, 51).addBox(-2.0667F, 9.3333F, -1.95F, 4.0F, 3.0F, 4.0F, 0.0F, false);
			Saddle = new ModelRenderer(this);
			Saddle.setRotationPoint(0.0F, 0.0F, 0.0F);
			Saddle.setTextureOffset(80, 0).addBox(-5.0F, 1.0F, 0.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
			Saddle.setTextureOffset(106, 9).addBox(-1.5F, 0.0F, 0.0F, 3.0F, 1.0F, 2.0F, 0.0F, false);
			Saddle.setTextureOffset(80, 9).addBox(-4.0F, 0.0F, 6.0F, 8.0F, 1.0F, 2.0F, 0.0F, false);
			Saddle.setTextureOffset(74, 4).addBox(-6.0F, 7.0F, 1.5F, 1.0F, 2.0F, 2.0F, 0.0F, false);
			Saddle.setTextureOffset(99, 1).addBox(-6.0F, 1.0F, 2.0F, 1.0F, 6.0F, 1.0F, 0.0F, false);
			Saddle.setTextureOffset(102, 0).addBox(5.0F, 1.0F, 2.0F, 1.0F, 6.0F, 1.0F, 0.0F, false);
			Saddle.setTextureOffset(74, 0).addBox(5.0F, 7.0F, 1.5F, 1.0F, 2.0F, 2.0F, 0.0F, false);
			Chest = new ModelRenderer(this);
			Chest.setRotationPoint(0.0F, 0.0F, 0.0F);
			cube_r9 = new ModelRenderer(this);
			cube_r9.setRotationPoint(0.0F, 0.0F, 0.0F);
			Chest.addChild(cube_r9);
			setRotationAngle(cube_r9, 0.0F, 1.5708F, 0.0F);
			cube_r9.setTextureOffset(0, 47).addBox(-14.0F, 2.0F, 5.0F, 8.0F, 8.0F, 3.0F, 0.0F, false);
			cube_r9.setTextureOffset(0, 34).addBox(-14.0F, 2.0F, -8.0F, 8.0F, 8.0F, 3.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Head.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Body.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Tail.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			BackRLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			BackLLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			FrontLLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			FrontRLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Saddle.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Chest.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
			this.FrontLLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.BackRLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.BackLLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.FrontRLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.Tail.rotateAngleY = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		}
	}

}
