
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.IRenderTypeBuffer;

import net.mcreator.mocreatures.entity.JellyfishBarrelEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class JellyfishBarrelRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(JellyfishBarrelEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelBarreljelly(), 1f) {
					{
						this.addLayer(new GlowingLayer<>(this));
					}

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/barreljelly.png");
					}
				};
			});
		}
	}

	@OnlyIn(Dist.CLIENT)
	private static class GlowingLayer<T extends Entity, M extends EntityModel<T>> extends LayerRenderer<T, M> {
		public GlowingLayer(IEntityRenderer<T, M> er) {
			super(er);
		}

		public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, T entitylivingbaseIn, float limbSwing,
				float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch) {
			IVertexBuilder ivertexbuilder = bufferIn
					.getBuffer(RenderType.getEyes(new ResourceLocation("mocreatures:textures/entities/barrelglow.png")));
			this.getEntityModel().render(matrixStackIn, ivertexbuilder, 15728640, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1);
		}
	}

	// Made with Blockbench 3.8.4
	// Exported for Minecraft version 1.15 - 1.16
	// Paste this class into your mod and generate all required imports
	public static class ModelBarreljelly extends EntityModel<Entity> {
		private final ModelRenderer Body;
		private final ModelRenderer cube_r1;
		private final ModelRenderer bb_main;
		private final ModelRenderer cube_r2;

		public ModelBarreljelly() {
			textureWidth = 256;
			textureHeight = 256;
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, 24.0F, 0.0F);
			Body.setTextureOffset(94, 28).addBox(-15.0F, -66.0F, -9.0F, 2.0F, 11.0F, 20.0F, 0.0F, false);
			Body.setTextureOffset(86, 63).addBox(13.0F, -66.0F, -9.0F, 2.0F, 11.0F, 20.0F, 0.0F, false);
			Body.setTextureOffset(0, 84).addBox(-10.0F, -66.0F, 14.0F, 20.0F, 11.0F, 2.0F, 0.0F, false);
			Body.setTextureOffset(82, 4).addBox(-10.0F, -66.0F, -14.0F, 20.0F, 11.0F, 2.0F, 0.0F, false);
			Body.setTextureOffset(0, 0).addBox(-10.0F, -72.0F, -9.0F, 20.0F, 3.0F, 20.0F, 0.0F, false);
			Body.setTextureOffset(0, 63).addBox(-13.0F, -69.0F, 10.0F, 26.0F, 17.0F, 4.0F, 0.0F, false);
			Body.setTextureOffset(54, 27).addBox(-13.0F, -69.0F, -12.0F, 26.0F, 17.0F, 4.0F, 0.0F, false);
			Body.setTextureOffset(42, 83).addBox(9.0F, -69.0F, -8.0F, 4.0F, 17.0F, 18.0F, 0.0F, false);
			Body.setTextureOffset(62, 48).addBox(-13.0F, -69.0F, -8.0F, 4.0F, 17.0F, 18.0F, 0.0F, false);
			Body.setTextureOffset(82, 2).addBox(-14.0F, -52.0F, 14.0F, 28.0F, 1.0F, 1.0F, 0.0F, false);
			Body.setTextureOffset(82, 0).addBox(-14.0F, -52.0F, -13.0F, 28.0F, 1.0F, 1.0F, 0.0F, false);
			Body.setTextureOffset(54, 0).addBox(-14.0F, -52.0F, -12.0F, 1.0F, 1.0F, 26.0F, 0.0F, false);
			Body.setTextureOffset(26, 36).addBox(13.0F, -52.0F, -12.0F, 1.0F, 1.0F, 26.0F, 0.0F, false);
			Body.setTextureOffset(140, 122).addBox(-10.0F, -25.0F, 0.0F, 2.0F, 7.0F, 2.0F, 0.0F, false);
			Body.setTextureOffset(40, 138).addBox(-8.0F, -25.0F, -7.3F, 2.0F, 7.0F, 2.0F, 0.0F, false);
			Body.setTextureOffset(126, 4).addBox(-0.4F, -25.0F, -10.0F, 2.0F, 7.0F, 2.0F, 0.0F, false);
			Body.setTextureOffset(106, 17).addBox(6.6F, -25.0F, -7.5F, 2.0F, 7.0F, 2.0F, 0.0F, false);
			Body.setTextureOffset(98, 17).addBox(9.0F, -25.0F, -0.5F, 2.0F, 7.0F, 2.0F, 0.0F, false);
			Body.setTextureOffset(88, 57).addBox(6.7F, -25.0F, 6.9F, 2.0F, 7.0F, 2.0F, 0.0F, false);
			Body.setTextureOffset(70, 48).addBox(-0.5F, -25.0F, 9.2F, 2.0F, 7.0F, 2.0F, 0.0F, false);
			cube_r1 = new ModelRenderer(this);
			cube_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
			Body.addChild(cube_r1);
			setRotationAngle(cube_r1, 0.0F, 0.0F, 0.0F);
			cube_r1.setTextureOffset(0, 23).addBox(-7.5F, -25.0F, 7.2F, 2.0F, 7.0F, 2.0F, 0.0F, false);
			bb_main = new ModelRenderer(this);
			bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
			bb_main.setTextureOffset(0, 23).addBox(-5.8F, -70.6F, -6.0F, 13.0F, 26.0F, 13.0F, 0.0F, false);
			bb_main.setTextureOffset(96, 120).addBox(4.8F, -47.8F, 4.8F, 6.0F, 20.0F, 6.0F, 0.0F, false);
			bb_main.setTextureOffset(48, 118).addBox(-9.6F, -47.8F, -9.6F, 6.0F, 20.0F, 6.0F, 0.0F, false);
			bb_main.setTextureOffset(118, 17).addBox(4.8F, -47.8F, -9.6F, 6.0F, 20.0F, 6.0F, 0.0F, false);
			bb_main.setTextureOffset(24, 112).addBox(7.2F, -47.8F, -2.4F, 6.0F, 20.0F, 6.0F, 0.0F, false);
			bb_main.setTextureOffset(110, 94).addBox(-2.4F, -47.8F, 7.2F, 6.0F, 20.0F, 6.0F, 0.0F, false);
			bb_main.setTextureOffset(0, 97).addBox(-12.0F, -47.8F, -2.4F, 6.0F, 20.0F, 6.0F, 0.0F, false);
			bb_main.setTextureOffset(86, 94).addBox(-2.4F, -47.8F, -12.0F, 6.0F, 20.0F, 6.0F, 0.0F, false);
			bb_main.setTextureOffset(142, 22).addBox(8.0F, -47.0F, 3.6F, 4.0F, 18.0F, 1.0F, 0.0F, false);
			bb_main.setTextureOffset(140, 103).addBox(8.0F, -47.0F, -3.3F, 4.0F, 18.0F, 1.0F, 0.0F, false);
			bb_main.setTextureOffset(140, 62).addBox(5.6F, -47.0F, -10.8F, 4.0F, 18.0F, 1.0F, 0.0F, false);
			bb_main.setTextureOffset(138, 43).addBox(-1.6F, -47.0F, -13.2F, 4.0F, 18.0F, 1.0F, 0.0F, false);
			bb_main.setTextureOffset(30, 138).addBox(-8.8F, -47.0F, -10.8F, 4.0F, 18.0F, 1.0F, 0.0F, false);
			bb_main.setTextureOffset(20, 138).addBox(-11.2F, -47.0F, -3.4F, 4.0F, 18.0F, 1.0F, 0.0F, false);
			bb_main.setTextureOffset(70, 0).addBox(-11.2F, -47.0F, 3.6F, 4.0F, 18.0F, 1.0F, 0.0F, false);
			bb_main.setTextureOffset(10, 0).addBox(-1.6F, -47.0F, 13.2F, 4.0F, 18.0F, 1.0F, 0.0F, false);
			bb_main.setTextureOffset(0, 0).addBox(5.6F, -47.0F, 10.8F, 4.0F, 18.0F, 1.0F, 0.0F, false);
			bb_main.setTextureOffset(136, 134).addBox(13.4F, -47.0F, -1.2F, 1.0F, 18.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(136, 0).addBox(3.6F, -47.0F, 8.4F, 1.0F, 18.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(134, 81).addBox(-3.4F, -47.0F, 8.4F, 1.0F, 18.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(130, 59).addBox(-13.0F, -47.0F, -1.2F, 1.0F, 18.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(10, 123).addBox(-10.6F, -47.0F, -8.4F, 1.0F, 18.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(0, 123).addBox(-3.4F, -47.0F, -10.8F, 1.0F, 18.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(120, 120).addBox(3.5F, -47.0F, -10.8F, 1.0F, 18.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(120, 59).addBox(11.0F, -47.0F, -8.4F, 1.0F, 18.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(110, 59).addBox(11.0F, -47.0F, 6.0F, 1.0F, 18.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(24, 97).addBox(-1.6F, -28.2F, 8.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(68, 91).addBox(-11.2F, -28.2F, -1.2F, 4.0F, 4.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(44, 84).addBox(-8.8F, -28.2F, -8.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(68, 83).addBox(-1.6F, -28.2F, -10.8F, 4.0F, 4.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(82, 17).addBox(5.6F, -28.2F, -8.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(54, 48).addBox(8.0F, -28.2F, -1.2F, 4.0F, 4.0F, 4.0F, 0.0F, false);
			bb_main.setTextureOffset(39, 23).addBox(5.6F, -28.2F, 6.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
			cube_r2 = new ModelRenderer(this);
			cube_r2.setRotationPoint(0.0F, 0.0F, 0.0F);
			bb_main.addChild(cube_r2);
			setRotationAngle(cube_r2, 0.0F, 0.0F, 0.0F);
			cube_r2.setTextureOffset(44, 92).addBox(-8.8F, -28.2F, 6.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
			cube_r2.setTextureOffset(130, 116).addBox(-10.6F, -47.0F, 6.0F, 1.0F, 18.0F, 4.0F, 0.0F, false);
			cube_r2.setTextureOffset(60, 0).addBox(-8.8F, -47.0F, 10.8F, 4.0F, 18.0F, 1.0F, 0.0F, false);
			cube_r2.setTextureOffset(72, 120).addBox(-9.6F, -47.8F, 4.8F, 6.0F, 20.0F, 6.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Body.render(matrixStack, buffer, packedLight, packedOverlay);
			bb_main.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {

		}
	}

}
