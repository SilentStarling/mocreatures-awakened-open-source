
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.IRenderTypeBuffer;

import net.mcreator.mocreatures.entity.JellyfishLionsManeEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class JellyfishLionsManeRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(JellyfishLionsManeEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelLargeJelly(), 1f) {
					{
						this.addLayer(new GlowingLayer<>(this));
					}

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/lionsmane.png");
					}
				};
			});
		}
	}

	@OnlyIn(Dist.CLIENT)
	private static class GlowingLayer<T extends Entity, M extends EntityModel<T>> extends LayerRenderer<T, M> {
		public GlowingLayer(IEntityRenderer<T, M> er) {
			super(er);
		}

		public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, T entitylivingbaseIn, float limbSwing,
				float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch) {
			IVertexBuilder ivertexbuilder = bufferIn
					.getBuffer(RenderType.getEyes(new ResourceLocation("mocreatures:textures/entities/maneglow.png")));
			this.getEntityModel().render(matrixStackIn, ivertexbuilder, 15728640, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1);
		}
	}

	// Made with Blockbench 3.8.4
	// Exported for Minecraft version 1.15 - 1.16
	// Paste this class into your mod and generate all required imports
	public static class ModelLargeJelly extends EntityModel<Entity> {
		private final ModelRenderer Body;
		private final ModelRenderer SmallTentacles;
		private final ModelRenderer bb_main;

		public ModelLargeJelly() {
			textureWidth = 256;
			textureHeight = 256;
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, 24.0F, 0.0F);
			Body.setTextureOffset(24, 90).addBox(-15.0F, -66.0F, -9.0F, 2.0F, 11.0F, 20.0F, 0.0F, false);
			Body.setTextureOffset(88, 87).addBox(13.0F, -66.0F, -9.0F, 2.0F, 11.0F, 20.0F, 0.0F, false);
			Body.setTextureOffset(104, 25).addBox(-10.0F, -66.0F, 14.0F, 20.0F, 11.0F, 2.0F, 0.0F, false);
			Body.setTextureOffset(90, 72).addBox(-10.0F, -66.0F, -14.0F, 20.0F, 11.0F, 2.0F, 0.0F, false);
			Body.setTextureOffset(44, 28).addBox(-10.0F, -72.0F, -9.0F, 20.0F, 3.0F, 20.0F, 0.0F, false);
			Body.setTextureOffset(64, 0).addBox(-13.0F, -69.0F, 10.0F, 26.0F, 17.0F, 4.0F, 0.0F, false);
			Body.setTextureOffset(56, 51).addBox(-13.0F, -69.0F, -12.0F, 26.0F, 17.0F, 4.0F, 0.0F, false);
			Body.setTextureOffset(0, 75).addBox(9.0F, -69.0F, -8.0F, 4.0F, 17.0F, 18.0F, 0.0F, false);
			Body.setTextureOffset(64, 72).addBox(-13.0F, -69.0F, -8.0F, 4.0F, 17.0F, 18.0F, 0.0F, false);
			Body.setTextureOffset(64, 23).addBox(-14.0F, -52.0F, 14.0F, 28.0F, 1.0F, 1.0F, 0.0F, false);
			Body.setTextureOffset(64, 21).addBox(-14.0F, -52.0F, -13.0F, 28.0F, 1.0F, 1.0F, 0.0F, false);
			Body.setTextureOffset(28, 51).addBox(-14.0F, -52.0F, -12.0F, 1.0F, 1.0F, 26.0F, 0.0F, false);
			Body.setTextureOffset(0, 48).addBox(13.0F, -52.0F, -12.0F, 1.0F, 1.0F, 26.0F, 0.0F, false);
			SmallTentacles = new ModelRenderer(this);
			SmallTentacles.setRotationPoint(0.0F, 24.0F, 0.0F);
			SmallTentacles.setTextureOffset(116, 51).addBox(10.0F, -52.0F, 0.0F, 2.0F, 6.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(112, 85).addBox(-12.0F, -52.0F, -9.0F, 2.0F, 6.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(112, 38).addBox(-7.0F, -52.0F, 12.0F, 2.0F, 6.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(68, 107).addBox(9.0F, -46.0F, -1.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(74, 78).addBox(-13.0F, -46.0F, -10.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(58, 78).addBox(-8.0F, -46.0F, 11.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(6, 118).addBox(10.0F, -37.0F, -2.0F, 2.0F, 5.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(74, 117).addBox(-12.0F, -37.0F, -11.0F, 2.0F, 5.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(82, 116).addBox(-7.0F, -37.0F, 10.0F, 2.0F, 5.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(56, 89).addBox(11.0F, -32.0F, -1.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(66, 78).addBox(-11.0F, -32.0F, -10.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(50, 78).addBox(-6.0F, -32.0F, 11.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(48, 89).addBox(-12.0F, -52.0F, 7.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(42, 78).addBox(4.0F, -52.0F, 12.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(34, 78).addBox(4.0F, -52.0F, -11.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(120, 97).addBox(-12.0F, -43.0F, 6.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(120, 91).addBox(4.0F, -43.0F, 11.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(120, 85).addBox(4.0F, -43.0F, -12.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(112, 93).addBox(-13.0F, -39.0F, 5.0F, 2.0F, 6.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(16, 110).addBox(3.0F, -39.0F, 10.0F, 2.0F, 6.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(8, 110).addBox(3.0F, -39.0F, -13.0F, 2.0F, 6.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(0, 110).addBox(-12.0F, -33.0F, 4.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(104, 38).addBox(4.0F, -33.0F, 9.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(48, 100).addBox(4.0F, -33.0F, -14.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(76, 107).addBox(-13.0F, -25.0F, 5.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(56, 100).addBox(3.0F, -25.0F, 10.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(120, 38).addBox(10.0F, -52.0F, 5.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(44, 64).addBox(3.0F, -25.0F, -13.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(98, 118).addBox(-12.0F, -52.0F, 0.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(114, 118).addBox(-5.0F, -52.0F, -11.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(36, 61).addBox(11.0F, -48.0F, 5.0F, 2.0F, 10.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(56, 0).addBox(-11.0F, -48.0F, 0.0F, 2.0F, 10.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(28, 61).addBox(-4.0F, -48.0F, -11.0F, 2.0F, 10.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(16, 48).addBox(12.0F, -38.0F, 6.0F, 2.0F, 12.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(48, 0).addBox(-10.0F, -38.0F, 1.0F, 2.0F, 12.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(8, 48).addBox(-3.0F, -38.0F, -10.0F, 2.0F, 12.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(44, 51).addBox(12.0F, -26.0F, 5.0F, 2.0F, 11.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(28, 48).addBox(-10.0F, -26.0F, 0.0F, 2.0F, 11.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(36, 48).addBox(-3.0F, -26.0F, -11.0F, 2.0F, 11.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(66, 119).addBox(11.0F, -15.0F, 6.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(90, 118).addBox(-11.0F, -15.0F, 1.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(106, 118).addBox(-4.0F, -15.0F, -10.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(116, 59).addBox(10.0F, -52.0F, -8.0F, 2.0F, 5.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(0, 86).addBox(-3.0F, -52.0F, 10.0F, 2.0F, 5.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(8, 86).addBox(-9.0F, -52.0F, -11.0F, 2.0F, 5.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(26, 78).addBox(10.0F, -47.0F, -7.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(0, 75).addBox(-3.0F, -47.0F, 11.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(8, 75).addBox(-8.0F, -47.0F, -10.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(0, 48).addBox(11.0F, -38.0F, -7.0F, 2.0F, 12.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(0, 0).addBox(-2.0F, -38.0F, 11.0F, 2.0F, 12.0F, 2.0F, 0.0F, false);
			SmallTentacles.setTextureOffset(8, 0).addBox(-7.0F, -38.0F, -10.0F, 2.0F, 12.0F, 2.0F, 0.0F, false);
			bb_main = new ModelRenderer(this);
			bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
			bb_main.setTextureOffset(16, 62).addBox(3.0F, -17.0F, 11.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			bb_main.setTextureOffset(14, 118).addBox(4.0F, -8.0F, 11.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			bb_main.setTextureOffset(8, 62).addBox(-14.0F, -17.0F, 5.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			bb_main.setTextureOffset(116, 66).addBox(-13.0F, -8.0F, 5.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			bb_main.setTextureOffset(0, 62).addBox(3.0F, -17.0F, -14.0F, 2.0F, 9.0F, 2.0F, 0.0F, false);
			bb_main.setTextureOffset(112, 101).addBox(4.0F, -8.0F, -14.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Body.render(matrixStack, buffer, packedLight, packedOverlay);
			SmallTentacles.render(matrixStack, buffer, packedLight, packedOverlay);
			bb_main.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {

		}
	}

}
