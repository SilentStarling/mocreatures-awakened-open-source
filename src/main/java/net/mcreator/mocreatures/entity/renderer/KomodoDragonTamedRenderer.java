
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.KomodoDragonTamedEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class KomodoDragonTamedRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(KomodoDragonTamedEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelKomodo(), 0.5f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/komodo.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.8.4
	// Exported for Minecraft version 1.15 - 1.16
	// Paste this class into your mod and generate all required imports
	public static class ModelKomodo extends EntityModel<Entity> {
		private final ModelRenderer Saddle;
		private final ModelRenderer Body;
		private final ModelRenderer Tail;
		private final ModelRenderer Head;
		private final ModelRenderer RFrontLeg;
		private final ModelRenderer LFrontLeg;
		private final ModelRenderer RBackLeg;
		private final ModelRenderer LBackLeg;

		public ModelKomodo() {
			textureWidth = 64;
			textureHeight = 64;
			Saddle = new ModelRenderer(this);
			Saddle.setRotationPoint(-6.0F, 24.0F, 2.0F);
			Saddle.setTextureOffset(20, 7).addBox(3.5F, -10.0F, -5.0F, 5.0F, 1.0F, 8.0F, 0.0F, false);
			Saddle.setTextureOffset(19, 16).addBox(3.5F, -10.5F, 1.0F, 5.0F, 1.0F, 2.0F, 0.0F, false);
			Saddle.setTextureOffset(31, 17).addBox(4.5F, -10.5F, -5.0F, 3.0F, 1.0F, 2.0F, 0.0F, false);
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, 17.375F, 1.375F);
			Body.setTextureOffset(0, 0).addBox(-3.0F, -2.875F, -0.375F, 6.0F, 7.0F, 8.0F, 0.0F, false);
			Body.setTextureOffset(0, 15).addBox(-3.0F, -2.875F, -7.375F, 6.0F, 6.0F, 7.0F, 0.0F, false);
			Body.setTextureOffset(18, 20).addBox(-2.0F, -2.875F, 6.625F, 4.0F, 5.0F, 8.0F, 0.0F, false);
			Body.setTextureOffset(34, 34).addBox(-2.0F, -2.875F, -13.375F, 4.0F, 5.0F, 6.0F, 0.0F, false);
			Tail = new ModelRenderer(this);
			Tail.setRotationPoint(0.0F, 16.2F, 17.5667F);
			Tail.setTextureOffset(14, 33).addBox(-1.0F, -1.5F, 2.4333F, 2.0F, 3.0F, 8.0F, 0.0F, false);
			Tail.setTextureOffset(34, 16).addBox(-0.5F, -1.4F, 7.4333F, 1.0F, 2.0F, 8.0F, 0.0F, false);
			Tail.setTextureOffset(0, 28).addBox(-1.5F, -1.6F, -3.8667F, 3.0F, 4.0F, 8.0F, 0.0F, false);
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, 16.7667F, -12.1667F);
			Head.setTextureOffset(38, 0).addBox(-1.5F, -2.2667F, -4.8333F, 3.0F, 2.0F, 6.0F, 0.0F, false);
			Head.setTextureOffset(20, 0).addBox(-1.0F, 0.4333F, -4.8333F, 2.0F, 1.0F, 6.0F, 0.0F, false);
			Head.setTextureOffset(15, 0).addBox(-1.5F, 0.3333F, -1.8333F, 3.0F, 0.0F, 5.0F, 0.0F, false);
			RFrontLeg = new ModelRenderer(this);
			RFrontLeg.setRotationPoint(-3.6667F, 19.1667F, -5.3333F);
			RFrontLeg.setTextureOffset(27, 45).addBox(-2.3333F, -1.6667F, -1.1667F, 4.0F, 3.0F, 3.0F, 0.0F, false);
			RFrontLeg.setTextureOffset(41, 45).addBox(-2.8333F, -0.1667F, -1.1667F, 3.0F, 4.0F, 3.0F, 0.0F, false);
			RFrontLeg.setTextureOffset(0, 40).addBox(-2.8333F, 3.8333F, -3.1667F, 3.0F, 1.0F, 5.0F, 0.0F, false);
			LFrontLeg = new ModelRenderer(this);
			LFrontLeg.setRotationPoint(3.6667F, 19.1667F, -5.3333F);
			LFrontLeg.setTextureOffset(26, 33).addBox(-1.6667F, -1.6667F, -1.1667F, 4.0F, 3.0F, 3.0F, 0.0F, false);
			LFrontLeg.setTextureOffset(12, 50).addBox(-0.1667F, -0.1667F, -1.1667F, 3.0F, 4.0F, 3.0F, 0.0F, false);
			LFrontLeg.setTextureOffset(38, 8).addBox(-0.1667F, 3.8333F, -3.1667F, 3.0F, 1.0F, 5.0F, 0.0F, false);
			RBackLeg = new ModelRenderer(this);
			RBackLeg.setRotationPoint(-3.6667F, 19.1667F, 7.6667F);
			RBackLeg.setTextureOffset(0, 47).addBox(-2.8333F, -0.1667F, -1.1667F, 3.0F, 4.0F, 3.0F, 0.0F, false);
			RBackLeg.setTextureOffset(11, 44).addBox(-2.8333F, 3.8333F, -3.1667F, 3.0F, 1.0F, 5.0F, 0.0F, false);
			RBackLeg.setTextureOffset(44, 14).addBox(-2.3333F, -1.6667F, -1.1667F, 4.0F, 3.0F, 3.0F, 0.0F, false);
			LBackLeg = new ModelRenderer(this);
			LBackLeg.setRotationPoint(3.6667F, 19.1667F, 7.6667F);
			LBackLeg.setTextureOffset(30, 0).addBox(-1.6667F, -1.6667F, -1.1667F, 4.0F, 3.0F, 3.0F, 0.0F, false);
			LBackLeg.setTextureOffset(48, 26).addBox(-0.1667F, -0.1667F, -1.1667F, 3.0F, 4.0F, 3.0F, 0.0F, false);
			LBackLeg.setTextureOffset(37, 28).addBox(-0.1667F, 3.8333F, -3.1667F, 3.0F, 1.0F, 5.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Saddle.render(matrixStack, buffer, packedLight, packedOverlay);
			Body.render(matrixStack, buffer, packedLight, packedOverlay);
			Tail.render(matrixStack, buffer, packedLight, packedOverlay);
			Head.render(matrixStack, buffer, packedLight, packedOverlay);
			RFrontLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			LFrontLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			RBackLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			LBackLeg.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
			this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.RBackLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.LBackLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.RFrontLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.LFrontLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		}
	}

}
