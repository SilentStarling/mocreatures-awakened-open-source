
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.MocWolfTamedEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class MocWolfTamedRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(MocWolfTamedEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelMocWolf(), 0.5f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						MocWolfTamedEntity.CustomEntity _moc_wolf = (MocWolfTamedEntity.CustomEntity) entity;
                       return new ResourceLocation("mocreatures:textures/moc_wolf_" + String.valueOf(_moc_wolf.getVariant()) + ".png");
					}
				};
			});
		}
	}

	// Made with Blockbench 4.4.3
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelMocWolf extends EntityModel<Entity> {
		private final ModelRenderer Head;
		private final ModelRenderer LEar_r1;
		private final ModelRenderer REar_r1;
		private final ModelRenderer Nose_r1;
		private final ModelRenderer RSide_r1;
		private final ModelRenderer LSide_r1;
		private final ModelRenderer Body;
		private final ModelRenderer Neck2_r1;
		private final ModelRenderer Neck_r1;
		private final ModelRenderer Tail;
		private final ModelRenderer TailD_r1;
		private final ModelRenderer TailC_r1;
		private final ModelRenderer TailB_r1;
		private final ModelRenderer TailA_r1;
		private final ModelRenderer RFrontLeg;
		private final ModelRenderer Leg2B_r1;
		private final ModelRenderer Leg2A_r1;
		private final ModelRenderer LFrontLeg;
		private final ModelRenderer Leg1B_r1;
		private final ModelRenderer Leg1A_r1;
		private final ModelRenderer RBackLeg;
		private final ModelRenderer Leg4C_r1;
		private final ModelRenderer Leg4B_r1;
		private final ModelRenderer Leg4A_r1;
		private final ModelRenderer LBackLeg;
		private final ModelRenderer Leg3A_r1;
		private final ModelRenderer Leg3C_r1;
		private final ModelRenderer Leg3B_r1;

		public ModelMocWolf() {
			textureWidth = 64;
			textureHeight = 128;
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0262F, 6.2227F, -11.9977F);
			Head.setTextureOffset(0, 0).addBox(-4.0262F, -2.2227F, -4.0023F, 8.0F, 8.0F, 6.0F, 0.0F, false);
			Head.setTextureOffset(16, 33).addBox(-2.0262F, 4.7773F, -5.0023F, 4.0F, 1.0F, 2.0F, 0.0F, false);
			Head.setTextureOffset(0, 25).addBox(-2.0262F, 2.7773F, -10.0023F, 4.0F, 2.0F, 6.0F, 0.0F, false);
			Head.setTextureOffset(1, 34).addBox(-2.0262F, 4.7773F, -9.5023F, 4.0F, 1.0F, 5.0F, 0.0F, false);
			LEar_r1 = new ModelRenderer(this);
			LEar_r1.setRotationPoint(1.9738F, -3.7227F, -1.0023F);
			Head.addChild(LEar_r1);
			setRotationAngle(LEar_r1, 0.0F, 0.0F, 0.2618F);
			LEar_r1.setTextureOffset(13, 14).addBox(-0.9F, -2.5F, 0.5F, 3.0F, 5.0F, 1.0F, 0.0F, false);
			REar_r1 = new ModelRenderer(this);
			REar_r1.setRotationPoint(-2.0262F, -3.7227F, -0.0023F);
			Head.addChild(REar_r1);
			setRotationAngle(REar_r1, 0.0F, 0.0F, -0.2618F);
			REar_r1.setTextureOffset(22, 0).addBox(-1.9F, -2.5F, -0.5F, 3.0F, 5.0F, 1.0F, 0.0F, false);
			Nose_r1 = new ModelRenderer(this);
			Nose_r1.setRotationPoint(-0.0262F, -0.0227F, -6.9023F);
			Head.addChild(Nose_r1);
			setRotationAngle(Nose_r1, 0.3491F, 0.0F, 0.0F);
			Nose_r1.setTextureOffset(44, 33).addBox(-1.5F, 1.6F, -3.8F, 3.0F, 2.0F, 7.0F, 0.0F, false);
			RSide_r1 = new ModelRenderer(this);
			RSide_r1.setRotationPoint(-3.2266F, 3.2773F, 2.6833F);
			Head.addChild(RSide_r1);
			setRotationAngle(RSide_r1, 0.0F, -0.2182F, 0.0F);
			RSide_r1.setTextureOffset(28, 45).addBox(-1.0F, -3.0F, -2.0F, 2.0F, 6.0F, 6.0F, 0.0F, false);
			LSide_r1 = new ModelRenderer(this);
			LSide_r1.setRotationPoint(3.1742F, 3.2773F, 2.6833F);
			Head.addChild(LSide_r1);
			setRotationAngle(LSide_r1, 0.0F, 0.2618F, 0.0F);
			LSide_r1.setTextureOffset(28, 33).addBox(-1.0F, -3.0F, -2.0F, 2.0F, 6.0F, 6.0F, 0.0F, false);
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, 11.8164F, -6.4039F);
			setRotationAngle(Body, 1.5708F, 0.0F, 0.0F);
			Body.setTextureOffset(0, 40).addBox(-3.0F, 0.4039F, -3.6836F, 6.0F, 16.0F, 8.0F, 0.0F, false);
			Body.setTextureOffset(20, 15).addBox(-4.0F, -4.0961F, -5.6836F, 8.0F, 8.0F, 10.0F, 0.0F, false);
			Neck2_r1 = new ModelRenderer(this);
			Neck2_r1.setRotationPoint(0.0F, 15.9039F, -8.1836F);
			Body.addChild(Neck2_r1);
			setRotationAngle(Neck2_r1, -2.1817F, 0.0F, 0.0F);
			Neck2_r1.setTextureOffset(0, 14).addBox(-1.5F, 5.0F, -25.5F, 3.0F, 4.0F, 7.0F, 0.0F, false);
			Neck_r1 = new ModelRenderer(this);
			Neck_r1.setRotationPoint(0.0F, 12.9039F, -6.1836F);
			Body.addChild(Neck_r1);
			setRotationAngle(Neck_r1, 2.6616F, 0.0F, 0.0F);
			Neck_r1.setTextureOffset(0, 65).addBox(-3.5F, 13.0F, -3.5F, 7.0F, 8.0F, 7.0F, 0.0F, false);
			Tail = new ModelRenderer(this);
			Tail.setRotationPoint(0.0F, 8.3723F, 9.9025F);
			TailD_r1 = new ModelRenderer(this);
			TailD_r1.setRotationPoint(0.0F, 13.0277F, -3.0025F);
			Tail.addChild(TailD_r1);
			setRotationAngle(TailD_r1, 1.0036F, 0.0F, 0.0F);
			TailD_r1.setTextureOffset(52, 69).addBox(-1.5F, 4.5F, 9.2F, 3.0F, 5.0F, 3.0F, 0.0F, false);
			TailC_r1 = new ModelRenderer(this);
			TailC_r1.setRotationPoint(0.0F, 11.5277F, -3.0025F);
			Tail.addChild(TailC_r1);
			setRotationAngle(TailC_r1, 1.0472F, 0.0F, 0.0F);
			TailC_r1.setTextureOffset(48, 59).addBox(-2.0F, 4.0F, 7.0F, 4.0F, 6.0F, 4.0F, 0.0F, false);
			TailB_r1 = new ModelRenderer(this);
			TailB_r1.setRotationPoint(0.0F, 6.1277F, 0.0975F);
			Tail.addChild(TailB_r1);
			setRotationAngle(TailB_r1, 0.6545F, 0.0F, 0.0F);
			TailB_r1.setTextureOffset(48, 49).addBox(-2.0F, -3.0F, 2.0F, 4.0F, 6.0F, 4.0F, 0.0F, false);
			TailA_r1 = new ModelRenderer(this);
			TailA_r1.setRotationPoint(0.0F, 2.1277F, -0.9025F);
			Tail.addChild(TailA_r1);
			setRotationAngle(TailA_r1, 1.1781F, 0.0F, 0.0F);
			TailA_r1.setTextureOffset(52, 42).addBox(-1.5F, -1.0F, -0.2F, 3.0F, 4.0F, 3.0F, 0.0F, false);
			RFrontLeg = new ModelRenderer(this);
			RFrontLeg.setRotationPoint(-4.9689F, 14.0797F, -6.2811F);
			RFrontLeg.setTextureOffset(28, 106).addBox(-1.5378F, 8.4203F, -2.2189F, 3.0F, 2.0F, 3.0F, 0.0F, false);
			Leg2B_r1 = new ModelRenderer(this);
			Leg2B_r1.setRotationPoint(0.0689F, 5.6412F, 0.2356F);
			RFrontLeg.addChild(Leg2B_r1);
			setRotationAngle(Leg2B_r1, -0.1745F, 0.0F, 0.0F);
			Leg2B_r1.setTextureOffset(28, 96).addBox(-1.0F, -4.0F, -1.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
			Leg2A_r1 = new ModelRenderer(this);
			Leg2A_r1.setRotationPoint(4.9689F, 9.9203F, 6.2811F);
			RFrontLeg.addChild(Leg2A_r1);
			setRotationAngle(Leg2A_r1, 0.2182F, 0.0F, 0.0F);
			Leg2A_r1.setTextureOffset(28, 84).addBox(-6.0F, -15.0F, -5.5F, 2.0F, 8.0F, 4.0F, 0.0F, false);
			LFrontLeg = new ModelRenderer(this);
			LFrontLeg.setRotationPoint(4.9644F, 13.5519F, -5.6955F);
			LFrontLeg.setTextureOffset(28, 79).addBox(-1.4711F, 8.9481F, -2.2045F, 3.0F, 2.0F, 3.0F, 0.0F, false);
			Leg1B_r1 = new ModelRenderer(this);
			Leg1B_r1.setRotationPoint(-4.9644F, 9.4481F, 5.6955F);
			LFrontLeg.addChild(Leg1B_r1);
			setRotationAngle(Leg1B_r1, -0.1745F, 0.0F, 0.0F);
			Leg1B_r1.setTextureOffset(28, 69).addBox(3.9F, -6.8F, -7.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
			Leg1A_r1 = new ModelRenderer(this);
			Leg1A_r1.setRotationPoint(0.0356F, -1.5519F, -0.8045F);
			LFrontLeg.addChild(Leg1A_r1);
			setRotationAngle(Leg1A_r1, 0.2182F, 0.0F, 0.0F);
			Leg1A_r1.setTextureOffset(28, 57).addBox(-1.0F, -2.8F, -1.0F, 2.0F, 8.0F, 4.0F, 0.0F, false);
			RBackLeg = new ModelRenderer(this);
			RBackLeg.setRotationPoint(-3.9767F, 13.1672F, 6.7582F);
			RBackLeg.setTextureOffset(14, 93).addBox(-1.53F, 9.3328F, 0.8418F, 3.0F, 2.0F, 3.0F, 0.0F, false);
			Leg4C_r1 = new ModelRenderer(this);
			Leg4C_r1.setRotationPoint(-3.0233F, 20.5998F, 6.0824F);
			RBackLeg.addChild(Leg4C_r1);
			setRotationAngle(Leg4C_r1, -0.1745F, 0.0F, 0.0F);
			Leg4C_r1.setTextureOffset(14, 83).addBox(2.0F, -16.7F, -6.3F, 2.0F, 8.0F, 2.0F, 0.0F, false);
			Leg4B_r1 = new ModelRenderer(this);
			Leg4B_r1.setRotationPoint(3.9767F, 9.8328F, -7.7582F);
			RBackLeg.addChild(Leg4B_r1);
			setRotationAngle(Leg4B_r1, -0.5236F, 0.0F, 0.0F);
			Leg4B_r1.setTextureOffset(14, 76).addBox(-4.9F, -11.8F, 2.5F, 2.0F, 2.0F, 5.0F, 0.0F, false);
			Leg4A_r1 = new ModelRenderer(this);
			Leg4A_r1.setRotationPoint(-0.0233F, 0.4547F, 0.2759F);
			RBackLeg.addChild(Leg4A_r1);
			setRotationAngle(Leg4A_r1, -0.3491F, 0.0F, 0.0F);
			Leg4A_r1.setTextureOffset(14, 64).addBox(-1.0F, -4.5F, -2.5F, 2.0F, 7.0F, 5.0F, 0.0F, false);
			LBackLeg = new ModelRenderer(this);
			LBackLeg.setRotationPoint(3.9733F, 12.9998F, 7.3964F);
			LBackLeg.setTextureOffset(0, 93).addBox(-1.48F, 9.5002F, -0.3964F, 3.0F, 2.0F, 3.0F, 0.0F, false);
			Leg3A_r1 = new ModelRenderer(this);
			Leg3A_r1.setRotationPoint(3.0267F, 16.2583F, 4.5883F);
			LBackLeg.addChild(Leg3A_r1);
			setRotationAngle(Leg3A_r1, -0.3491F, 0.0F, 0.0F);
			Leg3A_r1.setTextureOffset(0, 64).addBox(-4.0F, -17.5F, -12.5F, 2.0F, 7.0F, 5.0F, 0.0F, false);
			Leg3C_r1 = new ModelRenderer(this);
			Leg3C_r1.setRotationPoint(3.0267F, 21.7671F, 5.4442F);
			LBackLeg.addChild(Leg3C_r1);
			setRotationAngle(Leg3C_r1, -0.1745F, 0.0F, 0.0F);
			Leg3C_r1.setTextureOffset(0, 83).addBox(-4.0F, -17.9F, -7.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
			Leg3B_r1 = new ModelRenderer(this);
			Leg3B_r1.setRotationPoint(2.9267F, 22.2714F, 4.726F);
			LBackLeg.addChild(Leg3B_r1);
			setRotationAngle(Leg3B_r1, -0.5236F, 0.0F, 0.0F);
			Leg3B_r1.setTextureOffset(0, 76).addBox(-4.0F, -16.0F, -15.5F, 2.0F, 2.0F, 5.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Head.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Body.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Tail.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			RFrontLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			LFrontLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			RBackLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			LBackLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
			this.LFrontLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.RBackLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.Tail.rotateAngleZ = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.LBackLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.RFrontLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		}
	}

}
