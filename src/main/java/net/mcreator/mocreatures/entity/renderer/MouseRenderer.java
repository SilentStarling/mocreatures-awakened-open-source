package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.MouseEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class MouseRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(MouseEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelMouse(), 0.5f) {
					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						MouseEntity.CustomEntity _mouse = (MouseEntity.CustomEntity) entity;
						return new ResourceLocation("mocreatures:textures/moc_mouse_" + String.valueOf(_mouse.getVariant()) + ".png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.8.4
	// Exported for Minecraft version 1.15 - 1.16
	// Paste this class into your mod and generate all required imports
	public static class ModelMouse extends EntityModel<Entity> {
		private final ModelRenderer Head;
		private final ModelRenderer FrontL;
		private final ModelRenderer FrontR;
		private final ModelRenderer RearL;
		private final ModelRenderer RearR;
		private final ModelRenderer BodyF;
		private final ModelRenderer Tail;
		public ModelMouse() {
			textureWidth = 32;
			textureHeight = 16;
			Head = new ModelRenderer(this);
			Head.setRotationPoint(-0.25F, 21.8F, -5.2F);
			Head.setTextureOffset(12, 10).addBox(0.5F, -1.3F, -1.3F, 2.0F, 2.0F, 0.0F, 0.0F, false);
			Head.setTextureOffset(10, 10).addBox(-2.5F, -1.3F, -1.8F, 2.0F, 2.0F, 0.0F, 0.0F, false);
			Head.setTextureOffset(14, 12).addBox(0.0F, -2.3F, -0.3F, 2.0F, 2.0F, 1.0F, 0.0F, false);
			Head.setTextureOffset(7, 12).addBox(-2.0F, -2.3F, -0.3F, 2.0F, 2.0F, 1.0F, 0.0F, false);
			Head.setTextureOffset(0, 0).addBox(-1.0F, -0.8F, -2.3F, 2.0F, 2.0F, 3.0F, 0.0F, false);
			FrontL = new ModelRenderer(this);
			FrontL.setRotationPoint(1.1F, 23.5F, -3.5F);
			FrontL.setTextureOffset(0, 9).addBox(-0.5F, -0.5F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			FrontR = new ModelRenderer(this);
			FrontR.setRotationPoint(-1.2F, 23.5F, -3.5F);
			FrontR.setTextureOffset(0, 9).addBox(-0.5F, -0.5F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			RearL = new ModelRenderer(this);
			RearL.setRotationPoint(1.5F, 23.5F, 0.5F);
			RearL.setTextureOffset(0, 9).addBox(-0.5F, -0.5F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			RearR = new ModelRenderer(this);
			RearR.setRotationPoint(-1.5F, 23.5F, 0.5F);
			RearR.setTextureOffset(0, 9).addBox(-0.5F, -0.5F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			BodyF = new ModelRenderer(this);
			BodyF.setRotationPoint(0.0F, 20.0F, -2.0F);
			BodyF.setTextureOffset(10, 0).addBox(-1.5F, 0.5F, -2.5F, 3.0F, 3.0F, 6.0F, 0.0F, false);
			Tail = new ModelRenderer(this);
			Tail.setRotationPoint(-0.1F, 22.0F, 1.5F);
			Tail.setTextureOffset(18, 9).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Head.render(matrixStack, buffer, packedLight, packedOverlay);
			FrontL.render(matrixStack, buffer, packedLight, packedOverlay);
			FrontR.render(matrixStack, buffer, packedLight, packedOverlay);
			RearL.render(matrixStack, buffer, packedLight, packedOverlay);
			RearR.render(matrixStack, buffer, packedLight, packedOverlay);
			BodyF.render(matrixStack, buffer, packedLight, packedOverlay);
			Tail.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
			this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.FrontR.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.Tail.rotateAngleY = MathHelper.cos(f * 0.6662F) * f1;
			this.RearL.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.RearR.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.FrontL.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		}
	}
}
