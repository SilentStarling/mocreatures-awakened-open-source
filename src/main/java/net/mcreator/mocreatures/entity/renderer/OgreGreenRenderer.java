package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.OgreGreenEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class OgreGreenRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(OgreGreenEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelOgre(), 0.5f) {
					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						OgreGreenEntity.CustomEntity _ogre_green = (OgreGreenEntity.CustomEntity) entity;
						return new ResourceLocation("mocreatures:textures/moc_ogre_green_" + String.valueOf(_ogre_green.getVariant()) + ".png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.8.4
	// Exported for Minecraft version 1.15 - 1.16
	// Paste this class into your mod and generate all required imports
	public static class ModelOgre extends EntityModel<Entity> {
		private final ModelRenderer RArm;
		private final ModelRenderer LArm;
		private final ModelRenderer RLeg;
		private final ModelRenderer LLeg;
		private final ModelRenderer Body;
		private final ModelRenderer Head2;
		private final ModelRenderer Head3;
		private final ModelRenderer Head1;
		public ModelOgre() {
			textureWidth = 128;
			textureHeight = 128;
			RArm = new ModelRenderer(this);
			RArm.setRotationPoint(-15.0F, -10.0F, 2.0F);
			RArm.setTextureOffset(0, 66).addBox(-4.0F, -1.0F, -3.0F, 6.0F, 9.0F, 8.0F, 0.0F, false);
			RArm.setTextureOffset(0, 46).addBox(-5.0F, 7.0F, -4.0F, 8.0F, 12.0F, 8.0F, 0.0F, false);
			RArm.setTextureOffset(0, 31).addBox(0.0F, -3.0F, -4.0F, 8.0F, 7.0F, 8.0F, 0.0F, false);
			RArm.setTextureOffset(86, 64).addBox(-3.0F, 8.0F, 3.5F, 4.0F, 3.0F, 1.0F, 0.0F, false);
			RArm.setTextureOffset(74, 90).addBox(-3.0F, 14.0F, 4.0F, 3.0F, 3.0F, 2.0F, 0.0F, false);
			RArm.setTextureOffset(30, 83).addBox(-4.0F, 13.0F, -12.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);
			RArm.setTextureOffset(83, 42).addBox(-3.0F, 14.0F, -14.0F, 3.0F, 3.0F, 2.0F, 0.0F, false);
			RArm.setTextureOffset(60, 118).addBox(-4.0F, 13.0F, -19.0F, 5.0F, 5.0F, 5.0F, 0.0F, false);
			RArm.setTextureOffset(32, 39).addBox(-2.0F, 9.0F, -19.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
			RArm.setTextureOffset(52, 118).addBox(-2.5F, 14.5F, -22.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
			RArm.setTextureOffset(52, 118).addBox(-7.0F, 14.5F, -18.0F, 3.0F, 2.0F, 2.0F, 0.0F, false);
			RArm.setTextureOffset(0, 0).addBox(-2.5F, 18.0F, -19.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
			RArm.setTextureOffset(32, 3).addBox(-3.5F, 20.0F, -19.5F, 4.0F, 3.0F, 5.0F, 0.0F, false);
			RArm.setTextureOffset(52, 118).addBox(-2.5F, 18.0F, -18.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
			RArm.setTextureOffset(52, 118).addBox(-2.5F, 10.0F, -18.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
			RArm.setTextureOffset(52, 118).addBox(1.0F, 14.5F, -18.0F, 3.0F, 2.0F, 2.0F, 0.0F, false);
			RArm.setTextureOffset(24, 104).addBox(-3.0F, 14.0F, -8.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
			LArm = new ModelRenderer(this);
			LArm.setRotationPoint(7.0F, -10.0F, 2.0F);
			LArm.setTextureOffset(96, 31).addBox(0.0F, -3.0F, -4.0F, 8.0F, 7.0F, 8.0F, 0.0F, false);
			LArm.setTextureOffset(86, 64).addBox(7.0F, 8.0F, 3.5F, 4.0F, 3.0F, 1.0F, 0.0F, false);
			LArm.setTextureOffset(74, 90).addBox(7.0F, 14.0F, 4.0F, 3.0F, 3.0F, 2.0F, 0.0F, false);
			LArm.setTextureOffset(30, 83).addBox(6.0F, 13.0F, -12.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);
			LArm.setTextureOffset(83, 42).addBox(7.0F, 14.0F, -14.0F, 3.0F, 3.0F, 2.0F, 0.0F, false);
			LArm.setTextureOffset(60, 118).addBox(6.0F, 13.0F, -19.0F, 5.0F, 5.0F, 5.0F, 0.0F, false);
			LArm.setTextureOffset(24, 104).addBox(7.0F, 14.0F, -8.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
			LArm.setTextureOffset(0, 0).addBox(7.5F, 18.0F, -19.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
			LArm.setTextureOffset(32, 39).addBox(8.0F, 9.0F, -19.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
			LArm.setTextureOffset(52, 118).addBox(7.5F, 14.5F, -22.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
			LArm.setTextureOffset(96, 46).addBox(5.0F, 7.0F, -4.0F, 8.0F, 12.0F, 8.0F, 0.0F, false);
			LArm.setTextureOffset(52, 118).addBox(11.0F, 14.5F, -18.0F, 3.0F, 2.0F, 2.0F, 0.0F, false);
			LArm.setTextureOffset(52, 118).addBox(7.5F, 18.0F, -18.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
			LArm.setTextureOffset(52, 118).addBox(7.5F, 10.0F, -18.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
			LArm.setTextureOffset(52, 118).addBox(3.0F, 14.5F, -18.0F, 3.0F, 2.0F, 2.0F, 0.0F, false);
			LArm.setTextureOffset(100, 66).addBox(6.0F, -1.0F, -3.0F, 6.0F, 9.0F, 8.0F, 0.0F, false);
			RLeg = new ModelRenderer(this);
			RLeg.setRotationPoint(-2.0F, 4.0F, 1.0F);
			RLeg.setTextureOffset(0, 83).addBox(-10.0F, 0.0F, -5.0F, 10.0F, 11.0F, 10.0F, 0.0F, false);
			RLeg.setTextureOffset(0, 88).addBox(-7.0F, 10.0F, -4.75F, 4.0F, 4.0F, 1.0F, 0.0F, false);
			RLeg.setTextureOffset(20, 123).addBox(-4.0F, 18.0F, -7.0F, 3.0F, 2.0F, 3.0F, 0.0F, false);
			RLeg.setTextureOffset(0, 123).addBox(-9.0F, 18.0F, -6.5F, 5.0F, 2.0F, 3.0F, 0.0F, false);
			RLeg.setTextureOffset(0, 104).addBox(-9.0F, 9.0F, -4.0F, 8.0F, 11.0F, 8.0F, 0.0F, false);
			LLeg = new ModelRenderer(this);
			LLeg.setRotationPoint(2.0F, 4.0F, 1.0F);
			LLeg.setTextureOffset(88, 83).addBox(0.0F, 0.0F, -5.0F, 10.0F, 11.0F, 10.0F, 0.0F, false);
			LLeg.setTextureOffset(118, 88).addBox(3.0F, 10.0F, -4.75F, 4.0F, 4.0F, 1.0F, 0.0F, false);
			LLeg.setTextureOffset(112, 123).addBox(4.0F, 18.0F, -6.5F, 5.0F, 2.0F, 3.0F, 0.0F, false);
			LLeg.setTextureOffset(96, 123).addBox(1.0F, 18.0F, -7.0F, 3.0F, 2.0F, 3.0F, 0.0F, false);
			LLeg.setTextureOffset(96, 104).addBox(1.0F, 9.0F, -4.0F, 8.0F, 11.0F, 8.0F, 0.0F, false);
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, 24.0F, 0.0F);
			Body.setTextureOffset(39, 20).addBox(-7.0F, -38.0F, -3.0F, 14.0F, 3.0F, 11.0F, 0.0F, false);
			Body.setTextureOffset(32, 34).addBox(-9.5F, -36.8F, -7.3F, 19.0F, 11.0F, 13.0F, 0.0F, false);
			Body.setTextureOffset(28, 58).addBox(-11.0F, -27.0F, -6.0F, 22.0F, 11.0F, 14.0F, 0.0F, false);
			Body.setTextureOffset(32, 118).addBox(-4.0F, -16.0F, 6.0F, 8.0F, 8.0F, 2.0F, 0.0F, false);
			Body.setTextureOffset(32, 118).addBox(-4.0F, -16.0F, -6.0F, 8.0F, 8.0F, 2.0F, 0.0F, false);
			Head2 = new ModelRenderer(this);
			Head2.setRotationPoint(-7.0F, -14.2273F, -2.1818F);
			Head2.setTextureOffset(0, 0).addBox(-5.0F, -10.7727F, -3.8182F, 10.0F, 12.0F, 12.0F, 0.0F, false);
			Head2.setTextureOffset(46, 28).addBox(2.5F, -4.7727F, -4.3182F, 1.0F, 2.0F, 1.0F, 0.0F, false);
			Head2.setTextureOffset(0, 24).addBox(-4.0F, -2.7727F, -5.8182F, 8.0F, 2.0F, 2.0F, 0.0F, false);
			Head2.setTextureOffset(120, 46).addBox(-1.0F, -15.7727F, -3.8182F, 2.0F, 6.0F, 2.0F, 0.0F, false);
			Head2.setTextureOffset(24, 30).addBox(2.0F, -10.7727F, -8.8182F, 2.0F, 2.0F, 5.0F, 0.0F, false);
			Head2.setTextureOffset(24, 30).addBox(-4.0F, -10.7727F, -8.8182F, 2.0F, 2.0F, 5.0F, 0.0F, false);
			Head2.setTextureOffset(80, 24).addBox(-4.0F, -10.2727F, -5.8182F, 8.0F, 3.0F, 2.0F, 0.0F, false);
			Head2.setTextureOffset(116, 4).addBox(-1.0F, -5.7727F, -5.8182F, 2.0F, 2.0F, 1.0F, 0.0F, false);
			Head2.setTextureOffset(116, 0).addBox(-2.0F, -5.7727F, -4.8182F, 4.0F, 2.0F, 2.0F, 0.0F, false);
			Head2.setTextureOffset(39, 28).addBox(-3.5F, -4.7727F, -4.3182F, 1.0F, 2.0F, 1.0F, 0.0F, false);
			Head2.setTextureOffset(21, 24).addBox(-3.0F, -2.7727F, -5.8182F, 6.0F, 3.0F, 3.0F, 0.0F, false);
			Head3 = new ModelRenderer(this);
			Head3.setRotationPoint(7.9286F, -14.25F, 0.9286F);
			Head3.setTextureOffset(100, 24).addBox(4.0714F, -7.75F, -1.9286F, 3.0F, 5.0F, 2.0F, 0.0F, false);
			Head3.setTextureOffset(46, 3).addBox(-3.9286F, -6.75F, -5.4286F, 6.0F, 2.0F, 1.0F, 0.0F, false);
			Head3.setTextureOffset(60, 9).addBox(-2.4286F, -7.25F, -4.4286F, 3.0F, 2.0F, 1.0F, 0.0F, false);
			Head3.setTextureOffset(42, 83).addBox(-5.9286F, -10.75F, -6.9286F, 10.0F, 12.0F, 12.0F, 0.0F, false);
			Head3.setTextureOffset(46, 0).addBox(-3.9286F, -9.75F, -7.4286F, 6.0F, 2.0F, 1.0F, 0.0F, false);
			Head3.setTextureOffset(80, 118).addBox(-2.9286F, -15.75F, -2.9286F, 4.0F, 6.0F, 4.0F, 0.0F, false);
			Head3.setTextureOffset(22, 68).addBox(-4.9286F, -2.75F, -7.9286F, 8.0F, 2.0F, 2.0F, 0.0F, false);
			Head3.setTextureOffset(83, 34).addBox(-4.4286F, -4.75F, -7.4286F, 1.0F, 2.0F, 1.0F, 0.0F, false);
			Head3.setTextureOffset(87, 34).addBox(-2.4286F, -3.75F, -7.4286F, 1.0F, 1.0F, 1.0F, 0.0F, false);
			Head3.setTextureOffset(96, 34).addBox(-0.4286F, -3.75F, -7.4286F, 1.0F, 1.0F, 1.0F, 0.0F, false);
			Head3.setTextureOffset(100, 34).addBox(1.5714F, -4.75F, -7.4286F, 1.0F, 2.0F, 1.0F, 0.0F, false);
			Head3.setTextureOffset(26, 50).addBox(5.0714F, -3.75F, -1.9286F, 1.0F, 2.0F, 2.0F, 0.0F, false);
			Head3.setTextureOffset(32, 58).addBox(5.0714F, -4.75F, -2.9286F, 1.0F, 4.0F, 4.0F, 0.0F, false);
			Head3.setTextureOffset(110, 24).addBox(-8.9286F, -7.75F, -1.9286F, 3.0F, 5.0F, 2.0F, 0.0F, false);
			Head1 = new ModelRenderer(this);
			Head1.setRotationPoint(0.0F, 24.0F, 0.0F);
			Head1.setTextureOffset(80, 0).addBox(-6.0F, -49.0F, -6.0F, 12.0F, 12.0F, 12.0F, 0.0F, false);
			Head1.setTextureOffset(68, 7).addBox(-5.0F, -48.5F, -8.0F, 10.0F, 3.0F, 2.0F, 0.0F, false);
			Head1.setTextureOffset(80, 4).addBox(-1.0F, -44.0F, -8.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
			Head1.setTextureOffset(80, 0).addBox(-2.0F, -44.0F, -7.0F, 4.0F, 2.0F, 2.0F, 0.0F, false);
			Head1.setTextureOffset(60, 4).addBox(-3.5F, -43.0F, -6.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
			Head1.setTextureOffset(64, 4).addBox(-1.5F, -42.0F, -6.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
			Head1.setTextureOffset(72, 4).addBox(0.5F, -42.0F, -6.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
			Head1.setTextureOffset(76, 4).addBox(2.5F, -43.0F, -6.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
			Head1.setTextureOffset(60, 0).addBox(-4.0F, -41.0F, -7.0F, 8.0F, 2.0F, 2.0F, 0.0F, false);
			Head1.setTextureOffset(60, 12).addBox(-9.0F, -46.0F, -1.0F, 3.0F, 5.0F, 2.0F, 0.0F, false);
			Head1.setTextureOffset(32, 58).addBox(-8.0F, -43.0F, -2.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
			Head1.setTextureOffset(26, 50).addBox(-8.0F, -42.0F, -1.0F, 1.0F, 2.0F, 2.0F, 0.0F, false);
			Head1.setTextureOffset(70, 12).addBox(6.0F, -46.0F, -1.0F, 3.0F, 5.0F, 2.0F, 0.0F, false);
			Head1.setTextureOffset(32, 58).addBox(7.0F, -43.0F, -2.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
			Head1.setTextureOffset(26, 50).addBox(7.0F, -42.0F, -1.0F, 1.0F, 2.0F, 2.0F, 0.0F, false);
			Head1.setTextureOffset(82, 83).addBox(-2.0F, -45.0F, 6.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
			Head1.setTextureOffset(78, 107).addBox(-3.0F, -46.0F, 10.0F, 6.0F, 8.0F, 3.0F, 0.0F, false);
			Head1.setTextureOffset(60, 107).addBox(-3.0F, -43.5F, 8.6F, 6.0F, 8.0F, 3.0F, 0.0F, false);
			Head1.setTextureOffset(42, 107).addBox(-3.0F, -39.4F, 8.4F, 6.0F, 8.0F, 3.0F, 0.0F, false);
			Head1.setTextureOffset(120, 31).addBox(-1.0F, -54.0F, -6.0F, 2.0F, 6.0F, 2.0F, 0.0F, false);
			Head1.setTextureOffset(46, 6).addBox(-6.0F, -49.0F, -11.0F, 2.0F, 2.0F, 5.0F, 0.0F, false);
			Head1.setTextureOffset(44, 13).addBox(-6.0F, -52.0F, -11.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
			Head1.setTextureOffset(46, 6).addBox(4.0F, -49.0F, -11.0F, 2.0F, 2.0F, 5.0F, 0.0F, false);
			Head1.setTextureOffset(52, 13).addBox(4.0F, -52.0F, -11.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			RArm.render(matrixStack, buffer, packedLight, packedOverlay);
			LArm.render(matrixStack, buffer, packedLight, packedOverlay);
			RLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			LLeg.render(matrixStack, buffer, packedLight, packedOverlay);
			Body.render(matrixStack, buffer, packedLight, packedOverlay);
			Head2.render(matrixStack, buffer, packedLight, packedOverlay);
			Head3.render(matrixStack, buffer, packedLight, packedOverlay);
			Head1.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
			this.LLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.LArm.rotateAngleX = MathHelper.cos(f * 0.6662F) * f1;
			this.Head1.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head1.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.Head3.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head3.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.Head2.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head2.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.RLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.RArm.rotateAngleX = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
		}
	}
}
