
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.RaccoonEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class RaccoonRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(RaccoonEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelRaccoon(), 0.3f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/raccoon.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 4.2.4
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelRaccoon extends EntityModel<Entity> {
		private final ModelRenderer Body;
		private final ModelRenderer cube_r1;
		private final ModelRenderer Head;
		private final ModelRenderer cube_r2;
		private final ModelRenderer cube_r3;
		private final ModelRenderer RightEar;
		private final ModelRenderer LeftEar;
		private final ModelRenderer RightFrontLeg;
		private final ModelRenderer cube_r4;
		private final ModelRenderer cube_r5;
		private final ModelRenderer LeftFrontLeg;
		private final ModelRenderer cube_r6;
		private final ModelRenderer cube_r7;
		private final ModelRenderer RightBackLeg;
		private final ModelRenderer cube_r8;
		private final ModelRenderer cube_r9;
		private final ModelRenderer LeftBackLeg;
		private final ModelRenderer cube_r10;
		private final ModelRenderer cube_r11;
		private final ModelRenderer Tail;
		private final ModelRenderer cube_r12;
		private final ModelRenderer Tail2;
		private final ModelRenderer cube_r13;

		public ModelRaccoon() {
			textureWidth = 64;
			textureHeight = 64;
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, 18.0F, 0.0F);
			Body.setTextureOffset(0, 0).addBox(-3.0F, -3.0F, -6.0F, 6.0F, 6.0F, 12.0F, 0.0F, false);
			cube_r1 = new ModelRenderer(this);
			cube_r1.setRotationPoint(0.0F, -1.625F, -6.325F);
			Body.addChild(cube_r1);
			setRotationAngle(cube_r1, -0.4363F, 0.0F, 0.0F);
			cube_r1.setTextureOffset(46, 4).addBox(-2.5F, -2.0F, -1.5F, 5.0F, 4.0F, 3.0F, 0.0F, false);
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, 16.5F, -6.5F);
			Head.setTextureOffset(38, 21).addBox(-4.0F, -3.0F, -5.0F, 8.0F, 6.0F, 5.0F, 0.0F, false);
			Head.setTextureOffset(24, 25).addBox(-1.5F, 0.0F, -9.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
			cube_r2 = new ModelRenderer(this);
			cube_r2.setRotationPoint(2.8F, 1.0F, -4.2F);
			Head.addChild(cube_r2);
			setRotationAngle(cube_r2, 0.0F, 0.5236F, 0.0F);
			cube_r2.setTextureOffset(0, 40).addBox(-1.5F, -2.0F, 0.0F, 3.0F, 4.0F, 4.0F, 0.0F, false);
			cube_r3 = new ModelRenderer(this);
			cube_r3.setRotationPoint(-2.8F, 1.0F, -4.2F);
			Head.addChild(cube_r3);
			setRotationAngle(cube_r3, 0.0F, -0.5236F, 0.0F);
			cube_r3.setTextureOffset(0, 32).addBox(-1.5F, -2.0F, 0.0F, 3.0F, 4.0F, 4.0F, 0.0F, false);
			RightEar = new ModelRenderer(this);
			RightEar.setRotationPoint(-2.5F, -3.0F, -1.5F);
			Head.addChild(RightEar);
			RightEar.setTextureOffset(24, 18).addBox(-1.5F, -2.0F, -0.5F, 3.0F, 2.0F, 1.0F, 0.0F, false);
			LeftEar = new ModelRenderer(this);
			LeftEar.setRotationPoint(2.5F, -3.0F, -1.5F);
			Head.addChild(LeftEar);
			LeftEar.setTextureOffset(24, 22).addBox(-1.5F, -2.0F, -0.5F, 3.0F, 2.0F, 1.0F, 0.0F, true);
			RightFrontLeg = new ModelRenderer(this);
			RightFrontLeg.setRotationPoint(-3.0F, 18.175F, -4.35F);
			RightFrontLeg.setTextureOffset(46, 0).addBox(-1.0F, 4.85F, -1.6F, 3.0F, 1.0F, 3.0F, 0.0F, false);
			cube_r4 = new ModelRenderer(this);
			cube_r4.setRotationPoint(0.5F, 1.75F, 2.0F);
			RightFrontLeg.addChild(cube_r4);
			setRotationAngle(cube_r4, -0.3491F, 0.0F, 0.0F);
			cube_r4.setTextureOffset(46, 11).addBox(-1.0F, 0.125F, -1.175F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			cube_r5 = new ModelRenderer(this);
			cube_r5.setRotationPoint(0.0F, 0.875F, 0.5F);
			RightFrontLeg.addChild(cube_r5);
			setRotationAngle(cube_r5, 0.5236F, 0.0F, 0.0F);
			cube_r5.setTextureOffset(36, 0).addBox(-1.0F, -2.5F, -1.5F, 2.0F, 5.0F, 3.0F, 0.0F, false);
			LeftFrontLeg = new ModelRenderer(this);
			LeftFrontLeg.setRotationPoint(3.0F, 18.175F, -4.35F);
			LeftFrontLeg.setTextureOffset(46, 0).addBox(-2.0F, 4.85F, -1.6F, 3.0F, 1.0F, 3.0F, 0.0F, true);
			cube_r6 = new ModelRenderer(this);
			cube_r6.setRotationPoint(-0.5F, 1.75F, 2.0F);
			LeftFrontLeg.addChild(cube_r6);
			setRotationAngle(cube_r6, -0.3491F, 0.0F, 0.0F);
			cube_r6.setTextureOffset(54, 11).addBox(-1.0F, 0.125F, -1.175F, 2.0F, 4.0F, 2.0F, 0.0F, false);
			cube_r7 = new ModelRenderer(this);
			cube_r7.setRotationPoint(0.0F, 0.875F, 0.5F);
			LeftFrontLeg.addChild(cube_r7);
			setRotationAngle(cube_r7, 0.5236F, 0.0F, 0.0F);
			cube_r7.setTextureOffset(36, 8).addBox(-1.0F, -2.5F, -1.5F, 2.0F, 5.0F, 3.0F, 0.0F, false);
			RightBackLeg = new ModelRenderer(this);
			RightBackLeg.setRotationPoint(-4.0F, 18.225F, 1.575F);
			RightBackLeg.setTextureOffset(46, 0).addBox(-1.0F, 4.8F, -0.7F, 3.0F, 1.0F, 3.0F, 0.0F, false);
			cube_r8 = new ModelRenderer(this);
			cube_r8.setRotationPoint(0.5F, 3.875F, 2.25F);
			RightBackLeg.addChild(cube_r8);
			setRotationAngle(cube_r8, 0.9163F, 0.0F, 0.0F);
			cube_r8.setTextureOffset(10, 27).addBox(-1.0F, -1.15F, -2.3F, 2.0F, 2.0F, 3.0F, 0.0F, false);
			cube_r9 = new ModelRenderer(this);
			cube_r9.setRotationPoint(0.0F, 0.275F, 0.425F);
			RightBackLeg.addChild(cube_r9);
			setRotationAngle(cube_r9, 0.9163F, 0.0F, 0.0F);
			cube_r9.setTextureOffset(12, 18).addBox(-1.0F, -1.5F, -2.05F, 2.0F, 5.0F, 4.0F, 0.0F, false);
			LeftBackLeg = new ModelRenderer(this);
			LeftBackLeg.setRotationPoint(4.0F, 18.225F, 1.575F);
			LeftBackLeg.setTextureOffset(46, 0).addBox(-2.0F, 4.8F, -0.7F, 3.0F, 1.0F, 3.0F, 0.0F, true);
			cube_r10 = new ModelRenderer(this);
			cube_r10.setRotationPoint(-0.5F, 3.875F, 2.25F);
			LeftBackLeg.addChild(cube_r10);
			setRotationAngle(cube_r10, 0.9163F, 0.0F, 0.0F);
			cube_r10.setTextureOffset(0, 27).addBox(-1.0F, -1.15F, -2.3F, 2.0F, 2.0F, 3.0F, 0.0F, false);
			cube_r11 = new ModelRenderer(this);
			cube_r11.setRotationPoint(0.0F, 0.275F, 0.425F);
			LeftBackLeg.addChild(cube_r11);
			setRotationAngle(cube_r11, 0.9163F, 0.0F, 0.0F);
			cube_r11.setTextureOffset(0, 18).addBox(-1.0F, -1.5F, -2.05F, 2.0F, 5.0F, 4.0F, 0.0F, false);
			Tail = new ModelRenderer(this);
			Tail.setRotationPoint(0.0F, 16.5F, 5.5F);
			setRotationAngle(Tail, -0.4363F, 0.0F, 0.0F);
			cube_r12 = new ModelRenderer(this);
			cube_r12.setRotationPoint(-1.5F, 0.0F, 0.0F);
			Tail.addChild(cube_r12);
			setRotationAngle(cube_r12, -1.5708F, 0.0F, 0.0F);
			cube_r12.setTextureOffset(0, 3).addBox(0.0F, -6.0F, -1.5F, 3.0F, 6.0F, 3.0F, 0.0F, false);
			Tail2 = new ModelRenderer(this);
			Tail2.setRotationPoint(0.0F, 0.0577F, 5.3406F);
			Tail.addChild(Tail2);
			setRotationAngle(Tail2, 0.3491F, 0.0F, 0.0F);
			cube_r13 = new ModelRenderer(this);
			cube_r13.setRotationPoint(0.0F, 0.0F, 0.0F);
			Tail2.addChild(cube_r13);
			setRotationAngle(cube_r13, -1.5708F, 0.0F, 0.0F);
			cube_r13.setTextureOffset(24, 3).addBox(-1.5F, -6.0F, -1.5F, 3.0F, 6.0F, 3.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			matrixStack.push();
            matrixStack.translate(0f, 1.5f - 0.8 * 1.5f, 0f);
            matrixStack.scale(0.8f, 0.8f, 0.8f);
			Body.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Head.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			RightFrontLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			LeftFrontLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			RightBackLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			LeftBackLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Tail.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			matrixStack.pop();
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
			this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.LeftFrontLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.LeftBackLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.RightFrontLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.RightBackLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.Tail.rotateAngleZ = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
		}
	}

}
