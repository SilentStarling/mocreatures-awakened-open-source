
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.RoachEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class RoachRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(RoachEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelMoCModelRoach(), 0.3f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/roach.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.7.4
	// Exported for Minecraft version 1.15
	// Paste this class into your mod and generate all required imports
	public static class ModelMoCModelRoach extends EntityModel<Entity> {
		private final ModelRenderer Head;
		private final ModelRenderer Thorax;
		private final ModelRenderer FrontLegs;
		private final ModelRenderer MidLegs;
		private final ModelRenderer RearLegs;
		private final ModelRenderer Abdomen;
		private final ModelRenderer TailL;
		private final ModelRenderer TailR;
		private final ModelRenderer LShellClosed;
		private final ModelRenderer RShellClosed;
		private final ModelRenderer LShellOpen;
		private final ModelRenderer RShellOpen;
		private final ModelRenderer LeftWing;
		private final ModelRenderer RightWing;

		public ModelMoCModelRoach() {
			textureWidth = 32;
			textureHeight = 32;
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, 23.0F, -2.0F);
			setRotationAngle(Head, -2.1712F, 0.0F, 0.0F);
			Head.setTextureOffset(0, 0).addBox(-0.5F, 0.0F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			Thorax = new ModelRenderer(this);
			Thorax.setRotationPoint(0.0F, 22.0F, -1.0F);
			Thorax.setTextureOffset(0, 3).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
			FrontLegs = new ModelRenderer(this);
			FrontLegs.setRotationPoint(0.0F, 23.0F, -1.8F);
			setRotationAngle(FrontLegs, -1.1154F, 0.0F, 0.0F);
			FrontLegs.setTextureOffset(0, 11).addBox(-2.0F, 0.0F, 0.0F, 4.0F, 2.0F, 0.0F, 0.0F, false);
			MidLegs = new ModelRenderer(this);
			MidLegs.setRotationPoint(0.0F, 23.0F, -1.2F);
			setRotationAngle(MidLegs, 1.2641F, 0.0F, 0.0F);
			MidLegs.setTextureOffset(0, 13).addBox(-2.5F, 0.0F, 0.0F, 5.0F, 2.0F, 0.0F, 0.0F, false);
			RearLegs = new ModelRenderer(this);
			RearLegs.setRotationPoint(0.0F, 23.0F, -0.4F);
			setRotationAngle(RearLegs, 1.3682F, 0.0F, 0.0F);
			RearLegs.setTextureOffset(0, 15).addBox(-2.0F, 0.0F, 0.0F, 4.0F, 4.0F, 0.0F, 0.0F, false);
			Abdomen = new ModelRenderer(this);
			Abdomen.setRotationPoint(0.0F, 22.0F, 0.0F);
			setRotationAngle(Abdomen, 1.4277F, 0.0F, 0.0F);
			Abdomen.setTextureOffset(0, 6).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 4.0F, 1.0F, 0.0F, false);
			TailL = new ModelRenderer(this);
			TailL.setRotationPoint(0.0F, 23.0F, 3.6F);
			setRotationAngle(TailL, 1.5541F, 0.6458F, 0.0F);
			TailL.setTextureOffset(2, 29).addBox(-0.5F, 0.0F, 0.0F, 1.0F, 2.0F, 0.0F, 0.0F, false);
			TailR = new ModelRenderer(this);
			TailR.setRotationPoint(0.0F, 23.0F, 3.6F);
			setRotationAngle(TailR, 1.5541F, -0.6458F, 0.0F);
			TailR.setTextureOffset(0, 29).addBox(-0.5F, 0.0F, 0.0F, 1.0F, 2.0F, 0.0F, 0.0F, false);
			LShellClosed = new ModelRenderer(this);
			LShellClosed.setRotationPoint(0.0F, 21.5F, -1.5F);
			setRotationAngle(LShellClosed, -0.1487F, -0.0873F, 0.192F);
			LShellClosed.setTextureOffset(4, 23).addBox(0.0F, 0.0F, 0.0F, 2.0F, 0.0F, 6.0F, 0.0F, false);
			RShellClosed = new ModelRenderer(this);
			RShellClosed.setRotationPoint(0.0F, 21.5F, -1.5F);
			setRotationAngle(RShellClosed, -0.1487F, 0.0873F, -0.192F);
			RShellClosed.setTextureOffset(0, 23).addBox(-2.0F, 0.0F, 0.0F, 2.0F, 0.0F, 6.0F, 0.0F, false);
			LShellOpen = new ModelRenderer(this);
			LShellOpen.setRotationPoint(0.0F, 21.5F, -1.5F);
			setRotationAngle(LShellOpen, 1.117F, -0.0873F, 1.0472F);
			LShellOpen.setTextureOffset(4, 23).addBox(0.0F, 0.0F, 0.0F, 2.0F, 0.0F, 6.0F, 0.0F, false);
			RShellOpen = new ModelRenderer(this);
			RShellOpen.setRotationPoint(0.0F, 21.5F, -1.5F);
			setRotationAngle(RShellOpen, 1.117F, 0.0873F, -1.0472F);
			RShellOpen.setTextureOffset(0, 23).addBox(-2.0F, 0.0F, 0.0F, 2.0F, 0.0F, 6.0F, 0.0F, false);
			LeftWing = new ModelRenderer(this);
			LeftWing.setRotationPoint(0.0F, 21.5F, -1.5F);
			setRotationAngle(LeftWing, 0.0F, -1.0472F, -0.4363F);
			LeftWing.setTextureOffset(11, 21).addBox(0.0F, 1.0F, -1.0F, 6.0F, 0.0F, 2.0F, 0.0F, true);
			RightWing = new ModelRenderer(this);
			RightWing.setRotationPoint(0.0F, 21.5F, -1.5F);
			setRotationAngle(RightWing, 0.0F, 1.0472F, 0.4363F);
			RightWing.setTextureOffset(11, 19).addBox(-6.0F, 1.0F, -1.0F, 6.0F, 0.0F, 2.0F, 0.0F, true);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Head.render(matrixStack, buffer, packedLight, packedOverlay);
			Thorax.render(matrixStack, buffer, packedLight, packedOverlay);
			FrontLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			MidLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			RearLegs.render(matrixStack, buffer, packedLight, packedOverlay);
			Abdomen.render(matrixStack, buffer, packedLight, packedOverlay);
			TailL.render(matrixStack, buffer, packedLight, packedOverlay);
			TailR.render(matrixStack, buffer, packedLight, packedOverlay);
			LShellClosed.render(matrixStack, buffer, packedLight, packedOverlay);
			RShellClosed.render(matrixStack, buffer, packedLight, packedOverlay);
			LShellOpen.render(matrixStack, buffer, packedLight, packedOverlay);
			RShellOpen.render(matrixStack, buffer, packedLight, packedOverlay);
			LeftWing.render(matrixStack, buffer, packedLight, packedOverlay);
			RightWing.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {

			this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
		}
	}

}
