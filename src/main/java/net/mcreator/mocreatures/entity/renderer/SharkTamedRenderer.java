
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.SharkTamedEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class SharkTamedRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(SharkTamedEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelShark(), 0.5f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/shark.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.9.3
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelShark extends EntityModel<Entity> {
		private final ModelRenderer body;
		private final ModelRenderer cube_r1;
		private final ModelRenderer cube_r2;
		private final ModelRenderer cube_r3;
		private final ModelRenderer head;
		private final ModelRenderer cube_r4;
		private final ModelRenderer cube_r5;
		private final ModelRenderer cube_r6;
		private final ModelRenderer tail;
		private final ModelRenderer cube_r7;
		private final ModelRenderer cube_r8;

		public ModelShark() {
			textureWidth = 64;
			textureHeight = 64;
			body = new ModelRenderer(this);
			body.setRotationPoint(-1.0F, 21.8522F, 0.1411F);
			body.setTextureOffset(0, 0).addBox(-3.0F, -4.8522F, -10.1411F, 6.0F, 8.0F, 18.0F, 0.0F, false);
			cube_r1 = new ModelRenderer(this);
			cube_r1.setRotationPoint(-5.6465F, 4.2522F, -1.6589F);
			body.addChild(cube_r1);
			setRotationAngle(cube_r1, -0.0175F, 0.2182F, -0.5236F);
			cube_r1.setTextureOffset(30, 12).addBox(-4.0F, -0.5F, -2.0F, 8.0F, 1.0F, 4.0F, 0.0F, false);
			cube_r2 = new ModelRenderer(this);
			cube_r2.setRotationPoint(5.6465F, 4.2522F, -1.6589F);
			body.addChild(cube_r2);
			setRotationAngle(cube_r2, -0.0175F, -0.2182F, 0.5236F);
			cube_r2.setTextureOffset(28, 36).addBox(-4.0F, -0.5F, -2.0F, 8.0F, 1.0F, 4.0F, 0.0F, false);
			cube_r3 = new ModelRenderer(this);
			cube_r3.setRotationPoint(1.0F, -2.6522F, 4.4589F);
			body.addChild(cube_r3);
			setRotationAngle(cube_r3, 0.7854F, 0.0F, 0.0F);
			cube_r3.setTextureOffset(30, 0).addBox(-1.5F, -7.0F, -4.0F, 1.0F, 4.0F, 8.0F, 0.0F, false);
			head = new ModelRenderer(this);
			head.setRotationPoint(-1.0F, 18.85F, -9.375F);
			cube_r4 = new ModelRenderer(this);
			cube_r4.setRotationPoint(-3.0F, -1.35F, -0.725F);
			head.addChild(cube_r4);
			setRotationAngle(cube_r4, -0.7854F, 0.0F, 0.0F);
			cube_r4.setTextureOffset(36, 41).addBox(0.55F, -0.7F, 0.2F, 1.0F, 6.0F, 6.0F, 0.0F, false);
			cube_r4.setTextureOffset(22, 41).addBox(4.45F, -0.7F, 0.2F, 1.0F, 6.0F, 6.0F, 0.0F, false);
			cube_r5 = new ModelRenderer(this);
			cube_r5.setRotationPoint(1.0F, 4.05F, -4.725F);
			head.addChild(cube_r5);
			setRotationAngle(cube_r5, -0.2618F, 0.0F, 0.0F);
			cube_r5.setTextureOffset(36, 26).addBox(-3.5F, -1.5F, 0.5F, 5.0F, 2.0F, 5.0F, 0.0F, false);
			cube_r6 = new ModelRenderer(this);
			cube_r6.setRotationPoint(1.0F, -3.45F, 9.275F);
			head.addChild(cube_r6);
			setRotationAngle(cube_r6, 0.5236F, 0.0F, 0.0F);
			cube_r6.setTextureOffset(18, 26).addBox(-3.5F, -3.0F, -16.5F, 5.0F, 2.0F, 8.0F, 0.0F, false);
			tail = new ModelRenderer(this);
			tail.setRotationPoint(-1.0F, 20.6667F, 8.6667F);
			tail.setTextureOffset(0, 26).addBox(-2.0F, -2.6667F, -0.6667F, 4.0F, 6.0F, 10.0F, 0.0F, false);
			cube_r7 = new ModelRenderer(this);
			cube_r7.setRotationPoint(1.0F, 2.3333F, 7.3333F);
			tail.addChild(cube_r7);
			setRotationAngle(cube_r7, -0.7854F, 0.0F, 0.0F);
			cube_r7.setTextureOffset(0, 42).addBox(-1.5F, -3.0F, 0.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);
			cube_r8 = new ModelRenderer(this);
			cube_r8.setRotationPoint(1.0F, 0.3333F, 9.3333F);
			tail.addChild(cube_r8);
			setRotationAngle(cube_r8, 0.5236F, 0.0F, 0.0F);
			cube_r8.setTextureOffset(0, 0).addBox(-1.5F, -4.0F, -1.0F, 1.0F, 4.0F, 8.0F, 0.0F, false);
		}

		@Override
		public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
			// previously the render function, render code was moved to a method below
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			body.render(matrixStack, buffer, packedLight, packedOverlay);
			head.render(matrixStack, buffer, packedLight, packedOverlay);
			tail.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}
	}

}
