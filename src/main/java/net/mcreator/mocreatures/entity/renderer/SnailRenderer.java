package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.SnailEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class SnailRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(SnailEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelMoCModelSnail(), 0.3f) {
					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						SnailEntity.CustomEntity _snail = (SnailEntity.CustomEntity) entity;
						return new ResourceLocation("mocreatures:textures/moc_snail_" + String.valueOf(_snail.getVariant()) + ".png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.7.4
	// Exported for Minecraft version 1.15
	// Paste this class into your mod and generate all required imports
	public static class ModelMoCModelSnail extends EntityModel<Entity> {
		private final ModelRenderer Head;
		private final ModelRenderer Antenna;
		private final ModelRenderer Body;
		private final ModelRenderer ShellUp;
		private final ModelRenderer ShellDown;
		private final ModelRenderer Tail;
		public ModelMoCModelSnail() {
			textureWidth = 32;
			textureHeight = 32;
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, 21.8F, -1.0F);
			setRotationAngle(Head, -0.4538F, 0.0F, 0.0F);
			Head.setTextureOffset(0, 6).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			Antenna = new ModelRenderer(this);
			Antenna.setRotationPoint(0.0F, 19.4F, -1.0F);
			setRotationAngle(Antenna, 0.0524F, 0.0F, 0.0F);
			Antenna.setTextureOffset(8, 0).addBox(-1.5F, 0.0F, -1.0F, 3.0F, 2.0F, 0.0F, 0.0F, false);
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, 22.0F, 0.0F);
			Body.setTextureOffset(0, 0).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
			ShellUp = new ModelRenderer(this);
			ShellUp.setRotationPoint(0.0F, 22.3F, -0.2F);
			setRotationAngle(ShellUp, 0.2269F, 0.0F, 0.0F);
			ShellUp.setTextureOffset(12, 0).addBox(-1.0F, -3.0F, 0.0F, 2.0F, 3.0F, 3.0F, 0.0F, false);
			ShellDown = new ModelRenderer(this);
			ShellDown.setRotationPoint(0.0F, 21.0F, 0.0F);
			ShellDown.setTextureOffset(12, 0).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 3.0F, 3.0F, 0.0F, false);
			Tail = new ModelRenderer(this);
			Tail.setRotationPoint(0.0F, 23.0F, 3.0F);
			Tail.setTextureOffset(1, 2).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 1.0F, 3.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Head.render(matrixStack, buffer, packedLight, packedOverlay);
			Antenna.render(matrixStack, buffer, packedLight, packedOverlay);
			Body.render(matrixStack, buffer, packedLight, packedOverlay);
			ShellUp.render(matrixStack, buffer, packedLight, packedOverlay);
			ShellDown.render(matrixStack, buffer, packedLight, packedOverlay);
			Tail.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
		}
	}
}
