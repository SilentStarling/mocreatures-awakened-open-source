
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.SnakeBabyEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class SnakeBabyRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(SnakeBabyEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelSnake(), 0.3f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						SnakeBabyEntity.CustomEntity _snake = (SnakeBabyEntity.CustomEntity) entity;
						return new ResourceLocation("mocreatures:textures/moc_snake_" + String.valueOf(_snake.getVariant()) + ".png");
					}
				};
			});
		}
	}

	// Made with Blockbench 4.2.4
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelSnake extends EntityModel<Entity> {
		private final ModelRenderer Head;
		private final ModelRenderer UpMouth;
		private final ModelRenderer Tongue;
		private final ModelRenderer DownMouth;
		private final ModelRenderer Body;
		private final ModelRenderer Body2;
		private final ModelRenderer Body3;
		private final ModelRenderer Body4;
		private final ModelRenderer Body5;
		private final ModelRenderer Body6;
		private final ModelRenderer Body7;
		private final ModelRenderer Body8;
		private final ModelRenderer Body9;
		private final ModelRenderer Body10;
		private final ModelRenderer Body11;
		private final ModelRenderer Body12;
		private final ModelRenderer Body13;
		private final ModelRenderer Body14;
		private final ModelRenderer Body15;
		private final ModelRenderer Body16;
		private final ModelRenderer Body17;
		private final ModelRenderer Body18;
		private final ModelRenderer Body19;
		private final ModelRenderer CobraHead;
		private final ModelRenderer CobraHood1;
		private final ModelRenderer cube_r1;
		private final ModelRenderer cube_r2;
		private final ModelRenderer CobraHood2;
		private final ModelRenderer cube_r3;
		private final ModelRenderer cube_r4;
		private final ModelRenderer cube_r5;
		private final ModelRenderer cube_r6;
		private final ModelRenderer cube_r7;
		private final ModelRenderer CobraHood3;
		private final ModelRenderer cube_r8;

		public ModelSnake() {
			textureWidth = 64;
			textureHeight = 32;
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, 24.0F, -27.0F);
			Head.setTextureOffset(0, 0).addBox(-1.0F, -1.0F, -2.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			UpMouth = new ModelRenderer(this);
			UpMouth.setRotationPoint(0.0F, 0.0F, 0.0F);
			Head.addChild(UpMouth);
			UpMouth.setTextureOffset(16, 0).addBox(-0.5F, -0.8F, -4.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			UpMouth.setTextureOffset(44, 0).addBox(0.35F, -0.2F, -3.8F, 0.0F, 1.0F, 1.0F, 0.0F, false);
			UpMouth.setTextureOffset(46, 0).addBox(-0.35F, -0.2F, -3.8F, 0.0F, 1.0F, 1.0F, 0.0F, false);
			Tongue = new ModelRenderer(this);
			Tongue.setRotationPoint(0.0F, 0.0F, 0.0F);
			Head.addChild(Tongue);
			Tongue.setTextureOffset(28, 0).addBox(-0.5F, 0.0F, -4.0F, 1.0F, 0.0F, 3.0F, 0.0F, false);
			DownMouth = new ModelRenderer(this);
			DownMouth.setRotationPoint(0.0F, 0.0F, 0.0F);
			Head.addChild(DownMouth);
			DownMouth.setTextureOffset(22, 0).addBox(-0.5F, -0.2F, -4.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, 24.0F, 0.5F);
			Body.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -27.75F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, -26.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body2 = new ModelRenderer(this);
			Body2.setRotationPoint(-1.0F, 0.0F, -25.5F);
			Body.addChild(Body2);
			Body2.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body2.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body3 = new ModelRenderer(this);
			Body3.setRotationPoint(-2.0F, 0.0F, -23.5F);
			Body.addChild(Body3);
			Body3.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body3.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body4 = new ModelRenderer(this);
			Body4.setRotationPoint(-3.0F, 0.0F, -21.5F);
			Body.addChild(Body4);
			Body4.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body4.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body5 = new ModelRenderer(this);
			Body5.setRotationPoint(-2.0F, 0.0F, -19.5F);
			Body.addChild(Body5);
			Body5.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body5.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body6 = new ModelRenderer(this);
			Body6.setRotationPoint(-1.0F, 0.0F, -17.5F);
			Body.addChild(Body6);
			Body6.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body6.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body7 = new ModelRenderer(this);
			Body7.setRotationPoint(0.0F, 0.0F, -15.5F);
			Body.addChild(Body7);
			Body7.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body7.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body8 = new ModelRenderer(this);
			Body8.setRotationPoint(1.0F, 0.0F, -13.5F);
			Body.addChild(Body8);
			Body8.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body8.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body9 = new ModelRenderer(this);
			Body9.setRotationPoint(2.0F, 0.0F, -11.5F);
			Body.addChild(Body9);
			Body9.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body9.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body10 = new ModelRenderer(this);
			Body10.setRotationPoint(3.0F, 0.0F, -9.5F);
			Body.addChild(Body10);
			Body10.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body10.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body11 = new ModelRenderer(this);
			Body11.setRotationPoint(2.0F, 0.0F, -7.5F);
			Body.addChild(Body11);
			Body11.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body11.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body12 = new ModelRenderer(this);
			Body12.setRotationPoint(1.0F, 0.0F, -5.5F);
			Body.addChild(Body12);
			Body12.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body12.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body13 = new ModelRenderer(this);
			Body13.setRotationPoint(0.0F, 0.0F, -3.5F);
			Body.addChild(Body13);
			Body13.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body13.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body14 = new ModelRenderer(this);
			Body14.setRotationPoint(-1.0F, 0.0F, -1.5F);
			Body.addChild(Body14);
			Body14.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body14.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body15 = new ModelRenderer(this);
			Body15.setRotationPoint(-2.0F, 0.0F, 0.5F);
			Body.addChild(Body15);
			Body15.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body15.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body16 = new ModelRenderer(this);
			Body16.setRotationPoint(-3.0F, 0.0F, 2.5F);
			Body.addChild(Body16);
			Body16.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body16.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body17 = new ModelRenderer(this);
			Body17.setRotationPoint(-2.0F, 0.0F, 4.5F);
			Body.addChild(Body17);
			Body17.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body17.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body18 = new ModelRenderer(this);
			Body18.setRotationPoint(-1.0F, 0.0F, 6.5F);
			Body.addChild(Body18);
			Body18.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body18.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body19 = new ModelRenderer(this);
			Body19.setRotationPoint(0.0F, 0.0F, 8.5F);
			Body.addChild(Body19);
			Body19.setTextureOffset(8, 0).addBox(-1.0F, -1.0F, -0.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			Body19.setTextureOffset(8, 4).addBox(-1.0F, -1.0F, 1.25F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			CobraHead = new ModelRenderer(this);
			CobraHead.setRotationPoint(0.0F, 24.0F, 0.0F);
			CobraHood1 = new ModelRenderer(this);
			CobraHood1.setRotationPoint(0.0F, -0.1667F, -24.8333F);
			CobraHead.addChild(CobraHood1);
			setRotationAngle(CobraHood1, 0.0F, -0.2182F, 0.0F);
			cube_r1 = new ModelRenderer(this);
			cube_r1.setRotationPoint(1.5F, -0.0833F, 0.3333F);
			CobraHood1.addChild(cube_r1);
			setRotationAngle(cube_r1, 1.6581F, 0.0F, 0.0F);
			cube_r1.setTextureOffset(16, 4).addBox(-2.25F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, true);
			cube_r1.setTextureOffset(16, 4).addBox(-0.75F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, true);
			cube_r1.setTextureOffset(16, 4).addBox(-2.75F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			cube_r1.setTextureOffset(16, 4).addBox(-4.25F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			cube_r2 = new ModelRenderer(this);
			cube_r2.setRotationPoint(1.0F, 0.1667F, -0.6667F);
			CobraHood1.addChild(cube_r2);
			setRotationAngle(cube_r2, 1.7453F, 0.0F, 0.0F);
			cube_r2.setTextureOffset(16, 4).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, true);
			cube_r2.setTextureOffset(16, 4).addBox(-3.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			CobraHood2 = new ModelRenderer(this);
			CobraHood2.setRotationPoint(-1.0F, -0.375F, -22.625F);
			CobraHead.addChild(CobraHood2);
			setRotationAngle(CobraHood2, 0.0F, -0.2182F, 0.0F);
			cube_r3 = new ModelRenderer(this);
			cube_r3.setRotationPoint(1.75F, 0.125F, 0.625F);
			CobraHood2.addChild(cube_r3);
			setRotationAngle(cube_r3, 1.4835F, 0.0F, 0.0F);
			cube_r3.setTextureOffset(16, 8).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, true);
			cube_r3.setTextureOffset(16, 8).addBox(-2.5F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, true);
			cube_r3.setTextureOffset(16, 8).addBox(-3.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			cube_r3.setTextureOffset(16, 8).addBox(-4.5F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			cube_r4 = new ModelRenderer(this);
			cube_r4.setRotationPoint(0.75F, -0.125F, -0.625F);
			CobraHood2.addChild(cube_r4);
			setRotationAngle(cube_r4, 0.0F, 1.5708F, -1.5708F);
			cube_r4.setTextureOffset(16, 4).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, true);
			cube_r5 = new ModelRenderer(this);
			cube_r5.setRotationPoint(2.25F, -0.125F, -0.625F);
			CobraHood2.addChild(cube_r5);
			setRotationAngle(cube_r5, 0.0F, -1.5708F, 1.5708F);
			cube_r5.setTextureOffset(16, 4).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, true);
			cube_r6 = new ModelRenderer(this);
			cube_r6.setRotationPoint(-0.75F, -0.125F, -0.625F);
			CobraHood2.addChild(cube_r6);
			setRotationAngle(cube_r6, 0.0F, -1.5708F, 1.5708F);
			cube_r6.setTextureOffset(16, 4).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			cube_r7 = new ModelRenderer(this);
			cube_r7.setRotationPoint(-2.25F, -0.125F, -0.625F);
			CobraHood2.addChild(cube_r7);
			setRotationAngle(cube_r7, 0.0F, 1.5708F, -1.5708F);
			cube_r7.setTextureOffset(16, 4).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, false);
			CobraHood3 = new ModelRenderer(this);
			CobraHood3.setRotationPoint(-2.0F, 0.0F, -21.0F);
			CobraHead.addChild(CobraHood3);
			setRotationAngle(CobraHood3, -0.0138F, -0.3051F, 0.0457F);
			cube_r8 = new ModelRenderer(this);
			cube_r8.setRotationPoint(1.0F, 0.0F, 0.0F);
			CobraHood3.addChild(cube_r8);
			setRotationAngle(cube_r8, 1.3963F, 0.0F, 0.0F);
			cube_r8.setTextureOffset(16, 8).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, true);
			cube_r8.setTextureOffset(16, 8).addBox(-3.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, -0.25F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			matrixStack.push();
            matrixStack.translate(0f, 1.5f - 0.6 * 1.5f, 0f);
            matrixStack.scale(0.6f, 0.6f, 0.6f);
			Head.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			Body.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			CobraHead.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
			matrixStack.pop();
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
			this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
		}
	}

}
