package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.TurkeyEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class TurkeyRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(TurkeyEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new Modelturkey(), 0.5f) {
					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						TurkeyEntity.CustomEntity _turkey = (TurkeyEntity.CustomEntity) entity;
						return new ResourceLocation("mocreatures:textures/moc_turkey_" + String.valueOf(_turkey.getVariant()) + ".png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.8.4
	// Exported for Minecraft version 1.15 - 1.16
	// Paste this class into your mod and generate all required imports
	public static class Modelturkey extends EntityModel<Entity> {
		private final ModelRenderer bb_main;
		private final ModelRenderer cube_r1;
		private final ModelRenderer cube_r2;
		private final ModelRenderer cube_r3;
		private final ModelRenderer cube_r4;
		private final ModelRenderer cube_r5;
		private final ModelRenderer cube_r6;
		public Modelturkey() {
			textureWidth = 64;
			textureHeight = 64;
			bb_main = new ModelRenderer(this);
			bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
			bb_main.setTextureOffset(28, 5).addBox(-3.5F, 0.0F, -2.0F, 3.0F, 0.0F, 3.0F, 0.0F, false);
			bb_main.setTextureOffset(22, 5).addBox(0.5F, 0.0F, -2.0F, 3.0F, 0.0F, 3.0F, 0.0F, false);
			bb_main.setTextureOffset(37, 0).addBox(1.5F, -5.0F, 0.0F, 1.0F, 5.0F, 1.0F, 0.0F, false);
			bb_main.setTextureOffset(34, 8).addBox(-2.5F, -5.0F, 0.0F, 1.0F, 5.0F, 1.0F, 0.0F, false);
			bb_main.setTextureOffset(0, 0).addBox(-4.0F, -12.0F, -4.0F, 8.0F, 8.0F, 9.0F, 0.0F, false);
			bb_main.setTextureOffset(23, 23).addBox(-2.5F, -13.0F, -3.0F, 5.0F, 7.0F, 9.0F, 0.0F, false);
			cube_r1 = new ModelRenderer(this);
			cube_r1.setRotationPoint(8.0F, -8.0F, 5.0F);
			bb_main.addChild(cube_r1);
			setRotationAngle(cube_r1, -0.3054F, 0.0F, 0.0F);
			cube_r1.setTextureOffset(0, 17).addBox(-16.0F, -12.0F, 0.0F, 16.0F, 12.0F, 0.0F, 0.0F, false);
			cube_r2 = new ModelRenderer(this);
			cube_r2.setRotationPoint(0.5F, -13.0952F, -8.4497F);
			bb_main.addChild(cube_r2);
			setRotationAngle(cube_r2, 0.7854F, 0.0F, 0.0F);
			cube_r2.setTextureOffset(16, 29).addBox(-1.0F, 0.0F, 0.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
			cube_r3 = new ModelRenderer(this);
			cube_r3.setRotationPoint(1.0F, -13.2425F, -6.5191F);
			bb_main.addChild(cube_r3);
			setRotationAngle(cube_r3, 0.48F, 0.0F, 0.0F);
			cube_r3.setTextureOffset(25, 0).addBox(-2.0F, -2.0F, 0.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
			cube_r4 = new ModelRenderer(this);
			cube_r4.setRotationPoint(0.0F, 1.5F, -1.8F);
			bb_main.addChild(cube_r4);
			setRotationAngle(cube_r4, -0.2182F, 0.0F, 0.0F);
			cube_r4.setTextureOffset(0, 0).addBox(-1.0F, -15.3F, -7.5F, 2.0F, 6.0F, 2.0F, 0.0F, false);
			cube_r5 = new ModelRenderer(this);
			cube_r5.setRotationPoint(0.0F, -7.9F, -3.8F);
			bb_main.addChild(cube_r5);
			setRotationAngle(cube_r5, 0.6109F, 0.0F, 0.0F);
			cube_r5.setTextureOffset(0, 29).addBox(-3.0F, -3.0F, -2.0F, 6.0F, 6.0F, 4.0F, 0.0F, false);
			cube_r6 = new ModelRenderer(this);
			cube_r6.setRotationPoint(4.7F, -7.9F, 0.1F);
			bb_main.addChild(cube_r6);
			setRotationAngle(cube_r6, -0.3491F, 0.0F, 0.0F);
			cube_r6.setTextureOffset(13, 32).addBox(-0.5F, -3.0F, -3.5F, 1.0F, 6.0F, 7.0F, 0.0F, false);
			cube_r6.setTextureOffset(32, 10).addBox(-9.9F, -3.0F, -3.5F, 1.0F, 6.0F, 7.0F, 0.0F, false);
		}

		@Override
		public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
			// previously the render function, render code was moved to a method below
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			bb_main.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}
	}
}
