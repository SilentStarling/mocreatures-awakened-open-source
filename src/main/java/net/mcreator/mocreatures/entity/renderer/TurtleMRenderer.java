
package net.mcreator.mocreatures.entity.renderer;

import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.MobRenderer;

import net.mcreator.mocreatures.entity.TurtleMEntity;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@OnlyIn(Dist.CLIENT)
public class TurtleMRenderer {
	public static class ModelRegisterHandler {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public void registerModels(ModelRegistryEvent event) {
			RenderingRegistry.registerEntityRenderingHandler(TurtleMEntity.entity, renderManager -> {
				return new MobRenderer(renderManager, new ModelBigTurtle(), 0.5f) {

					@Override
					public ResourceLocation getEntityTexture(Entity entity) {
						return new ResourceLocation("mocreatures:textures/entities/turtlem.png");
					}
				};
			});
		}
	}

	// Made with Blockbench 3.9.3
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelBigTurtle extends EntityModel<Entity> {
		private final ModelRenderer RFront;
		private final ModelRenderer LFront;
		private final ModelRenderer RBack;
		private final ModelRenderer LBack;
		private final ModelRenderer Head;
		private final ModelRenderer Body;
		private final ModelRenderer Tail;

		public ModelBigTurtle() {
			textureWidth = 128;
			textureHeight = 128;
			RFront = new ModelRenderer(this);
			RFront.setRotationPoint(-7.0F, 20.0F, -7.0F);
			RFront.setTextureOffset(54, 8).addBox(-2.0F, -2.0F, -2.0F, 4.0F, 6.0F, 4.0F, 0.0F, false);
			LFront = new ModelRenderer(this);
			LFront.setRotationPoint(7.0F, 20.0F, -7.0F);
			LFront.setTextureOffset(0, 20).addBox(-2.0F, -2.0F, -2.0F, 4.0F, 6.0F, 4.0F, 0.0F, false);
			RBack = new ModelRenderer(this);
			RBack.setRotationPoint(-7.0F, 20.0F, 7.0F);
			RBack.setTextureOffset(0, 40).addBox(-2.0F, -2.0F, -2.0F, 4.0F, 6.0F, 4.0F, 0.0F, false);
			LBack = new ModelRenderer(this);
			LBack.setRotationPoint(7.0F, 20.0F, 7.0F);
			LBack.setTextureOffset(0, 0).addBox(-2.0F, -2.0F, -2.0F, 4.0F, 6.0F, 4.0F, 0.0F, false);
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.0F, 18.0F, -10.0F);
			Head.setTextureOffset(48, 40).addBox(-3.0F, -2.0F, -7.0F, 6.0F, 4.0F, 8.0F, 0.0F, false);
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, 24.0F, 0.0F);
			Body.setTextureOffset(0, 40).addBox(-8.0F, -6.0F, -8.0F, 16.0F, 2.0F, 16.0F, 0.0F, false);
			Body.setTextureOffset(0, 0).addBox(-9.0F, -8.0F, -9.0F, 18.0F, 2.0F, 18.0F, 0.0F, false);
			Body.setTextureOffset(0, 20).addBox(-8.0F, -12.0F, -8.0F, 16.0F, 4.0F, 16.0F, 0.0F, false);
			Body.setTextureOffset(48, 20).addBox(-6.0F, -14.0F, -6.0F, 12.0F, 2.0F, 12.0F, 0.0F, false);
			Tail = new ModelRenderer(this);
			Tail.setRotationPoint(0.0F, 19.0F, 9.0F);
			Tail.setTextureOffset(54, 0).addBox(-2.0F, -1.0F, -1.0F, 4.0F, 2.0F, 6.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			RFront.render(matrixStack, buffer, packedLight, packedOverlay);
			LFront.render(matrixStack, buffer, packedLight, packedOverlay);
			RBack.render(matrixStack, buffer, packedLight, packedOverlay);
			LBack.render(matrixStack, buffer, packedLight, packedOverlay);
			Head.render(matrixStack, buffer, packedLight, packedOverlay);
			Body.render(matrixStack, buffer, packedLight, packedOverlay);
			Tail.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {

			this.Head.rotateAngleY = f3 / (180F / (float) Math.PI);
			this.Head.rotateAngleX = f4 / (180F / (float) Math.PI);
			this.Tail.rotateAngleZ = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.RFront.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
			this.RBack.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.LFront.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
			this.LBack.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		}
	}

}
