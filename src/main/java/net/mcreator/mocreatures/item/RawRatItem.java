
package net.mcreator.mocreatures.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.Rarity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Item;
import net.minecraft.item.Food;
import net.minecraft.block.BlockState;

import net.mcreator.mocreatures.MocreaturesModElements;

@MocreaturesModElements.ModElement.Tag
public class RawRatItem extends MocreaturesModElements.ModElement {
	@ObjectHolder("mocreatures:raw_rat")
	public static final Item block = null;

	public RawRatItem(MocreaturesModElements instance) {
		super(instance, 802);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemCustom());
	}

	public static class ItemCustom extends Item {
		public ItemCustom() {
			super(new Item.Properties().group(ItemGroup.FOOD).maxStackSize(64).rarity(Rarity.COMMON)
					.food((new Food.Builder()).hunger(2).saturation(0.3f)

							.meat().build()));
			setRegistryName("raw_rat");
		}

		@Override
		public int getItemEnchantability() {
			return 0;
		}

		@Override
		public float getDestroySpeed(ItemStack par1ItemStack, BlockState par2Block) {
			return 0F;
		}
	}
}
