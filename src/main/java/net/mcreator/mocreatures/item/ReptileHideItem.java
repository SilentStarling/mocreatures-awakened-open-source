
package net.mcreator.mocreatures.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.UseAction;
import net.minecraft.item.Rarity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.block.BlockState;

import net.mcreator.mocreatures.itemgroup.MocItemGroup;
import net.mcreator.mocreatures.MocreaturesModElements;

@MocreaturesModElements.ModElement.Tag
public class ReptileHideItem extends MocreaturesModElements.ModElement {
	@ObjectHolder("mocreatures:reptile_hide")
	public static final Item block = null;

	public ReptileHideItem(MocreaturesModElements instance) {
		super(instance, 486);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemCustom());
	}

	public static class ItemCustom extends Item {
		public ItemCustom() {
			super(new Item.Properties().group(MocItemGroup.tab).maxStackSize(64).rarity(Rarity.COMMON));
			setRegistryName("reptile_hide");
		}

		@Override
		public UseAction getUseAction(ItemStack itemstack) {
			return UseAction.EAT;
		}

		@Override
		public int getItemEnchantability() {
			return 0;
		}

		@Override
		public float getDestroySpeed(ItemStack par1ItemStack, BlockState par2Block) {
			return 1F;
		}
	}
}
