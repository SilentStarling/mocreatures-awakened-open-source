package net.mcreator.mocreatures.procedures;

import net.minecraft.potion.EffectInstance;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.potion.GrownPotionEffect;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;

public class BearPotionExpireProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure BearPotionExpire!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof LivingEntity)
			((LivingEntity) entity).addPotionEffect(new EffectInstance(GrownPotionEffect.potion, (int) 100, (int) 1, (true), (true)));
	}
}
