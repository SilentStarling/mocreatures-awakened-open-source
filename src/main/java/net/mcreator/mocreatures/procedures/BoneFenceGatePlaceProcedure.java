package net.mcreator.mocreatures.procedures;

import net.minecraft.world.IWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.Direction;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.block.BoneFenceGateWEBlock;
import net.mcreator.mocreatures.block.BoneFenceGateNSBlock;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;

public class BoneFenceGatePlaceProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency world for procedure BoneFenceGatePlace!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency x for procedure BoneFenceGatePlace!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency y for procedure BoneFenceGatePlace!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency z for procedure BoneFenceGatePlace!");
			return;
		}
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure BoneFenceGatePlace!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		Entity entity = (Entity) dependencies.get("entity");
		if ((entity.getHorizontalFacing()) == Direction.NORTH) {
			world.setBlockState(new BlockPos(x, y, z), BoneFenceGateNSBlock.block.getDefaultState(), 3);
		} else if ((entity.getHorizontalFacing()) == Direction.SOUTH) {
			world.setBlockState(new BlockPos(x, y, z), BoneFenceGateNSBlock.block.getDefaultState(), 3);
		} else if ((entity.getHorizontalFacing()) == Direction.EAST) {
			world.setBlockState(new BlockPos(x, y, z), BoneFenceGateWEBlock.block.getDefaultState(), 3);
		} else if ((entity.getHorizontalFacing()) == Direction.WEST) {
			world.setBlockState(new BlockPos(x, y, z), BoneFenceGateWEBlock.block.getDefaultState(), 3);
		}
	}
}
