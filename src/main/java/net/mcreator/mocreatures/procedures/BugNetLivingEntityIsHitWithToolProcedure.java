package net.mcreator.mocreatures.procedures;

import net.minecraftforge.items.ItemHandlerHelper;

import net.minecraft.item.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.item.SnailBottleYellowItem;
import net.mcreator.mocreatures.item.SnailBottleOrangeItem;
import net.mcreator.mocreatures.item.SnailBottleItem;
import net.mcreator.mocreatures.item.SnailBottleGreenItem;
import net.mcreator.mocreatures.item.SnailBottleBrownItem;
import net.mcreator.mocreatures.item.SnailBottleBlackItem;
import net.mcreator.mocreatures.item.RoachBottleItem;
import net.mcreator.mocreatures.item.MaggotBottleItem;
import net.mcreator.mocreatures.item.FlyBottleItem;
import net.mcreator.mocreatures.item.FireflyBottleItem;
import net.mcreator.mocreatures.item.DragonflyBottleRedItem;
import net.mcreator.mocreatures.item.DragonflyBottleLBlueItem;
import net.mcreator.mocreatures.item.DragonflyBottleGreenItem;
import net.mcreator.mocreatures.item.DragonflyBottleDBlueItem;
import net.mcreator.mocreatures.item.CricketBottleItem;
import net.mcreator.mocreatures.item.CricketBottleBrightItem;
import net.mcreator.mocreatures.item.ButterflyBottleZebraItem;
import net.mcreator.mocreatures.item.ButterflyBottleYellowItem;
import net.mcreator.mocreatures.item.ButterflyBottleWhiteItem;
import net.mcreator.mocreatures.item.ButterflyBottleOrangeItem;
import net.mcreator.mocreatures.item.ButterflyBottleGreenItem;
import net.mcreator.mocreatures.item.ButterflyBottleBrownItem;
import net.mcreator.mocreatures.item.ButterflyBottleBlueItem;
import net.mcreator.mocreatures.item.AntBottleItem;
import net.mcreator.mocreatures.entity.SnailEntity;
import net.mcreator.mocreatures.entity.RoachEntity;
import net.mcreator.mocreatures.entity.MaggotEntity;
import net.mcreator.mocreatures.entity.FlyEntity;
import net.mcreator.mocreatures.entity.FireflyEntity;
import net.mcreator.mocreatures.entity.DragonflyEntity;
import net.mcreator.mocreatures.entity.CricketEntity;
import net.mcreator.mocreatures.entity.ButterflyEntity;
import net.mcreator.mocreatures.entity.AntEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;

public class BugNetLivingEntityIsHitWithToolProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure BugNetLivingEntityIsHitWithTool!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency sourceentity for procedure BugNetLivingEntityIsHitWithTool!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if ((sourceentity instanceof PlayerEntity)
				? ((PlayerEntity) sourceentity).inventory.hasItemStack(new ItemStack(Items.GLASS_BOTTLE))
				: false) {
			if (sourceentity instanceof PlayerEntity) {
				ItemStack _stktoremove = new ItemStack(Items.GLASS_BOTTLE);
				((PlayerEntity) sourceentity).inventory.func_234564_a_(p -> _stktoremove.getItem() == p.getItem(), (int) 1,
						((PlayerEntity) sourceentity).container.func_234641_j_());
			}
		}
		if (entity instanceof ButterflyEntity.CustomEntity) {
			if (entity.getPersistentData().getDouble("Variant") == 1) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(ButterflyBottleBlueItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 2) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(ButterflyBottleOrangeItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 3) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(ButterflyBottleGreenItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 4) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(ButterflyBottleBrownItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 5) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(ButterflyBottleWhiteItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 6) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(ButterflyBottleYellowItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 7) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(ButterflyBottleZebraItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			}
		} else if (entity instanceof AntEntity.CustomEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			if (sourceentity instanceof PlayerEntity) {
				ItemStack _setstack = new ItemStack(AntBottleItem.block);
				_setstack.setCount((int) 1);
				ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
			}
		} else if (entity instanceof CricketEntity.CustomEntity) {
			if (entity.getPersistentData().getDouble("Variant") == 1) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(CricketBottleBrightItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 2) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(CricketBottleItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			}
		} else if (entity instanceof DragonflyEntity.CustomEntity) {
			if (entity.getPersistentData().getDouble("Variant") == 1) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(DragonflyBottleGreenItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 2) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(DragonflyBottleLBlueItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 3) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(DragonflyBottleRedItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 4) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(DragonflyBottleDBlueItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			}
		} else if (entity instanceof FireflyEntity.CustomEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			if (sourceentity instanceof PlayerEntity) {
				ItemStack _setstack = new ItemStack(FireflyBottleItem.block);
				_setstack.setCount((int) 1);
				ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
			}
		} else if (entity instanceof FlyEntity.CustomEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			if (sourceentity instanceof PlayerEntity) {
				ItemStack _setstack = new ItemStack(FlyBottleItem.block);
				_setstack.setCount((int) 1);
				ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
			}
		} else if (entity instanceof MaggotEntity.CustomEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			if (sourceentity instanceof PlayerEntity) {
				ItemStack _setstack = new ItemStack(MaggotBottleItem.block);
				_setstack.setCount((int) 1);
				ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
			}
		} else if (entity instanceof RoachEntity.CustomEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			if (sourceentity instanceof PlayerEntity) {
				ItemStack _setstack = new ItemStack(RoachBottleItem.block);
				_setstack.setCount((int) 1);
				ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
			}
		} else if (entity instanceof SnailEntity.CustomEntity) {
			if (entity.getPersistentData().getDouble("Variant") == 1) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(SnailBottleBrownItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 2) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(SnailBottleGreenItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 3) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(SnailBottleYellowItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 4) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(SnailBottleItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 5) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(SnailBottleOrangeItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 6) {
				if (!entity.world.isRemote())
					entity.remove();
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _setstack = new ItemStack(SnailBottleBlackItem.block);
					_setstack.setCount((int) 1);
					ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
				}
			}
		}
	}
}
