package net.mcreator.mocreatures.procedures;

import net.minecraft.util.Hand;
import net.minecraft.potion.EffectInstance;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.potion.BabyPotionEffect;
import net.mcreator.mocreatures.item.CaptureAmuletItem;
import net.mcreator.mocreatures.item.CaptureAmuletEntOakItem;
import net.mcreator.mocreatures.item.CaptureAmuletEntBirchItem;
import net.mcreator.mocreatures.item.CaptureAmuletDeerBabyItem;
import net.mcreator.mocreatures.entity.EntOakEntity;
import net.mcreator.mocreatures.entity.EntBirchEntity;
import net.mcreator.mocreatures.entity.DeerBabyEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;
import java.util.Collection;

public class CaptureAmuletProc2Procedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure CaptureAmuletProc2!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency sourceentity for procedure CaptureAmuletProc2!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY)
				.getItem() == CaptureAmuletItem.block) {
			if (entity instanceof DeerBabyEntity.CustomEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletDeerBabyItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (new Object() {
							int check(Entity _entity) {
								if (_entity instanceof LivingEntity) {
									Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
									for (EffectInstance effect : effects) {
										if (effect.getPotion() == BabyPotionEffect.potion)
											return effect.getDuration();
									}
								}
								return 0;
							}
						}.check(entity)));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof EntOakEntity.CustomEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletEntOakItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof EntBirchEntity.CustomEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletEntBirchItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			}
		}
	}
}
