package net.mcreator.mocreatures.procedures;

import net.minecraft.util.text.Color;
import net.minecraft.util.Hand;
import net.minecraft.potion.EffectInstance;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.passive.TurtleEntity;
import net.minecraft.entity.passive.StriderEntity;
import net.minecraft.entity.passive.SquidEntity;
import net.minecraft.entity.passive.SheepEntity;
import net.minecraft.entity.passive.RabbitEntity;
import net.minecraft.entity.passive.PigEntity;
import net.minecraft.entity.passive.MooshroomEntity;
import net.minecraft.entity.passive.DolphinEntity;
import net.minecraft.entity.passive.CowEntity;
import net.minecraft.entity.passive.ChickenEntity;
import net.minecraft.entity.passive.BeeEntity;
import net.minecraft.entity.passive.BatEntity;
import net.minecraft.entity.monster.HoglinEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.potion.BabyPotionEffect;
import net.mcreator.mocreatures.item.CaptureAmuletTurtleVItem;
import net.mcreator.mocreatures.item.CaptureAmuletStriderItem;
import net.mcreator.mocreatures.item.CaptureAmuletSquidItem;
import net.mcreator.mocreatures.item.CaptureAmuletSheepItem;
import net.mcreator.mocreatures.item.CaptureAmuletRabbitItem;
import net.mcreator.mocreatures.item.CaptureAmuletPigItem;
import net.mcreator.mocreatures.item.CaptureAmuletMouseItem;
import net.mcreator.mocreatures.item.CaptureAmuletMooshroomItem;
import net.mcreator.mocreatures.item.CaptureAmuletJellyfishPMOWItem;
import net.mcreator.mocreatures.item.CaptureAmuletJellyfishNettleItem;
import net.mcreator.mocreatures.item.CaptureAmuletJellyfishMoonItem;
import net.mcreator.mocreatures.item.CaptureAmuletJellyfishLionItem;
import net.mcreator.mocreatures.item.CaptureAmuletJellyfishBarrelItem;
import net.mcreator.mocreatures.item.CaptureAmuletItem;
import net.mcreator.mocreatures.item.CaptureAmuletHoglinItem;
import net.mcreator.mocreatures.item.CaptureAmuletDolphinVItem;
import net.mcreator.mocreatures.item.CaptureAmuletDeerItem;
import net.mcreator.mocreatures.item.CaptureAmuletCowItem;
import net.mcreator.mocreatures.item.CaptureAmuletChickenItem;
import net.mcreator.mocreatures.item.CaptureAmuletBoarItem;
import net.mcreator.mocreatures.item.CaptureAmuletBoarBabyItem;
import net.mcreator.mocreatures.item.CaptureAmuletBeeItem;
import net.mcreator.mocreatures.item.CaptureAmuletBatItem;
import net.mcreator.mocreatures.entity.MouseEntity;
import net.mcreator.mocreatures.entity.JellyfishPMOWEntity;
import net.mcreator.mocreatures.entity.JellyfishNettleEntity;
import net.mcreator.mocreatures.entity.JellyfishMoonEntity;
import net.mcreator.mocreatures.entity.JellyfishLionsManeEntity;
import net.mcreator.mocreatures.entity.JellyfishBarrelEntity;
import net.mcreator.mocreatures.entity.DeerEntity;
import net.mcreator.mocreatures.entity.BoarEntity;
import net.mcreator.mocreatures.entity.BoarBabyEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.stream.Stream;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.util.AbstractMap;

public class CaptureAmuletProcProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure CaptureAmuletProc!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency sourceentity for procedure CaptureAmuletProc!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		double Color = 0;
		if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY)
				.getItem() == CaptureAmuletItem.block) {
			if (entity instanceof CowEntity) {
				if (entity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletCowItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) entity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (entity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) entity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof ChickenEntity) {
				if (entity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletChickenItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) entity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (entity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) entity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof SheepEntity) {
				if (entity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletSheepItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) entity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (entity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) entity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Color", (entity.getPersistentData().getDouble("Color")));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof PigEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletPigItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof BeeEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletBeeItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof BatEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletBatItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof HoglinEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletHoglinItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof MooshroomEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletMooshroomItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof SquidEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletSquidItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof RabbitEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletRabbitItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("RabbitType", (entity.getPersistentData().getDouble("RabbitType")));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof StriderEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletStriderItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof DolphinEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletDolphinVItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof TurtleEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletTurtleVItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof MouseEntity.CustomEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletMouseItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Variant", ((MouseEntity.CustomEntity) entity).getVariant());
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof DeerEntity.CustomEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletDeerItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Variant", ((DeerEntity.CustomEntity) entity).getVariant());
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof JellyfishLionsManeEntity.CustomEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletJellyfishLionItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof JellyfishBarrelEntity.CustomEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletJellyfishBarrelItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof JellyfishMoonEntity.CustomEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletJellyfishMoonItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof JellyfishNettleEntity.CustomEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletJellyfishNettleItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof JellyfishPMOWEntity.CustomEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletJellyfishPMOWItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof BoarEntity.CustomEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletBoarItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Variant", ((BoarEntity.CustomEntity) entity).getVariant());
				if (!entity.world.isRemote())
					entity.remove();
			} else if (entity instanceof BoarBabyEntity.CustomEntity) {
				if (sourceentity instanceof LivingEntity) {
					ItemStack _setstack = new ItemStack(CaptureAmuletBoarBabyItem.block);
					_setstack.setCount((int) 1);
					((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
					if (sourceentity instanceof ServerPlayerEntity)
						((ServerPlayerEntity) sourceentity).inventory.markDirty();
				}
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putString("Name", (entity.getDisplayName().getString()));
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Variant", ((BoarBabyEntity.CustomEntity) entity).getVariant());
				((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
						.putDouble("Age", (new Object() {
							int check(Entity _entity) {
								if (_entity instanceof LivingEntity) {
									Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
									for (EffectInstance effect : effects) {
										if (effect.getPotion() == BabyPotionEffect.potion)
											return effect.getDuration();
									}
								}
								return 0;
							}
						}.check(entity)));
				if (!entity.world.isRemote())
					entity.remove();
			} else {
				CaptureAmuletProc2Procedure.executeProcedure(
						Stream.of(new AbstractMap.SimpleEntry<>("entity", entity), new AbstractMap.SimpleEntry<>("sourceentity", sourceentity))
								.collect(HashMap::new, (_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll));
			}
		}
	}
}
