package net.mcreator.mocreatures.procedures;

import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.Variant;

import net.mcreator.mocreatures.entity.CrabEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;

public class CrabColorProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure CrabColor!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double Variant = 0;
		if (entity instanceof CrabEntity.CustomEntity) {
			if (((CrabEntity.CustomEntity) entity).getVariant() == 0) {
				Variant = Math.ceil(5 * Math.random());
				((CrabEntity.CustomEntity) entity).setVariant((int) Variant);
			}
		}
	}
}
