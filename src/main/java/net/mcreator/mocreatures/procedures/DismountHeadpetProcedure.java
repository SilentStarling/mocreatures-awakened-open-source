package net.mcreator.mocreatures.procedures;

import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;

public class DismountHeadpetProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure DismountHeadpet!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity.isPassenger()) {
			if ((entity.getRidingEntity()).isSneaking()) {
				entity.stopRiding();
			}
		}
	}
}
