package net.mcreator.mocreatures.procedures;

import net.minecraft.world.IWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.potion.EffectInstance;
import net.minecraft.item.ItemStack;
import net.minecraft.item.BlockItem;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;
import net.minecraft.block.Blocks;
import net.minecraft.block.BlockState;

import net.mcreator.mocreatures.potion.EntPlantPotionPotionEffect;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;
import java.util.Collection;

public class EntPlantingProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency world for procedure EntPlanting!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency x for procedure EntPlanting!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency y for procedure EntPlanting!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency z for procedure EntPlanting!");
			return;
		}
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure EntPlanting!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		Entity entity = (Entity) dependencies.get("entity");
		boolean found = false;
		double sx = 0;
		double sy = 0;
		double sz = 0;
		ItemStack Plant = ItemStack.EMPTY;
		if (new Object() {
			boolean check(Entity _entity) {
				if (_entity instanceof LivingEntity) {
					Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
					for (EffectInstance effect : effects) {
						if (effect.getPotion() == EntPlantPotionPotionEffect.potion)
							return true;
					}
				}
				return false;
			}
		}.check(entity)) {
			Plant = ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHeldItemMainhand() : ItemStack.EMPTY);
			if ((world.getBlockState(new BlockPos(x, y - 1, z))).getBlock() == Blocks.GRASS_BLOCK) {
				if (BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:ent"))
						.contains((world.getBlockState(new BlockPos(x + 1, y, z))).getBlock())) {
					world.setBlockState(new BlockPos(x, y, z), (new Object() {
						public BlockState toBlock(ItemStack _stk) {
							if (_stk.getItem() instanceof BlockItem) {
								return ((BlockItem) _stk.getItem()).getBlock().getDefaultState();
							}
							return Blocks.AIR.getDefaultState();
						}
					}.toBlock((Plant))), 3);
				}
			}
			if ((world.getBlockState(new BlockPos(x + 1, y - 1, z))).getBlock() == Blocks.GRASS_BLOCK) {
				if (BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:ent"))
						.contains((world.getBlockState(new BlockPos(x + 1, y, z))).getBlock())) {
					world.setBlockState(new BlockPos(x + 1, y, z), (new Object() {
						public BlockState toBlock(ItemStack _stk) {
							if (_stk.getItem() instanceof BlockItem) {
								return ((BlockItem) _stk.getItem()).getBlock().getDefaultState();
							}
							return Blocks.AIR.getDefaultState();
						}
					}.toBlock((Plant))), 3);
				}
			}
			if ((world.getBlockState(new BlockPos(x - 1, y - 1, z))).getBlock() == Blocks.GRASS_BLOCK) {
				if (BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:ent"))
						.contains((world.getBlockState(new BlockPos(x - 1, y, z))).getBlock())) {
					world.setBlockState(new BlockPos(x - 1, y, z), (new Object() {
						public BlockState toBlock(ItemStack _stk) {
							if (_stk.getItem() instanceof BlockItem) {
								return ((BlockItem) _stk.getItem()).getBlock().getDefaultState();
							}
							return Blocks.AIR.getDefaultState();
						}
					}.toBlock((Plant))), 3);
				}
			}
			if ((world.getBlockState(new BlockPos(x, y - 1, z - 1))).getBlock() == Blocks.GRASS_BLOCK) {
				if (BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:ent"))
						.contains((world.getBlockState(new BlockPos(x, y, z - 1))).getBlock())) {
					world.setBlockState(new BlockPos(x, y, z - 1), (new Object() {
						public BlockState toBlock(ItemStack _stk) {
							if (_stk.getItem() instanceof BlockItem) {
								return ((BlockItem) _stk.getItem()).getBlock().getDefaultState();
							}
							return Blocks.AIR.getDefaultState();
						}
					}.toBlock((Plant))), 3);
				}
			}
			if ((world.getBlockState(new BlockPos(x, y - 1, z + 1))).getBlock() == Blocks.GRASS_BLOCK) {
				if (BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:ent"))
						.contains((world.getBlockState(new BlockPos(x, y, z + 1))).getBlock())) {
					world.setBlockState(new BlockPos(x, y, z + 1), (new Object() {
						public BlockState toBlock(ItemStack _stk) {
							if (_stk.getItem() instanceof BlockItem) {
								return ((BlockItem) _stk.getItem()).getBlock().getDefaultState();
							}
							return Blocks.AIR.getDefaultState();
						}
					}.toBlock((Plant))), 3);
				}
			}
			if ((world.getBlockState(new BlockPos(x + 1, y - 1, z + 1))).getBlock() == Blocks.GRASS_BLOCK) {
				if (BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:ent"))
						.contains((world.getBlockState(new BlockPos(x + 1, y, z + 1))).getBlock())) {
					world.setBlockState(new BlockPos(x + 1, y, z + 1), (new Object() {
						public BlockState toBlock(ItemStack _stk) {
							if (_stk.getItem() instanceof BlockItem) {
								return ((BlockItem) _stk.getItem()).getBlock().getDefaultState();
							}
							return Blocks.AIR.getDefaultState();
						}
					}.toBlock((Plant))), 3);
				}
			}
			if ((world.getBlockState(new BlockPos(x - 1, y - 1, z + 1))).getBlock() == Blocks.GRASS_BLOCK) {
				if (BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:ent"))
						.contains((world.getBlockState(new BlockPos(x - 1, y, z + 1))).getBlock())) {
					world.setBlockState(new BlockPos(x - 1, y, z + 1), (new Object() {
						public BlockState toBlock(ItemStack _stk) {
							if (_stk.getItem() instanceof BlockItem) {
								return ((BlockItem) _stk.getItem()).getBlock().getDefaultState();
							}
							return Blocks.AIR.getDefaultState();
						}
					}.toBlock((Plant))), 3);
				}
			}
			if ((world.getBlockState(new BlockPos(x - 1, y - 1, z - 1))).getBlock() == Blocks.GRASS_BLOCK) {
				if (BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:ent"))
						.contains((world.getBlockState(new BlockPos(x - 1, y, z - 1))).getBlock())) {
					world.setBlockState(new BlockPos(x - 1, y, z - 1), (new Object() {
						public BlockState toBlock(ItemStack _stk) {
							if (_stk.getItem() instanceof BlockItem) {
								return ((BlockItem) _stk.getItem()).getBlock().getDefaultState();
							}
							return Blocks.AIR.getDefaultState();
						}
					}.toBlock((Plant))), 3);
				}
			}
			if ((world.getBlockState(new BlockPos(x + 1, y - 1, z - 1))).getBlock() == Blocks.GRASS_BLOCK) {
				if (BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:ent"))
						.contains((world.getBlockState(new BlockPos(x + 1, y, z - 1))).getBlock())) {
					world.setBlockState(new BlockPos(x + 1, y, z - 1), (new Object() {
						public BlockState toBlock(ItemStack _stk) {
							if (_stk.getItem() instanceof BlockItem) {
								return ((BlockItem) _stk.getItem()).getBlock().getDefaultState();
							}
							return Blocks.AIR.getDefaultState();
						}
					}.toBlock((Plant))), 3);
				}
			}
			(((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHeldItemMainhand() : ItemStack.EMPTY)).shrink((int) 1);
			if (entity instanceof LivingEntity) {
				((LivingEntity) entity).removePotionEffect(EntPlantPotionPotionEffect.potion);
			}
		}
	}
}
