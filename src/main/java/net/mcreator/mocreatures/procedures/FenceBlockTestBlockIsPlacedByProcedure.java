package net.mcreator.mocreatures.procedures;

import net.minecraft.world.IWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Direction;
import net.minecraft.tags.BlockTags;
import net.minecraft.state.Property;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.DirectionProperty;
import net.minecraft.block.BlockState;

import net.mcreator.mocreatures.block.BoneFenceXBlock;
import net.mcreator.mocreatures.block.BoneFenceWBlock;
import net.mcreator.mocreatures.block.BoneFenceTWBlock;
import net.mcreator.mocreatures.block.BoneFenceTSBlock;
import net.mcreator.mocreatures.block.BoneFenceTNBlock;
import net.mcreator.mocreatures.block.BoneFenceTEBlock;
import net.mcreator.mocreatures.block.BoneFenceStraightVBlock;
import net.mcreator.mocreatures.block.BoneFenceStraightBlock;
import net.mcreator.mocreatures.block.BoneFenceSWBlock;
import net.mcreator.mocreatures.block.BoneFenceSEBlock;
import net.mcreator.mocreatures.block.BoneFenceSBlock;
import net.mcreator.mocreatures.block.BoneFenceNWBlock;
import net.mcreator.mocreatures.block.BoneFenceNEBlock;
import net.mcreator.mocreatures.block.BoneFenceNBlock;
import net.mcreator.mocreatures.block.BoneFenceEBlock;
import net.mcreator.mocreatures.block.BoneFenceBlock;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;

public class FenceBlockTestBlockIsPlacedByProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency world for procedure FenceBlockTestBlockIsPlacedBy!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency x for procedure FenceBlockTestBlockIsPlacedBy!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency y for procedure FenceBlockTestBlockIsPlacedBy!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency z for procedure FenceBlockTestBlockIsPlacedBy!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		boolean east = false;
		boolean west = false;
		boolean north = false;
		boolean south = false;
		try {
			BlockState _bs = world.getBlockState(new BlockPos(x, y, z));
			DirectionProperty _property = (DirectionProperty) _bs.getBlock().getStateContainer().getProperty("facing");
			if (_property != null) {
				world.setBlockState(new BlockPos(x, y, z), _bs.with(_property, Direction.NORTH), 3);
			} else {
				world.setBlockState(new BlockPos(x, y, z),
						_bs.with((EnumProperty<Direction.Axis>) _bs.getBlock().getStateContainer().getProperty("axis"), Direction.NORTH.getAxis()),
						3);
			}
		} catch (Exception e) {
		}
		if (world.getBlockState(new BlockPos(x + 1, y, z)).isSolid()
				|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
						.contains((world.getBlockState(new BlockPos(x + 1, y, z))).getBlock())) {
			if (world.getBlockState(new BlockPos(x - 1, y, z)).isSolid()
					|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
							.contains((world.getBlockState(new BlockPos(x - 1, y, z))).getBlock())) {
				if (world.getBlockState(new BlockPos(x, y, z + 1)).isSolid()
						|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
								.contains((world.getBlockState(new BlockPos(x, y, z + 1))).getBlock())) {
					if (world.getBlockState(new BlockPos(x, y, z - 1)).isSolid()
							|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
									.contains((world.getBlockState(new BlockPos(x, y, z - 1))).getBlock())) {
						{
							BlockPos _bp = new BlockPos(x, y, z);
							BlockState _bs = BoneFenceXBlock.block.getDefaultState();
							BlockState _bso = world.getBlockState(_bp);
							for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
								Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
								if (_property != null && _bs.get(_property) != null)
									try {
										_bs = _bs.with(_property, (Comparable) entry.getValue());
									} catch (Exception e) {
									}
							}
							world.setBlockState(_bp, _bs, 3);
						}
					} else {
						{
							BlockPos _bp = new BlockPos(x, y, z);
							BlockState _bs = BoneFenceTSBlock.block.getDefaultState();
							BlockState _bso = world.getBlockState(_bp);
							for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
								Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
								if (_property != null && _bs.get(_property) != null)
									try {
										_bs = _bs.with(_property, (Comparable) entry.getValue());
									} catch (Exception e) {
									}
							}
							world.setBlockState(_bp, _bs, 3);
						}
					}
				} else if (world.getBlockState(new BlockPos(x, y, z - 1)).isSolid()
						|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
								.contains((world.getBlockState(new BlockPos(x, y, z - 1))).getBlock())) {
					{
						BlockPos _bp = new BlockPos(x, y, z);
						BlockState _bs = BoneFenceTNBlock.block.getDefaultState();
						BlockState _bso = world.getBlockState(_bp);
						for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
							Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
							if (_property != null && _bs.get(_property) != null)
								try {
									_bs = _bs.with(_property, (Comparable) entry.getValue());
								} catch (Exception e) {
								}
						}
						world.setBlockState(_bp, _bs, 3);
					}
				} else {
					{
						BlockPos _bp = new BlockPos(x, y, z);
						BlockState _bs = BoneFenceStraightBlock.block.getDefaultState();
						BlockState _bso = world.getBlockState(_bp);
						for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
							Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
							if (_property != null && _bs.get(_property) != null)
								try {
									_bs = _bs.with(_property, (Comparable) entry.getValue());
								} catch (Exception e) {
								}
						}
						world.setBlockState(_bp, _bs, 3);
					}
				}
			} else if (world.getBlockState(new BlockPos(x, y, z + 1)).isSolid()
					|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
							.contains((world.getBlockState(new BlockPos(x, y, z + 1))).getBlock())) {
				if (world.getBlockState(new BlockPos(x, y, z - 1)).isSolid()
						|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
								.contains((world.getBlockState(new BlockPos(x, y, z - 1))).getBlock())) {
					{
						BlockPos _bp = new BlockPos(x, y, z);
						BlockState _bs = BoneFenceTEBlock.block.getDefaultState();
						BlockState _bso = world.getBlockState(_bp);
						for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
							Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
							if (_property != null && _bs.get(_property) != null)
								try {
									_bs = _bs.with(_property, (Comparable) entry.getValue());
								} catch (Exception e) {
								}
						}
						world.setBlockState(_bp, _bs, 3);
					}
				} else {
					{
						BlockPos _bp = new BlockPos(x, y, z);
						BlockState _bs = BoneFenceSEBlock.block.getDefaultState();
						BlockState _bso = world.getBlockState(_bp);
						for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
							Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
							if (_property != null && _bs.get(_property) != null)
								try {
									_bs = _bs.with(_property, (Comparable) entry.getValue());
								} catch (Exception e) {
								}
						}
						world.setBlockState(_bp, _bs, 3);
					}
				}
			} else if (world.getBlockState(new BlockPos(x, y, z - 1)).isSolid()
					|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
							.contains((world.getBlockState(new BlockPos(x, y, z - 1))).getBlock())) {
				{
					BlockPos _bp = new BlockPos(x, y, z);
					BlockState _bs = BoneFenceNEBlock.block.getDefaultState();
					BlockState _bso = world.getBlockState(_bp);
					for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
						Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
						if (_property != null && _bs.get(_property) != null)
							try {
								_bs = _bs.with(_property, (Comparable) entry.getValue());
							} catch (Exception e) {
							}
					}
					world.setBlockState(_bp, _bs, 3);
				}
			} else {
				{
					BlockPos _bp = new BlockPos(x, y, z);
					BlockState _bs = BoneFenceEBlock.block.getDefaultState();
					BlockState _bso = world.getBlockState(_bp);
					for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
						Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
						if (_property != null && _bs.get(_property) != null)
							try {
								_bs = _bs.with(_property, (Comparable) entry.getValue());
							} catch (Exception e) {
							}
					}
					world.setBlockState(_bp, _bs, 3);
				}
			}
		} else if (world.getBlockState(new BlockPos(x - 1, y, z)).isSolid()
				|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
						.contains((world.getBlockState(new BlockPos(x - 1, y, z))).getBlock())) {
			if (world.getBlockState(new BlockPos(x, y, z + 1)).isSolid()
					|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
							.contains((world.getBlockState(new BlockPos(x, y, z + 1))).getBlock())) {
				if (world.getBlockState(new BlockPos(x, y, z - 1)).isSolid()
						|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
								.contains((world.getBlockState(new BlockPos(x, y, z - 1))).getBlock())) {
					{
						BlockPos _bp = new BlockPos(x, y, z);
						BlockState _bs = BoneFenceTWBlock.block.getDefaultState();
						BlockState _bso = world.getBlockState(_bp);
						for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
							Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
							if (_property != null && _bs.get(_property) != null)
								try {
									_bs = _bs.with(_property, (Comparable) entry.getValue());
								} catch (Exception e) {
								}
						}
						world.setBlockState(_bp, _bs, 3);
					}
				} else {
					{
						BlockPos _bp = new BlockPos(x, y, z);
						BlockState _bs = BoneFenceSWBlock.block.getDefaultState();
						BlockState _bso = world.getBlockState(_bp);
						for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
							Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
							if (_property != null && _bs.get(_property) != null)
								try {
									_bs = _bs.with(_property, (Comparable) entry.getValue());
								} catch (Exception e) {
								}
						}
						world.setBlockState(_bp, _bs, 3);
					}
				}
			} else if (world.getBlockState(new BlockPos(x, y, z - 1)).isSolid()
					|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
							.contains((world.getBlockState(new BlockPos(x, y, z - 1))).getBlock())) {
				{
					BlockPos _bp = new BlockPos(x, y, z);
					BlockState _bs = BoneFenceNWBlock.block.getDefaultState();
					BlockState _bso = world.getBlockState(_bp);
					for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
						Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
						if (_property != null && _bs.get(_property) != null)
							try {
								_bs = _bs.with(_property, (Comparable) entry.getValue());
							} catch (Exception e) {
							}
					}
					world.setBlockState(_bp, _bs, 3);
				}
			} else {
				{
					BlockPos _bp = new BlockPos(x, y, z);
					BlockState _bs = BoneFenceWBlock.block.getDefaultState();
					BlockState _bso = world.getBlockState(_bp);
					for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
						Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
						if (_property != null && _bs.get(_property) != null)
							try {
								_bs = _bs.with(_property, (Comparable) entry.getValue());
							} catch (Exception e) {
							}
					}
					world.setBlockState(_bp, _bs, 3);
				}
			}
		} else if (world.getBlockState(new BlockPos(x, y, z + 1)).isSolid()
				|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
						.contains((world.getBlockState(new BlockPos(x, y, z + 1))).getBlock())) {
			if (world.getBlockState(new BlockPos(x, y, z - 1)).isSolid()
					|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
							.contains((world.getBlockState(new BlockPos(x, y, z - 1))).getBlock())) {
				{
					BlockPos _bp = new BlockPos(x, y, z);
					BlockState _bs = BoneFenceStraightVBlock.block.getDefaultState();
					BlockState _bso = world.getBlockState(_bp);
					for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
						Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
						if (_property != null && _bs.get(_property) != null)
							try {
								_bs = _bs.with(_property, (Comparable) entry.getValue());
							} catch (Exception e) {
							}
					}
					world.setBlockState(_bp, _bs, 3);
				}
			} else {
				{
					BlockPos _bp = new BlockPos(x, y, z);
					BlockState _bs = BoneFenceSBlock.block.getDefaultState();
					BlockState _bso = world.getBlockState(_bp);
					for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
						Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
						if (_property != null && _bs.get(_property) != null)
							try {
								_bs = _bs.with(_property, (Comparable) entry.getValue());
							} catch (Exception e) {
							}
					}
					world.setBlockState(_bp, _bs, 3);
				}
			}
		} else if (world.getBlockState(new BlockPos(x, y, z - 1)).isSolid()
				|| BlockTags.getCollection().getTagByID(new ResourceLocation("mocreatures:bone_fence_tag"))
						.contains((world.getBlockState(new BlockPos(x, y, z - 1))).getBlock())) {
			{
				BlockPos _bp = new BlockPos(x, y, z);
				BlockState _bs = BoneFenceNBlock.block.getDefaultState();
				BlockState _bso = world.getBlockState(_bp);
				for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
					Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
					if (_property != null && _bs.get(_property) != null)
						try {
							_bs = _bs.with(_property, (Comparable) entry.getValue());
						} catch (Exception e) {
						}
				}
				world.setBlockState(_bp, _bs, 3);
			}
		} else {
			{
				BlockPos _bp = new BlockPos(x, y, z);
				BlockState _bs = BoneFenceBlock.block.getDefaultState();
				BlockState _bso = world.getBlockState(_bp);
				for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
					Property _property = _bs.getBlock().getStateContainer().getProperty(entry.getKey().getName());
					if (_property != null && _bs.get(_property) != null)
						try {
							_bs = _bs.with(_property, (Comparable) entry.getValue());
						} catch (Exception e) {
						}
				}
				world.setBlockState(_bp, _bs, 3);
			}
		}
	}
}
