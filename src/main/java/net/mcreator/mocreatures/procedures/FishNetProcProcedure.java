package net.mcreator.mocreatures.procedures;

import net.minecraft.util.Hand;
import net.minecraft.potion.EffectInstance;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.potion.BabyPotionEffect;
import net.mcreator.mocreatures.item.FishNetSharkItem;
import net.mcreator.mocreatures.item.FishNetSharkBabyItem;
import net.mcreator.mocreatures.item.FishNetMantaRayItem;
import net.mcreator.mocreatures.item.FishNetItem;
import net.mcreator.mocreatures.entity.SharkTamedEntity;
import net.mcreator.mocreatures.entity.SharkBabyEntity;
import net.mcreator.mocreatures.entity.MantaRayEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;
import java.util.Collection;

public class FishNetProcProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure FishNetProc!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency sourceentity for procedure FishNetProc!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if (((entity instanceof TameableEntity && sourceentity instanceof LivingEntity)
				? ((TameableEntity) entity).isOwner((LivingEntity) sourceentity)
				: false) == true) {
			if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY)
					.getItem() == FishNetItem.block) {
				if (entity instanceof SharkTamedEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(FishNetSharkItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof MantaRayEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(FishNetMantaRayItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof SharkBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(FishNetSharkBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				}
			} else if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY)
					.getItem() == FishNetItem.block) {
				if (entity instanceof SharkTamedEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(FishNetSharkItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof MantaRayEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(FishNetMantaRayItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof SharkBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(FishNetSharkBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
				}
			}
		}
	}
}
