package net.mcreator.mocreatures.procedures;

import net.minecraft.potion.Effects;
import net.minecraft.potion.EffectInstance;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;

public class FlyingWalkingProcedure {

	public static boolean executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure FlyingWalking!");
			return false;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity.isOnGround() == true) {
			entity.setNoGravity((true));
			if (entity instanceof LivingEntity)
				((LivingEntity) entity).addPotionEffect(new EffectInstance(Effects.LEVITATION, (int) 20, (int) 1, (false), (false)));
		} else {
			entity.setNoGravity((false));
			if (entity instanceof LivingEntity)
				((LivingEntity) entity).addPotionEffect(new EffectInstance(Effects.SLOW_FALLING, (int) 20, (int) 1, (false), (false)));
			if (entity instanceof LivingEntity) {
				((LivingEntity) entity).removePotionEffect(Effects.LEVITATION);
			}
		}
		return true;
	}
}
