package net.mcreator.mocreatures.procedures;

import net.minecraftforge.items.ItemHandlerHelper;

import net.minecraft.world.IWorld;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.item.HeartOfSpiritItem;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;

public class FullMoonDropsProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency world for procedure FullMoonDrops!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency sourceentity for procedure FullMoonDrops!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if (world.getDimensionType().getMoonPhase(world.func_241851_ab()) == 8) {
			if (sourceentity instanceof PlayerEntity) {
				ItemStack _setstack = new ItemStack(HeartOfSpiritItem.block);
				_setstack.setCount((int) 1);
				ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
			}
		}
	}
}
