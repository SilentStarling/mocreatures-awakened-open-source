package net.mcreator.mocreatures.procedures;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;

import net.minecraft.world.World;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.Variant;

import net.mcreator.mocreatures.entity.GoatEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;
import java.util.HashMap;

public class GoatColorProcedure {
	@Mod.EventBusSubscriber
	private static class GlobalTrigger {
		@SubscribeEvent
		public static void onEntitySpawned(EntityJoinWorldEvent event) {
			Entity entity = event.getEntity();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			World world = event.getWorld();
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("event", event);
			executeProcedure(dependencies);
		}
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure GoatColor!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double Variant = 0;
		if (entity instanceof GoatEntity.CustomEntity) {
			if (entity.rotationYaw == 5) {
				Variant = 1;
				((GoatEntity.CustomEntity) entity).setVariant((int) Variant);
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 10) {
				Variant = 2;
				((GoatEntity.CustomEntity) entity).setVariant((int) Variant);
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 15) {
				Variant = 3;
				((GoatEntity.CustomEntity) entity).setVariant((int) Variant);
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 20) {
				((GoatEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 4;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 25) {
				((GoatEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 5;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 30) {
				((GoatEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 6;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 35) {
				((GoatEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 7;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 40) {
				((GoatEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 8;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 45) {
				((GoatEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 9;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 50) {
				((GoatEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 10;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 55) {
				((GoatEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 11;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 60) {
				((GoatEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 12;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else {
				if (((GoatEntity.CustomEntity) entity).getVariant() == 0) {
					Variant = Math.ceil(12 * Math.random());
					((GoatEntity.CustomEntity) entity).setVariant((int) Variant);
					entity.getPersistentData().putDouble("Variant", Variant);
				}
			}
		}
	}
}
