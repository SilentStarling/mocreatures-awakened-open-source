package net.mcreator.mocreatures.procedures;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.potion.EffectInstance;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.potion.GrownPotionEffect;
import net.mcreator.mocreatures.entity.GoatEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.function.Function;
import java.util.Map;
import java.util.Comparator;
import java.util.Collection;

public class GoatGrowProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency world for procedure GoatGrow!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency x for procedure GoatGrow!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency y for procedure GoatGrow!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency z for procedure GoatGrow!");
			return;
		}
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure GoatGrow!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		Entity entity = (Entity) dependencies.get("entity");
		Entity Big = null;
		if (((Entity) world
				.getEntitiesWithinAABB(ItemEntity.class,
						new AxisAlignedBB(x - (2 / 2d), y - (2 / 2d), z - (2 / 2d), x + (2 / 2d), y + (2 / 2d), z + (2 / 2d)), null)
				.stream().sorted(new Object() {
					Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
						return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
					}
				}.compareDistOf(x, y, z)).findFirst().orElse(null)) != null) {
			if (!((Entity) world
					.getEntitiesWithinAABB(ItemEntity.class,
							new AxisAlignedBB(x - (2 / 2d), y - (2 / 2d), z - (2 / 2d), x + (2 / 2d), y + (2 / 2d), z + (2 / 2d)), null)
					.stream().sorted(new Object() {
						Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
							return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
						}
					}.compareDistOf(x, y, z)).findFirst().orElse(null)).world.isRemote())
				((Entity) world
						.getEntitiesWithinAABB(ItemEntity.class,
								new AxisAlignedBB(x - (2 / 2d), y - (2 / 2d), z - (2 / 2d), x + (2 / 2d), y + (2 / 2d), z + (2 / 2d)), null)
						.stream().sorted(new Object() {
							Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
								return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
							}
						}.compareDistOf(x, y, z)).findFirst().orElse(null)).remove();
		}
		if (new Object() {
			boolean check(Entity _entity) {
				if (_entity instanceof LivingEntity) {
					Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
					for (EffectInstance effect : effects) {
						if (effect.getPotion() == GrownPotionEffect.potion)
							return true;
					}
				}
				return false;
			}
		}.check(entity)) {
			if (entity instanceof LivingEntity) {
				((LivingEntity) entity).removePotionEffect(GrownPotionEffect.potion);
			}
			if (entity.getPersistentData().getDouble("Variant") == 1) {
				if (world instanceof ServerWorld) {
					Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
					entityToSpawn.setLocationAndAngles(x, y, z, (float) 5, (float) 50);
					entityToSpawn.setRenderYawOffset((float) 5);
					entityToSpawn.setRotationYawHead((float) 5);
					entityToSpawn.setMotion(0, 0, 0);
					if (entityToSpawn instanceof MobEntity)
						((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
								SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
					world.addEntity(entityToSpawn);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 2) {
				if (world instanceof ServerWorld) {
					Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
					entityToSpawn.setLocationAndAngles(x, y, z, (float) 10, (float) 50);
					entityToSpawn.setRenderYawOffset((float) 10);
					entityToSpawn.setRotationYawHead((float) 10);
					entityToSpawn.setMotion(0, 0, 0);
					if (entityToSpawn instanceof MobEntity)
						((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
								SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
					world.addEntity(entityToSpawn);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 3) {
				if (world instanceof ServerWorld) {
					Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
					entityToSpawn.setLocationAndAngles(x, y, z, (float) 15, (float) 50);
					entityToSpawn.setRenderYawOffset((float) 15);
					entityToSpawn.setRotationYawHead((float) 15);
					entityToSpawn.setMotion(0, 0, 0);
					if (entityToSpawn instanceof MobEntity)
						((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
								SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
					world.addEntity(entityToSpawn);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 4) {
				if (world instanceof ServerWorld) {
					Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
					entityToSpawn.setLocationAndAngles(x, y, z, (float) 20, (float) 50);
					entityToSpawn.setRenderYawOffset((float) 20);
					entityToSpawn.setRotationYawHead((float) 20);
					entityToSpawn.setMotion(0, 0, 0);
					if (entityToSpawn instanceof MobEntity)
						((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
								SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
					world.addEntity(entityToSpawn);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 5) {
				if (world instanceof ServerWorld) {
					Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
					entityToSpawn.setLocationAndAngles(x, y, z, (float) 25, (float) 50);
					entityToSpawn.setRenderYawOffset((float) 25);
					entityToSpawn.setRotationYawHead((float) 25);
					entityToSpawn.setMotion(0, 0, 0);
					if (entityToSpawn instanceof MobEntity)
						((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
								SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
					world.addEntity(entityToSpawn);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 6) {
				if (world instanceof ServerWorld) {
					Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
					entityToSpawn.setLocationAndAngles(x, y, z, (float) 30, (float) 50);
					entityToSpawn.setRenderYawOffset((float) 30);
					entityToSpawn.setRotationYawHead((float) 30);
					entityToSpawn.setMotion(0, 0, 0);
					if (entityToSpawn instanceof MobEntity)
						((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
								SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
					world.addEntity(entityToSpawn);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 7) {
				if (world instanceof ServerWorld) {
					Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
					entityToSpawn.setLocationAndAngles(x, y, z, (float) 35, (float) 50);
					entityToSpawn.setRenderYawOffset((float) 35);
					entityToSpawn.setRotationYawHead((float) 35);
					entityToSpawn.setMotion(0, 0, 0);
					if (entityToSpawn instanceof MobEntity)
						((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
								SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
					world.addEntity(entityToSpawn);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 8) {
				if (world instanceof ServerWorld) {
					Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
					entityToSpawn.setLocationAndAngles(x, y, z, (float) 40, (float) 50);
					entityToSpawn.setRenderYawOffset((float) 40);
					entityToSpawn.setRotationYawHead((float) 40);
					entityToSpawn.setMotion(0, 0, 0);
					if (entityToSpawn instanceof MobEntity)
						((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
								SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
					world.addEntity(entityToSpawn);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 9) {
				if (world instanceof ServerWorld) {
					Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
					entityToSpawn.setLocationAndAngles(x, y, z, (float) 45, (float) 50);
					entityToSpawn.setRenderYawOffset((float) 45);
					entityToSpawn.setRotationYawHead((float) 45);
					entityToSpawn.setMotion(0, 0, 0);
					if (entityToSpawn instanceof MobEntity)
						((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
								SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
					world.addEntity(entityToSpawn);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 10) {
				if (world instanceof ServerWorld) {
					Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
					entityToSpawn.setLocationAndAngles(x, y, z, (float) 50, (float) 50);
					entityToSpawn.setRenderYawOffset((float) 50);
					entityToSpawn.setRotationYawHead((float) 50);
					entityToSpawn.setMotion(0, 0, 0);
					if (entityToSpawn instanceof MobEntity)
						((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
								SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
					world.addEntity(entityToSpawn);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 11) {
				if (world instanceof ServerWorld) {
					Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
					entityToSpawn.setLocationAndAngles(x, y, z, (float) 55, (float) 50);
					entityToSpawn.setRenderYawOffset((float) 55);
					entityToSpawn.setRotationYawHead((float) 55);
					entityToSpawn.setMotion(0, 0, 0);
					if (entityToSpawn instanceof MobEntity)
						((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
								SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
					world.addEntity(entityToSpawn);
				}
			} else if (entity.getPersistentData().getDouble("Variant") == 12) {
				if (world instanceof ServerWorld) {
					Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
					entityToSpawn.setLocationAndAngles(x, y, z, (float) 60, (float) 50);
					entityToSpawn.setRenderYawOffset((float) 60);
					entityToSpawn.setRotationYawHead((float) 60);
					entityToSpawn.setMotion(0, 0, 0);
					if (entityToSpawn instanceof MobEntity)
						((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
								SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
					world.addEntity(entityToSpawn);
				}
			}
			Big = (Entity) world
					.getEntitiesWithinAABB(GoatEntity.CustomEntity.class,
							new AxisAlignedBB(x - (4 / 2d), y - (4 / 2d), z - (4 / 2d), x + (4 / 2d), y + (4 / 2d), z + (4 / 2d)), null)
					.stream().sorted(new Object() {
						Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
							return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
						}
					}.compareDistOf(x, y, z)).findFirst().orElse(null);
			if ((entity.getDisplayName().getString()).equals("Goat")) {
				entity.extinguish();
			} else {
				Big.setCustomName(new StringTextComponent((entity.getDisplayName().getString())));
			}
			if (Big instanceof LivingEntity)
				((LivingEntity) Big).setHealth((float) ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
			if ((entity instanceof TameableEntity) ? ((TameableEntity) entity).isTamed() : false) {
				if ((Big instanceof TameableEntity)
						&& (((entity instanceof TameableEntity) ? ((TameableEntity) entity).getOwner() : null) instanceof PlayerEntity)) {
					((TameableEntity) Big).setTamed(true);
					((TameableEntity) Big)
							.setTamedBy((PlayerEntity) ((entity instanceof TameableEntity) ? ((TameableEntity) entity).getOwner() : null));
				}
				if (!entity.world.isRemote())
					entity.remove();
			} else {
				if (!entity.world.isRemote())
					entity.remove();
			}
		}
	}
}
