package net.mcreator.mocreatures.procedures;

import net.minecraft.potion.EffectInstance;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.potion.BabyPotionEffect;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;

public class HorseT2BabySpawnProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure HorseT2BabySpawn!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity.rotationPitch == 50) {
			entity.extinguish();
		} else {
			if (entity instanceof LivingEntity)
				((LivingEntity) entity).addPotionEffect(new EffectInstance(BabyPotionEffect.potion, (int) 25000, (int) 1, (false), (false)));
		}
	}
}
