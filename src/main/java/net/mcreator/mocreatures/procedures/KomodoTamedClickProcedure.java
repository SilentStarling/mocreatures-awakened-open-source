package net.mcreator.mocreatures.procedures;

import net.minecraftforge.items.ItemHandlerHelper;

import net.minecraft.item.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.item.PetAmuletEmptyItem;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.stream.Stream;
import java.util.Map;
import java.util.HashMap;
import java.util.AbstractMap;

public class KomodoTamedClickProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure KomodoTamedClick!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency sourceentity for procedure KomodoTamedClick!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY)
				.getItem() == PetAmuletEmptyItem.block) {
			PetAmuletProc2Procedure.executeProcedure(
					Stream.of(new AbstractMap.SimpleEntry<>("entity", entity), new AbstractMap.SimpleEntry<>("sourceentity", sourceentity))
							.collect(HashMap::new, (_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll));
		}
		if ((entity instanceof TameableEntity && sourceentity instanceof LivingEntity)
				? ((TameableEntity) entity).isOwner((LivingEntity) sourceentity)
				: false) {
			if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY)
					.getItem() == Items.SHEARS) {
				if (entity.getPersistentData().getBoolean("Saddle") == true) {
					if (sourceentity instanceof PlayerEntity) {
						ItemStack _setstack = new ItemStack(Items.SADDLE);
						_setstack.setCount((int) 1);
						ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) sourceentity), _setstack);
					}
					entity.getPersistentData().putBoolean("Saddle", (false));
				}
			}
			if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY)
					.getItem() == Items.SADDLE) {
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _stktoremove = ((sourceentity instanceof LivingEntity)
							? ((LivingEntity) sourceentity).getHeldItemMainhand()
							: ItemStack.EMPTY);
					((PlayerEntity) sourceentity).inventory.func_234564_a_(p -> _stktoremove.getItem() == p.getItem(), (int) 1,
							((PlayerEntity) sourceentity).container.func_234641_j_());
				}
				entity.getPersistentData().putBoolean("Saddle", (true));
			} else {
				if (entity.getPersistentData().getBoolean("Saddle") == true) {
					sourceentity.startRiding(entity);
				}
			}
		}
	}
}
