package net.mcreator.mocreatures.procedures;

import net.minecraft.item.ItemStack;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.item.PetAmuletEmptyItem;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;
import java.util.Collections;

public class MocWolfBabyAmuletProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency sourceentity for procedure MocWolfBabyAmulet!");
			return;
		}
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY)
				.getItem() == PetAmuletEmptyItem.block) {
			PetAmuletProc5Procedure.executeProcedure(Collections.emptyMap());
		}
	}
}
