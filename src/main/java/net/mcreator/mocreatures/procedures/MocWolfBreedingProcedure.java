package net.mcreator.mocreatures.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.potion.EffectInstance;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.potion.WolfFedPotionEffect;
import net.mcreator.mocreatures.potion.MocWolfBPotionEffect;
import net.mcreator.mocreatures.item.RawTurkeyItem;
import net.mcreator.mocreatures.item.PetAmuletEmptyItem;
import net.mcreator.mocreatures.entity.MocWolfBabyEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;
import java.util.Collections;
import java.util.Collection;

public class MocWolfBreedingProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency world for procedure MocWolfBreeding!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency x for procedure MocWolfBreeding!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency y for procedure MocWolfBreeding!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency z for procedure MocWolfBreeding!");
			return;
		}
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure MocWolfBreeding!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency sourceentity for procedure MocWolfBreeding!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		boolean found = false;
		double sx = 0;
		double sy = 0;
		double sz = 0;
		if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY)
				.getItem() == RawTurkeyItem.block) {
			if (new Object() {
				boolean check(Entity _entity) {
					if (_entity instanceof LivingEntity) {
						Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
						for (EffectInstance effect : effects) {
							if (effect.getPotion() == WolfFedPotionEffect.potion)
								return true;
						}
					}
					return false;
				}
			}.check(entity)) {
				entity.extinguish();
			} else if (new Object() {
				boolean check(Entity _entity) {
					if (_entity instanceof LivingEntity) {
						Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
						for (EffectInstance effect : effects) {
							if (effect.getPotion() == MocWolfBPotionEffect.potion)
								return true;
						}
					}
					return false;
				}
			}.check(sourceentity)) {
				if (sourceentity instanceof PlayerEntity) {
					ItemStack _stktoremove = new ItemStack(RawTurkeyItem.block);
					((PlayerEntity) sourceentity).inventory.func_234564_a_(p -> _stktoremove.getItem() == p.getItem(), (int) 1,
							((PlayerEntity) sourceentity).container.func_234641_j_());
				}
				if (world instanceof ServerWorld) {
					((ServerWorld) world).spawnParticle(ParticleTypes.HEART, x, y, z, (int) 20, 3, 3, 3, 1);
				}
				new Object() {
					private int ticks = 0;
					private float waitTicks;
					private IWorld world;

					public void start(IWorld world, int waitTicks) {
						this.waitTicks = waitTicks;
						MinecraftForge.EVENT_BUS.register(this);
						this.world = world;
					}

					@SubscribeEvent
					public void tick(TickEvent.ServerTickEvent event) {
						if (event.phase == TickEvent.Phase.END) {
							this.ticks += 1;
							if (this.ticks >= this.waitTicks)
								run();
						}
					}

					private void run() {
						if (world instanceof ServerWorld) {
							Entity entityToSpawn = new MocWolfBabyEntity.CustomEntity(MocWolfBabyEntity.entity, (World) world);
							entityToSpawn.setLocationAndAngles(x, y, z, world.getRandom().nextFloat() * 360F, 0);
							if (entityToSpawn instanceof MobEntity)
								((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world,
										world.getDifficultyForLocation(entityToSpawn.getPosition()), SpawnReason.MOB_SUMMONED,
										(ILivingEntityData) null, (CompoundNBT) null);
							world.addEntity(entityToSpawn);
						}
						MinecraftForge.EVENT_BUS.unregister(this);
					}
				}.start(world, (int) 200);
			} else {
				if (entity instanceof LivingEntity)
					((LivingEntity) entity).addPotionEffect(new EffectInstance(WolfFedPotionEffect.potion, (int) 200, (int) 1, (false), (false)));
				if (sourceentity instanceof LivingEntity)
					((LivingEntity) sourceentity)
							.addPotionEffect(new EffectInstance(MocWolfBPotionEffect.potion, (int) 200, (int) 1, (false), (false)));
			}
		} else if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY)
				.getItem() == PetAmuletEmptyItem.block) {
			PetAmuletProc5Procedure.executeProcedure(Collections.emptyMap());
		}
	}
}
