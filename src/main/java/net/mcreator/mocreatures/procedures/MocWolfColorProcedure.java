package net.mcreator.mocreatures.procedures;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;

import net.minecraft.world.World;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.Variant;

import net.mcreator.mocreatures.entity.MocWolfEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;
import java.util.HashMap;

public class MocWolfColorProcedure {
	@Mod.EventBusSubscriber
	private static class GlobalTrigger {
		@SubscribeEvent
		public static void onEntitySpawned(EntityJoinWorldEvent event) {
			Entity entity = event.getEntity();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			World world = event.getWorld();
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("event", event);
			executeProcedure(dependencies);
		}
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure MocWolfColor!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double Variant = 0;
		if (entity instanceof MocWolfEntity.CustomEntity) {
			if (entity.rotationYaw == 5) {
				((MocWolfEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 1;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 10) {
				((MocWolfEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 2;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 15) {
				((MocWolfEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 3;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 20) {
				((MocWolfEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 4;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 25) {
				((MocWolfEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 5;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 30) {
				((MocWolfEntity.CustomEntity) entity).setVariant((int) Variant);
				Variant = 6;
				entity.getPersistentData().putDouble("Variant", Variant);
			} else {
				if (((MocWolfEntity.CustomEntity) entity).getVariant() == 0) {
					Variant = Math.ceil(6 * Math.random());
					((MocWolfEntity.CustomEntity) entity).setVariant((int) Variant);
					entity.getPersistentData().putDouble("Variant", Variant);
				}
			}
		}
	}
}
