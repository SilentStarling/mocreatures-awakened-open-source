package net.mcreator.mocreatures.procedures;

import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;

public class NoHostileTamedProcedure {

	public static boolean executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure NoHostileTamed!");
			return false;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (((entity instanceof TameableEntity) ? ((TameableEntity) entity).isTamed() : false) == true) {
			return false;
		}
		return true;
	}
}
