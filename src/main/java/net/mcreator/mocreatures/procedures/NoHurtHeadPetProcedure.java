package net.mcreator.mocreatures.procedures;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.event.entity.living.LivingAttackEvent;

import net.minecraft.world.World;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.entity.DuckEntity;
import net.mcreator.mocreatures.entity.BunnyEntity;
import net.mcreator.mocreatures.entity.BirdPigeonEntity;
import net.mcreator.mocreatures.entity.BirdCockatooEntity;
import net.mcreator.mocreatures.entity.BirdCardinalEntity;
import net.mcreator.mocreatures.entity.BirdCanaryEntity;
import net.mcreator.mocreatures.entity.BirdBlueEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;
import java.util.HashMap;

public class NoHurtHeadPetProcedure {
	@Mod.EventBusSubscriber
	private static class GlobalTrigger {
		@SubscribeEvent
		public static void onEntityAttacked(LivingAttackEvent event) {
			if (event != null && event.getEntity() != null) {
				Entity entity = event.getEntity();
				Entity sourceentity = event.getSource().getTrueSource();
				Entity immediatesourceentity = event.getSource().getImmediateSource();
				double i = entity.getPosX();
				double j = entity.getPosY();
				double k = entity.getPosZ();
				double amount = event.getAmount();
				World world = entity.world;
				Map<String, Object> dependencies = new HashMap<>();
				dependencies.put("x", i);
				dependencies.put("y", j);
				dependencies.put("z", k);
				dependencies.put("amount", amount);
				dependencies.put("world", world);
				dependencies.put("entity", entity);
				dependencies.put("sourceentity", sourceentity);
				dependencies.put("immediatesourceentity", immediatesourceentity);
				dependencies.put("event", event);
				executeProcedure(dependencies);
			}
		}
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure NoHurtHeadPet!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof BirdCanaryEntity.CustomEntity) {
			if (entity.isPassenger()) {
				if (dependencies.get("event") != null) {
					Object _obj = dependencies.get("event");
					if (_obj instanceof Event) {
						Event _evt = (Event) _obj;
						if (_evt.isCancelable())
							_evt.setCanceled(true);
					}
				}
			}
		} else if (entity instanceof BirdBlueEntity.CustomEntity) {
			if (entity.isPassenger()) {
				if (dependencies.get("event") != null) {
					Object _obj = dependencies.get("event");
					if (_obj instanceof Event) {
						Event _evt = (Event) _obj;
						if (_evt.isCancelable())
							_evt.setCanceled(true);
					}
				}
			}
		} else if (entity instanceof BirdCardinalEntity.CustomEntity) {
			if (entity.isPassenger()) {
				if (dependencies.get("event") != null) {
					Object _obj = dependencies.get("event");
					if (_obj instanceof Event) {
						Event _evt = (Event) _obj;
						if (_evt.isCancelable())
							_evt.setCanceled(true);
					}
				}
			}
		} else if (entity instanceof BirdCockatooEntity.CustomEntity) {
			if (entity.isPassenger()) {
				if (dependencies.get("event") != null) {
					Object _obj = dependencies.get("event");
					if (_obj instanceof Event) {
						Event _evt = (Event) _obj;
						if (_evt.isCancelable())
							_evt.setCanceled(true);
					}
				}
			}
		} else if (entity instanceof BirdPigeonEntity.CustomEntity) {
			if (entity.isPassenger()) {
				if (dependencies.get("event") != null) {
					Object _obj = dependencies.get("event");
					if (_obj instanceof Event) {
						Event _evt = (Event) _obj;
						if (_evt.isCancelable())
							_evt.setCanceled(true);
					}
				}
			}
		} else if (entity instanceof BirdPigeonEntity.CustomEntity) {
			if (entity.isPassenger()) {
				if (dependencies.get("event") != null) {
					Object _obj = dependencies.get("event");
					if (_obj instanceof Event) {
						Event _evt = (Event) _obj;
						if (_evt.isCancelable())
							_evt.setCanceled(true);
					}
				}
			}
		} else if (entity instanceof BunnyEntity.CustomEntity) {
			if (entity.isPassenger()) {
				if (dependencies.get("event") != null) {
					Object _obj = dependencies.get("event");
					if (_obj instanceof Event) {
						Event _evt = (Event) _obj;
						if (_evt.isCancelable())
							_evt.setCanceled(true);
					}
				}
			}
		} else if (entity instanceof DuckEntity.CustomEntity) {
			if (entity.isPassenger()) {
				if (dependencies.get("event") != null) {
					Object _obj = dependencies.get("event");
					if (_obj instanceof Event) {
						Event _evt = (Event) _obj;
						if (_evt.isCancelable())
							_evt.setCanceled(true);
					}
				}
			}
		}
	}
}
