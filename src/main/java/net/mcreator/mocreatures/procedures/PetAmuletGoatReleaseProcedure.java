package net.mcreator.mocreatures.procedures;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.Hand;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.Variant;

import net.mcreator.mocreatures.item.PetAmuletGoatItem;
import net.mcreator.mocreatures.item.PetAmuletEmptyItem;
import net.mcreator.mocreatures.entity.GoatEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.function.Function;
import java.util.Map;
import java.util.Comparator;

public class PetAmuletGoatReleaseProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency world for procedure PetAmuletGoatRelease!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency x for procedure PetAmuletGoatRelease!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency y for procedure PetAmuletGoatRelease!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency z for procedure PetAmuletGoatRelease!");
			return;
		}
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure PetAmuletGoatRelease!");
			return;
		}
		if (dependencies.get("itemstack") == null) {
			if (!dependencies.containsKey("itemstack"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency itemstack for procedure PetAmuletGoatRelease!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		Entity entity = (Entity) dependencies.get("entity");
		ItemStack itemstack = (ItemStack) dependencies.get("itemstack");
		double Variant = 0;
		if (itemstack.getOrCreateTag().getDouble("Variant") == 1) {
			if (world instanceof ServerWorld) {
				Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
				entityToSpawn.setLocationAndAngles(x, y, z, (float) 5, (float) 0);
				entityToSpawn.setRenderYawOffset((float) 5);
				entityToSpawn.setRotationYawHead((float) 5);
				if (entityToSpawn instanceof MobEntity)
					((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
							SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
				world.addEntity(entityToSpawn);
			}
		} else if (itemstack.getOrCreateTag().getDouble("Variant") == 2) {
			if (world instanceof ServerWorld) {
				Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
				entityToSpawn.setLocationAndAngles(x, y, z, (float) 10, (float) 0);
				entityToSpawn.setRenderYawOffset((float) 10);
				entityToSpawn.setRotationYawHead((float) 10);
				if (entityToSpawn instanceof MobEntity)
					((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
							SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
				world.addEntity(entityToSpawn);
			}
		} else if (itemstack.getOrCreateTag().getDouble("Variant") == 3) {
			if (world instanceof ServerWorld) {
				Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
				entityToSpawn.setLocationAndAngles(x, y, z, (float) 15, (float) 0);
				entityToSpawn.setRenderYawOffset((float) 15);
				entityToSpawn.setRotationYawHead((float) 15);
				if (entityToSpawn instanceof MobEntity)
					((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
							SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
				world.addEntity(entityToSpawn);
			}
		} else if (itemstack.getOrCreateTag().getDouble("Variant") == 4) {
			if (world instanceof ServerWorld) {
				Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
				entityToSpawn.setLocationAndAngles(x, y, z, (float) 20, (float) 0);
				entityToSpawn.setRenderYawOffset((float) 20);
				entityToSpawn.setRotationYawHead((float) 20);
				if (entityToSpawn instanceof MobEntity)
					((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
							SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
				world.addEntity(entityToSpawn);
			}
		} else if (itemstack.getOrCreateTag().getDouble("Variant") == 5) {
			if (world instanceof ServerWorld) {
				Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
				entityToSpawn.setLocationAndAngles(x, y, z, (float) 25, (float) 0);
				entityToSpawn.setRenderYawOffset((float) 25);
				entityToSpawn.setRotationYawHead((float) 25);
				if (entityToSpawn instanceof MobEntity)
					((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
							SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
				world.addEntity(entityToSpawn);
			}
		} else if (itemstack.getOrCreateTag().getDouble("Variant") == 6) {
			if (world instanceof ServerWorld) {
				Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
				entityToSpawn.setLocationAndAngles(x, y, z, (float) 30, (float) 0);
				entityToSpawn.setRenderYawOffset((float) 30);
				entityToSpawn.setRotationYawHead((float) 30);
				if (entityToSpawn instanceof MobEntity)
					((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
							SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
				world.addEntity(entityToSpawn);
			}
		} else if (itemstack.getOrCreateTag().getDouble("Variant") == 7) {
			if (world instanceof ServerWorld) {
				Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
				entityToSpawn.setLocationAndAngles(x, y, z, (float) 35, (float) 0);
				entityToSpawn.setRenderYawOffset((float) 35);
				entityToSpawn.setRotationYawHead((float) 35);
				if (entityToSpawn instanceof MobEntity)
					((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
							SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
				world.addEntity(entityToSpawn);
			}
		} else if (itemstack.getOrCreateTag().getDouble("Variant") == 8) {
			if (world instanceof ServerWorld) {
				Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
				entityToSpawn.setLocationAndAngles(x, y, z, (float) 40, (float) 0);
				entityToSpawn.setRenderYawOffset((float) 40);
				entityToSpawn.setRotationYawHead((float) 40);
				if (entityToSpawn instanceof MobEntity)
					((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
							SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
				world.addEntity(entityToSpawn);
			}
		} else if (itemstack.getOrCreateTag().getDouble("Variant") == 9) {
			if (world instanceof ServerWorld) {
				Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
				entityToSpawn.setLocationAndAngles(x, y, z, (float) 45, (float) 0);
				entityToSpawn.setRenderYawOffset((float) 45);
				entityToSpawn.setRotationYawHead((float) 45);
				if (entityToSpawn instanceof MobEntity)
					((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
							SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
				world.addEntity(entityToSpawn);
			}
		} else if (itemstack.getOrCreateTag().getDouble("Variant") == 10) {
			if (world instanceof ServerWorld) {
				Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
				entityToSpawn.setLocationAndAngles(x, y, z, (float) 50, (float) 0);
				entityToSpawn.setRenderYawOffset((float) 50);
				entityToSpawn.setRotationYawHead((float) 50);
				if (entityToSpawn instanceof MobEntity)
					((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
							SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
				world.addEntity(entityToSpawn);
			}
		} else if (itemstack.getOrCreateTag().getDouble("Variant") == 11) {
			if (world instanceof ServerWorld) {
				Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
				entityToSpawn.setLocationAndAngles(x, y, z, (float) 55, (float) 0);
				entityToSpawn.setRenderYawOffset((float) 55);
				entityToSpawn.setRotationYawHead((float) 55);
				if (entityToSpawn instanceof MobEntity)
					((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
							SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
				world.addEntity(entityToSpawn);
			}
		} else if (itemstack.getOrCreateTag().getDouble("Variant") == 12) {
			if (world instanceof ServerWorld) {
				Entity entityToSpawn = new GoatEntity.CustomEntity(GoatEntity.entity, (World) world);
				entityToSpawn.setLocationAndAngles(x, y, z, (float) 60, (float) 0);
				entityToSpawn.setRenderYawOffset((float) 60);
				entityToSpawn.setRotationYawHead((float) 60);
				if (entityToSpawn instanceof MobEntity)
					((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
							SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
				world.addEntity(entityToSpawn);
			}
		}
		if ((((Entity) world
				.getEntitiesWithinAABB(GoatEntity.CustomEntity.class,
						new AxisAlignedBB(x - (4 / 2d), y - (4 / 2d), z - (4 / 2d), x + (4 / 2d), y + (4 / 2d), z + (4 / 2d)), null)
				.stream().sorted(new Object() {
					Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
						return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
					}
				}.compareDistOf(x, y, z)).findFirst().orElse(null)) instanceof TameableEntity) && (entity instanceof PlayerEntity)) {
			((TameableEntity) ((Entity) world
					.getEntitiesWithinAABB(GoatEntity.CustomEntity.class,
							new AxisAlignedBB(x - (4 / 2d), y - (4 / 2d), z - (4 / 2d), x + (4 / 2d), y + (4 / 2d), z + (4 / 2d)), null)
					.stream().sorted(new Object() {
						Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
							return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
						}
					}.compareDistOf(x, y, z)).findFirst().orElse(null))).setTamed(true);
			((TameableEntity) ((Entity) world
					.getEntitiesWithinAABB(GoatEntity.CustomEntity.class,
							new AxisAlignedBB(x - (4 / 2d), y - (4 / 2d), z - (4 / 2d), x + (4 / 2d), y + (4 / 2d), z + (4 / 2d)), null)
					.stream().sorted(new Object() {
						Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
							return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
						}
					}.compareDistOf(x, y, z)).findFirst().orElse(null))).setTamedBy((PlayerEntity) entity);
		}
		if ((((Entity) world
				.getEntitiesWithinAABB(GoatEntity.CustomEntity.class,
						new AxisAlignedBB(x - (4 / 2d), y - (4 / 2d), z - (4 / 2d), x + (4 / 2d), y + (4 / 2d), z + (4 / 2d)), null)
				.stream().sorted(new Object() {
					Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
						return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
					}
				}.compareDistOf(x, y, z)).findFirst().orElse(null)) instanceof TameableEntity && entity instanceof LivingEntity)
						? ((TameableEntity) ((Entity) world
								.getEntitiesWithinAABB(GoatEntity.CustomEntity.class,
										new AxisAlignedBB(x - (4 / 2d), y - (4 / 2d), z - (4 / 2d), x + (4 / 2d), y + (4 / 2d), z + (4 / 2d)), null)
								.stream().sorted(new Object() {
									Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
										return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
									}
								}.compareDistOf(x, y, z)).findFirst().orElse(null))).isOwner((LivingEntity) entity)
						: false) {
			if (((Entity) world
					.getEntitiesWithinAABB(GoatEntity.CustomEntity.class,
							new AxisAlignedBB(x - (4 / 2d), y - (4 / 2d), z - (4 / 2d), x + (4 / 2d), y + (4 / 2d), z + (4 / 2d)), null)
					.stream().sorted(new Object() {
						Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
							return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
						}
					}.compareDistOf(x, y, z)).findFirst().orElse(null)) instanceof LivingEntity)
				((LivingEntity) ((Entity) world
						.getEntitiesWithinAABB(GoatEntity.CustomEntity.class,
								new AxisAlignedBB(x - (4 / 2d), y - (4 / 2d), z - (4 / 2d), x + (4 / 2d), y + (4 / 2d), z + (4 / 2d)), null)
						.stream().sorted(new Object() {
							Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
								return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
							}
						}.compareDistOf(x, y, z)).findFirst().orElse(null))).setHealth((float) (itemstack.getOrCreateTag().getDouble("Health")));
		}
		if ((((Entity) world
				.getEntitiesWithinAABB(GoatEntity.CustomEntity.class,
						new AxisAlignedBB(x - (4 / 2d), y - (4 / 2d), z - (4 / 2d), x + (4 / 2d), y + (4 / 2d), z + (4 / 2d)), null)
				.stream().sorted(new Object() {
					Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
						return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
					}
				}.compareDistOf(x, y, z)).findFirst().orElse(null)) instanceof TameableEntity && entity instanceof LivingEntity)
						? ((TameableEntity) ((Entity) world
								.getEntitiesWithinAABB(GoatEntity.CustomEntity.class,
										new AxisAlignedBB(x - (4 / 2d), y - (4 / 2d), z - (4 / 2d), x + (4 / 2d), y + (4 / 2d), z + (4 / 2d)), null)
								.stream().sorted(new Object() {
									Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
										return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
									}
								}.compareDistOf(x, y, z)).findFirst().orElse(null))).isOwner((LivingEntity) entity)
						: false) {
			if ((itemstack.getOrCreateTag().getString("Name")).equals("Goat")) {
				entity.extinguish();
			} else {
				((Entity) world
						.getEntitiesWithinAABB(GoatEntity.CustomEntity.class,
								new AxisAlignedBB(x - (4 / 2d), y - (4 / 2d), z - (4 / 2d), x + (4 / 2d), y + (4 / 2d), z + (4 / 2d)), null)
						.stream().sorted(new Object() {
							Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
								return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
							}
						}.compareDistOf(x, y, z)).findFirst().orElse(null))
						.setCustomName(new StringTextComponent((itemstack.getOrCreateTag().getString("Name"))));
			}
		}
		if (((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHeldItemMainhand() : ItemStack.EMPTY)
				.getItem() == PetAmuletGoatItem.block) {
			if (entity instanceof LivingEntity) {
				ItemStack _setstack = new ItemStack(PetAmuletEmptyItem.block);
				_setstack.setCount((int) 1);
				((LivingEntity) entity).setHeldItem(Hand.MAIN_HAND, _setstack);
				if (entity instanceof ServerPlayerEntity)
					((ServerPlayerEntity) entity).inventory.markDirty();
			}
		} else if (((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHeldItemOffhand() : ItemStack.EMPTY)
				.getItem() == PetAmuletGoatItem.block) {
			if (entity instanceof LivingEntity) {
				ItemStack _setstack = new ItemStack(PetAmuletEmptyItem.block);
				_setstack.setCount((int) 1);
				((LivingEntity) entity).setHeldItem(Hand.OFF_HAND, _setstack);
				if (entity instanceof ServerPlayerEntity)
					((ServerPlayerEntity) entity).inventory.markDirty();
			}
		}
	}
}
