package net.mcreator.mocreatures.procedures;

import net.minecraft.util.Hand;
import net.minecraft.potion.EffectInstance;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.potion.BabyPotionEffect;
import net.mcreator.mocreatures.item.PetamuletBearPandaBabyItem;
import net.mcreator.mocreatures.item.PetAmuletTurtleItem;
import net.mcreator.mocreatures.item.PetAmuletTurtleBabyItem;
import net.mcreator.mocreatures.item.PetAmuletMammothWoollyItem;
import net.mcreator.mocreatures.item.PetAmuletMammothRiverItem;
import net.mcreator.mocreatures.item.PetAmuletKomodoItem;
import net.mcreator.mocreatures.item.PetAmuletKomodoBabyItem;
import net.mcreator.mocreatures.item.PetAmuletFoxBabyItem;
import net.mcreator.mocreatures.item.PetAmuletEmptyItem;
import net.mcreator.mocreatures.item.PetAmuletEleAsianItem;
import net.mcreator.mocreatures.item.PetAmuletEleAfricanItem;
import net.mcreator.mocreatures.item.PetAmuletBearPolarBabyItem;
import net.mcreator.mocreatures.item.PetAmuletBearBrownBabyItem;
import net.mcreator.mocreatures.item.PetAmuletBearBlackBabyItem;
import net.mcreator.mocreatures.entity.WoollyMammothEntity;
import net.mcreator.mocreatures.entity.TurtleSmallEntity;
import net.mcreator.mocreatures.entity.TurtleREntity;
import net.mcreator.mocreatures.entity.TurtleMEntity;
import net.mcreator.mocreatures.entity.TurtleLEntity;
import net.mcreator.mocreatures.entity.TurtleDEntity;
import net.mcreator.mocreatures.entity.TurtleBigEntity;
import net.mcreator.mocreatures.entity.RiverMammothEntity;
import net.mcreator.mocreatures.entity.MocFoxBabyEntity;
import net.mcreator.mocreatures.entity.KomodoDragonTamedEntity;
import net.mcreator.mocreatures.entity.KomodoDragonBabyEntity;
import net.mcreator.mocreatures.entity.BearPolarBabyEntity;
import net.mcreator.mocreatures.entity.BearPandaBabyEntity;
import net.mcreator.mocreatures.entity.BearGrizzlyBabyEntity;
import net.mcreator.mocreatures.entity.BearBlackBabyEntity;
import net.mcreator.mocreatures.entity.AsianElephantEntity;
import net.mcreator.mocreatures.entity.AfricanElephantEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.stream.Stream;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.util.AbstractMap;

public class PetAmuletProc2Procedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure PetAmuletProc2!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency sourceentity for procedure PetAmuletProc2!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if (((entity instanceof TameableEntity && sourceentity instanceof LivingEntity)
				? ((TameableEntity) entity).isOwner((LivingEntity) sourceentity)
				: false) == true) {
			if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY)
					.getItem() == PetAmuletEmptyItem.block) {
				if (entity instanceof AfricanElephantEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletEleAfricanItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof AsianElephantEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletEleAsianItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof WoollyMammothEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletMammothWoollyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof RiverMammothEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletMammothRiverItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof KomodoDragonTamedEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletKomodoItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearBlackBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBearBlackBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearGrizzlyBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBearBrownBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearPolarBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBearPolarBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearPandaBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetamuletBearPandaBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof KomodoDragonBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletKomodoBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof MocFoxBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletFoxBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof TurtleBigEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurtleItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof TurtleSmallEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurtleBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof TurtleDEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurtleItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof TurtleLEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurtleItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof TurtleMEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurtleItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof TurtleREntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurtleItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else {
					PetAmuletProc3Procedure.executeProcedure(
							Stream.of(new AbstractMap.SimpleEntry<>("entity", entity), new AbstractMap.SimpleEntry<>("sourceentity", sourceentity))
									.collect(HashMap::new, (_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll));
				}
			} else if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY)
					.getItem() == PetAmuletEmptyItem.block) {
				if (entity instanceof AfricanElephantEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletEleAfricanItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof AsianElephantEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletEleAsianItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof WoollyMammothEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletMammothWoollyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof RiverMammothEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletMammothRiverItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof KomodoDragonTamedEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletKomodoItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearBlackBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBearBlackBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearGrizzlyBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBearBrownBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearPolarBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBearPolarBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearPandaBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetamuletBearPandaBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof KomodoDragonBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletKomodoBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof MocFoxBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletFoxBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof TurtleBigEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurtleItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof TurtleSmallEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurtleBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof TurtleDEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurtleItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof TurtleLEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurtleItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof TurtleMEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurtleItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof TurtleREntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurtleItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else {
					PetAmuletProc3Procedure.executeProcedure(
							Stream.of(new AbstractMap.SimpleEntry<>("entity", entity), new AbstractMap.SimpleEntry<>("sourceentity", sourceentity))
									.collect(HashMap::new, (_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll));
				}
			}
		}
	}
}
