package net.mcreator.mocreatures.procedures;

import net.minecraft.util.Hand;
import net.minecraft.potion.EffectInstance;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.potion.BabyPotionEffect;
import net.mcreator.mocreatures.item.PetAmuletTurkeyBabyItem;
import net.mcreator.mocreatures.item.PetAmuletRaccoonItem;
import net.mcreator.mocreatures.item.PetAmuletMammothWoollyBabyItem;
import net.mcreator.mocreatures.item.PetAmuletMammothRiverBabyItem;
import net.mcreator.mocreatures.item.PetAmuletEmptyItem;
import net.mcreator.mocreatures.item.PetAmuletEleAsianBabyItem;
import net.mcreator.mocreatures.item.PetAmuletEleAfricanBabyItem;
import net.mcreator.mocreatures.item.PetAmuletBunnyItem;
import net.mcreator.mocreatures.item.PetAmuletBunnyBabyItem;
import net.mcreator.mocreatures.item.PetAmuletBirdRavenItem;
import net.mcreator.mocreatures.item.PetAmuletBirdPigeonItem;
import net.mcreator.mocreatures.item.PetAmuletBirdCockatooItem;
import net.mcreator.mocreatures.item.PetAmuletBirdCardinalItem;
import net.mcreator.mocreatures.item.PetAmuletBirdCanaryItem;
import net.mcreator.mocreatures.item.PetAmuletBirdBlueItem;
import net.mcreator.mocreatures.entity.WoollyMammothBabyEntity;
import net.mcreator.mocreatures.entity.TurkeyBabyEntity;
import net.mcreator.mocreatures.entity.RiverMammothBabyEntity;
import net.mcreator.mocreatures.entity.RaccoonEntity;
import net.mcreator.mocreatures.entity.BunnyEntity;
import net.mcreator.mocreatures.entity.BunnyBabyEntity;
import net.mcreator.mocreatures.entity.BirdRavenEntity;
import net.mcreator.mocreatures.entity.BirdPigeonEntity;
import net.mcreator.mocreatures.entity.BirdCockatooEntity;
import net.mcreator.mocreatures.entity.BirdCardinalEntity;
import net.mcreator.mocreatures.entity.BirdCanaryEntity;
import net.mcreator.mocreatures.entity.BirdBlueEntity;
import net.mcreator.mocreatures.entity.AsianElephantBabyEntity;
import net.mcreator.mocreatures.entity.AfricanElephantBabyEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;
import java.util.Collections;
import java.util.Collection;

public class PetAmuletProc3Procedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure PetAmuletProc3!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency sourceentity for procedure PetAmuletProc3!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if ((entity instanceof TameableEntity && sourceentity instanceof LivingEntity)
				? ((TameableEntity) entity).isOwner((LivingEntity) sourceentity)
				: false) {
			if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY)
					.getItem() == PetAmuletEmptyItem.block) {
				if (entity instanceof TurkeyBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurkeyBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Variant", ((TurkeyBabyEntity.CustomEntity) entity).getVariant());
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof AfricanElephantBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletEleAfricanBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof AsianElephantBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletEleAsianBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof WoollyMammothBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletMammothWoollyBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof RiverMammothBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletMammothRiverBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof RaccoonEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletRaccoonItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BunnyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBunnyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Variant", ((BunnyEntity.CustomEntity) entity).getVariant());
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BunnyBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBunnyBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Variant", ((BunnyBabyEntity.CustomEntity) entity).getVariant());
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BirdBlueEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBirdBlueItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BirdCanaryEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBirdCanaryItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BirdCardinalEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBirdCardinalItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BirdCockatooEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBirdCockatooItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BirdPigeonEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBirdPigeonItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BirdRavenEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBirdRavenItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else {
					PetAmuletProc4Procedure.executeProcedure(Collections.emptyMap());
				}
			} else if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY)
					.getItem() == PetAmuletEmptyItem.block) {
				if (entity instanceof TurkeyBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurkeyBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Variant", ((TurkeyBabyEntity.CustomEntity) entity).getVariant());
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof AfricanElephantBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletEleAfricanBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof AsianElephantBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletEleAsianBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof WoollyMammothBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletMammothWoollyBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof RiverMammothBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletMammothRiverBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof RaccoonEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletRaccoonItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BunnyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBunnyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Variant", ((BunnyEntity.CustomEntity) entity).getVariant());
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BunnyBabyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBunnyBabyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Variant", ((BunnyBabyEntity.CustomEntity) entity).getVariant());
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (new Object() {
								int check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == BabyPotionEffect.potion)
												return effect.getDuration();
										}
									}
									return 0;
								}
							}.check(entity)));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BirdBlueEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBirdBlueItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BirdCanaryEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBirdCanaryItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BirdCardinalEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBirdCardinalItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BirdCockatooEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBirdCockatooItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BirdPigeonEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBirdPigeonItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BirdRavenEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBirdRavenItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					if (!entity.world.isRemote())
						entity.remove();
				} else {
					PetAmuletProc4Procedure.executeProcedure(Collections.emptyMap());
				}
			}
		}
	}
}
