package net.mcreator.mocreatures.procedures;

import net.minecraft.util.Hand;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.passive.horse.MuleEntity;
import net.minecraft.entity.passive.horse.LlamaEntity;
import net.minecraft.entity.passive.horse.DonkeyEntity;
import net.minecraft.entity.passive.WolfEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.passive.ParrotEntity;
import net.minecraft.entity.passive.PandaEntity;
import net.minecraft.entity.passive.FoxEntity;
import net.minecraft.entity.passive.CatEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.item.PetAmuletWolfItem;
import net.mcreator.mocreatures.item.PetAmuletTurkeyItem;
import net.mcreator.mocreatures.item.PetAmuletParrotItem;
import net.mcreator.mocreatures.item.PetAmuletPandaVItem;
import net.mcreator.mocreatures.item.PetAmuletMuleItem;
import net.mcreator.mocreatures.item.PetAmuletLlamaItem;
import net.mcreator.mocreatures.item.PetAmuletGoatItem;
import net.mcreator.mocreatures.item.PetAmuletFoxVItem;
import net.mcreator.mocreatures.item.PetAmuletFoxItem;
import net.mcreator.mocreatures.item.PetAmuletEmptyItem;
import net.mcreator.mocreatures.item.PetAmuletDonkeyItem;
import net.mcreator.mocreatures.item.PetAmuletCatVItem;
import net.mcreator.mocreatures.item.PetAmuletBearPolarItem;
import net.mcreator.mocreatures.item.PetAmuletBearPandaItem;
import net.mcreator.mocreatures.item.PetAmuletBearBrownItem;
import net.mcreator.mocreatures.item.PetAmuletBearBlackItem;
import net.mcreator.mocreatures.entity.TurkeyEntity;
import net.mcreator.mocreatures.entity.MocFoxEntity;
import net.mcreator.mocreatures.entity.GoatEntity;
import net.mcreator.mocreatures.entity.BearPolarEntity;
import net.mcreator.mocreatures.entity.BearPandaEntity;
import net.mcreator.mocreatures.entity.BearGrizzlyEntity;
import net.mcreator.mocreatures.entity.BearBlackEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.stream.Stream;
import java.util.Map;
import java.util.HashMap;
import java.util.AbstractMap;

public class PetAmuletProcProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure PetAmuletProc!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency sourceentity for procedure PetAmuletProc!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if (((entity instanceof TameableEntity && sourceentity instanceof LivingEntity)
				? ((TameableEntity) entity).isOwner((LivingEntity) sourceentity)
				: false) == true) {
			if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY)
					.getItem() == PetAmuletEmptyItem.block) {
				if (entity instanceof MocFoxEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletFoxItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof CatEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletCatVItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("CatType", (entity.getPersistentData().getDouble("CatType")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof DonkeyEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletDonkeyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof LlamaEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletLlamaItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Variant", (entity.getPersistentData().getDouble("Variant")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof WolfEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletWolfItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("CollarColor", (entity.getPersistentData().getDouble("CollarColor")));
				} else if (entity instanceof PandaEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletPandaVItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("MainGene", (entity.getPersistentData().getString("MainGene")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("HiddenGene", (entity.getPersistentData().getString("HiddenGene")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof ParrotEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletParrotItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Variant", (entity.getPersistentData().getDouble("Variant")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof MuleEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletMuleItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof FoxEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletFoxVItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof GoatEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletGoatItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Variant", ((GoatEntity.CustomEntity) entity).getVariant());
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearBlackEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBearBlackItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearGrizzlyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBearBrownItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearPolarEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBearPolarItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearPandaEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBearPandaItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof TurkeyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurkeyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Variant", ((TurkeyEntity.CustomEntity) entity).getVariant());
					if (!entity.world.isRemote())
						entity.remove();
				} else {
					PetAmuletProc2Procedure.executeProcedure(
							Stream.of(new AbstractMap.SimpleEntry<>("entity", entity), new AbstractMap.SimpleEntry<>("sourceentity", sourceentity))
									.collect(HashMap::new, (_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll));
				}
			} else if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY)
					.getItem() == PetAmuletEmptyItem.block) {
				if (entity instanceof MocFoxEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletFoxItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof CatEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletCatVItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.MAIN_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("CatType", (entity.getPersistentData().getDouble("CatType")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof WolfEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletWolfItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("CollarColor", (entity.getPersistentData().getDouble("CollarColor")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof DonkeyEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletDonkeyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof LlamaEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletLlamaItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Variant", (entity.getPersistentData().getDouble("Variant")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof PandaEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletPandaVItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("MainGene", (entity.getPersistentData().getString("MainGene")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("HiddenGene", (entity.getPersistentData().getString("HiddenGene")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof ParrotEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletParrotItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Variant", (entity.getPersistentData().getDouble("Variant")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof MuleEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletMuleItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof FoxEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletFoxVItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Age", (entity.getPersistentData().getDouble("Age")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearBlackEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBearBlackItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearGrizzlyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBearBrownItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearPolarEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBearPolarItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof BearPandaEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletBearPandaItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putBoolean("Saddle", (entity.getPersistentData().getBoolean("Saddle")));
					if (!entity.world.isRemote())
						entity.remove();
				} else if (entity instanceof TurkeyEntity.CustomEntity) {
					if (sourceentity instanceof LivingEntity) {
						ItemStack _setstack = new ItemStack(PetAmuletTurkeyItem.block);
						_setstack.setCount((int) 1);
						((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
						if (sourceentity instanceof ServerPlayerEntity)
							((ServerPlayerEntity) sourceentity).inventory.markDirty();
					}
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Health", ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putString("Name", (entity.getDisplayName().getString()));
					((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY).getOrCreateTag()
							.putDouble("Variant", ((TurkeyEntity.CustomEntity) entity).getVariant());
					if (!entity.world.isRemote())
						entity.remove();
				} else {
					PetAmuletProc2Procedure.executeProcedure(
							Stream.of(new AbstractMap.SimpleEntry<>("entity", entity), new AbstractMap.SimpleEntry<>("sourceentity", sourceentity))
									.collect(HashMap::new, (_m, _e) -> _m.put(_e.getKey(), _e.getValue()), Map::putAll));
				}
			}
		}
	}
}
