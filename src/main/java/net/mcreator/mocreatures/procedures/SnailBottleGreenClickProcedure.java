package net.mcreator.mocreatures.procedures;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.item.SnailBottleGreenItem;
import net.mcreator.mocreatures.entity.SnailEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;

public class SnailBottleGreenClickProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency world for procedure SnailBottleGreenClick!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency x for procedure SnailBottleGreenClick!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency y for procedure SnailBottleGreenClick!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency z for procedure SnailBottleGreenClick!");
			return;
		}
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure SnailBottleGreenClick!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof PlayerEntity) {
			ItemStack _stktoremove = new ItemStack(SnailBottleGreenItem.block);
			((PlayerEntity) entity).inventory.func_234564_a_(p -> _stktoremove.getItem() == p.getItem(), (int) 1,
					((PlayerEntity) entity).container.func_234641_j_());
		}
		if (world instanceof ServerWorld) {
			Entity entityToSpawn = new SnailEntity.CustomEntity(SnailEntity.entity, (World) world);
			entityToSpawn.setLocationAndAngles(x, y, z, (float) 10, (float) 0);
			entityToSpawn.setRenderYawOffset((float) 10);
			entityToSpawn.setRotationYawHead((float) 10);
			if (entityToSpawn instanceof MobEntity)
				((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world, world.getDifficultyForLocation(entityToSpawn.getPosition()),
						SpawnReason.MOB_SUMMONED, (ILivingEntityData) null, (CompoundNBT) null);
			world.addEntity(entityToSpawn);
		}
	}
}
