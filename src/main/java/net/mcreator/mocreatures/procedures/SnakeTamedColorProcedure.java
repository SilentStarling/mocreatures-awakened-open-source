package net.mcreator.mocreatures.procedures;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;

import net.minecraft.world.World;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.Variant;

import net.mcreator.mocreatures.entity.SnakeTameEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.Map;
import java.util.HashMap;

public class SnakeTamedColorProcedure {
	@Mod.EventBusSubscriber
	private static class GlobalTrigger {
		@SubscribeEvent
		public static void onEntitySpawned(EntityJoinWorldEvent event) {
			Entity entity = event.getEntity();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			World world = event.getWorld();
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("event", event);
			executeProcedure(dependencies);
		}
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure SnakeTamedColor!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		double Variant = 0;
		if (entity instanceof SnakeTameEntity.CustomEntity) {
			if (entity.rotationYaw == 5) {
				Variant = 1;
				((SnakeTameEntity.CustomEntity) entity).setVariant((int) Variant);
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 10) {
				Variant = 2;
				((SnakeTameEntity.CustomEntity) entity).setVariant((int) Variant);
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 15) {
				Variant = 3;
				((SnakeTameEntity.CustomEntity) entity).setVariant((int) Variant);
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 20) {
				Variant = 4;
				((SnakeTameEntity.CustomEntity) entity).setVariant((int) Variant);
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 25) {
				Variant = 5;
				((SnakeTameEntity.CustomEntity) entity).setVariant((int) Variant);
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 30) {
				Variant = 6;
				((SnakeTameEntity.CustomEntity) entity).setVariant((int) Variant);
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 35) {
				Variant = 7;
				((SnakeTameEntity.CustomEntity) entity).setVariant((int) Variant);
				entity.getPersistentData().putDouble("Variant", Variant);
			} else if (entity.rotationYaw == 40) {
				Variant = 8;
				((SnakeTameEntity.CustomEntity) entity).setVariant((int) Variant);
				entity.getPersistentData().putDouble("Variant", Variant);
			} else {
				if (((SnakeTameEntity.CustomEntity) entity).getVariant() == 0) {
					Variant = Math.ceil(8 * Math.random());
					((SnakeTameEntity.CustomEntity) entity).setVariant((int) Variant);
					entity.getPersistentData().putDouble("Variant", Variant);
				}
			}
		}
	}
}
