package net.mcreator.mocreatures.procedures;

import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.potion.Effects;
import net.minecraft.potion.EffectInstance;
import net.minecraft.network.PacketBuffer;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.item.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.inventory.container.Container;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.Entity;

import net.mcreator.mocreatures.potion.WolfTamePotionEffect;
import net.mcreator.mocreatures.potion.WolfTame6PotionEffect;
import net.mcreator.mocreatures.potion.WolfTame5PotionEffect;
import net.mcreator.mocreatures.potion.WolfTame4PotionEffect;
import net.mcreator.mocreatures.potion.WolfTame3PotionEffect;
import net.mcreator.mocreatures.potion.WolfTame2PotionEffect;
import net.mcreator.mocreatures.potion.TamePotionEffect;
import net.mcreator.mocreatures.gui.TamingNameGui;
import net.mcreator.mocreatures.entity.MocWolfTamedEntity;
import net.mcreator.mocreatures.MocreaturesMod;

import java.util.function.Function;
import java.util.Map;
import java.util.Comparator;
import java.util.Collection;

import io.netty.buffer.Unpooled;

public class WolfTamingRitualProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency world for procedure WolfTamingRitual!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency x for procedure WolfTamingRitual!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency y for procedure WolfTamingRitual!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency z for procedure WolfTamingRitual!");
			return;
		}
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency entity for procedure WolfTamingRitual!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				MocreaturesMod.LOGGER.warn("Failed to load dependency sourceentity for procedure WolfTamingRitual!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		double WolfVariant = 0;
		if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY)
				.getItem() == Items.LEAD) {
			if (entity instanceof LivingEntity)
				((LivingEntity) entity).addPotionEffect(new EffectInstance(Effects.SPEED, (int) 100, (int) 1));
			if (sourceentity instanceof LivingEntity)
				((LivingEntity) sourceentity).addPotionEffect(new EffectInstance(Effects.SLOWNESS, (int) 100, (int) 1));
			if (entity.getPersistentData().getDouble("Variant") == 1) {
				if (sourceentity instanceof LivingEntity)
					((LivingEntity) sourceentity)
							.addPotionEffect(new EffectInstance(WolfTamePotionEffect.potion, (int) 1000, (int) 1, (false), (false)));
			} else if (entity.getPersistentData().getDouble("Variant") == 2) {
				if (sourceentity instanceof LivingEntity)
					((LivingEntity) sourceentity)
							.addPotionEffect(new EffectInstance(WolfTame2PotionEffect.potion, (int) 1000, (int) 1, (false), (false)));
			} else if (entity.getPersistentData().getDouble("Variant") == 3) {
				if (sourceentity instanceof LivingEntity)
					((LivingEntity) sourceentity)
							.addPotionEffect(new EffectInstance(WolfTame3PotionEffect.potion, (int) 1000, (int) 1, (false), (false)));
			} else if (entity.getPersistentData().getDouble("Variant") == 4) {
				if (sourceentity instanceof LivingEntity)
					((LivingEntity) sourceentity)
							.addPotionEffect(new EffectInstance(WolfTame4PotionEffect.potion, (int) 1000, (int) 1, (false), (false)));
			} else if (entity.getPersistentData().getDouble("Variant") == 5) {
				if (sourceentity instanceof LivingEntity)
					((LivingEntity) sourceentity)
							.addPotionEffect(new EffectInstance(WolfTame5PotionEffect.potion, (int) 1000, (int) 1, (false), (false)));
			} else if (entity.getPersistentData().getDouble("Variant") == 6) {
				if (sourceentity instanceof LivingEntity)
					((LivingEntity) sourceentity)
							.addPotionEffect(new EffectInstance(WolfTame6PotionEffect.potion, (int) 1000, (int) 1, (false), (false)));
			}
			new Object() {
				private int ticks = 0;
				private float waitTicks;
				private IWorld world;

				public void start(IWorld world, int waitTicks) {
					this.waitTicks = waitTicks;
					MinecraftForge.EVENT_BUS.register(this);
					this.world = world;
				}

				@SubscribeEvent
				public void tick(TickEvent.ServerTickEvent event) {
					if (event.phase == TickEvent.Phase.END) {
						this.ticks += 1;
						if (this.ticks >= this.waitTicks)
							run();
					}
				}

				private void run() {
					if (sourceentity instanceof PlayerEntity && !sourceentity.world.isRemote()) {
						((PlayerEntity) sourceentity).sendStatusMessage(new StringTextComponent("Time's Up!"), (false));
					}
					for (int index0 = 0; index0 < (int) (40); index0++) {
						if (((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1) > 5) {
							if (entity instanceof LivingEntity)
								((LivingEntity) entity).clearActivePotions();
						} else if (((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1) > 0) {
							if (!entity.world.isRemote())
								entity.remove();
							if (new Object() {
								boolean check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == WolfTamePotionEffect.potion)
												return true;
										}
									}
									return false;
								}
							}.check(sourceentity)) {
								if (world instanceof ServerWorld) {
									Entity entityToSpawn = new MocWolfTamedEntity.CustomEntity(MocWolfTamedEntity.entity, (World) world);
									entityToSpawn.setLocationAndAngles(x, y, z, (float) 5, (float) 50);
									entityToSpawn.setRenderYawOffset((float) 5);
									entityToSpawn.setRotationYawHead((float) 5);
									entityToSpawn.setMotion(0, 0, 0);
									if (entityToSpawn instanceof MobEntity)
										((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world,
												world.getDifficultyForLocation(entityToSpawn.getPosition()), SpawnReason.MOB_SUMMONED,
												(ILivingEntityData) null, (CompoundNBT) null);
									world.addEntity(entityToSpawn);
								}
								if (sourceentity instanceof LivingEntity) {
									((LivingEntity) sourceentity).removePotionEffect(WolfTamePotionEffect.potion);
								}
							} else if (new Object() {
								boolean check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == WolfTame2PotionEffect.potion)
												return true;
										}
									}
									return false;
								}
							}.check(sourceentity)) {
								if (world instanceof ServerWorld) {
									Entity entityToSpawn = new MocWolfTamedEntity.CustomEntity(MocWolfTamedEntity.entity, (World) world);
									entityToSpawn.setLocationAndAngles(x, y, z, (float) 10, (float) 50);
									entityToSpawn.setRenderYawOffset((float) 10);
									entityToSpawn.setRotationYawHead((float) 10);
									entityToSpawn.setMotion(0, 0, 0);
									if (entityToSpawn instanceof MobEntity)
										((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world,
												world.getDifficultyForLocation(entityToSpawn.getPosition()), SpawnReason.MOB_SUMMONED,
												(ILivingEntityData) null, (CompoundNBT) null);
									world.addEntity(entityToSpawn);
								}
								if (sourceentity instanceof LivingEntity) {
									((LivingEntity) sourceentity).removePotionEffect(WolfTame2PotionEffect.potion);
								}
							} else if (new Object() {
								boolean check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == WolfTame3PotionEffect.potion)
												return true;
										}
									}
									return false;
								}
							}.check(sourceentity)) {
								if (world instanceof ServerWorld) {
									Entity entityToSpawn = new MocWolfTamedEntity.CustomEntity(MocWolfTamedEntity.entity, (World) world);
									entityToSpawn.setLocationAndAngles(x, y, z, (float) 15, (float) 50);
									entityToSpawn.setRenderYawOffset((float) 15);
									entityToSpawn.setRotationYawHead((float) 15);
									entityToSpawn.setMotion(0, 0, 0);
									if (entityToSpawn instanceof MobEntity)
										((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world,
												world.getDifficultyForLocation(entityToSpawn.getPosition()), SpawnReason.MOB_SUMMONED,
												(ILivingEntityData) null, (CompoundNBT) null);
									world.addEntity(entityToSpawn);
								}
								if (sourceentity instanceof LivingEntity) {
									((LivingEntity) sourceentity).removePotionEffect(WolfTame3PotionEffect.potion);
								}
							} else if (new Object() {
								boolean check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == WolfTame4PotionEffect.potion)
												return true;
										}
									}
									return false;
								}
							}.check(sourceentity)) {
								if (world instanceof ServerWorld) {
									Entity entityToSpawn = new MocWolfTamedEntity.CustomEntity(MocWolfTamedEntity.entity, (World) world);
									entityToSpawn.setLocationAndAngles(x, y, z, (float) 20, (float) 50);
									entityToSpawn.setRenderYawOffset((float) 20);
									entityToSpawn.setRotationYawHead((float) 20);
									entityToSpawn.setMotion(0, 0, 0);
									if (entityToSpawn instanceof MobEntity)
										((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world,
												world.getDifficultyForLocation(entityToSpawn.getPosition()), SpawnReason.MOB_SUMMONED,
												(ILivingEntityData) null, (CompoundNBT) null);
									world.addEntity(entityToSpawn);
								}
								if (sourceentity instanceof LivingEntity) {
									((LivingEntity) sourceentity).removePotionEffect(WolfTame4PotionEffect.potion);
								}
							} else if (new Object() {
								boolean check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == WolfTame5PotionEffect.potion)
												return true;
										}
									}
									return false;
								}
							}.check(sourceentity)) {
								if (world instanceof ServerWorld) {
									Entity entityToSpawn = new MocWolfTamedEntity.CustomEntity(MocWolfTamedEntity.entity, (World) world);
									entityToSpawn.setLocationAndAngles(x, y, z, (float) 25, (float) 50);
									entityToSpawn.setRenderYawOffset((float) 25);
									entityToSpawn.setRotationYawHead((float) 25);
									entityToSpawn.setMotion(0, 0, 0);
									if (entityToSpawn instanceof MobEntity)
										((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world,
												world.getDifficultyForLocation(entityToSpawn.getPosition()), SpawnReason.MOB_SUMMONED,
												(ILivingEntityData) null, (CompoundNBT) null);
									world.addEntity(entityToSpawn);
								}
								if (sourceentity instanceof LivingEntity) {
									((LivingEntity) sourceentity).removePotionEffect(WolfTame5PotionEffect.potion);
								}
							} else if (new Object() {
								boolean check(Entity _entity) {
									if (_entity instanceof LivingEntity) {
										Collection<EffectInstance> effects = ((LivingEntity) _entity).getActivePotionEffects();
										for (EffectInstance effect : effects) {
											if (effect.getPotion() == WolfTame6PotionEffect.potion)
												return true;
										}
									}
									return false;
								}
							}.check(sourceentity)) {
								if (world instanceof ServerWorld) {
									Entity entityToSpawn = new MocWolfTamedEntity.CustomEntity(MocWolfTamedEntity.entity, (World) world);
									entityToSpawn.setLocationAndAngles(x, y, z, (float) 30, (float) 50);
									entityToSpawn.setRenderYawOffset((float) 30);
									entityToSpawn.setRotationYawHead((float) 30);
									entityToSpawn.setMotion(0, 0, 0);
									if (entityToSpawn instanceof MobEntity)
										((MobEntity) entityToSpawn).onInitialSpawn((ServerWorld) world,
												world.getDifficultyForLocation(entityToSpawn.getPosition()), SpawnReason.MOB_SUMMONED,
												(ILivingEntityData) null, (CompoundNBT) null);
									world.addEntity(entityToSpawn);
								}
								if (sourceentity instanceof LivingEntity) {
									((LivingEntity) sourceentity).removePotionEffect(WolfTame6PotionEffect.potion);
								}
							}
							if ((((Entity) world.getEntitiesWithinAABB(MocWolfTamedEntity.CustomEntity.class,
									new AxisAlignedBB(x - (8 / 2d), y - (8 / 2d), z - (8 / 2d), x + (8 / 2d), y + (8 / 2d), z + (8 / 2d)), null)
									.stream().sorted(new Object() {
										Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
											return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
										}
									}.compareDistOf(x, y, z)).findFirst().orElse(null)) instanceof TameableEntity)
									&& (sourceentity instanceof PlayerEntity)) {
								((TameableEntity) ((Entity) world.getEntitiesWithinAABB(MocWolfTamedEntity.CustomEntity.class,
										new AxisAlignedBB(x - (8 / 2d), y - (8 / 2d), z - (8 / 2d), x + (8 / 2d), y + (8 / 2d), z + (8 / 2d)), null)
										.stream().sorted(new Object() {
											Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
												return Comparator
														.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
											}
										}.compareDistOf(x, y, z)).findFirst().orElse(null))).setTamed(true);
								((TameableEntity) ((Entity) world.getEntitiesWithinAABB(MocWolfTamedEntity.CustomEntity.class,
										new AxisAlignedBB(x - (8 / 2d), y - (8 / 2d), z - (8 / 2d), x + (8 / 2d), y + (8 / 2d), z + (8 / 2d)), null)
										.stream().sorted(new Object() {
											Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
												return Comparator
														.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
											}
										}.compareDistOf(x, y, z)).findFirst().orElse(null))).setTamedBy((PlayerEntity) sourceentity);
							}
							if (((Entity) world.getEntitiesWithinAABB(MocWolfTamedEntity.CustomEntity.class,
									new AxisAlignedBB(x - (8 / 2d), y - (8 / 2d), z - (8 / 2d), x + (8 / 2d), y + (8 / 2d), z + (8 / 2d)), null)
									.stream().sorted(new Object() {
										Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
											return Comparator.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
										}
									}.compareDistOf(x, y, z)).findFirst().orElse(null)) instanceof LivingEntity)
								((LivingEntity) ((Entity) world.getEntitiesWithinAABB(MocWolfTamedEntity.CustomEntity.class,
										new AxisAlignedBB(x - (8 / 2d), y - (8 / 2d), z - (8 / 2d), x + (8 / 2d), y + (8 / 2d), z + (8 / 2d)), null)
										.stream().sorted(new Object() {
											Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
												return Comparator
														.comparing((Function<Entity, Double>) (_entcnd -> _entcnd.getDistanceSq(_x, _y, _z)));
											}
										}.compareDistOf(x, y, z)).findFirst().orElse(null)))
										.addPotionEffect(new EffectInstance(TamePotionEffect.potion, (int) 100, (int) 1, (false), (false)));
							{
								Entity _ent = sourceentity;
								if (_ent instanceof ServerPlayerEntity) {
									BlockPos _bpos = new BlockPos(x, y, z);
									NetworkHooks.openGui((ServerPlayerEntity) _ent, new INamedContainerProvider() {
										@Override
										public ITextComponent getDisplayName() {
											return new StringTextComponent("TamingName");
										}

										@Override
										public Container createMenu(int id, PlayerInventory inventory, PlayerEntity player) {
											return new TamingNameGui.GuiContainerMod(id, inventory,
													new PacketBuffer(Unpooled.buffer()).writeBlockPos(_bpos));
										}
									}, _bpos);
								}
							}
							break;
						}
					}
					MinecraftForge.EVENT_BUS.unregister(this);
				}
			}.start(world, (int) 100);
		}
	}
}
